---
layout: layouts/pages.njk
title: About Me
templateClass: tmpl-pages
eleventyNavigation:
  key: About Me
  order: 3
---

In web development, it's impossible to learn everything. But, just try to be 1% better every day.

> If you get one percent better each day for one year, you'll end up thirty-seven times better by the time you’re done.
> ~James Clear

![](tiny-gains-graph.jpg)
via [James Clear](https://jamesclear.com/continuous-improvement)


This blog is a example of that. Every single day I'm surprised by something new.
Sometimes, it's something I learned a decade ago, but it surprises me again. So I document it.

This was inspired by [Hashrocket's TIL](https://til.hashrocket.com/).
