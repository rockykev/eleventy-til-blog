const fs = require("fs");

function generateFrontMatter(stringDate) {
  const frontmatter = {
    title: "TIL PLACEHOLDER",
    date: stringDate,
    published: false,
    tags: `["", ""]`,
    series: false,
    canonical_url: false,
    description: "PLACEHOLDER",
    layout: "layouts/post.njk",
  };

  const markdownContent = `


  # HEADER
This is my document content.`;

  const frontmatterString = Object.entries(frontmatter)
    .map(([key, value]) => `${key}: ${value}`)
    .join("\n");

  const markdownWithFrontmatter = `---
${frontmatterString}
---
${markdownContent}`;

  return markdownWithFrontmatter;
}

function generateDailyFiles() {
  for (let i = 1; i <= 365; i++) {
    const year = "2023";
    const date = new Date(year, 0, i);
    const monthDate = date.getMonth() + 1;

    const month = monthDate.toString().length < 2 ? "0" + monthDate : monthDate;
    const monthFolderName = `${year}-${month}`;
    const dayFileName = String(date.getDate()).padStart(2, "0");

    const filename = `${monthFolderName}/_${date.getFullYear()}-${month}-${dayFileName}.md`;
    const content = generateFrontMatter(`${year}-${month}-${dayFileName}`);

    // console.log("simulating created: ", filename);
      if (!fs.existsSync(monthFolderName)) {
        fs.mkdirSync(monthFolderName);
      }

      fs.writeFile(filename, content, (err) => {
        if (err) throw err;
        console.log(`The file ${filename} has been created!`);
      });
  }
}

generateDailyFiles();

