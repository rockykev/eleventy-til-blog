---
title: TIL You can look for elements with specific attributes
date: 2020-08-29
published: true
tags: ["html", "css", "fe-masters-css-in-depth"]
series: false
canonical_url: false
description: "Target elements via CSS with certain attributes"
---

Say you wanted to target all links with pdfs, and add a pdf icon to the back.

You can use this css class to target it and add a pdf icon.

```css
a[href$="pdf"] {
  background-image: url(pdficon.gif);
}
a[href$="pdf"] {
  content: " (PDF)";
}
```

Want the position:
$ELEMENT[attr^=val] == starts
$ELEMENT[attr$=val] == ends
$ELEMENT[attr*=val] == whose
