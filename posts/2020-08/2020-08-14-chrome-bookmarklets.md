---
title: TIL You can inject JS functions as Chrome bookmarks
date: 2020-08-14
published: true
tags: ['chrome']
series: false
canonical_url: false
description: "You can put JS functions into a bookmark that will fire on the webpage you are on. "
---

You can put JS functions into a bookmark that will fire on the webpage you are on. 

For example: 
DELETE ALL IMAGES ON WEBSITE:
`javascript:(function(){ [].slice.call(document.querySelectorAll('img, .gist')).forEach(function(elem) { elem.remove(); }); })()`

> How to use the Bookmarklets:

1. Right Click the Bookmarks Bar.
2. There, one of the options will be to “Add Page”. Click it.
3. After that, you will notice the to bars on top of the menu.
4. Where It says “Name”, Name your JavaScript bookmarklet. Then, where it says “URL”, you Copy and paste the JavaScript in.

![](img/2020-08-14-bookmarks.png)

More recipes here:
[https://sites.google.com/view/awesomejavascripthacks/how-to-use](https://sites.google.com/view/awesomejavascripthacks/how-to-use)
