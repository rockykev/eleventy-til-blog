---
title: TIL hide Eleventy Blog
date: 2020-08-04
published: true
tags: ['eleventy', 'blog']
series: false
canonical_url: false
description: "Filter Eleventy Blog posts to hide draft posts"
---

Right now, Eleventy does not have a default way to filter through Markdown [frontmatter](https://jekyllrb.com/docs/front-matter/). *I'm using Jekyll's style, btw.*

Add this to your .eleventyignore file:
`**/_*.md`

All draft/non-published posts get ignored when building the site.
Since it stops it before being built, it also doesn't

Via [https://github.com/11ty/eleventy/issues/188#issuecomment-488350222](https://github.com/11ty/eleventy/issues/188#issuecomment-488350222)
A [programmatic method is in this post](https://dev.to/hirusi/11ty-eleventy-how-to-exclude-draft-collection-items-from-build-programmatically-3a9m).
And [another one](https://remysharp.com/2019/06/26/scheduled-and-draft-11ty-posts)
