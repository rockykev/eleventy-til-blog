---
title: TIL character-spacing CSS
date: 2020-08-09
published: true
tags: ['css']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

There's 3 types of spacing:

## UNSTYLED:
<p class="test">
The quick brown fox jumped over the lazy dog. Oh, how that <span>fox jumped,</span> <span>quick and brown,</span> while the dog never reacted because he was so very lazy.
</p>

## Letter Spacing
`letter-spacing: -1px`
<p class="test" style="letter-spacing: -1px">
The quick brown fox jumped over the lazy dog. Oh, how that <span>fox jumped,</span> <span>quick and brown,</span> while the dog never reacted because he was so very lazy.
</p>

## Word Spacing
`word-spacing: 5px`
<p class="test" style="word-spacing: 5px">
The quick brown fox jumped over the lazy dog. Oh, how that <span>fox jumped,</span> <span>quick and brown,</span> while the dog never reacted because he was so very lazy.
</p>

## Line height
`line-height: 1.2`
<p class="test" style="line-height: 1.2">
The quick brown fox jumped over the lazy dog. Oh, how that <span>fox jumped,</span> <span>quick and brown,</span> while the dog never reacted because he was so very lazy.
</p>


## Using Tailwind utility classes
https://tailwindcss.com/docs/letter-spacing
https://tailwindcss.com/docs/line-height

Tailwind doesn't have word spacing utilities.


via [https://www.quirksmode.org/css/text/spacing.html](https://www.quirksmode.org/css/text/spacing.html)
Dev docs: [https://developer.mozilla.org/en-US/docs/Web/CSS/word-spacing](https://developer.mozilla.org/en-US/docs/Web/CSS/word-spacing)
