---
title: TIL NVM separates global installs
date: 2020-08-27
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "NVM is awesome"
---

Node Version Manager (NVM) let's you switch node versions. Valuable when you're working on a bunch of projects with different node versions.

It also separates global packages to your node version!

Example:
For Node 8.9.4 - global packages are:
```
rockykev@radmins-MacBook-Pro new-star % npm list -g --depth 0
/Users/rockykev/.nvm/versions/node/v8.9.4/lib
├── gulp-cli@2.3.0
└── npm@5.6.0
```

For Node 14.15 - global packages are:
```
rockykev@radmins-MacBook-Pro new-star % nvm use 14.15
rockykev@radmins-MacBook-Pro new-star % npm list -g --depth 0
/Users/rockykev/.nvm/versions/node/v14.15.0/lib
└── npm@6.14.8
```

Notice that I don't have the gulp-cli in that version?
I can install a different version globally, which is pretty awesome.
