---
title: TIL How to target Language
date: 2020-08-28
published: true
tags: ["html", "css", "fe-masters-css-in-depth"]
series: false
canonical_url: false
description: "Target language with CSS"
---

YOUR DOM:

```html
<p lang="en-us"></p>
<p lang="en-uk"></p>
```

YOUR CSS:

```css
p[lang|="en"] {
}
```

You can now stylize all english words.
