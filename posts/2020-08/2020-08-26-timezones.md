---
title: TIL that devtools can test timezones
date: 2020-08-26
published: true
tags: ['devtools']
series: false
canonical_url: false
description: "How to test timezones"
---

How to test timezones

> The Sensors tab now lets you not only override geolocation, but also emulate arbitrary timezones and test the impact on your web apps. Perhaps surprisingly, this new feature improves the reliability of geolocation emulation as well: previously, web apps could detect location spoofing by matching the location against the user's local timezone. Now that geolocation and timezone emulation are coupled, this category of mismatches is eliminated.

Devtools > three dots > More Tools > Sensors

![](img/2020-08-26-devtools.png)


More info: [https://developers.google.com/web/updates/2019/10/devtools](https://developers.google.com/web/updates/2019/10/devtools)
