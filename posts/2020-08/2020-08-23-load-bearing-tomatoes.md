---
title: TIL Load Bearing Tomatoes
date: 2020-08-23
published: true
tags: ['gaming', 'devstories']
series: false
canonical_url: false
description: "Game developer code"
layout: layouts/post.njk
---

> “When I worked at EA I was told the nascar team had to leave a field goal post under the world because it broke the game if it was removed because of old madden code in the game,” developer Chris Wingard said on Twitter. Many other game workers commiserated in the replies over their own load-bearing tomatoes, or mushrooms that they had heard about in other games.

Via [https://www.polygon.com/2020/9/22/21451405/super-mario-galaxy-3d-all-stars-nintendo-switch-kinoko-mushroom-tomato-game-development](https://www.polygon.com/2020/9/22/21451405/super-mario-galaxy-3d-all-stars-nintendo-switch-kinoko-mushroom-tomato-game-development)
