---
title: TIL creating files in Powershell
date: 2020-08-06
published: true
tags: ['powershell', 'bash']
series: false
canonical_url: false
description: "Powershell Equiv to touch"
---

In linux/macs, you can create files by using `touch`.

So if you want a new file:
`touch newfile.txt`

Unless you're using win10's Powershell.
`New-Item -ItemType file example.txt`


Via [https://superuser.com/questions/502374/equivalent-of-linux-touch-to-create-an-empty-file-with-powershell](https://superuser.com/questions/502374/equivalent-of-linux-touch-to-create-an-empty-file-with-powershell)
