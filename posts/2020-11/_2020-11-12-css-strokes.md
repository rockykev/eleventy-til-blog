---
title: TIL CSS Stroke
date: 2020-11-12
published: true
tags: ["css"]
series: false
canonical_url: false
description: "STROKES AND STROKES"
---

Creating a text stroke is oddly difficult to do with CSS.

There's a supported CSS spec for `-webkit-text-stroke`, but the result is really ugly.

[https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-stroke](https://developer.mozilla.org/en-US/docs/Web/CSS/-webkit-text-stroke)

![](/src/img/2020-11-12-textstroke.png)

This [generator](http://owumaro.github.io/text-stroke-generator/) creates a text stroke using text-shadow hacks.

![](/src/img/2020-11-12-textstroke2.png)
