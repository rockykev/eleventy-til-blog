---
title: TIL Passing Ternaries into it's own function
date: 2020-11-11
published: true
tags: ["vue"]
series: false
canonical_url: false
description: "If the ternary gets complicated - you're better off doing it as a `computed` property."
---

If the ternary gets complicated - you're better off doing it as a `computed` property.

Rather than:

```
<template>
  <h1 :class="showElement ? 'is-visible is-bold position-top' : 'is-hidden'"> Hello Frontend Masters! </h1>
<template>

```

```
<template>
  <h1 :class="titleClassNames"> Hello Frontend Masters! </h1>
<template>

<script>
export default {
  computed: {
    titleClassNames() {
      return showElement ? 'is-visible is-bold position-top' : 'is-hidden';
    }
  }
}
</script>

```

![](/src/img/2020-11-11-computed.jpg)

Via Frontend Masters - Production-Grade Vue Workshop https://frontendmasters.com/workshops/production-vue/#player

![](_2020-11-11-ternary-vue_0.jpg)
