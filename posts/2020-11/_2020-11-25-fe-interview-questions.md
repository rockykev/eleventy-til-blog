---
title: TIL that I had to ask interview questions
date: 2020-11-25
published: true
tags: [""]
series: false
canonical_url: false
description: "Interview questions I should ask"
---

## GENERAL:

### 1. What are these acryonyms?

HTML
DOM
CSS
JS
JSON
XML
AJAX
CMS
CRON
CRUD
FTP
LAMP
REST
SDK
SQL
PHP

**GOAL OF QUESTION**
To gauge how they respond to certain technologies.
If they don't know what it is, press them a bit if they ever heard of it in context.

### 2. What is the difference between functional programming and OOP?

**GOAL**
For our purpose, it really doesn't matter that they know the exact details. To be part of modern web development means that you understand the two major design paradigns. To know nothing about it is a red flag.

**ANSWER**

Object Oriented Programming:

- Inheritance
- Encapsulate data (hide data from outside access)
- Polymorphism
- Instances and shared state
- Data and operations are tightly coupled.

Functional:

- Pure functions
- Avoid side-effects
- Data is copied, not mutated.
- First-class functions (You can pass the function around as a variable)

BONUS: Modern JS frameworks moved to functional programming for ease of state management and debugging.

### 3. Tell me what the LAMP stack looks like?

**GOAL**
The goal is to identify their familiarity with full-stack development. It's important to understand that there's a lot of layers involved with coding.

**ANSWER**
Linux.
Apache.
MySQL.
PHP.

### 4. What are ssh keys?

**GOAL**
This checks to see their experience with terminal commands, and git.

**ANSWER**
SSH keys are access credentials. There is a public/private key.
They are generated using bash commands in terminal or using a tool in windows like Putty.
They can be used to secure access to the backend of a website. (FTP)
They can be used to ensure 2-way communication within github/bitbucket.
They are required to access many AWS-CLI features.

### 5. What is the difference between backend, frontend, and full-stack?

**GOAL**
A basic check to see their understanding of the separation of skillsets.

**ANSWER**
Basic Answer:
Frontend - The code to generate the customer-facing content/features. The GUI, design, menu, a form's look, content.
Backend - The code to manage everything else. The data flowing to the database, page routing, form submission.
Fullstack - A magical unicorn that does both.

Advanced Answer:
As of 2020, Modern Web development using JS frameworks has moved a lot of work of the backend to the Frontend. Some companies (Can't find the reference but Google was mentioned) are now even calling it Frontend-Frontend and Frontend-backend to separate the widing responsibilities.

ref: https://css-tricks.com/the-widening-responsibility-for-front-end-developers/#fn1-return

### 6. What are APIS?

**GOAL**
Sort of a trick question - to guage their experience level.

**ANSWER**
Basic Answer:
The assumption is that api term are rest APIs. Rest Apis are endpoints that a consumer can call to pull queried data from a system without having to know the internal mechanisms.

Advanced Answer:
APIs are anything that you interface with as a consumer.

So if you're calling a function, that's an api. You have a set of data that you want the function to do something with, without being forced to read the function code.

### 7. What are some git actions you can take?

**GOAL**
This checks their understanding of git and familiarity, as well as their experience working with code bases.

**ANSWER**
add .
commit -m "MESSAGE"
push
merge
clone
pull
fetch

### 8. What is regex?

**GOAL**
Checking for familiarity of the language(?) and it's use-case, and why it's important.

**ANSWER**
Regex is a language or syntax to identify search patterns.
It's useful for manipulating strings or queries.

### 9. What is SSL Certificate?

**GOAL**
Basic Web dev! The goal is that they know what this is and why it exsits.

**ANSWER**
It's the little green lock next to websites. It's what made http into https.

> SSL (Secure Sockets Layer) and its successor, TLS (Transport Layer Security), are protocols for establishing authenticated and encrypted links between networked computers. Although the SSL protocol was deprecated with the release of TLS 1.0 in 1999, it is still common to refer to these related technologies as “SSL” or “SSL/TLS.” The most current version is TLS 1.3, defined in RFC 8446 (August 2018).

https://www.ssl.com/faqs/faq-what-is-ssl/

### 10. What is NPM/Yarn?

**GOAL**
Basic Web dev! The goal is that they know what this is and what it is used for.

**ANSWER**

NPM is a package manager that hosts JS modules.
NPM comes with Node.
Yarn is a alternative to NPM, and was created by Facebook/Google/others to solve NPM problems. It uses a different pattern to get JS modules.

### 11. What is the difference between server-side rendering and client-side rendering?

**GOAL**
Basic Web dev! The goal is that they know what this is and what it is used for.

**ANSWER**
Server-side rendering - Web content is made through requests that go back and forth to the server. It is affected by server/internet speeds/optimization. Full page reloads and slower rendering.

Client-side rendering - Web content is downloaded to the client and 'reacts' to the user. The payload is significantly larger in the beginning, but much faster through usages.

### 12. When you're looking for a way to solve a particular problem, what are some places you end up online?

**GOAL**
This is to check their tech experience in research. Red flags are ones where they don't really have any research material.

**ANSWER**

StackOverflow, language Docs (MDN, Ecmascript, Jquery Docs, Vue docs), cool websites (css-tricks), coding sandboxes (codepen, codesandbox), forums (programming subreddits)

### 13. What is a SQL Injection?

**GOAL**
Basic Web dev! The goal is to see if they have an eye for security.

**ANSWER**

It's when you don't sanitize your user inputs before they enter the system/database. Sanitizing/mutations must be checked on the server level, as client-side mutations can be easy to manipulate.

An example: Shoving a JS script inside the name field of a form.

---

## HTML:

### 1. What is the basic structure of a webpage - and what goes into it?

**GOAL**
Check their comfort level in writing HTML.

**ANSWER**
head, body.

### 2. What are some elements of a HTML form?

**GOAL**
Check out their understanding of a form element within HTML.

**ANSWER**

-Organization Pattern: Label, then inputs
-Input Types - radio, select, input, textarea, checkbox
-Element attributes like value, disabled, readonly

- -Submission (Serverside: form action) (Clientside: event listener)

### 3. What are good usecases for a link, vs a generic button, vs a submit button?

**GOAL**
Check their experience in basic markup, and also seeing if their experience in writing JS functions

**ANSWER**

links = For navigation
buttons = for actions (hide/show content, search box, filters, calculate form inputs)
submit = Reserved for forms.

### 4. What is the DOM?

**GOAL**
Basic web development!

**ANSWER**
It is a Document Object Model, a tree of a web page.

### 5. What is reflow?

**GOAL**
Advanced web development.

**ANSWER**
Reflow is triggered when a design element's positioning within the page is modified (usually changing width/height size, adding borders) and causes other things to shift. The page's FLOW changes, and has to reflow.

### 6. What is repaint?

**GOAL**
Advanced web development.

**ANSWER**
Repaint happens when the element -- repaint the screen.

### 7. What are data attributes in HTML?

**GOAL**
Advanced web development!

**ANSWER**
You can set up custom data attributes that can get passed to Javascript. Many input elements have value. But if you wanted to add a custom value on say, a `li` item, you would use a data attribute.

### 8. What are some primary usecases for avid, webp, webm, svg, gif, jpg, and png?

**GOAL**
Check on their familiarity with image types.

**ANSWER**
jpg = generic image type. Great for photos - bitmap images.
png = generic image type. Great for all use-cases (except animation), but larger file size.
gif = generic image type. Great for animations.
svg = next-generation image type. Great for vector images. Excellent for CSS animation.
webp = next-generation image type. Great for all use-cases (except animation), and better file size. 95% support.
webm = next-generation image type. Replacement for gifs.
avid = next-next generation image type. Even better than webp. Lower support.

### 9. Within HTML - when we say inline Javascript, or inline CSS... what does inline mean?

**GOAL**
Check to see if they are familiar with the different methods of inserting Javascript into a webpage.

**ANSWER**
Inline JS/Inline CSS means to write the code within markup directly.

For Inline JS, you're coding directly with `<script></script>` tags.
While Inline CSS, it could be using the `style` element attribute or coding with `<style></style>` tags.

### 10. What's the difference between a div, span, section, article, and body?

**GOAL**
Check their familiarity with HTML semantics.

**ANSWER**
div = generic block
span = for specific inline blocks.
section = block definition for separating content.
article = block definition specific for content.
body = Full body definition.

---

## CSS

### 1. What is bootstrap?

**GOAL**
Their familiarity with using CSS frameworks.

**ANSWER**
A CSS framework that creates a common language for web components. It allows for mobile responsiveness right ouf of the box.

UI

### 1. What are psuedo-elements?

**GOAL**
Basic web development.

**ANSWER**
They are design elements to style a specific part of the element.
Like `::before` `::after`, `::first-letter`

Might get confused by calling `::nth-child` or `::hover` -- those are psuedo-classes. But they're kinda interchangable and it's semantics.

### 1. What is flexbox?

**GOAL**
Basic web development - and focused on mobile friendly design.

**ANSWER**
It's allows elements to naturally flow/fit into space without hard coding the size of the element.

### 1. What is CSS Grid?

**GOAL**
Modern web development - and experienced with mobile friendly design.

**ANSWER**
It allows elements to fit in a grid without hardcoding the size of the element. Columns and rows.

### 1. What is the difference between IDs and classes?

**GOAL**
Basic web development

**ANSWER**
IDs are specific identifiers. They are useful for single elements that will only appear on the page once. They are valuable to laser-target them within Javascript.
Classes are for reusability.

### 1. What is CSS specificity rules?

**GOAL**
Advaned Web development. Familiarity with CSS specificity is a sign that they are familiar with nesting and properly grouping css.

**ANSWER**
It is a score system that CSS uses to determine what style has priority over another.
A class provides a value.
Being a ID provides a higher value.
Having `!important` gives it a even higher value.
Laser-focused targetting makes that score even higher still.

### 1. What is the difference between positioning elements: absolute, relative, fixed, static

**GOAL**
Basic web development and familiarity of organizing web content.

**ANSWER**
absolute - breaks out of the dom flow
relative - relative to the parent element. // Double check
fixed - stays relative to the viewport. So scrolling doesn't change it's position.
static - default setting. It's part of the flow of the web page. Not affected by top/bottom/left/right or z-index.

### 1. How would you check for mobile responsiveness?

**GOAL**
Familiarity with devtools or using online tools.
Red flag if they say they load the site on their phone.

**ANSWER**
Devtools.

### 1. What is a CSS variable?

**GOAL**
Advanced web development. Familiarity with DRY, even in CSS.

**ANSWER**
It allows you to save a variable that you can reuse in your stylesheet. So now you can modify just a variable to trickle down the effect everywhere else.

### 1. What is the difference between padding and margin?

**GOAL**
If they are familiar with element spacing/size.

**ANSWER**
Padding - inner spacing.
Margin - outer spacing.

Padding - Stretches out the button. So it makes the button "bigger".
Margin - The space around the button. So if you needed more empty space around it.

### 1. What are some benefits of SASS?

**GOAL**
Familiarity with modern tools to make writing CSS easier/efficient.

**ANSWER**
Better organization.
Allows you to import.
Nestling.
Mixins and extends.
Functions.

### 1. What is a mobile-first design?

**GOAL**
Advanced web development skillset.

**ANSWER**
It's the design concept of building pages from the mobile side upwards.

---

## PHP AND WORDPRESS TOGETHER

### 1. What is PHP? What is the purpose of it?

**GOAL**

**ANSWER**

### 2. In the chat, show me a "Hello World" with PHP?

**GOAL**

**ANSWER**

### 3. What is the current version of PHP?

**GOAL**

**ANSWER**

### 4. I need you to create a function that takes a variable, and then prints it out the result 20 times. Can you write that for me?

**GOAL**

**ANSWER**

### 5. What current version of WordPress?

**GOAL**

**ANSWER**

### 6. Have you ever built anything in WordPress?

**GOAL**

**ANSWER**

### 7. What does the WordPress architecture look like?

**GOAL**

**ANSWER**
themes, plugins, wp core.

### 8. How does WordPress generate a web page?

**GOAL**

**ANSWER** (share)

How php generates a webpage.

### 9. What are posts/pages/themes and plugins in WordPress?

**GOAL**

**ANSWER**

page = about page
post = blog post

---

## JAVASCRIPT

### 1. How is the relationship between Java and Javascript?

**GOAL**

**ANSWER**

### 2. How does Javascript work in web development?

**GOAL**

**ANSWER**
Functionality portion of pages.

### 3. What are var, let, and const?

**GOAL**

**ANSWER**
Variable declarations.

var = shouldn't be used - hoisting.
let =
const = constant variables

### 4. What are the types?

**GOAL**

**ANSWER**
array
Object

string
number
Boolean

symbol
BigInt

### 5. What's the difference between an object and a array?

**GOAL**

**ANSWER**

### 6. Right now, in the app... I want to turn a link to fire off a function. But the link will repaint the page. How do I stop it?

**GOAL**

**ANSWER**
event

prevent Default
stopPropograte - ()

### 7. I want to save data that a user inputs. How would I do that?

WRITE THIS QUESTION BETTER and not include database - it should be about client side stuff.

**GOAL**

**ANSWER**

1. query strings
2. cookies
3. localStorage
4. sessionStorage

Data is shared between all tabs and windows from the same origin.

### 8. What is ES5, ES6, modern Javascript?

WRITE THIS QUESTION BETTER -

**GOAL**

**ANSWER**
It's versions of javascript.
ES5 - start of javascript

syntatic sugar --

### 9. The page load is slow - and the assumption is that it's a Javascript thing. What is your process for identifying what JS files?

WRITE THIS QUESTION BETTER --

**GOAL**

**ANSWER**

Devtools -> Networks

---

## HIGHER LEVEL JAVASCRIPT

### 1. What is hoisting?

**GOAL**

**ANSWER**

You can call functions

### 2. What is event bubbling?

**GOAL**

**ANSWER**

### 3. What is `this`?

**GOAL**

**ANSWER**
In react class constructors --

### 4. What is babel and why should we use it? Why shouldn't we use it?

**GOAL**

**ANSWER**
It is a
transpile ->
and
polyfilling ->

Why shouldn't:

### 5. What are some Web Browser APIs that look like Javascript functions?

**GOAL**

**ANSWER**
console.log
fetch

https://developer.mozilla.org/en-US/docs/Web/API

### 6. What is a template literal and what are the benefits to using it instead of the alternatives?

**GOAL**
Intermediate Javascript.

**ANSWER**

It allows you to use variables directly within strings for clean.

### 7. What is a callback?

**GOAL**
Advanced Javascript.

**ANSWER**
A callback is a function passed as an argument to another function.

map
reduce
filter

Example: --

```
let areas = circles.map(getArea);

function getArea(radius) {
    return Math.floor(Math.PI * radius * radius);
}

console.log(areas(10));
```

### 8. Give me an example of a promise and it's relationship to callback hell?

**GOAL**
Advanced Javascript. Familiarity with ES6->ES2018 history.

**ANSWER**
Relationship to callback hell:
Lots of chained `.then` and nestled commands.

Aync...await is the upgrade that looks more like Javascript functions.

### 9. What is the difference between Declaritive and Imperative programming?

**GOAL**
Programming concepts. Checking their familiarity with modern JS, and the reasoning of the shift from OOP to functional programming.

**ANSWER**
Declarative Programming is like asking your friend to draw a landscape. You don’t care how they draw it, that’s up to them.

Imperative Programming is like your friend listening to Bob Ross tell them how to paint a landscape. While good ole Bob Ross isn’t exactly commanding, he is giving them step by step directions to get the desired result.

### 10. What is the event loop?

**GOAL**
Advanced Javascript. For judging their depth of their Javascript. The basic answer is below. But it is only a tiny bit of the entire process.

**ANSWER**
It's the mechanism that executes JS code. It's a queue system, and has a event stack (actions being fired and about to be fired), and a heap (the memory).

### HIGHER LEVEL JAVASCRIPT (or things I don't undersstand)

### 1. Explain Iterators and Generators?

**ANSWER**

### 2. How does a factory function work?

**ANSWER**

### 3. The different between Javascript classes and other languages classes?

**ANSWER**

### 3. Explain Currying.

**ANSWER**

### 4. What are some use cases for the Symbol type?

Using a generator.
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function*

```
var numbers = {
	*[Symbol.iterator]({
  	start = 0,
    end = 100,
    step = 1
  } = {}) {
  	for (let i = start; i <= end; i += step) {
    	yield i;
    }
  }
}
// print 6...30 by 4 steps
console.log(`My lucky numbers are: ${
    [...numbers[Symbol.iterator]({
    start: 6,
    end: 30,
    step: 4
    })]
 }`);
```

**ANSWER**

### 5. Explain Monads and good use cases.

**ANSWER**

### 6. How does the V8 engine parse javascript?

**ANSWER**

The tl;dr: There’s a stage for compiling, then a stage for running the code.
https://github.com/getify/You-Dont-Know-JS

### 7. Service Workers.

**ANSWER**

### 9. Why do they say that in Javascript, everything is an object - and why is it wrong?

**ANSWER**
This falls in the "Well actually" category. For example - null/boolean aren't objects. Super semantics.
https://www.ecma-international.org/ecma-262/9.0/index.html#sec-ecmascript-data-types-and-values (edited)

---

## CODING, STYLE AND WORK:

### 1. You're exploring a new project/task that you have never worked on before. What is your process from receiving it to finally completing it? (Look for the keywords 'ask for help')

**GOAL**

**ANSWER**

1. walking through the code
2. what other developers worked on it.
   a. are they still around?
   b. ask questions -
3. tackling the task
4. At code complete - ask for feedback/review - not a deviation.
5. check in

### 2. Tell me about a project management tool that you've used?

### 3. What is your experience working with a designer? What are some things you would ask/negotiate on?

**GOAL**
negioate changes
tools that are being used
got a pdf

### 4. Tell us about your experience with working remotely.

**GOAL**
Remote work check.

### 5. What are some rules you follow when you're reviewing code? How about when adding/modifying new code?

**GOAL**
Check their thinking at coding while thinking about the developer in the future.

Red flags:
Reviewing code
-Not asking for clarification

-

Adding/modifying code:
-not talking about coding styles
-not talking about branching/commits
-not thinking about asking for feedback

-

### 6. Tell me about a time you helped another dev asyncronously? Via email, or slack, or over the phone.

**GOAL**
Checks for:
The ability to work remotely.
The ability to ask for help remotely.
The ability to write GOOD help requests.

Red Flags:
Not respecting the time of the people they ask help from.
If they're just looking to steal help time
Being vague.
Not sharing enough details.
Not referencing links, screenshots, videos, code for help.

### 7. What is the goal of a PR - for both the story writer and the people reviewing it?

**GOAL**
review standards - no bugs - different perspective.

Provide feedback, learn from others create suggestions/ideas, congraduate when something works well

https://sourcelevel.io/blog/how-to-improve-your-communication-skills-on-code-review-in-8-bullets

Non-offense communication -

preferred set of standards.

### 8. What are some good use-cases to refactor? What are some bad use-cases for refactoring?

**GOAL**

### 9. When was a time you messed up badly?

**GOAL**
Humbleness.

### 10. What technical aspect do you just not understand (right now) that you wish you did?

**GOAL**
Humble-ness.

### 11. What is the most exciting thing you learned in the past 3 months?

**GOAL**
Checking if they like to continue learning. Maybe it's something to keep up to date, or maybe it's learning a foundational element of programming.
Red flags: Nothing. They aren't interested in learning for fun, but learning just to get by.

### 12. What are the developers on the internet that really influence your code & style?

**GOAL**
Learn about their influences. Additionally -- if they like devs who are egotistic, it could be reflective of their personality.

### 13. Who is a developer that you know in real life that you really admire? When was the last time you told them that?

**GOAL**
Gratitude check.

Red flags:
If they mock the question, or unable to answer the question - it's a sign of negative personality traits.

(Studies via a Berkeley Edu article)[https://greatergood.berkeley.edu/article/item/why_is_gratitude_so_hard_for_some_people]

- Potential signs of envy, materialism, narcissism, and cynicism
- Less likely to be atrustic/help others
- Super sensitive to negative life events (more likely to blame others, lash out, bring drama)
- Greater negativity bias

For (a less scientific article, and more pop-psychology)[https://www.powerofpositivity.com/habits-people-not-thankful-avoid/]

1.  PEOPLE WHO AREN’T THANKFUL ARE ALWAYS IN NEED
2.  PEOPLE WHO AREN’T THANKFUL DON’T HAVE TIME FOR YOU UNLESS THEY NEED SOMETHING
3.  PEOPLE WHO AREN’T THANKFUL EXPECT YOU TO HELP THEM
4.  THE WORLD REVOLVES AROUND THEM AND THEIR SCHEDULE
5.  PEOPLE WHO AREN’T THANKFUL OFTEN BITE THE HAND THAT FEEDS THEM
6.  PEOPLE WHO AREN’T THANKFUL WILL ALWAYS REMEMBER THE ONE TIME YOU DIDN’T HELP THEM

NOTE: The last half "When was the last time you told them that?" is incredibly divesive. I asked 8 devs, and half hated that last question. Which makes it even more valuable. Gratitude is a learnable skillset.

---
