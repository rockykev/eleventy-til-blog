---
title: TIL aria-labelledby sections
date: 2022-10-06
published: true
tags: ['accessibility', 'html']
series: false
canonical_url: false
description: "The aria-labelledby attribute provides a way to associate an section of the page marked up as a region or landmarks with text that is on the page that labels it."
layout: layouts/post.njk
---


For accessibility, using `aria-labelledby` can help people who navigate via sectional elements. That connects the header to the section.

> The purpose of this technique is to provide names for regions of a page that can be read by assistive technology. The aria-labelledby attribute provides a way to associate an section of the page marked up as a region or landmarks with text that is on the page that labels it.

> Landmark roles (or "landmarks") programmatically identify sections of a page. Landmarks help assistive technology (AT) users orient themselves to a page and help them navigate easily to various sections of a page.

```html

<section aria-labelledby="sectionHeader1">
  <h2 id="sectionHeader1">A great section</h2>
</section>


<section aria-labelledby="sectionHeader2">
  <h2 id="sectionHeader2">An even better section</h2>
</section>
```



[ARIA13: Using aria-labelledby to name regions and landmarks](https://www.w3.org/TR/WCAG20-TECHS/ARIA13.html#:~:text=The%20aria%2Dlabelledby%20attribute%20provides,identify%20sections%20of%20a%20page.)

via [number 14 - 101 Digital Accessibility (a11y) tips and tricks](https://dev.to/grahamthedev/101-digital-accessibility-tips-and-tricks-4728)
