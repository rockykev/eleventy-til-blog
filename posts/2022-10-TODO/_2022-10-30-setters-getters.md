---
title: TIL the benefits of setters and getters
date: 2022-10-30
published: true
tags: ["oop", "js", "react", "vue", "jsframeworks"]
series: false
canonical_url: false
description: "What are these 'get' and 'set'? They are functions that allow you to access or modify an object's properties! They're in a bunch of programming languages."
layout: layouts/post.njk
---

Why do you see setters and getters?

You might see something like this:

```js

// The Class Version
class Employee {
  constructor(name) {
    this._name = name;
  }

  doWork() {
    return `${this._name} is working`;
  }

  get name() {
    // when you get this by employeeInstance.name
    // the code below will be triggered
    console.log("get triggered!");
    return this._name.toUpperCase();
  }

  set name(newName) {
    // employeeInstance.name = 'xxx'
    console.log("set triggered!");
    if (newName) {
      this._name = newName;
    }
  }
}

// The Object Version
function Employee(name) {
  let _name = name; // Closure!

  this.doWork = function() {
    return `${_name} is working`;
  };

  Object.defineProperty(this, 'name', {
    get: function() {
      console.log('get triggered!');
      return _name.toUpperCase();
    },
    set: function(newName) {
      console.log('set triggered!');
      if (newName) {
        _name = newName;
      }
    }
  });
}

```

So what are these `get` and `set`? They are functions that allow you to access or modify an object's properties! They're in a bunch of programming languages.

The benefits is:

1. You can keep data hidden.

The only way to find that data is by using the `get` command.

2. Keep the data private.

You can't directly modify `_name`.

If you want to modify name, you have to use the `set` command.


3. Business logic.

What if you want to validation? Or change the data after setting?

In our use case, we uppercase the text!

Had you simply done `Employee._name` and now wanted to add extra logic, you would have to fix the code in a bunch of places and convert it into a function call.

That's one of the big benefits here.


> A difference between using a getter or setter and using a standard function is that getters/setters are automatically invoked on assignment. So it looks just like a normal property but behind the scenes you can have extra logic (or checks) to be run just before or after the assignment.

via https://stackoverflow.com/a/42342744/4096078

Any of this ring a bell?

YES! Reactivity! All those frameworks, when you're passing props to make them 'reactive'... it's setters and getters doing the heavy lifting!

