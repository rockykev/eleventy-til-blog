---
title: TIL
date: 2022-10-13
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---




### Facade	A single class that represents an entire subsystem
**Definition**
The Façade pattern provides an interface which shields clients from complex functionality in one or more subsystems. It is a simple pattern that may seem trivial but it is powerful and extremely useful. It is often present in systems that are built around a multi-layer architecture.

On the server, in a multi-layer web application you frequently have a presentation layer which is a client to a service layer. Communication between these two layers takes place via a well-defined API. This API, or façade, hides the complexities of the business objects and their interactions from the presentation layer.

Another area where Façades are used is in refactoring. Suppose you have a confusing or messy set of legacy objects that the client should not be concerned about. You can hide this code behind a Façade. The Façade exposes only what is necessary and presents a cleaner and easy-to-use interface.

https://www.dofactory.com/javascript/design-patterns/facade


Facades are a structural pattern that can often be seen in JavaScript libraries such as jQuery where, although an implementation may support methods with a wide range of behaviors, only a âfacade,â or limited abstraction of these methods, is presented to the public for use.

Facades generally have few disadvantages, but one concern worth noting is performance. Namely, one must determine whether there is an implicit cost to the abstraction a Facade offers to our implementation and, if so, whether this cost is justifiable. Going back to the jQuery library, most of us are aware that both getElementById("identifier") and $("#identifier") can be used to query an element on a page by its ID.
https://www.oreilly.com/library/view/learning-javascript-design/9781449334840/ch09s09.html

 Analogous to a facade in architecture, a facade is an object that serves as a front-facing interface masking more complex underlying or structural code. — Wikipedia

The Facade Pattern has several advantages, summarised in the following points:

The code is easier to use, understand, and test since the facade simplifies the interface.
Clean code because the client/context does not use a complex interface and the system is more flexible and reusable.



**Explain Like I'm 5**

**Use-cases**

**Code example with the pattern**
![](_2022-10-01-TODO--PATTERNS_5.png)

```js

// The main output
import { Facade } from './facade';

class Client {
  facade = new Facade();
  methodClient1() {
    this.facade.methodClient1();
  }

  methodClient2() {
    this.facade.methodClient2();
  }
}

const client = new Client();
client.methodClient1();
client.methodClient2();


// facade.js
import { FacadeSystem1 } from "./system1/facade-system1";
import { FacadeSystem2 } from "./system1/facade-system2";

export class Facade {
  private facadeSystem1 = new FacadeSystem1();
  private facadeSystem2 = new FacadeSystem2();

  methodClient1() {
    console.log('\n...methodClient1\n');
    this.facadeSystem1.methodA2();
    this.facadeSystem1.methodB();
    this.facadeSystem2.methodC();
    this.facadeSystem2. methodD();
  }

  methodClient2() {
  console.log('\n...methodClient2\n');
  this.facadeSystem1.methodA();
  this.facadeSystem2.methodC3();
  this.facadeSystem2. methodC();
  }
}
```

Notice the facade.js lets you remix various things


via https://levelup.gitconnected.com/understanding-design-patterns-facade-using-pokemon-and-dragonball-examples-c94f11326dd1

