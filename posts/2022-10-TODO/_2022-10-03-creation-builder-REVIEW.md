---
title: TIL the Builder Pattern
date: 2022-10-03
published: true
tags: ['oop', 'js']
series: false
canonical_url: false
description: "Builder pattern builds a complex object using simple objects by providing a step by step approach. This allows you to use setter methods to slowly build up your parameter list"
layout: layouts/post.njk
---


Builder pattern is a design pattern to provide a flexible solution for creating objects. Builder pattern separates the construction of a complex object from its representation.

Builder pattern builds a complex object using simple objects by providing a step by step approach.

> This allows you to use setter methods to slowly build up your parameter list. One additional method on a builder class is a build() method, which simply passes the builder object into the desired constructor, and returns the result.

via https://stackoverflow.com/a/8959150/4096078

**Explain Like I'm 5**

Using this pattern with simple implementations can make the code more complex than strictly necessary.

Imagine a scenario where you’re creating an application that allows the user to build their own house.

Since you want your users to like your product, you provide them a tremendous amount of options. They can add pools, gardens, internet, walls, and even a roof (imagine that, a real roof!).

The builder pattern recommends that you extract the components of the house into a new class. For our use case, let’s call it a HouseBuilder (surprise!). We would then add all the properties as methods on the HouseBuilder. We’d add addPools(), addGardens(), addInternet(), addWalls(), addRoof(), etc. After we add all those methods, we need to add a getHouse() method to retrieve the built house. Something important to note is that you’ll need to reset the builder after you retrieve the product. Conventionally this is done inside the getHouse() method, however you can manually call it from somewhere else.

via https://medium.com/swlh/deep-dive-into-design-patterns-with-javascript-the-builder-pattern-67b4986fc846

**Use-cases**

```js

// Setting up the initial values of what exists in this 'House'
class House {
  constructor() {
    this.walls = null;
    this.roof = null;
    this.pool = null;
    this.garden = null;
    this.internet = null;
  }

  setWalls(walls) {
    this.walls = walls;
  }

  setRoof(roof) {
    this.roof = roof;
  }

  setPool(pool) {
    this.pool = pool;
  }

  setGarden(garden) {
    this.garden = garden;
  }

  setInternet(internet) {
    this.internet = internet;
  }
}

// This function is about building the house
class HouseBuilder {
  constructor() {
    this.house = new House(); // new
  }

  buildWalls(walls) {
    this.house.setWalls(walls);
    return this; // returning this for method chaining
  }

  buildRoof(roof) {
    this.house.setRoof(roof);
    return this;
  }

  buildPool(pool) {
    this.house.setPool(pool);
    return this;
  }

  buildGarden(garden) {
    this.house.setGarden(garden);
    return this;
  }

  getHouse() {
    const house = this.house;
    this.reset();
    return house;
  }

  reset() {
    this.house = new House();
  }
}

const house = builder
  .buildWalls(4)
  .buildRoof('sloped')
  .buildGarden(['trees', 'grass', 'peacocks'])
  .getHouse();

```

However, there are some obvious downsides to using builders as well:

Bloated code: Adding a Builder class within our Car class required us to add a lot more code than if we just used the constructor. Additionally, it can seem repetitive since the builder class required similar attributes to the main class.

We need to construct two objects now: the Builder instance, and the actual Car itself. This is not ideal if your application is sensitive to memory constraints.

via https://www.sohamkamani.com/javascript/builder-pattern/


**Code example with the pattern**

```js
function Request() {
  this.url = '';
  this.method = '';
  this.payload = {};
}

function RequestBuilder() {

  this.request = new Request();

  this.forUrl = function(url) {
    this.request.url = url;
    return this;
  };

  this.useMethod = function(method) {
    this.request.method = method;
    return this;
  };

  this.payload = function(payload) {
    this.request.payload = payload;
    return this;
  };

  this.build = function() {
    return this.request;
  };

}

module.exports = RequestBuilder;
```
Via [Builder Pattern on Github](https://github.com/fbeline/design-patterns-JS/blob/master/src/creational/builder/builder.js)
