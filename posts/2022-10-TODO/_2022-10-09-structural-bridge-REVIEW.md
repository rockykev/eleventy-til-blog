---
title: TIL
date: 2022-10-09
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---



### Bridge	Separates an object’s interface from its implementation
**Definition**
The Bridge pattern allows two components, a client and a service, to work together with each component having its own interface. Bridge is a high-level architectural pattern and its main goal is to write better code through two levels of abstraction. It facilitates very loose coupling of objects. It is sometimes referred to as a double Adapter pattern.
https://www.dofactory.com/javascript/design-patterns/bridge


![](_2022-10-01-TODO--PATTERNS_1.png)

![](_2022-10-01-TODO--PATTERNS_2.png)
via https://matcha.fyi/bridge-pattern-javascript/

This is much more efficient and less prone to breakages than just have a single interface layer. For example, if the requirements in code set 1 changes, only the associated interface needs to change. Any changes made down that particular branch won’t impact on code set 2.


**Explain Like I'm 5**


**Use-cases**

**Code example with the pattern**
```js
// the initial 'thing'
// Assume it to be things you can't mess with. It's scary in here.
class Thing {
  constructor(name, thing) {
    this.name = name
    this.thing = thing
  }
}

// the next step, 'LivingThing'
class LivingThing extends Thing {
  constructor(name, bodyParts) {
    super(name, this)
    this.name = name
    // Bridge
    this.mouth = bodyParts?.mouth || null
  }

  eat(food) {
    this.mouth.open()
    this.mouth.chew(food)
    this.mouth.swallow()
    return this
  }
}

// The next next step, 'Character'
class Character extends LivingThing {
  constructor(name, thing) {
    super(name, this)
    this.thing = thing
    this.hp = 100
    this.chewing = null
  }

  attack(target) {
    target.hp -= 5
    return this
  }

  chew(food) {
    this.chewing = food
    return this
  }

  eat(food) {
    this.hp += this.chewing.hpCount
    return this
  }
}

// Extending that
class Swordsman extends Character {}
class Rogue extends Character {}
class Archer extends Character {}
class Sorceress extends Character {}


```
via https://betterprogramming.pub/the-bridge-design-pattern-in-javascript-5427dd1c8660


