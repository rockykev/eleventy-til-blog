---
title: TIL to stop using px for font-sizes
date: 2022-10-15
published: true
tags: ['css', 'font', 'size', 'accessibility']
series: false
canonical_url: false
description: "px is pixel, which is a single dot in a computer monitor. em is relative unit, but inherit. rem is relative unit, but only to the main node."
layout: layouts/post.njk
---


This is strictly for font-sizes.

I'm not even going to get started with media-queries, or width, or anything.

We're going to focus on the just fonts.

## The Pixel

`px` is Pixel.
In theory, 1px is equal to a single dot in a computer monitor or phone screen.

## The EM

The `em` means it's a relative unit, based on the element's calculated font size.  If our paragraph has a bottom margin of 1.5em, we're saying that it should be 1.5x the font size. This allows us to “anchor” one value to another, so that they scale proportionally.

## The REM

```html
<style>
  main {
    font-size: 1.125em;
  }
  article {
    font-size: 0.9em;
  }
  p.intro {
    font-size: 1.25em;
  }
</style>
<main>
  <article>
    <p class="intro">
      What size is this text?
    </p>
  </article>
</main>
```

Let's break this down.

1. Main says font-size is `1.125em`
2. article says font-size is `0.9em`
3. intro class says font-size is `1.25em`

What's the font size?

To figure it out, we have to multiply each ratio. The root font size is 16px by default, and so the equation is 16 × 1.125 × 0.9 × 1.25. The answer is 20.25 pixels.

WHAT A NIGHTMARE.

To solve this problem, the CSS language designers created the rem unit. It stands for “Root EM”.

So this checks the root node the `html` element, and ignores all other font-sizes.

So the root node is often defaulted to `16px`, which makes 1rem = 16px.




via [The Surprising Truth About Pixels and Accessibility](https://www.joshwcomeau.com/css/surprising-truth-about-pixels-and-accessibility/)
