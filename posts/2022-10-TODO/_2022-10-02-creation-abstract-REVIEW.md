---
title: TIL Abstract Factory pattern
date: 2022-10-02
published: true
tags: ['oop', 'js']
series: false
canonical_url: false
description: "A Abstract Factory provides interface for creating families of related or dependent objects without specifying their concrete classes"
layout: layouts/post.njk
---


## What is a Abstract Factory?

> [An Abstract Factory] Provide an interface for creating families of related or dependent objects without specifying their concrete classes
— via "Design Patterns: Elements of Reusable Object-Oriented Software"


It's like a factory method, but it creates objects that share a common concept.

A factory pattern:
```js
class Animal {
  constructor(name) {
    this.name = name;
  }

  speak() {
    console.log(`${this.name} makes a noise.`);
  }
}

class Dog extends Animal {
  constructor(name) {
    super(name);
  }

  speak() {
    console.log(`${this.name} barks.`);
  }
}

class Cat extends Animal {
  constructor(name) {
    super(name);
  }

  speak() {
    console.log(`${this.name} meows.`);
  }
}

class AnimalFactory {
  createAnimal(type, name) {
    switch (type) {
      case 'dog':
        return new Dog(name);
      case 'cat':
        return new Cat(name);
      default:
        throw new Error(`Invalid animal type: ${type}`);
    }
  }
}

const animalFactory = new AnimalFactory();
const dog = animalFactory.createAnimal('dog', 'Fido');
const cat = animalFactory.createAnimal('cat', 'Whiskers');
dog.speak(); // "Fido barks."
cat.speak(); // "Whiskers meows."
```

What if you want both a `carFactory`, a `animalFactory`, and a `plantFactory`?

> The problems solved by Abstract Factory are similar to those solved by the Factory-Method pattern, but with greater abstraction in the types of objects that need to be created. Therefore, in the case of Abstract Factory it is required to work with several families of products related to each other rather than in a set of products.

> Cleaner code since the Single Responsibility Principle (SRP) is respected since the responsibility of creating the concrete product is transferred to the concrete creator class instead of the client class having this responsibility.
via https://dev.to/carlillo/understanding-design-patterns-abstract-factory-23e7

In other words, you use a abstract factory pattern. It can look like this:

```js
class AbstractFactory {
  constructor(animalFactory, carFactory, plantFactory) {
    this.animalFactory = animalFactory;
    this.carFactory = carFactory;
    this.plantFactory = plantFactory;
  }

  createAnimal(type, name) {
    if (type === 'dog') {
    return this.animalFactory.createDog(name);
    } else if (type === 'cat') {
      return this.animalFactory.createCat(name);
    }
  }

  createCar(type) {
    return this.carFactory.createCar(type);
  }

  createPlant(type) {
    return this.plantFactory.createPlant(type);
  }
}

// pass in the factories you want
const animalFactory = new AnimalFactory();
const carFactory = new CarFactory();
const plantFactory = new PlantFactory();

// create the abstract factory
const abstractFactory = new AbstractFactory(animalFactory, carFactory, plantFactory);
```

Rule of thumb: Use a factory if your app needs control over object creation.

**Explain Like I'm 5**
Creation of characters in a video game. Let's think of the classic WoW (World of Warcraft) in which the player can have a set of objects depending on the race they choose. For example, we will have the races: Humans, Orcs and Magicians; which will have weapons and armor (products) that will be different depending on the race (the family of objects).

Rule of thumb: Use a factory if your app needs control over object creation.

via:
https://github.com/fbeline/design-patterns-JS/blob/master/src/creational/abstract-factory/abstract-factory.js
