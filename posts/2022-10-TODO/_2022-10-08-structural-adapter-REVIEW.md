---
title: TIL
date: 2022-10-08
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---


### Adapter	Match interfaces of different classes
**Definition**
The Adapter pattern translates one interface (an object‘s properties and methods) to another. Adapters allows programming components to work together that otherwise wouldn&lstqup;t because of mismatched interfaces. The Adapter pattern is also referred to as the Wrapper Pattern.
via https://www.dofactory.com/javascript/design-patterns/adapter

The adapter pattern introduces an intermediary piece of code that makes two parts of a system compatible with one another. It also injects an element of lose coupling by keeping the two pieces of code separate. It means that you can write your code however you want without the need to take the other piece of code into consideration. Your adapter code will do the necessary translation and give you what you need and in whatever format you want.

![](_2022-10-01-TODO--PATTERNS_0.png)
via https://matcha.fyi/bridge-pattern-javascript/

**Explain Like I'm 5**
Imagine you have to build another cart solution for your boss. But there’s a caveat — you’re not allowed to change the original code that spits out the cart data. Why? Because no one knows how that change will impact the rest of the application. It’s a fragile piece of art that no one wants to touch, not yet, or ever. The situation is one of those moments where you just don’t have the time or mental space to question it. You’ll just have to accept that it’s going to be like this until you fully transition out of your legacy systems.

But the process of transitioning is going to take a long time and you don’t want your code to be too intertwined with the old code. What do you do?

In situations like these, the adapter pattern comes in handy.
https://matcha.fyi/adapter-pattern-javascript/



Now, what is Adapter Pattern? If we bring the problem from the real domestic world, a adapter is used to connect two or more things which can't be connected with their original sockets.

An example of this is with the new cellphones. Normally most of the phones have a universal port (3.5mm) for headphones. So their connection is made directly.
With the new cellphones, the companies decided to remove the headphone port, leaving only the charging port.

Here is where the adapter comes into play. Companies made an adapter so you can still use the 3.5mm headphones using the charging port.
via https://dev.to/wecarrasco/adapter-pattern-with-javascript-4lgi

**Use-cases**
One scenario where Adapters are commonly used is when new components need to be integrated and work together with existing components in the application.

Another scenario is refactoring in which parts of the program are rewritten with an improved interface, but the old code still expects the original interface.

**Code example with the pattern**
```js
//original cart data that cannot be changed at source
 //let's pretend this is from a mysterious data service
 //we have no direct access to this
 var cart = [
   {item: "vintage clock", sku: 9284, value: 15.99},
   {item: "motivate carts", sku: 9232, value: 19.99}
 ]

 //old interface pulls in data from mysterious data service
 //we're not allowed to touch this
 function Cart(){
     return this.cart;
 }

 //our adapter code to translate the data
 //prevents us from consuming the Cart() directly
 function CartAdapter(){
   var originalCart = Cart();
   var adapterCart = originalCart.map(function(obj){
     return {
       item: obj.item,
       productId: obj.sku,
       price:obj.value

     }
   });

   return adapterCart;
 }

 //now we can use the result of the CartAdapter() however we want
 //changes in Cart() will result in only a change in the adapter
 //the rest of the code remains unimpacted once the adapter is fixed
 console.log(CartAdapter());
```
via https://matcha.fyi/adapter-pattern-javascript/

```js
// OLD Interface
function Shipping() {
    this.request = function (zipStart, zipEnd, weight) {
        // ...
        return "$49.75";
    }
}
// OLD Test
const shipping = new Shipping();
const cost = shipping.request("78701", "10010", "2 lbs");



// NEW Interface
function AdvancedShipping() {
    this.login = function (credentials) { /* ... */ };
    this.setStart = function (start) { /* ... */ };
    this.setDestination = function (destination) { /* ... */ };
    this.calculate = function (weight) { return "$39.50"; };
}

function ShippingAdapter(credentials) {
    var shipping = new AdvancedShipping();

    shipping.login(credentials);

    return {
        request: function (zipStart, zipEnd, weight) {
            shipping.setStart(zipStart);
            shipping.setDestination(zipEnd);
            return shipping.calculate(weight);
        }
    };
}
// NEW Test
const credentials = { token: "30a8-6ee1" };
const adapter = new ShippingAdapter(credentials);
const cost = adapter.request("78701", "10010", "2 lbs");

```

Another example: https://dev.to/wecarrasco/adapter-pattern-with-javascript-4lgi
