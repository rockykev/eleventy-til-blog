---
title: TIL Keyboard Traps
date: 2022-10-12
published: true
tags: ['accessibility', 'button', 'modal']
series: false
canonical_url: false
description: "A keyboard trap means that a person who uses a keyboard can enter a part of a page and then gets stuck in a focus loop, with no way to get out of it other than reloading the whole page."
layout: layouts/post.njk
---

A keyboard trap means that a person who uses a keyboard can enter a part of a page and then gets stuck in a focus loop, with no way to get out of it other than reloading the whole page.

This tends to happen a lot with modals where developers forget to add a proper close button (they use a `<div>` and a click handler rather than a `<button>` to close the modal, so it isn't reachable via keyboard).

![](_2022-10-12-keyboard-trap_0.png)
[image source](https://design.gitlab.com/accessibility/keyboard-only)


To solve it:

Make sure you can close modals with a `<button>` that is reachable via keyboard and also make sure that the Esc key closes any modals or lets you escape from any control where you trap focus.

Expected behavior

* After encountering a keyboard trap a user should be able to “untrap” the focus with the Esc key, a close button, or other action that navigates the user away from the current experience.

* When the user exits the keyboard trap (without page reload or navigating away) the focus should return to the previously focused element, usually the one that triggered the keyboard trap in the first place.

via [W3C - Keyboard Trap](https://www.w3.org/TR/UNDERSTANDING-WCAG20/keyboard-operation-trapping.html)

via [number 48 - 101 Digital Accessibility (a11y) tips and tricks](https://dev.to/grahamthedev/101-digital-accessibility-tips-and-tricks-4728)
