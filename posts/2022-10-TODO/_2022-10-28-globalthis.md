---
title: TIL globalThis
date: 2022-10-28
published: true
tags: ['odd', 'javascript', 'this', 'mdn']
series: false
canonical_url: false
description: "On the web you can use `window`, `self`, or `frames`. But in Web Workers only `self` will work. In Node.js none of these work, and you must instead use `global`."
layout: layouts/post.njk
---

Today I stumbled on `globalThis`.

> On the web you can use `window`, `self`, or `frames`. But in Web Workers only `self` will work. In Node.js none of these work, and you must instead use `global`.

What a mess right?

Some languages like Swift and Python call it `self` and others like Java call it `this`.

Why not just `this`?

`this` in Javascript can refer to the instance of an object or class.
It also changes depending on how a function is called.

The context of where you use it changes, and it even changes if you are in strict vs non-strict mode.

A way to accurately call `this` on the global object, without having to remember where the Javascript is being called.

[mdn](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/globalThis)
