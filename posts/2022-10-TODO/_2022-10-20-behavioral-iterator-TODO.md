---
title: TIL structuredClone() for Javascript
date: 2022-10-20
published: true
tags: ['javascript', 'object', 'mdn']
series: false
canonical_url: false
description: "Reminder that objects aren't copied. Objects are instead referenced. Transferred objects are detached from the original object and attached to the new object; they are no longer accessible in the original object."
layout: layouts/post.njk
---

Reminder that objects aren't copied. Objects are instead referenced.

```js
// Original object
let stevesLunch = {
	sandwich: 'turkey',
	chips: 'cape cod',
	drink: 'soda'
};

// Assumed "clone" of the object
// Now Dave has the same food that Steve has.
let davesLunch = stevesLunch;

// Remove "chips" from Steve's lunch
delete stevesLunch.chips;

// OH NO, dave's Lunch also removed the chips!
// {sandwich: "turkey", drink: "soda"}
console.log(davesLunch);
```
In fact, dave's lunch is just pointing to steve's lunch! There actually wasn't a clone!

When cloning objects, multidimensional objects sort of fail.

So instead, you want to use `structuredClone()`

> The method also allows transferable objects in the original value to be transferred rather than cloned to the new object. Transferred objects are detached from the original object and attached to the new object; they are no longer accessible in the original object.

The [MDN for structuredClone()](https://developer.mozilla.org/en-US/docs/Web/API/structuredClone)


```js

const original = { name: "MDN" };
original.itself = original;

// Clone it
const clone = structuredClone(original);

// Original object
let stevesLunch = {
	sandwich: 'turkey',
	chips: 'cape cod',
	drink: 'soda'
};

// Assumed "clone" of the object
// Now Dave has the same food that Steve has.
let davesLunch = structuredClone(stevesLunch);

// Remove "chips" from Steve's lunch
delete stevesLunch.chips;

// Dave's lunch is untouched!
// output: {sandwich: 'turkey', chips: 'cape cod', drink: 'soda'}
console.log(davesLunch);
```



via [Creating a deep copy of an object or array with the structuredClone() method in vanilla JavaScript](https://gomakethings.com/creating-a-deep-copy-of-an-object-or-array-with-the-structuredclone-method-in-vanilla-javascript/)
