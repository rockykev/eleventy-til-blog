---
title: TIL
date: 2022-10-14
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---




### Flyweight	A fine-grained instance used for efficient sharing

**Definition**
The Flyweight pattern conserves memory by sharing large numbers of fine-grained objects efficiently. Shared flyweight objects are immutable, that is, they cannot be changed as they represent the characteristics that are shared with other objects.

An example of the Flyweight Pattern is within the JavaScript engine itself which maintains a list of immutable strings that are shared across the application.
https://www.dofactory.com/javascript/design-patterns/flyweight

his pattern provides ways to decrease object count thus improving application required objects structure. It aims to minimize the use of memory in an application by sharing as much data as possible with related objects. One important feature of flyweight objects is that they are immutable. This means that they cannot be modified once they have been constructed.
Each "flyweight" object is divided into two pieces: the state-dependent (extrinsic) part, and the state-independent (intrinsic) part. The intrinsic state is stored (shared) in the Flyweight object. The extrinsic state is stored or computed by client objects and passed to the Flyweight when its operations are invoked.

Intrinsic information may be required by methods on our objects, which they absolutely cannot function without. Extrinsic information can be removed and stored externally.
https://dev.to/jazsmith24/javascript-design-patterns-flyweight-pattern-1g5g

**Explain Like I'm 5**
Other examples include characters and line-styles in a word processor, or 'digit receivers' in a public switched telephone network application. You will find flyweights mostly in utility type applications such as word processors, graphics programs, and network apps; they are less often used in data-driven business type applications.

**Use-cases**


**Code example with the pattern**

Notice the children elements are similar?
```js
// Version 1 - BEFORE
const elems = [
  {
    tagName: 'div',
    style: { width: '28.5px', height: '20px' },
    children: [
      { tagName: 'input', style: { width: '28.5px', height: '20px' } },
      { tagName: 'input', style: { width: '28.5px', height: '20px' } },
      { tagName: 'select', style: { width: '28.5px', height: '20px' } },
      { tagName: 'input', style: { width: '28.5px', height: '20px' } },
    ],
  },
]
```


```js
// Version 2
const inputElement = {  tagName: 'input', style: { width: '28.5px', height: '20px' } }

const elems = [
  {
    tagName: 'div',
    style: { width: '28.5px', height: '20px' },
    children: [
      inputElement,
      inputElement,
      { tagName: 'select', style: { width: '28.5px', height: '20px' } },
      inputElement,
    ],
  },
]
```
via https://jsmanifest.com/power-of-flyweight-design-pattern-in-javascript/

