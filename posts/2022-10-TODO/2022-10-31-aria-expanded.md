---
title: TIL aria-expanded for showing when things are hidden/shown
date: 2022-10-31
published: true
tags: ['accessibility', 'html', 'accordion']
series: false
canonical_url: false
description: "The aria-expanded attribute is set on an element to indicate if a control is expanded or collapsed, and whether or not its child elements are displayed or hidden."
layout: layouts/post.njk
---

Say you had a accordion.

`aria-expanded`

> The aria-expanded attribute is set on an element to indicate if a control is expanded or collapsed, and whether or not its child elements are displayed or hidden.

```html
<style>
.topic {
    display: none;
    margin-bottom: 1em;
    padding: .25em;
    border: black thin solid;
    background-color: #EEEEFF;
    width: 40em;
}
</style>


<button class="buttonControl" aria-controls="topic1" aria-expanded="false">
    <span>Show</span> Topic 1
</button>

<div id="topic1" class="topic" role="region" tabindex="-1" >
    Topic 1 is all about being Topic 1 and may or may not have anything to do with other topics.
</div>

```
code via [W3C wiki](https://www.w3.org/WAI/GL/wiki/Using_the_WAI-ARIA_aria-expanded_state_to_mark_expandable_and_collapsible_regions#Example_1:_Using_a_button_to_collapse_and_expand_a_region)


via the [MDN](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-expanded)


via [#99 - 101 Digital Accessibility (a11y) tips and tricks](https://dev.to/grahamthedev/101-digital-accessibility-tips-and-tricks-4728)
