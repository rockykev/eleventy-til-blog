---
title: TIL the Singleton Pattern
date: 2022-10-07
published: true
tags: ['js', 'oop']
series: false
canonical_url: false
description: "An anti-pattern in JS: A Singleton is an object which can only be instantiated only once. A singleton pattern creates a new instance if one doesn’t exist. If an instance exists, it simply returns a reference to that object. Any repeated calls to the constructor would always fetch the same object."
layout: layouts/post.njk
---

A Singleton is an object which can only be instantiated only once. A singleton pattern creates a new instance if one doesn’t exist. If an instance exists, it simply returns a reference to that object. Any repeated calls to the constructor would always fetch the same object.

In Javascript, it's considered a Anti-pattern to use the Singleton Class Pattern.

> In many programming languages, such as Java or C++, it's not possible to directly create objects the way we can in JavaScript. In those object-oriented programming languages, we need to create a class, which creates an object.  However, the class implementation ...is  actually overkill. Since we can directly create objects in JavaScript, we can simply use a regular object to achieve the exact same result.

via [Singleton Pattern via patterns.dev](https://www.patterns.dev/posts/singleton-pattern/)

In React and Vue, we use state management tools like Redux and Vuex instead of using Singletons.

Don't bother with the Pattern format.

The formal definition: "Ensure a class only has one instance, and provide a **global point** of access to it."

The most common use-case of a Singleton is a global variable. Having global point (global variables) is generally considered as a bad design decision. Global scope pollution can end up in accidentally overwriting the value of a global variable, which can lead to a lot of unexpected behavior.


**Explain Like I'm 5**

These are just object literals. No need to make it complex.

For example:

```js
const user = {
  name: 'Rocky',
  age: 37,
  job: 'Developer',
  greet: function() {
    console.log('Hey now!');
  }
};

const otherUser = user; // copy all of Rocky

user.name = "Marcy"

console.log(user.name); // Marcy
console.log(otherUser.name); // Marcy, but it's supposed to be Rocky

```
In Javascript, objects are passed by reference not by value. So there is only a single object in the memory.




**Code example with the pattern**

CLASS VERSION:

This is how this pattern would be written using the patterns in other languages
```js
// singleton.js
let instance;
let counter = 0;

class Counter {

  // This is so we can only have it called once
  constructor() {
    if (instance) {
      throw new Error("You can only create one instance!");
    }
    instance = this;
  }

  getInstance() {
    return this;
  }

  getCount() {
    return counter;
  }

  increment() {
    return ++counter;
  }

  decrement() {
    return --counter;
  }
}

const singletonCounter = Object.freeze(new Counter());
export default singletonCounter;
```

OBJECT VERSION
```js
let count = 0;

const counter = {
  increment() {
    return ++count;
  },
  decrement() {
    return --count;
  }
};

Object.freeze(counter);
export { counter };
```
