---
title: TIL
date: 2022-10-16
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---


### Proxy	An object representing another object
**Definition**
The Proxy pattern provides a surrogate or placeholder object for another object and controls access to this other object.

With a Proxy object, we get more control over the interactions with certain objects. A proxy object can determine the behavior whenever we're interacting with the object, for example when we're getting a value, or setting a value.
Generally speaking, a proxy means a stand-in for someone else. Instead of speaking to that person directly, you'll speak to the proxy person who will represent the person you were trying to reach. The same happens in JavaScript: instead of interacting with the target object directly, we'll interact with the Proxy object.

Overusing the Proxy object or performing heavy operations on each handler method invocation can easily affect the performance of your application negatively. It's best to not use proxies for performance-critical code.
https://www.patterns.dev/posts/proxy-pattern/

The difference between proxy and facade patterns are that we can proxies create interfaces for one object.
https://levelup.gitconnected.com/basic-javascript-design-patterns-decorators-facades-and-proxies-2309eb485229

The purpose of the Proxy pattern is to provide a placeholder for another object to control and manage access to it. The pattern acts as a wrapper that allows you to perform something either before or after the request gets through to the original object.

1. Remote Proxy: Provides a local representative for an object that is located on a remote server.
2. Virtual Proxy: Creates expensive objects on demand and acts as a representative. The Virtual Proxy often defers the creation of the object until it is needed.
3. Protection Proxy: Controls access to the original object. Protection proxies are useful when objects should have different access rights.

https://javascript.plainenglish.io/software-design-patterns-with-typescript-examples-proxy-f3a671fe1cb3

**Explain Like I'm 5**
Chances are high that you own a credit card. In that case, you basically own a proxy for what is in your bank account.

Our credit card acts as a Proxy for our money — the RealSubject.

**Use-cases**

**Code example with the pattern**


```ts
/**
 * This is our Subject
 */
interface Payment {
  pay(amount: number): void;
}

/**
 * This is our RealSubject
 */
class RealMoney implements Payment {
  public pay(amount: number): void {
    alert(`Successfully paid amount: ${amount}`);
  }
}


/**
 * This is our Proxy
 */
const creditCardProxy = new Proxy(realMoney, {
  get: (object, prop: keyof RealMoney) => {

    if (prop === 'pay') {
      const pin = prompt('Please enter your PIN:');
      if (pin !== '1234') {
        const errorMsg = 'PIN is not correct. Payment declined.';
        alert(errorMsg)
        throw new Error(errorMsg)
      }
      return object[prop];
    }

    throw new Error('Function not implemented for credit card.')
  },
});


const realMoney = new RealMoney();
creditCardProxy.pay(50);
```
via https://javascript.plainenglish.io/software-design-patterns-with-typescript-examples-proxy-f3a671fe1cb3


