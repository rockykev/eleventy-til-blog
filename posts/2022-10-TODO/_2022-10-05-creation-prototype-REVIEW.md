---
title: TIL the prototype pattern in JS
date: 2022-10-05
published: true
tags: ['js', "proto", 'oop']
series: false
canonical_url: false
description: "The prototype pattern is a useful way to share properties among many objects of the same type. The prototype is an object that's native to JavaScript, and can be accessed by objects through the prototype chain"
layout: layouts/post.njk
---

The prototype pattern is a useful way to share properties among many objects of the same type. The prototype is an object that's native to JavaScript, and can be accessed by objects through the prototype chain.

The Prototype design pattern relies on the JavaScript prototypical inheritance. The prototype model is used mainly for creating objects in performance-intensive situations.

> The value of `__proto__` on any instance of the constructor, is a direct reference to the constructor's prototype!

via https://www.patterns.dev/posts/prototype-pattern/


The objects created are clones (shallow clones) of the original object that are passed around.

> One use case of the prototype pattern is performing an extensive database operation to create an object used for other parts of the application. If another process needs to use this object, instead of having to perform this substantial database operation, it would be advantageous to clone the previously created object.

via https://www.digitalocean.com/community/conceptual-articles/prototype-design-pattern-in-javascript


The prototype pattern allows us to easily let objects access and inherit properties from other objects. Since the prototype chain allows us to access properties that aren't directly defined on the object itself, we can avoid duplication of methods and properties, thus reducing the amount of memory used.


**Code example with the pattern**
```js

// The initial dog
class Dog {
  constructor(name) {
    this.name = name;
  }

  bark() {
    console.log("Woof!");
  }
}

// this adds a new fly function
class SuperDog extends Dog {
  constructor(name) {
    super(name);
  }

  fly() {
    console.log(`Flying!`);
  }
}

const dog1 = new SuperDog("Daisy");
dog1.bark(); // this reaches into __proto__
dog1.fly(); // this exists in the superdog

```
