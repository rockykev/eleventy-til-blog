---
title: TIL Factory Pattern
date: 2022-10-04
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

Factory Pattern is a pattern to create new objects without specifying the exact class or constructor function from which the object will be created.

The factory pattern is used to create objects without exposing the instantiation logic. This pattern can be used when we need to generate a different object depending upon a specific condition.

Factory Methods are frequently used in applications that manage, maintain, or manipulate collections of objects that are different but at the same time have many characteristics (i.e. methods and properties) in common.

Factory gives us an object creation mechanism that is both flexible and reusable.

**Explain Like I'm 5**

It lets us generate a template of a object!

**Use-cases**

Using a Factory Function where you 'return' the object.

```js
// creates factory function
function vehicleFactory (manufacturer, plateNO) {
    return  {
        manufacturer,
        plateNO,
        startEngine () {
            console.log("reving engine")
        },
        drive () {
            console.log("driving car...")
        }
    }
}

const Vehicle1 = vehicleFactory("Toyota", 12345);
console.log(Vehicle1)
//{
//  manufacturer: 'Toyota',
//  plateNO: 12345,
//  startEngine: [Function: startEngine],
//  drive: [Function: drive]
//}

```

Creating the object using the `new` keyword:

```js
// initial Factory
function Alien(name, phrase) {
    this.name = name
    this.phrase = phrase
    this.species = "alien"
}

// adding additional functions
Alien.prototype.fly = () => console.log("Zzzzzziiiiiinnnnnggggg!!")
Alien.prototype.sayPhrase = () => console.log(this.phrase)

const alien1 = new Alien("Ali", "I'm Ali the alien!")

console.log(alien1.name) // output "Ali"
console.log(alien1.phrase) // output "I'm Ali the alien!"
alien1.fly() // output "Zzzzzziiiiiinnnnnggggg"
```


As a export:

This module allows you to define which car it is and it's internal stats. Literally a BMW template, and a BMW Factory.

```js
function bmwFactory(type) {
  if (type === 'X5')
    return new Bmw(type, 108000, 300);
  if (type === 'X6')
    return new Bmw(type, 111000, 320);
}

function Bmw(model, price, maxSpeed) {
  this.model = model;
  this.price = price;
  this.maxSpeed = maxSpeed;
}

module.exports = bmwFactory;
```

via [Factory Github example](https://github.com/fbeline/design-patterns-JS/blob/master/src/creational/factory/factory.js)
