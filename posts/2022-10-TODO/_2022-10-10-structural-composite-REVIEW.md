---
title: TIL
date: 2022-10-10
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---




### Composite	A tree structure of simple and composite objects
**Definition**
The Composite pattern allows the creation of objects with properties that are primitive items or a collection of objects. Each item in the collection can hold other collections themselves, creating deeply nested structures.
https://www.dofactory.com/javascript/design-patterns/composite

![](_2022-10-01-TODO--PATTERNS_3.png)

Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly.



The composite pattern as explained by the Gang of Four is a design pattern which allows you to "compose objects into tree structures to represent part-whole hierarchies." If you want to have a better understanding of how your favorite frameworks work or learn to build your own view framework, this pattern is the place to start. JavaScript frameworks like React and Vue use the composite pattern to build user interfaces.

What makes this pattern so powerful is that individual objects can be treated the same as composite objects because they share an interface. This means you can reuse objects without worrying about whether they will play nicely with others. While it may seem more obvious to use this pattern in the design of user interfaces or graphics, it is possible to use the composite pattern in any case where the objects in your application have a tree structure. To give you a different perspective, in this post you will be learning how to use the composite pattern by designing a game engine.
https://x-team.com/blog/understanding-the-composite-pattern/


**Explain Like I'm 5**
Single Objects can be thought of as standalone objects that will implement similar behaviour matching a predefined contract.

Compoite Objects are made up of either Single Objects and/or other Composite Objects.


Let's break it down a bit. Let's say we buy a Printer at the store. It comes in a Box. When we open the Box, we see there is a Printer in the Box, but that there is also another Box along side it. This Box contains a Power Lead and a USB Adapter for the Printer.

We can think of the Printer itself as a Single Object, whilst the Box is a Composite Object. It is has a Printer and it has another Box. This nested Box has a Power Lead and a USB Adapter, both Single Objects, making this Box a Composite Object.
https://dev.to/coly010/the-composite-pattern-design-patterns-meet-the-frontend-445e



**Use-cases**

**Code example with the pattern**

![](_2022-10-01-TODO--PATTERNS_4.png)
https://x-team.com/blog/understanding-the-composite-pattern/

```
.
└── Equipment
    ├── FloppyDisk
    ├── HardDrive
    ├── Memory
    └── Pattern
        └── Cabinet
```
via https://designpatternsgame.com/patterns/composite

```js
//Equipment
class Equipment {
  getPrice() {
    return this.price || 0;
  }

  getName() {
    return this.name;
  }

  setName(name) {
    this.name = name;
  }
}

class Pattern extends Equipment {
  constructor() {
    super();
    this.equipments = [];
  }

  add(equipment) {
    this.equipments.push(equipment);
  }

  getPrice() {
    return this.equipments
      .map(equipment => {
        return equipment.getPrice();
      })
      .reduce((a, b) => {
        return a + b;
      });
  }
}

class Cabinet extends Pattern {
  constructor() {
    super();
    this.setName('cabinet');
  }
}

// --- leafs ---
class FloppyDisk extends Equipment {
  constructor() {
    super();
    this.setName('Floppy Disk');
    this.price = 70;
  }
}

class HardDrive extends Equipment {
  constructor() {
    super();
    this.setName('Hard Drive');
    this.price = 250;
  }
}

class Memory extends Equipment {
  constructor() {
    super();
    this.setName('Memory');
    this.price = 280;
  }
}


export { Cabinet, FloppyDisk, HardDrive, Memory };
```


