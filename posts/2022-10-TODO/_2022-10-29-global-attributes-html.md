---
title: TIL global attributes in HTML
date: 2022-10-29
published: true
tags: ['html', 'js', 'badidea']
series: false
canonical_url: false
description: "Warning: The use of event handler content attributes is discouraged. The mix of HTML and JavaScript often produces unmaintainable code, and the execution of event handler attributes may also be blocked by content security policies."
layout: layouts/post.njk
---


I saw this [hack here for CSS](https://javascript.plainenglish.io/simple-css-hack-to-reduce-page-load-time-366f7aaaa3be), which I can't say I recommend.


```html
<link rel="stylesheet" href="/path/to/my.css" media="print" onload="this.media='all'">
```

What was curious to me was the `onload` attribute.

I've seen it done before, mostly for hacking.

Like, `<img src="#" onerror="runMaliciousCode()">`

Before I share what this is:

> Warning: The use of event handler content attributes is discouraged. The mix of HTML and JavaScript often produces unmaintainable code, and the execution of event handler attributes may also be blocked by content security policies.

via [the MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes#event_handler_attributes)





So now that you know not to use them, let's dig deeper!

These are called [Global Attributes](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes), and there's actually a whole bunch of them!

Not all of them work for every HTML element.

```
The event handler attributes:

onabort, onautocomplete, onautocompleteerror, onblur, oncancel, oncanplay, oncanplaythrough, onchange, onclick, onclose, oncontextmenu, oncuechange, ondblclick, ondrag, ondragend, ondragenter, ondragleave, ondragover, ondragstart, ondrop, ondurationchange, onemptied, onended, onerror, onfocus, oninput, oninvalid, onkeydown, onkeypress, onkeyup, onload, onloadeddata, onloadedmetadata, onloadstart, onmousedown, onmouseenter, onmouseleave, onmousemove, onmouseout, onmouseover, onmouseup, onmousewheel, onpause, onplay, onplaying, onprogress, onratechange, onreset, onresize, onscroll, onseeked, onseeking, onselect, onshow, onsort, onstalled, onsubmit, onsuspend, ontimeupdate, ontoggle, onvolumechange, onwaiting.
```

Now you know!

