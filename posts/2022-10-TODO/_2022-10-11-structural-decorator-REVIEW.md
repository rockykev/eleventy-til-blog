---
title: TIL
date: 2022-10-11
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---



### Decorator	Add responsibilities to objects dynamically
**Definition**
An example of a decorator is security management where business objects are given additional access to privileged information depending on the privileges of the authenticated user. For example, an HR manager gets to work with an employee object that has appended (i.e. is decorated with) the employee's salary record so that salary information can be viewed.

Decorators provide flexibility to statically typed languages by allowing runtime changes as opposed to inheritance which takes place at compile time. JavaScript, however, is a dynamic language and the ability to extend an object at runtime is baked into the language itself.

For this reason, the Decorator pattern is less relevant to JavaScript developers. In JavaScript the Extend and Mixin patterns subsume the Decorator pattern.
https://www.dofactory.com/javascript/design-patterns/decorator

**Explain Like I'm 5**
Decorators are a structural design pattern that aim to promote code reuse. Similar to Mixins, they can be considered another viable alternative to object subclassing.

. A Lord of the Rings game could require constructors for Hobbit, Elf, Orc, Wizard, Mountain Giant, Stone Giant, and so on, but there could easily be hundreds of these. If we then factored in capabilities, imagine having to create subclasses for each combination of capability typeâe.g., HobbitWithRing, HobbitWithSword, HobbitWithRingAndSword, and so on. This isnât very practical and certainly isnât manageable when we factor in a growing number of different abilities.

The Decorator pattern isnât heavily tied to how objects are created but instead focuses on the problem of extending their functionality. Rather than just relying on prototypal inheritance, we work with a single base object and progressively add decorator objects that provide the additional capabilities. The idea is that rather than subclassing, we add (decorate) properties or methods to a base object so itâs a little more streamlined.
https://www.oreilly.com/library/view/learning-javascript-design/9781449334840/ch09s14.html



**Use-cases**

**Code example with the pattern**

```js
var User = function (name) {
    this.name = name;

    this.say = function () {
        console.log("User: " + this.name);
    };
}

var DecoratedUser = function (user, street, city) {
    this.user = user;
    this.name = user.name;  // ensures interface stays the same
    this.street = street;
    this.city = city;

    this.say = function () {
        console.log("Decorated User: " + this.name + ", " +
            this.street + ", " + this.city);
    };
}

// The 'Regular' version
var user = new User("Kelly");
user.say();

// The 'Decorated' Version
var decorated = new DecoratedUser(user, "Broadway", "New York");
decorated.say();
```
via https://www.dofactory.com/javascript/design-patterns/decorator


