---
title: TIL When to use Design Patterns
date: 2022-10-01
published: false
tags: ['']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---



1 - copy/paste the blog posts

2 - grab the book
-----------


## What are Design Patterns
A design pattern is like a template for your project. It uses certain conventions and you can expect a specific kind of behavior from it.

**You would use Design Patterns for the following reasons:**

1. Avoid reinventing the wheel - Frequently faced design problems already have a well-defined solution that is associated with a pattern.

2. Easily reused - Reusing patterns assists in preventing minor subtle issues that can cause major problems in the application development process. This also improves code readability for coders and architects familiar with the patterns.

3. Enables efficient communication - Patterns add to a developer’s vocabulary. This allows developers to communicate using well-known, well-understood names for software interactions, making communication faster.

**Some of the cons to Design Patterns:**

1. Increases Complexity - Novices may try to apply the pattern wherever they can think of, even in situations where simpler code would do just fine.

2. Reduced Relevance - In many cases, patterns just become kludges that gave the programming language the much-needed super-abilities it lacked then. As the language features, frameworks, and libraries evolved, there is no reason to use a few patterns anymore.

----------------------
### Creational Patterns
* Abstract- Factory	Creates an instance of several families of classes
* Builder	- Separates object construction from its representation
* Factory - Method	Creates an instance of several derived classes
* Prototype- A fully initialized instance to be copied or cloned
* Singleton- A class of which only a single instance can exist



### Structural Patterns
* Adapter	Match interfaces of different classes
* Bridge	Separates an object’s interface from its implementation
* Composite	A tree structure of simple and composite objects
* Decorator	Add responsibilities to objects dynamically
* Facade	A single class that represents an entire subsystem
* Flyweight	A fine-grained instance used for efficient sharing
* Proxy	An object representing another object



----------------

## Design patterns
* There are [23 official patterns from the book Design Patterns - Elements of Reusable Object-Oriented Software]()


---------------------------
UNDONE
UNDONE
UNDONE
UNDONE
UNDONE
UNDONE
UNDONE
---------------------------


-----------------------------
### Behavioral Patterns
* Chain of Resp.	A way of passing a request between a chain of objects
* Command	Encapsulate a command request as an object
* Interpreter	A way to include language elements in a program
* Iterator	Sequentially access the elements of a collection
* Mediator	Defines simplified communication between classes
* Memento	Capture and restore an object's internal state
* Observer	A way of notifying change to a number of classes
* State	Alter an object's behavior when its state changes
* Strategy	Encapsulates an algorithm inside a class
* Template Method	Defer the exact steps of an algorithm to a subclass
* Visitor	Defines a new operation to a class without change


### References
[4 Design Patterns You Should Know for Web Development: Observer, Singleton, Strategy, and Decorator](https://www.freecodecamp.org/news/4-design-patterns-to-use-in-web-development/)
https://www.dofactory.com/javascript/design-patterns
