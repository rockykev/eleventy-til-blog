---
title: TIL about the 11,000 line file
date: 2022-05-06
published: true
tags: ['devstory', 'cleancode', 'horror']
series: false
canonical_url: false
description: "At work, I discovered a core project file that is almost 4,000 lines of code. Code flowed from top to bottom. I thought this is what nightmares are made from."
layout: layouts/post.njk
---


On a freelance project, I remember a `functions.php` file that was over 2,400 lines long. It was the longest code I ever experience in a single file.

Then, at work, I discovered a core project file that is almost 4,000 lines of code. Code flowed from top to bottom. I thought this is what nightmares are made from.

But then I stumbled on this story.

> Oh, the horror.

> It was a single code file with over 11,000 lines of VBScript.

> Countless people had made changes to this file over the years. But they did not appear to be software developers. Their roles ranged from IT support to business analyst. I'm shocked at how little value the project was given considering how many employees used these forms each day.

> It looked like the entire file would execute through from top to bottom, although I never actually confirmed it. The code followed a rough pattern of fetch some data about the user, check if some conditions are met, then do some action which repeated a thousand or so times.

> There was no version control. The only context about code changes was in the bug tracker and in the code comments, though I learned the hard way to not trust those.

Read the full story here: [The project with a single 11,000-line code file](https://austinhenley.com/blog/11000lines.html)
