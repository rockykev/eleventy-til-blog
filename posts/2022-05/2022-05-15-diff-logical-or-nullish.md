---
title: TIL of the difference between || and ??
date: 2022-05-15
published: true
tags: ['javascript', 'boolean', 'stackoverflow']
series: false
canonical_url: false
description: "The difference between || or ??"
layout: layouts/post.njk
---

What is `||`? That's logical OR.

What is `??`? That's nullish coalescing.

Both operators are often used to provide a default value if the first one is missing.

The main difference:

`||` is checking ONLY for falsy.

`??` is checking for null or undefined.

The problem with `||`:

```js
console.log(12 || "not found") // 12
console.log(0  || "not found") // "not found"

console.log(12 ?? "not found") // 12
console.log(0  ?? "not found") // 0

/////////
console.log("jane" || "not found") // "jane"
console.log(""     || "not found") // "not found"

console.log("jane" ?? "not found") // "jane"
console.log(""     ?? "not found") // ""

/////////
console.log(true  || "not found") // true
console.log(false || "not found") // "not found"

console.log(true  ?? "not found") // true
console.log(false ?? "not found") // false
```

You can start to see where the `||` can become problematic, and you're always safer to use `??`.

[via DMeechan's Stack Overflow comment](https://stackoverflow.com/questions/61480993/when-should-i-use-nullish-coalescing-vs-logical-or/61481631#61481631)
