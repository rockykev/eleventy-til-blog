---
title: TIL a nice checklist/strikethrough animation
date: 2022-05-07
published: true
tags: ['codepen', 'css']
series: false
canonical_url: false
description: "Animating checkboxes by using the `:checked` pseudo-class. If it's true, then include other things (like a before/after psuedo-class with other effects.)"
layout: layouts/post.njk
---


Dynamic checklist where if you check something, it animates a strikethrough!

The way it works is that we use the checkbox input type together with the `:checked` pseudo-class. If it's true, then include other things (like a before/after psuedo-class with other effects.)


<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="zYRozPP" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/zYRozPP">
  checklist crossout</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>



To see how that works --
when checked...

```css
.checklist {
    padding: 50px;
    position: relative;
    background: #043b3e;
    border-top: 50px solid #03a2f4;
}
.checklist h2 {
    color: #f3f3f3;
    font-size: 25px;
    padding: 10px 0;
    margin-left: 10px;
    display: inline-block;
    border-bottom: 4px solid #f3f3f3;
}
.checklist label {
    position: relative;
    display: block;
    margin: 40px 0;
    color: #fff;
    font-size: 24px;
    cursor: pointer;
}
.checklist input[type="checkbox"] {
    -webkit-appearance: none;
}
.checklist i {
    position: absolute;
    top: 2px;
    display: inline-block;
    width: 25px;
    height: 25px;
    border: 2px solid #fff;
}
.checklist input[type="checkbox"]:checked ~ i {
    top: 1px;
    height: 15px;
    width: 25px;
    border-top: none;
    border-right: none;
    transform: rotate(-45deg);
}
.checklist span {
    position: relative;
    left: 40px;
    transition: 0.5s;
}
.checklist span:before {
    content: '';
    position: absolute;
    top: 50%;
    left: 0;
    width: 100%;
    height: 1px;
    background: #fff;
    transform: translateY(-50%) scaleX(0);
    transform-origin: left;
    transition: transform 0.5s;
}
.checklist input[type="checkbox"]:checked ~ span:before {
    transform: translateY(-50%) scaleX(1);
    transform-origin: right;
    transition: transform 0.5s;
}
.checklist input[type="checkbox"]:checked ~ span {
    color: #154e6b;
}
```
