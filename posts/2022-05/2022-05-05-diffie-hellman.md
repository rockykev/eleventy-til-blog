---
title: TIL what the Diffie-Hellman Key exchange is
date: 2022-05-05
published: true
tags: ['algorithm', 'security']
series: false
canonical_url: false
description: "SSH uses RSA to establish an initial secure, authenticated connection, then uses Diffe-Hellman to establish an ephemeral (ie: one time) key to establish forward-security."
layout: layouts/post.njk
---



I was always curious of the handshake method between a `id_rsa` file and a server.

> In summary, SSH uses RSA to establish an initial secure, authenticated connection (ie: the server trusts the key and the client must have the key in order to connect), and then uses Diffe-Hellman to establish an ephemeral (ie: one time) key to establish forward-security.

via [this Stack exchange comment](https://security.stackexchange.com/questions/76894/how-does-ssh-use-both-rsa-and-diffie-hellman#:~:text=In%20summary%2C%20SSH%20uses%20RSA,key%20to%20establish%20forward%2Dsecurity.)

What is the Diffie-Hellman algorithm?

a ELI5:
(taken from this excellent blog post: [Diffie-Hellman Key Exchange](https://medium.com/@siddharthsrivastava76/diffie-hellman-key-exchange-5bf37f7f3ea8))

You and your friend agree on a number to can send secret messages to each other without anyone else being able to decipher them.

Lets put a caveat: you and your friend cannot meet up in the park and exchange this number.

For an extra level of difficulty, all messages exchanged between you and your friend are being eavesdropped on by someone who doesn’t like you: Eve.

In other words, a message that goes through the public space, but can only be decoded by the two parties.

![](2022-05-05-diffie-hellman_0.png)

Using colors:

The two parties who wish to communicate are Alice and Bob. Alice and Bob come up with common paint, which is yellow in this case. They agree on this. An interceptor also knows this colour, but that doesn’t matter. This is called the shared parameter.

Alice and Bob then choose random secret colours independently. None of them knows the other’s secret colour, only they know their own secret colour. In the above diagram, Alice only knows orange (called Alice’s private key) and Bob only knows green (called Bob’s private key). In more technical terms, Alice doesn’t know Bob’s private key and Bob doesn’t know Alice’s private key. Orange and green are collectively called the private keys.

Alice and Bob combine common paint with their private keys. Alice combines yellow with orange to get a peach colour (called Alice’s public key) and Bob combines yellow with green to get sky blue (called Bob’s public key). It is assumed here that given peach and yellow, it is difficult to extract the exact shade of orange used by Alice and the same for Bob. In other words, given the public key and the shared parameters, it is difficult to extract the private key.

Alice and Bob then exchange their public keys with each other. Alice gives Bob her peach colour and Bob gives Alice his blue colour. They then combine each other’s public key with their own private key to get the shared-secret key. Alice adds Bob’s blue to her orange to get brown and Bob adds Alice’s peach to his green to also get brown. Here’s why it works: adding green to red gives yellow, and adding red to green instead gives the same yellow. Using the same principle, Alice and Bob have both agreed on a secret colour without a third party breaking their secret.

## A bit of history

In 1976, two cryptologists, Whitfield Diffie and Martin Hellman, published a paper that outlined a method for solving this key-exchange problem in their paper “New Directions in Cryptography”. This paper outlines a protocol such that two parties create a key together over an insecure network. An eavesdropper observing the creation of this key cannot recreate the key shared between the two parties. The protocol is named ‘Diffie Hellman Key Exchange’ or just ‘Diffie-Hellman’ in honour of its inventors.

And there you have it!

(taken from this excellent blog post: [Diffie-Hellman Key Exchange](https://medium.com/@siddharthsrivastava76/diffie-hellman-key-exchange-5bf37f7f3ea8))
