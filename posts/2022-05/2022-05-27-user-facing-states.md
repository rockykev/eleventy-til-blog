---
title: TIL User-facing states
date: 2022-05-27
published: true
tags: ['ui','forms']
series: false
canonical_url: false
description:
layout: layouts/post.njk
---

This is a really sweet post about user-facing states. [All the user-facing states](https://ericwbailey.design/writing/all-the-user-facing-states/)

Essentially, they're the various 'states' that a element can be in based on the situation.

How I personally see it being used is as a common 'language', rather than with code.

For example:
There is a feature where the user can move the icon into the trashcan.
I might as a designer the following:
* When a user hovers over the icon, does anything change?
* What about when it's being 'selected'?
* What does the 'drag' state look like?
* What does the 'dropped' state look like?


The author argues against validation as a state. But for completion:

**Validation States:**
* Valid
* Invalid
* Warning

![](2022-05-27-user-facing-states_0.png)

via [Bootstrap Form validation States](https://csharpexamples.com/bootstrap-form-validation-states/)

**The Actual States:**

[All the user-facing states](https://ericwbailey.design/writing/all-the-user-facing-states/)

Resting - The status of something before someone has interacted with it, or other content affects it. Oftentimes referred to as “Base” or “Default.”

Hovered - When someone places a pointing device over an element, but has not yet taken action on it.

Active - An intermediate state that communicates someone has taken action on an element, and that it is in the process of navigating to a destination, triggering logic, or transmitting data. Also known as “pressed.”

Focused - When someone selects an element via keyboard or voice command, but has not yet taken action on it.

Visited - Someone has previously visited the resource the element is set to navigate to.

Loading - An element is fetching data from another internal or external resource.

Loaded - An element has fetched data from another internal or external resource and indicates that retrieval is completed. Loaded state isn't always communicated after loading state is applied.

Disabled - An element has had its interactivity conditionally removed.

Hidden - An element has been completely removed. It cannot be visually seen or accessed via assistive technology.

Readonly - Indicates element content that can only be read, and not interacted with.

Enabled - An element has been Activated, which unlocks the ability of additional state to be applied to the element, the page or view the element is contained on, or the overall site or app.

Checked - An element is marked for sending as data to another internal or external resource. A Checked element can be Focused, but it's selected state persists after focus is moved.

Unchecked - A Checked element that is no longer marked as wanting to be sent as data to another internal or external resource.

Indeterminate - An Indeterminate element is triggered on a parent element when some, but not all children elements are placed in a Checked state (thanks to Kilian Valkhof for the suggestion).

Selected - An element has been chosen as one or more criteria for a task. A Selected element can be Focused, but it's selected state persists after focus is moved.

Deselected - A Selected element that has had its selection removed. Elements that can be Selected, but have not yet been are considered to be Resting.

Dragged - An element has been Selected and is being moved to another location.

Dropped - A Dragged element is placed in a new location

Collapsed - An element is hiding the bulk of its content.

Expanded - An element is showing all of its collapsed content.

Resizing - An element has its height or width increased or decreased.

Dirty - An editable element has been manipulated by someone on one or more occurrences.

Pristine - An editable element has yet to be manipulated by someone.

Saving - An indeterminate state. It is triggered when an editable element in a Dirty state, where someone signals that their changes should be captured. Once the changes are successfully captured it reverts back to a Pristine state.

Overflowing - A child element's visual content is partially obscured by a parent element that restricts its height or width (thanks to Dave Rupert).

Scrolling - A child element is panning horizontally or vertically


Playing - An element is actively presenting prerecorded media or animation (thanks to Austin Gil).

Paused - Prerecorded media or animation an element is Playing is temporarily halted (thanks to Austin Gil).

Stopped - Prerecorded media or animation an element is playing reverted back to its start position.

Sticky - An element attaches itself to the side of the viewport opposite the direction someone scrolls and does not move out of view along with its surrounding content.

Unstuck - A Sticky element is removed from the side of the viewport and moved back to its original position.

