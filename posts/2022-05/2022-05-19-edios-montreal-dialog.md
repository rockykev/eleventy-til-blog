---
title: TIL about Edios-Montreal's dialog system
date: 2022-05-19
published: true
tags: ['gamedev', 'tools']
series: false
canonical_url: false
description: "These are the top-level goals the team needed to create with Codex. String IDs needed to be easily readable. Game scripts needed to be playable fast. Pain-free iteration. Ensemble recording. Fast audio importing. Easily provide context and error-checking for localization."
layout: layouts/post.njk
---

One of Guardians of the Galaxy's selling points is the charismatic cast of characters who are members of the titular Guardians.

This doesn't just manifest in dialogue trees. During almost every moment of gameplay, the Guardians are bantering with each other, with the player character, with NPCs—it's a very talky game.

That was built using the Codex System.

These are the top-level goals the team needed to create with Codex.

* String IDs needed to be easily readable.
* Game scripts needed to be playable fast.
* Pain-free iteration
* Ensemble recording
* Fast audio importing
* Easily provide context and error-checking for localization.

## Solutions:

**text-to-speech audio file**
When writers entered lines into Codex, the tool would automatically generate a text-to-speech audio file with a name for the string that would be permanently affiliated with that line for the rest of the process.

**unified filenames for quick swappability**
File names would follow a uniform structure, so that anyone looking at "C02_4140_OS_CNV_SEK_Tumble" could parse what they were looking at.

To make sure those writers could make fast edits, Codex's designers made sure that lines of dialogue retained their string IDs, so changing them wouldn't break the build.

As the team moved beyond text-to-speech to scratch audio and then voice actor audio, the audio team could count on that filename always being the same.

**export formats**
The Codex was designed to export dialogue in different formats for different individuals in the recording booth. Dialogue could be exported as conventional scripts for actors, or as .CSV files for the recording engineers.


**Organization and audio management**
Producers prepping for the recording sessions could also filter scripts by character or actor, and in Codex engineers could tag "keepers" so everyone with access to the tool could identify the best audio takes.

Codex's engineers also created a system to let producers do meta-analysis of scenes when setting up a day's recording. They could identify how many actors were needed, count how many lines were on the recording docket, and combine those data points for very accurate predictions about how long a recording day could run.

**Localization**
There was also a system that to record different language performances for the game's localization. When working with recording producers from different parts of the globe, Anderson said that the most notable feedback she received was that the metadata writers could add to Codex was "the most helpful" element for foreign-language actors.


via [Behind Codex, the tool powering the dialogue of Marvel's Guardians of the Galaxy](https://www.gamedeveloper.com/gdc2022/behind-codex-the-tool-powering-the-dialogue-of-marvel-s-guardians-of-the-galaxy)
