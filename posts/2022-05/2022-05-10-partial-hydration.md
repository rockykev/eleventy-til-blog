---
title: TIL about Partial Hydration
date: 2022-05-10
published: true
tags: ['javascript', 'frameworks', 'ssr']
series: false
canonical_url: false
description: "Partial rehydration is an extension of the idea of progressive rehydration where individual pieces of a server-rendered application are 'booted up' over time. This contrasts with the approach of initializing the entire application at once."
layout: layouts/post.njk
---


One of the selling points of SSR (static Site rendering) is that the only thing a SSR generator ships to the user is a pure HTML file. It's significantly faster and a smaller package.

But what if you have something dynamic, like a website with a login where features change based on the role?

You'll need to either do it server-side, or what is called `partial hydration`. So this is based off of the concept of `hydration`.

> Hydration (or rehydration) is a technique that uses client-side JavaScript to convert static HTML pages into dynamic web pages by attaching event handlers to the HTML elements. The JavaScript can be delivered either through static hosting or server-side rendering.

[what is partial hydration and why is everyone talking about it?](https://dev.to/ajcwebdev/what-is-partial-hydration-and-why-is-everyone-talking-about-it-3k56)


So `Partial Hydration`... This is based off the Island Architecture... another keyword!

[The Island Architecture in a nutshell](https://jasonformat.com/islands-architecture/)

> The general idea of an “Islands” architecture is deceptively simple: render HTML pages on the server, and inject placeholders or slots around highly dynamic regions. These placeholders/slots contain the server-rendered HTML output from their corresponding widget. They denote regions that can then be "hydrated" on the client into small self-contained widgets, reusing their server-rendered initial HTML.

## Okay so what is it?

In The [Cost Of Client-side Rehydration (February 8, 2019)](https://addyosmani.com/blog/rehydration/), Addy Osmani wonders if the benefits of rehydration were outweighed by the Uncanny Valley that is created for users by painting pixels early.

To illustrate this point, he created the following graphic:

![](2022-05-10-partial-hydration_0.png)


Partial rehydration is an extension of the idea of progressive rehydration where individual pieces of a server-rendered application are “booted up” over time. This contrasts with the approach of initializing the entire application at once.

## Can we use it?

Some frameworks (like Astro) already have partial hydration set up. There's experiments on others.

But it's an interesting future!

References:
https://dev.to/ajcwebdev/what-is-partial-hydration-and-why-is-everyone-talking-about-it-3k56
https://betterprogramming.pub/the-future-of-micro-frontends-2f527f97d506
https://addyosmani.com/blog/rehydration/
