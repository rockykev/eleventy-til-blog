---
title: TIL you can make your cursor display images
date: 2022-05-13
published: true
tags: ['css', 'images']
series: false
canonical_url: false
description: "The cursor element allows you to use 'url()'. It's required to include a fallback keyword (like 'auto') in case the image is broken."
layout: layouts/post.njk
---

You can use CSS to make your cursor display images.

```css
.card-image-cursor {
  background-color: #D11A5A;
  cursor: url(https://stackdiary.com/tools/assets/img/tools/html-beautifier.svg), auto;
}
```

The cursor element allows you to use `url()`. [mdn](https://developer.mozilla.org/en-US/docs/Web/CSS/cursor)

It's required to include a fallback keyword (like `auto`) in case the image is broken.

So yes, you can totally make it into pizza if you want.

via [10 Useful CSS Tricks for Front-end Developers](https://stackdiary.com/useful-css-tricks/)
