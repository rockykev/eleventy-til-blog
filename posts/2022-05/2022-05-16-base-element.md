---
title: TIL of the base element
date: 2022-05-16
published: true
tags: ['html', 'mdn', 'links']
series: false
canonical_url: false
description: "base element adds the domain link to all relative links."
layout: layouts/post.njk
---

I opened up a HTML4 project from a decade ago. When I work on sites locally, my local tools use a custom domain `website.com.myhouse`, so I know for a fact that i'm not working on production. This weird HTML4 project kept routing everything to the PROD version, and i wasn't sure why.


That's when I learned about `<base>`.

What it does is define a 'default' for all of your href and src references.

```html
<head>
   <!-- base element -->
  <base href="https://www.website.com/" target="_blank">
</head>

<body>
<img src="images/image.png">
<a href="new-page.html">HTML base Tag</a>
</body>
```

Which becomes:
```html
<body>
<img src="https://www.website.com/images/image.png">
<a href="https://www.website.com/new-page.html">HTML base Tag</a>
</body>
```

The `<base>` tag must have either an href or a target attribute present, or both.

There can only be one single `<base>` element in a document, and it must be inside the `<head>` element.

[mdn](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base)
