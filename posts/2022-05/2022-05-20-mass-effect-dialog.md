---
title: TIL how Mass Effect did their Dialog Production
date: 2022-05-20
published: true
tags: ['gamedev', 'voiceover']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

via [Dialog Production for BioWare's Mass Effect: Lessons Learned and Future Plans](https://gdcvault.com/play/206/Dialog-Production-for-BioWare-s)

Overall, Writers are part of the design.

**Text-to-speech**
* Not used on Mass Effect
* Verifies that playback works
* Ballparks memory requirements
* Informs team of plot and context

**Scripts generated**
* Generated on per-character/conversation
* Sent to the Sound Stage VO Director
* Creates locallization/translates
* Any changes notifies VO Co-cordinator

The script also has a naming convention:
```
<LANGUAGE>_<CONVERSATION NAME>_<STRING ID>_<GENDER>
```

**QA Testing Procedure**
* Is the right character saying the right thing
* Is text the same as dialog
* Are the lips in sync
* Is the level of the dialog consistent with the scene
* Is the dialog being rendered correctly

**Re-record and pickups**
Why they happen:

* Change in game-design
* Wrong interpretation
* Wrong timing flow
* Tech error
* Notify location
