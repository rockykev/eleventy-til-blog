---
title: TIL the meta refresh redirect method
date: 2022-05-17
published: true
tags: ['html', 'link']
series: false
canonical_url: false
description: "A Meta refresh redirect is a client-side redirect. Unlike 301 and 302 redirects that happen on the web server, a meta refresh redirect instructs the web browser to go to a different web page after a specified time span"
layout: layouts/post.njk
---

There's a pure HTML way to refresh/redirect your user.

> A Meta refresh redirect is a client-side redirect. Unlike 301 and 302 redirects that happen on the web server, a meta refresh redirect instructs the web browser to go to a different web page after a specified time span.

```
<head>
  <meta http-equiv="refresh" content="4; URL='https://ahrefs.com/blog/301-redirects/'" />
</head>
```

That `4` in the `content` attribute represents 4 seconds.

## Why you shouldn't use it:

* meta refresh redirects are quite often used by spammers to fool search engines, so if you use this option quite often, search engines may consider your site being spam and remove it from their index;

* when used for redirection, meta refresh tag does not pass much or any link juice.

The [MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta#attr-http-equiv)

via [What is 'meta refresh redirect' and why is it considered a critical issue?](https://help.ahrefs.com/en/articles/2433739-what-is-meta-refresh-redirect-and-why-is-it-considered-a-critical-issue)
