---
title: TIL blogpost - training new hires
date: 2022-05-30
published: true
tags: ['retrospective', 'blog', 'training', 'newhire']
series: false
canonical_url: false
description: "A few months ago, I shifted from Wizbang Codemonkey to Lead Cat Wrangler (non-official titles obviously). My team went from six devs to thirteen in less than a year. And I would be in charge of managing a brand new team, as WELL as all the new hires. The Peter Principle is always a concern."
layout: layouts/post.njk
---

See the post here: https://dev.to/rockykev/5-principles-i-followed-to-onboard-new-devs-and-keep-my-sanity-a-retrospect-38i7

<hr>

It's officially been more than a month since [new hires joined my team](https://dev.to/rockykev/what-new-hires-think-about-3i99), and I'm proud to say they've been pretty successfully onboarded.

A few months ago, I shifted from **Wizbang Codemonkey** to **Lead Cat Wrangler** (non-official titles obviously). My team went from six devs to thirteen in less than a year. And I would be in charge of managing a brand new team, as WELL as all the new hires. The [Peter Principle](https://en.wikipedia.org/wiki/Peter_principle) is always a concern. *(tl;dr - you get promoted up and up until you fail.)*

The business went, "Like your new team? Btw, here are 4 new hires. A junior, 2 mid-levels, and a senior. Okay byeeeeeee!"

How does this feel? Jim Gaffigan said it best.

![Imagine you're drowning and someone hands you a baby](https://pbs.twimg.com/media/DXsbmBeW4AEB_RL.jpg)

It took me a while to see this perspective. As a developer, in a nutshell - our job is to solve problems, but with code. We create a set of processes (a system) and run down the list. We pivot as you discover new problems. We keep working until the hit the finish line.

4 new hires at multiple levels of experience? This is just a new problem! Life is defined by the challenges you face. And I got this! 💪

This post will cover the 5 Principles I followed. I hope it's educational. And if 10... 20... 100 devs are thrown at me, this process continues to scale.

## What do new hires think about?

Tech interviews are a 💩-show. So no surprise that new hires have a lot of unknowns. For example: Where do I go to get questions answered? Who is this person? What is this tech stack? What is this in-house term? Am I doing this right?

I wrote the full list in my post: [What New Hires think about](https://dev.to/rockykev/what-new-hires-think-about-3i99)

As a **Lead Cat Wrangler** and a person in power (I can choose the toppings for the lunch pizza!) - do I continue contributing to the 'Sink-or-Swim' methodology that plagues many companies in the tech industry?

Or do I [continue expanding on the dope culture](https://dev.to/rockykev/these-6-rules-created-a-really-dope-culture-at-my-job-5fj5) that I want through onboarding, nurturing, and growth?

## The Principles of managing new hires successfully

In the book "Principles", the author Ray Dalio, lays down a framework based on the unique company culture he developed while running Bridgewater Associates.

I like the term **"Principles"**. These aren't facts or laws. They're systems that with practice, can lead to success. They might require some rejiggering from company to company. But they can fit into any training system. For example, [the burpee](https://www.artofmanliness.com/health-fitness/fitness/the-burpee-the-one-exercise-to-rule-them-all/) is considered one of the best exercises to incorporate. Use it separately, put it in a routine, or remix it into some P90x, and it still works!

This post is a retrospect of the principles I followed to achieve the following:
* Train four new hires (junior, 2 mid-level, and a senior dev)
* Build up a new team with some new rituals
* Manage chaos and new business objectives
* Pretend like I'm in control and not secretly wanting to hide under my desk until the pain stops
* Avoid hating programming (like what [happened 15+ years ago](https://dev.to/rockykev/how-i-became-a-developer-in-about-15-years-552k))

![Sarah Scribble's webcomic - internal screaming](2022-05-30-blogpost-new-hires_0.png)
Via [Sarah Andersen](https://twitter.com/sarahcandersen/status/826815552651935744)

I'm fortunate enough to have a background in a lot of different companies/onboarding styles, and working in the online education space. So these principles come from theories in higher education, executive coaching, and personal development.

## Principle 1: Eliminate Bottlenecks, Including You

As **Wizbang Codemonkey**, I was thrown into the sun and told to come back with fire. (Call me **Prometheus** because I bring back the 🔥🔥🔥)

As a new **Lead Cat Wrangler**, I can't solve problems, manage every new problem, AND teach new hires. The industry is paved with the bones of burned-out developers who think only they can solve everything.

I had to trust people to solve problems without me.

You might say, "But only I can solve that problem!"

Why is that?

To successfully cat wrangle, I need to create processes where I can offload work quickly and trust that the developer in charge has everything they need to close that loop.

Sakichi Toyoda, the founder of Toyota Motor Company and the father of Japanese Industrialization, created the [5 Whys system for Root Cause Analysis](https://jeffmcneill.com/sakichi-toyoda-and-the-five-whys-root-cause-analysis/). You keep asking 'why' the problem even occurred.

If I'm the only one who can solve a specific problem, then what needs to happen where I'm not the bottleneck?

**As a dev example:** if you modify one data point, and it creates side effects somewhere else? Your system is too tightly coupled! If I get sick for two days, I should not crash the system.

One solution is to provide enough documentation and code examples to get progress going. (Check out Principle 3 & 4)

Other times, the solution is to isolate the hard parts. Break tickets into pieces - some for new hires, and some for specialists.

Additionally, trust goes both ways. Code may not be how you would have solved it. But then you're being a dependency again, which is a recipe for disaster. **Perfect is the enemy of the good**. (And you can ALWAYS make refactor tickets to clean things up in the future and make it part of your todos. Check out [How to convince your boss to refactor](https://philippe.bourgau.net/how-to-convince-your-business-to-sponsor-a-large-scale-refactoring/).)

**FURTHER READING**
* [Beware the One-person team](https://www.effectiveengineer.com/blog/beware-the-one-person-team) via EffectiveEngineer
* [The Reality of Dev Burnout](https://betterprogramming.pub/falling-down-the-reality-of-developer-burnout-59c9079446ef) via BetterProgramming
* [Decouple your code](https://mindsers.blog/post/decouple-your-code/) via Mindsers Blog
* [The Goal](https://en.wikipedia.org/wiki/The_Goal_(novel)) and [The Phoenix project](https://www.goodreads.com/en/book/show/17255186-the-phoenix-project) (Book recommendations)


## Principles 2: Organize or Die ☠️

The rules of [Maker Time Vs Manager Time](https://fs.blog/maker-vs-manager/) are pretty simple. Managers do work by going into meetings. Makers need uninterrupted time to focus. A quick "hey you busy?" can cost a developer [23 minutes and 15 seconds just to get back to where they left off](https://www.washingtonpost.com/news/inspired-life/wp/2015/06/01/interruptions-at-work-can-cost-you-up-to-6-hours-a-day-heres-how-to-avoid-them/) (*But whos counting amirite?*)

![maker vs manager](2022-05-30-blogpost-new-hires_1.png)
(image via https://blog.doist.com/makers-schedule-remote-work/)

As **Wizbang Codemonkey**, I can do long uninterrupted periods to focus on solving a technical issue.

As a new **Lead Cat Wrangler**, I get emails and DMs from project managers, all day ev'ry day.

Looks like **Maker Time** no longer apply to me. :-(

I approached this by:

1) **Still have a maker mindset.** I still timebox. Nobody talks to me during these hours.
2) **Dedicated times for support.** Treat it like college Open Office hours. I don't respond immediately unless I have to.
3) **Relying on checklists**. Remember when ego-centric surgeons refused to use checklists, but the [ones that did reduced death by 40%?](https://www.hsph.harvard.edu/news/magazine/fall08checklist/)
4) **Never trusting my memory.** Everything becomes a note to review or a to-do to take action on. What gets written gets done.

As new business expectations occurred, I moved my calendar around.

As the new hires needed less support, I decreased my 'helping' time.

As new patterns arose, I updated my checklist.

If I ever dropped the ball, I make a mini-retrospective of why that happened and generate plans to mitigate that, and focus on continuous improvement.

This system isn't perfect. It continues to evolve.

**FURTHER READING**
* [How Timeboxing works](https://hbr.org/2018/12/how-timeboxing-works-and-why-it-will-make-you-more-productive) and [Stop Letting email control your workday](https://hbr.org/2017/09/stop-letting-email-control-your-work-day) by Haravard Business Review
* How to organize and get things done [With Workflowy](https://ericaheinz.com/notes/how-i-use-workflowy-to-get-things-done/), [with Mindmaps](https://lifehacker.com/how-to-use-mind-maps-to-unleash-your-brains-creativity-1348869811), [With Notion](https://nira.com/notion-project-management/)
* [Kaizen Project Management](https://blog.kainexus.com/improvement-disciplines/kaizen/kaizen-program-management-the-fundamentals)  and [The Power of 1% gains](https://jamesclear.com/continuous-improvement)

## Principle 3: Provide many mechanisms for growth

Everyone learns differently. It's why we have so many [different teaching styles](https://resilienteducator.com/classroom-resources/5-types-of-classroom-teaching-styles/).

My team of devs are big on being learners. It's why I encourage my team to buy courses and dedicate time to studying at least once a month. For the managers who don't dedicate resources to studying: What if we train them and they leave? [What if you don't and they stayed?](https://www.peakindicators.com/blog/what-if-we-train-them-they-leave-what-if-we-don-t-they-stay)

### Empowerment through learning and growth

The 4 new hires are: a junior, 2 mid-levels, and a senior.

Imagine being in a classroom teaching Lego crafting of different skillsets.

* **To the Junior** -> "Good job! This combination of random blocks does look like a house! Great job!"
* **To the senior** -> "Your Millenial Falcon is supposed to use Round 2x2 Bricks, not Round Corner 2x2s! What do they teach you in Lego school?!"

In other words, skill level affects how you teach new hires. In education, academic boredom or over-complexity can lead to bad learning outcomes. This leads to a **Goldilocks principle** of "just right". But "just right" doesn't scale!

> The added confusion:
> In the web dev industry, I have met a lot of devs with a decade in the industry, but [1-year of experience 10 times](https://www.linkedin.com/pulse/do-you-have-10-years-experience-just-1-year-repeated-times-mackay/). You know, the ones who built websites since the late 2000s but still use `floats`. The ones who never went deep in their architecture and still zip files as source control. The ones who gave my company bad reviews on Glassdoor because they refused to continue their education and Bootcamp students have more modern development skills than they do. And no IM NOT BITTER THANKS FOR ASKING.

Additionally - job titles such as **junior, mid-level**, and **senior** are kinda meaningless to training.

In the book [Pragmatic Programmers](https://pragprog.com/titles/tpp20/the-pragmatic-programmer-20th-anniversary-edition/), the authors Andrew Hunt and David Thomas reuse the [Dreyfus Model](https://www.bumc.bu.edu/facdev-medicine/files/2012/03/Dreyfus-skill-level.pdf) for how developers at different stages of their skillset prefer to learn.


**Stage 1 - Novices:**

ACTION: *Novices need recipes.*

Tell them how to do things. Novices are very concerned about succeeding. They don't want to learn everything, they want to accomplish their immediate goal.

**Stage 2 - Advanced Beginners:**

ACTION: *Advanced beginners don't want the big picture.*

They have some confidence to do things on their own and can start using the advice in the correct context. But they don't want lengthy theories or be spoon-fed the basics again.

![The Dreyfus Model](https://moleseyhill.com/images/dreyfus-model.png) (Via [ResearchGate](https://www.researchgate.net/figure/Model-of-skill-acquisition-Novice-to-expert-Dreyfus-Dreyfus-1988_fig2_296964846))

**Stage 3 - Competents:**

ACTION: *Competents can troubleshoot.*

They can troubleshoot problems on their own, and figure out solutions to modern problems. They are more deliberate about their planning. They're seen as 'having initiative' and 'being resourceful', and can mentor earlier stages without pulling bandwidth from experts.

**Stage 4 - Proficients:**

ACTION: *Proficient practitioners can self-correct.*

They need the big picture and will seek out and understand the entire framework. They read case studies/projects/patterns and can learn effectively from history, even if they didn't participate in it firsthand. They are very frustrated by oversimplified information.

They know how to use maxims: "Test everything that can possibly break."

Novices see this as a recipe and go, "What do I test? All the setters/getters? All the return statements?" and test everything. Proficient practitioners know to only test the core components that can cause an issue.

**Stage 5 - Expert:**

ACTION: *Experts work from intuition.*

They are the primary source of information. They write articles and books. They can diagram an application and go a mile deep into its architecture. Statistically speaking, roughly 1-5% of the population.

----

Using that framework, our tasks are written in a way where tickets are grouped into one of those categories. It allows us to provide meaningful work during the different stages of learning.

### Collaborative learning and growth

Be mindful. Be empathetic. Remember their learning style. Have an open-door policy to allow them to ask questions, no matter how dumb it sounds.

![Mathematically, 10,000 people hears about that new thing every day](https://imgs.xkcd.com/comics/ten_thousand.png)

For learning to scale, my team has these support systems.

**Support System 1:** We have chat channels specifically for asking questions. Even the more experienced devs ask questions, to facilitate an environment where we're all growing.

**Support System 2:** We have a collective knowledge base that anyone can edit.

**Support System 3:** We modify learning based on the needs of the new hire.

* Some learn with peer programming/shadowing.
* Some learn with pseudocode.
* Some learn by giving hints. Give them the boundaries.
* Some learn by pointing to documentation & example code.
* Some learn by building a separate project using an online course.
* Some learn by teaching it to others.

No one-size-fits-all. And there are different modes of learning depending on the task.

For example, using myself:
With design work, pseudocode all the way! But please let me shadow you when we're doing backend database work. And just give me documentation if I'm working on APIs. DIfferent modes for different tasks.

**Support System 4:** We spend a lot of time course-correcting

At the end of the day -- everything ends up in Code Review.

There's ego in coding. Not everyone takes feedback well at all. New hires are often following the patterns they had in their prior life and in unfamiliar territory, and defend themselves of why they chose that and why you're wrong.

At my company, we have the luxury of time. New hires get used to LONG code review times. Some PRs can sit in code review for a week. It's a point of frustration for new hires who come from website firms where you can quick-and-dirty `!important` everything and ship that code.

Not here! We will take the time to do it right. And we'll be extra supportive and polite about it. (And if the ticket is time-sensitive, is it a good ticket for new hires?)

**FURTHER READING**
* [How to make Good Code Reviews even better](https://stackoverflow.blog/2019/09/30/how-to-make-good-code-reviews-better/)
* [Learning How to Learn](https://www.goodreads.com/book/show/36647421-learning-how-to-learn) (Book Recommendation)
* [Pragmatic Programmers](https://pragprog.com/titles/tpp20/the-pragmatic-programmer-20th-anniversary-edition/), [The Mythical Man-month](https://en.wikipedia.org/wiki/The_Mythical_Man-Month), and [Peopleware: Productive Projects and Teams](https://en.wikipedia.org/wiki/Peopleware:_Productive_Projects_and_Teams) (Book Recommendation)


## Principle 4: Providing Doable work

One of the fears of new hires is if they're doing something correctly. That paralysis slows down growth and makes PRs take 300% longer.

And if they aren't doing it correctly, the new hire's confidence is shattered! They feel miserable. They feel insecure. Imposter syndrome creeps, and they go into [The Valley of Despair](https://www.enr.com/articles/53456-a-guide-through-the-valley-of-despair).

![Emotional Cycle of Change, the Valley of Despair](2022-05-30-blogpost-new-hires_2.png)
Image via [TBD-stategies](https://tbd-strategies.com/the-valley-of-despair-in-the-change-cycle/)

Creating "Doable work" means the following:

* Tasks that are within their skillset
* Tasks that are meaningful, that feel like real contributions. (I was given 10 'fix the typo' tasks and I wanted to crawl inside a shark.)
* Tasks that have a well-defined scope of expectations
* Tasks with a history of success


### Tagging tasks as "Good First Issue"
One solution that the Open source community created is tagging tickets as `Good First Issue`. The issue isn't easy. But it's a simple task to allow someone new to be able to learn the system, the culture, and the feedback.

In these specially tagged tickets, the scope is well defined, and there's also a history of success. Included in the task are completed PRs that do the same/something similar to reference for help. A new hire can review the PR and look at the feedback and comments, and attempt to understand how and why.

I also encourage more seasoned developers to LOOK and tag opportunities that can be good for first-timers. Great developers know when to solve a problem themselves vs allowing others to shine!

Overall, these `Good First Issue` tickets give new hires small wins. And small wins make big wins.

### Buffet-style selection for picking tasks

The common methodology at workplaces is to micro-manage your new hires until they ~~conform and become a goopy gray matter~~ meet your definition of success. You let them do some shadowing, force them into meetings and peer coding, and ask them questions like "Do you know how to copy/paste?" to "How can we use prototypical inheritance instead of creating thousands of functions?".

Not wanting to deal with that micro-management noise... my team elected to treat our tickets 'buffet-style'.

Overall, we provide dozens of tickets up for grabs.

* **Novice tickets** that have a clear step-by-step. Do X, then Y, then Z.
* **Advance Beginner tickets** that are a bit vaguer, but a lot of patterns to imitate. I treat these tickets like a puzzle game. Here's the end goal, and here are some hints!
* **Competent tickets** are free-flowing! I rely on the developer to tell ME how they plan to solve it.

**If the mid-levels want to tackle novice tickets?** Sure! If the junior wants to take **complex tickets**, I'll back them up, and give them an out if it's too much pressure.

**Concerned that the higher role devs will take novice tickets ?** You shouldn't be! The goal isn't to complete the ticket -- it's to get comfortable in the company. Just like in Final Fantasy, we need to get to 100% before we can use our [Limit Breaks/Overdrives](https://finalfantasy.fandom.com/wiki/Overdrive_(Final_Fantasy_X)).

**New tasks that have never been done before?** The solution: Create a safe sandbox, but with restrictions. Maybe the task needs to be broken into multiple tiers (some novice, some competent, some expert-level tickets) and you handle the complex ones. The tickets are still clearly defined, there's a scope to stay within, and define restrictions to avoid going too deep into the rabbit hole.

*Note: restrictions do create stress. But stress isn't the root of evil. There's [good stress and bad stress](https://www.summahealth.org/flourish/entries/2021/01/stress-management-how-to-tell-the-difference-between-good-and-bad-stress). Avoiding stress is a bit like helicopter parenting - it can cause more damage then it saves.*

**FURTHER READING**

* [Writing SMART goals](https://www.atlassian.com/blog/productivity/how-to-write-smart-goals)
* [Open-Source Good First Issues](https://goodfirstissue.dev/)
* [How to write a good answer](https://stackoverflow.com/help/how-to-answer) via StackOverflow
* [Drive: The Surprising Truth About What Motivates Us](https://www.danpink.com/books/drive/) (Book Recommendation)

## Principle 5: Frequent Check-ins

*(Note: This is the Principle I personally need the most work on. I'm data-driven, and would prefer to throw technology like [15Five](https://www.15five.com/) at this. But I can't.)*

All developers need to be empowered that we trust their ideas and feel ownership of the code. But developers are also very good at getting into rabbit holes and [can't see the forest from the trees](https://www.dictionary.com/browse/can-t-see-the-forest-for-the-trees).

Checking in too frequently and they won't feel confident in solving problems (Breaking Principle 1). If you let them go on their own too long, they can go into a spiral (breaking Principle 4)

Unfortunately, I don't have a direct answer here.

I have multiple touch-points to see how the new hire is doing.

We work remotely. Which means I can't just hover over their shoulder.

* **Check-in during Standups:** I'm guilty of using standup as a place to practice my comedy bits, very Michael Scott-esque. But new hires do open up and share their blockers!
* **I reach out asynchronously** through chat/email for a quick update.
* **I do 'temperature checks' by asking others.** "Is [X developer] doing okay?"

I constantly test my check-in methods. My check-ins are now a bit focused, with a lot of vagueness (oxymoron!). I ask these basic questions to break the ice:

1. **How is everything?** -> Some share their day. Others share their immediate feelings.
2. **What are you blocked by?** -> They share what they're working on. Or if they're stuck, they try to explain how.
3. **How can I help?** -> This simple powerful phrase empowers the new hire to think through exactly what they need.

**FURTHER READING**

* [21 Important Check-In Questions Your Team Wishes You'd Ask](https://www.polly.ai/blog/21-simple-yet-powerful-check-in-questions-and-how-to-ask-them) via Polly
* [Keeping Your People Connected And Productive While Working Remotely](https://www.15five.com/blog/keep-your-people-connected-while-working-remotely/) and [9 ways to giving effective employee feedback](https://www.15five.com/blog/9-ways-to-give-effective-employee-feedback/) via 15Five

## These Principles aren't just for new hires

Although these principles were geared toward scaling onboarding new hires, I discovered they made me a better developer/leader.

**Principle 1: Eliminate bottlenecks, including you** has allowed me to identify what's important to me and find as many ways to remove myself from being a dependency.

**Principle 2: Organize or die** forced me to choose my time wisely. Burnout is a real thing.

**Principle 3: Provide many mechanisms for growth** made me think about my learning styles. I'm figuring this out as a cat wrangler! I went through my own *Valley of Despair*, and have climbed out of it.

**Principle 4: Providing Doable work** forced me to think about clear expectations and scope in words. I messed up a LOT on this task, and my support team continued providing feedback.

**Principle 5: Frequent Check-ins** elevated me out of a *'thinking about myself'* to a *'think about the people who need me'*.

As developers, we're here to solve problems using systems and patterns.

And scaling new hires is just another problem to solve.
