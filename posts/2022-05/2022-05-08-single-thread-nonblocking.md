---
title: TIL what Single Threaded & Non-blocking means in JS
date: 2022-05-08
published: true
tags: ['javascript', 'advanced']
series: false
canonical_url: false
description: "Javascript is a single-threaded language. This means it has one call stack and one memory heap. Non-blocking means it'll read from top to bottom and throw anything async in the queue."
layout: layouts/post.njk
---

Javascript is a single-threaded language. This means it has one call stack and one memory heap. As expected, it executes code in order and must finish executing a piece code before moving onto the next. So if we print from 1to 5 it will execute every line one by one and can’t execute all prints at a single time.

![](2022-05-08-single-thread-nonblocking_0.png)

Let's use a real example:

```js
console.log("started");

const url = 'https://mdn.github.io/learning-area/javascript/oojs/json/superheroes.json';

fetch(url).then(() => {
      console.log("data fetched");
      // return response.json();
    })

console.log("ended");
```

In Javascript, the result will be:
```
started
ended
data fetched
```

What actually happens is for async calls, they get added to the queue and is revisited AFTER the stack is cleared (after the code goes from top to bottom).

In blocking languages, like C, or PHP, it will instead be:
```
started
(going out to fetch the data)
data fetched
ended
```

via [What does Single Threaded & Non-Blocking mean in JavaScript?](shttps://javascript.plainenglish.io/js-single-thread-non-blocking-explained-d5de012a33cf)
