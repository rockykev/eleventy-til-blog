---
title: TIL how to create adaptive SVGs
date: 2022-05-29
published: true
tags: ['svg','images', 'html']
series: false
canonical_url: false
description: "SVG markup is XML using an .svg file type extension which allows it to hold more dynamic types of code. SVG is able to scale up and down without quality loss, and can potentially be very small in size, they can also have embedded CSS, even embedded media queries."
layout: layouts/post.njk
---

Favicons are tiny icons on your browser. They're this guy:

![](https://storage.googleapis.com/web-dev-uploads/video/vS06HQ1YTsbMKSFTIPl2iogUQP73/SfYwSCUREc4XrS0ZAiZN.mp4)

See how the icon changes when darkmode is established?

You can do that with SVG files.

SVG files are amazing for this use-case.

SVG markup is XML using an .svg file type extension which allows it to hold more dynamic types of code.

SVG is able to scale up and down without quality loss, and can potentially be very small in size, they can also have embedded CSS, even embedded media queries. This means if an SVG favicon is used in a reader app or bookmarks bars, there's a chance the user could get a theme relevant (light or dark) icon due to dark preference styles provided inside the SVG.

You can open up the `svg` file in any text editor.

```html

<!-- start with the opening svg tag -->
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 447 428">

    <!-- styles, just like html -->
      <style>
        .favicon-stroke {
          stroke-width: 8px;
          stroke: #8929ff;
        }
        #skull-outline { fill: white }
        #eyes-and-nose, #hat-outline { fill: #8929ff }
        #hat-fill, #hat-bill { fill: #e662e6 }
      </style>


    <!-- g - group tag -->
    <g id="skull">
    <path id="skull-outline" class="favicon-stroke" stroke-linejoin="round" d="M19.62 188.39A166.62 166.62 0 0 1 186.24 21.77c115.25 0 166.61 74.59 166.61 166.62 0 1.83-.08 3.64-.13 5.46h.13s.68 175.09.68 178.65c0 30.11-16.26 41.67-36.32 41.67-12.7 0-35.22-3.93-36.22-32.69h-.2c-1 28.76-16.81 32.69-36.22 32.69-18 0-32.87-6.78-35.77-32.53-2.9 25.75-17.8 32.53-35.8 32.53-20.06 0-36.32-11.56-36.32-41.67 0-2.48.36-24.88.36-24.88A166.68 166.68 0 0 1 19.62 188.39Z" />
    <path id="eyes-and-nose" d="M180.77 205.76c0 23.64 12.84 42.81 28.68 42.81s28.68-19.17 28.68-42.81-12.84-42.82-28.68-42.82-28.68 19.17-28.68 42.82M275 205.76c0 23.64 12.84 42.81 28.68 42.81s28.68-19.17 28.68-42.81-12.84-42.82-28.68-42.82S275 182.11 275 205.76M264.51 276.85s-29.26 43.53-20.12 49.23c7.07 4.41 20.49-16.71 20.49-16.71s12.82 22.58 16.76 20c16.24-10.71-17.13-52.5-17.13-52.5"/>
    <path id="jawline" class="favicon-stroke" fill="none" stroke-linecap="round" d="M114.92 284.33c22.54-1 22 7 22 62.48" />
  </g>

</svg>
```

Just like a html file, you can add style tags. And that's how the magic works!

```html
  <style>
    @media (prefers-color-scheme: dark) {
      .favicon-stroke { stroke: #343a40 }
      #skull-outline { fill: #adb5bd }
      #hat-outline { fill: #343a40 }
      #eyes-and-nose { fill: #343a40 }
    }
  </style>
```


via [Building an adaptive favicon](https://web.dev/building-an-adaptive-favicon/)

[MDN on SVGs](https://developer.mozilla.org/en-US/docs/Web/SVG)
