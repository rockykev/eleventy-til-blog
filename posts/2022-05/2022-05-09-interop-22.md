---
title: TIL about Interop 22
date: 2022-05-09
published: true
tags: ['browser', 'standard']
series: false
canonical_url: false
description: "interoperability — when each underlying web technology is implemented in the same way in every browser"
layout: layouts/post.njk
---


In 2000, Microsoft (being Microsoft at the time), decided to implement Javascript the way they wanted to, which they called JScript. The browser experience was a nightmare.

Fast forward to today: we want to ensure that the web always works the same on any browser.

This is possible through `interoperability` — when each underlying web technology is implemented in the same way in every browser.

> In 2022, Apple, Bocoup, Google, Igalia, Microsoft, and Mozilla have come together to commit to improve interoperability in 15 key areas that will have the most impact on web developer experience, in a project called Interop 2022.

via https://webkit.org/blog/12288/working-together-on-interop-2022/

The Interlop 22 group will work on the following:

* Cascade Layers
* Color Spaces and Functions
* Containment
* Dialog Element
* Forms
* Scrolling
* Subgrid
* Typography and Encodings
* Viewport Units
* Web Compat

There are also five additional areas that have been adopted from Google and Microsoft’s “Compat 2021” effort:

* Aspect Ratio
* Flexbox
* Grid
* Sticky Positioning
* Transforms

[Full list here](https://github.com/web-platform-tests/interop-2022/issues?q=is%3Aissue+label%3Aproposal+sort%3Acreated-asc)

What does this mean?

Dev experience is about to get better.

via [Interop on Interop
](https://css-tricks.com/newsletter/293-interop-on-interop/)
