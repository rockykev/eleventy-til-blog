---
title: TIL how a url goes gets data from the internet
date: 2022-05-12
published: true
tags: ['browser', 'network']
series: false
canonical_url: false
description: "Ever wonder what happens when you type a url in a address bar in the browser?"
layout: layouts/post.njk
---

Ever wonder what happens when you type a url in a address bar in the browser? Follow this handy guide!

![](2022-05-12-url-to-internet_0.png)

In text form:

1. We'll start with a Url.
1. That url gets turned into a IP address via the DNS server.
1. We start a TCP connection to get approval. (SYN, SYN-ACK, ACK)
1. Once approved, we'll make a HTTP request to the world wide web.
1. That gets the header.
1. That gets the responses and payload.
1. The payload becomes data (HTML, CSS, JS)
1. The browser puts together the data (parsing HTML, making a dom tree, making a CSSOM tree, sprinking Javascript)
1. The browser adds any final touches.
1. Finally it gets to you.

via [Wassim Chegham](https://dev.to/wassimchegham/ever-wondered-what-happens-when-you-type-in-a-url-in-an-address-bar-in-a-browser-3dob)
