---
title: TIL these game design resources from Alex (@TychoBolt)
date: 2022-05-18
published: true
tags: ['twitter', 'gamedev']
series: false
canonical_url: false
description: "These two massive docs that contain a wealth of info."
layout: layouts/post.njk
---

The two links:
* https://docs.google.com/document/d/1fAlf2MwEFTwePwzbP3try1H0aYa9kpVBHPBkyIq-caY/edit

* https://docs.google.com/spreadsheets/d/1QhFyPfYSjHv7PjibGrslF3mNW_CIDXWv9o-iQgLbu1o/edit#gid=1632070911

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Made a 122 page Level Design document where I gather useful senior dev knowledge.<br>The goal was to gather up all this knowledge in one place. Both for learning and as reference.<br><br>Hopefully people find it useful and any feedback is welcome 😄 <br><br>Link:<a href="https://t.co/MwvsXEjd95">https://t.co/MwvsXEjd95</a> <a href="https://t.co/RvaCcpphEo">pic.twitter.com/RvaCcpphEo</a></p>&mdash; Alex (@TychoBolt) <a href="https://twitter.com/TychoBolt/status/1272578494543904771?ref_src=twsrc%5Etfw">June 15, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Since I collect various useful tools &amp; reading material, I&#39;m sharing them all in one sheet that I will continuously update. <br>Has sections on level design, accessibility, recommended reading, design tools, etc.<br>Hope this is useful! 😀<a href="https://t.co/CeXAn4FHt5">https://t.co/CeXAn4FHt5</a> <a href="https://t.co/TJ1EjQPJP2">pic.twitter.com/TJ1EjQPJP2</a></p>&mdash; Alex (@TychoBolt) <a href="https://twitter.com/TychoBolt/status/1182541355337289728?ref_src=twsrc%5Etfw">October 11, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


via [u/Dramakun's reddit question](https://www.reddit.com/r/gamedesign/comments/thxa8p/searching_for_notes_from_senior_game_designer/)
