---
title: TIL how to make numbers the same width and prevent shifting layout
date: 2022-05-02
published: true
tags: ['css', 'fonts', 'twitter', 'mdn']
series: false
canonical_url: false
description: "If your font supports it, you can use 'font-variant-numeric: tabular-nums'."
layout: layouts/post.njk
---

I give up at how font characters are different sizes. Outside of just changing font types, you're kinda screwed.

But there's hope with numbers. It's called `font-variant-numeric`

> i think i've shared this tip before but if your font supports it, you can use `font-variant-numeric: tabular-nums` in your CSS to keep your numerals the same width and prevent shifting layout.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">✨i think i&#39;ve shared this tip before but if your font supports it, you can use `font-variant-numeric: tabular-nums` in your CSS to keep your numerals the same width and prevent shifting layout. <a href="https://t.co/uqFDLXmI4T">pic.twitter.com/uqFDLXmI4T</a></p>&mdash; henry ✷ (@xdesro) <a href="https://twitter.com/xdesro/status/1508172487347036169?ref_src=twsrc%5Etfw">March 27, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


The `font-variant-numeric` CSS property controls the usage of alternate glyphs for numbers, fractions, and ordinal markers.

via [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/font-variant-numeric)
