---
title: TIL how to create a delay snippet for JS
date: 2022-05-04
published: true
tags: ['javascript', 'performance']
series: false
canonical_url: false
description: "We check for any user interaction. The 'eventList' array has eventlisteners, listening to those movements. Or we wait 5 seconds. Then Switch the 'delay' attribute with 'src' attribute. Run the 'cookie.js' code!"
layout: layouts/post.njk
---

I think a lot about Javascript performance and SEO, mostly CLS (Cumulative Layout Shift) in Core Web Vitals.

In other words: if elements on your page move around as the page loads, then you’ve got a high CLS. Which is bad.

Cookie warnings do that!

I found this really neat trick from [Implementing JavaScript Delay for Cookie Consent Banner](https://dariusz.wieckiewicz.org/en/implementing-js-delay-for-cookie-consent-banner/).


## How it's done:

First --
```html
<script delay="cookie.js" defer></script>
```

If you're going, `delay` isn't a real html attribute! YEP!

This next bit of code explains it:

```html
<script>
const autoLoadDuration = 5; //In Seconds
const eventList = ["keydown", "mousemove", "wheel", "touchmove", "touchstart", "touchend"];

const autoLoadTimeout = setTimeout(runScripts, autoLoadDuration * 1000);

eventList.forEach(function(event) {
    window.addEventListener(event, triggerScripts, { passive: true })
});

function triggerScripts() {
    runScripts();
    clearTimeout(autoLoadTimeout);
    eventList.forEach(function(event) {
         window.removeEventListener(event, triggerScripts, { passive: true });
    });
}

function runScripts() {
    document.querySelectorAll("script[delay]").forEach(function(scriptTag) {
        scriptTag.setAttribute("src", scriptTag.getAttribute("delay"));
    });
}
</script>
```

So to explain like I'm 5:

1a. We check for any user interaction. The `eventList` array has eventlisteners, listening to those movements.
1b. Or we wait 5 seconds.
2. If anything from step 1 is true, then...
3. Switch the `delay` attribute with `src` attribute.
4. Run the `cookie.js` code!

It's really elegant as it keeps Google happy, keeps the user happy, and protects you from online privacy act stuff.
