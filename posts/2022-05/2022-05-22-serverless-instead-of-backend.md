---
title: TIL Serverless Functions instead of a backend service
date: 2022-05-22
published: true
tags: ['serverless','netlify']
series: false
canonical_url: false
description: "Back in the day, you had to spin a server. Now you can just throw it on the cloud."
layout: layouts/post.njk
---

I was thinking about this problem before. What to do when you have a lot of APIs you have to call.

> For example, if the front end combines the data from several backend services and derives a few additional fields, a common approach is to add a proxy API so the frontend isn’t making multiple API calls and doing a bunch of business logic on the client side.

Via [Jason Lengstorf](https://css-tricks.com/serverless-functions-the-secret-to-ultra-productive-front-end-teams/)


Let’s look at two common approaches to handling this task:

Option 1: Build an Express app on Node to create the REST API

Option 2: Use serverless functions to create the REST API

## Option 1: Express App

Essentially you create a standalone app that's primary job is to act as middleware between your Frontend and that API endpoint(s) you are trying to call.

The issue Jason brings up is that Deployment comes with a lot of overhead:
* Ensuring that the deployment is live (usually with a healthcheck)
* The deployment can handle traffic spikes
* You manage your own deployment.


## Option 2: Serverless Functions

You create that same standalone app, but make it serverless functions.

Essentially functions as a service.


## The major differences

Option 1 is setting up a server to handle all of this. It's deploying and managing a 'server', and everything that comes with it.

It's both a pro and a con.

Option 2 is putting code in the cloud, and having it be used only when it's being called. It 'just works'.

The major con is that serverless function is part of the cloud. If you're already using a cloud service provider (Like AWS, Netlify, Heroku), then this is great!

I like serverless functions a lot.

