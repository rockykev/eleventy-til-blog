---
title: TIL using datalist for a pure HTML input suggestion
date: 2022-05-01
published: true
tags: ['html', 'input', 'mdn']
series: false
canonical_url: false
description: "Web developers do not use the power of 'datalists' enough. It combines the power of a 'selector', with the ability to type out your choice using the keyboard."
layout: layouts/post.njk
---

Web developers do not use the power of `datalists` enough.

It combines the power of a `selector`, with the ability to type out your choice using the keyboard.

```html
<label for="ice-cream-choice">Choose a flavor:</label>
<input list="ice-cream-flavors" id="ice-cream-choice" name="ice-cream-choice" />

<datalist id="ice-cream-flavors">
    <option value="Chocolate">
    <option value="Coconut">
    <option value="Mint">
    <option value="Strawberry">
    <option value="Vanilla">
</datalist>

```

Best of all, this is native! No [JS to auto-complete](https://www.w3schools.com/howto/howto_js_autocomplete.asp)!

**How-To:**
You bind the `input` and the `datalist` together.

Via the [MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist)

You can even bind colors!

```html
<input type="color" list="redColors">
<datalist id="redColors">
  <option value="#800000">
  <option value="#8B0000">
  <option value="#A52A2A">
  <option value="#DC143C">
</datalist>
```

via [5 HTML Tricks Nobody is Talking About](https://javascript.plainenglish.io/5-html-tricks-nobody-is-talking-about-a0480104fe19)
