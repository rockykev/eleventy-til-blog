---
title: TIL about Micro-frontends
date: 2022-05-21
published: true
tags: ['frameworks', 'javascript', 'webdev']
series: false
canonical_url: false
description: "Micro frontends are a new pattern where web application UIs (front ends) are composed from semi-independent fragments that can be built by different teams using different technologies."
layout: layouts/post.njk
---

Micro frontends are a new pattern where web application UIs (front ends) are composed from semi-independent fragments that can be built by different teams using different technologies. Micro-frontend architectures resemble back-end architectures where back ends are composed from semi-independent microservices.

via [The Strengths and Benefits of Micro Frontends](https://www.toptal.com/front-end/micro-frontends-strengths-benefits)

It's still pretty new, so there's no single dominant implementation and no clear “best” micro-frontend framework.

Using the Island Architecture concept, (more at [The Island Architecture in a nutshell](https://jasonformat.com/islands-architecture/)):


![](2022-05-21-micro-frontends_0.png)

It'll be three separate apps. You can build all three in Vue, each in it's own framework, or even one in JS, one in WebAssembly, and one in some weird Python-to-JS compiler.


The big unknown?

How will the micro-frameworks talk to each other?

[Luca Mezzalira's post](https://betterprogramming.pub/the-future-of-micro-frontends-2f527f97d506) has an idea:

In the mental model I created for designing micro-frontends, it’s encouraged to communicate between micro-frontends using a `publish-subscribe` pattern for enforcing the boundaries between micro-frontends, avoid or at least reduce the design-time coupling that leads to more autonomous teams

So this can be a event bus, or a separate application that watches for states.
