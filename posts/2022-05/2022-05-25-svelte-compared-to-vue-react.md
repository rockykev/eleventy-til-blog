---
title: TIL how Svelte is different fom Vue/React
date: 2022-05-25
published: true
tags: ['javascript','frameworks', 'svelte', 'vue', 'react']
series: false
canonical_url: false
description:
layout: layouts/post.njk
---

There's a joke in Web Dev that new frameworks pop up everytime someone sneezes. And from 2014-2022, it was kinda  a reality.

Well anyways, here's Svelte, which was started by Rich Harris and now maintained by a core group of developers.

And it's actually getting a lot of traction!

> Svelte has been adopted by a number of high-profile web companies including The New York Times, Apple, Spotify, Square, Yahoo, ByteDance, Rakuten, Bloomberg, Reuters, Ikea, Facebook, and Brave.

via [the Wiki](https://en.wikipedia.org/wiki/Svelte)

## What is it

Svelte offers a lightweight framework, with powerful features and syntactic sugar to make the developer's job easier. The main difference with Svelte is the library internal engine, because Svelte is primarily a compiler.

## Reactivity in JS Frameworks

Reactivity is what occurs when an event happening in our application (for example a button is clicked or a value is saved) triggers another action (update the display for example): a piece of code reacts accordingly and updates the DOM.

The reactivity of React and VueJS is based on a virtual DOM system. It is actually a JavaScript object that contributes to the internal functioning of these two libraries. Behind the scenes, React/Vue create a object representation of the entire page. Reactivity is done by doing diffs between the two versions, and update the REAL DOM after that.

```
// jsx
function HelloMessage(props) {
	return (
		<div className="greeting">
			Hello {props.name}
		</div>
	);
}

// js
// this is the object interpretation
function HelloMessage(props) {
	return React.createElement(
		'div',
		{ className: 'greeting' },
		'Hello ',
		props.name
	);
}
```
Rich Harris likes to provoke React/Vue a bit and remind them that they're not really reactive. [Virtual DOM is pure overhead](https://svelte.dev/blog/virtual-dom-is-pure-overhead)

Reactivity in Svelte is based on the assignment of variables, which means that defining a variable in Svelte already makes it reactive.

The added instructions simply say "each time an assignment occurs, calculate the dependencies or side effects, and update the DOM accordingly". Svelte being a compiler, it can calculate the side effects of each user action at the build phase, something its competitors working at runtime can't do.

## Runtime vs Compile Time

In React, Vue, Angular, etc... work is done at runtime. A simple "Hello World" application bundles the entire framework code. For React, it's kinda massive!

(You might think i'm 💩-ing on React. I'm not. I like React, and it solves a lot of problems. But I live in reality and data.)

Svelte only translates instructions into browser-optimized code during the build phase.

Svelte will only be the tool that can understand that code and generate standard browser-optimized JavaScript. Since it is a compiler, no unessential code will be embedded in the application. The final bundle will only contain what the developer has written following the Svelte syntax, translated by the compiler.


## So now what

Is this the future?

Uncertain.

I still think React is the defacto JS language in terms of popularity. I still find myself explaining things in React terms, because everyone just 'gets it'.

Vue takes all the best ideas of React, focuses on sticking with web fundamentals, and makes it work. It's been my preferred framework for years because it just feels more natural than writing JSX.

Svelte is the newer kid, that ignores the Virtual DOM and tries to be more purer. I'm curious of the hype.

They all have their own pros and cons. And it never hurts to learn all three!

But in the end, the real decision if we use it in my company will be made based on the community size/support.


REFERENCE:
https://dev.to/zenika/svelte-why-so-much-hype-2k61
