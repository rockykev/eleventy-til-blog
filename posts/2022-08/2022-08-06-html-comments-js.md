---
title: TIL using html comments in javascript
date: 2022-08-06
published: true
tags: ['html', 'comments', 'badidea']
series: false
canonical_url: false
description: "This is in the 'bad idea' area! But just a need little factoid!"
layout: layouts/post.njk
---


This is in the 'bad idea' area! But just a need little factoid!

```js
console.log(1); <!-- log 1 -->
<!-- above statement logs 1 -->
```

The 'why' is because browsers without JS would ignore script tags, and is a super ancient legacy behavior.

```
<script>
  <!--
    // browsers without JavaScript support think this is all in a big comment,
    // and so don't render this as plain text
    alert("Hi!");
  -->
</script>
```

[HTML comments work in JavaScript too](https://smitop.com/post/js-html-comments/)
