---
title: TIL prefers-color-scheme automatically
date: 2022-08-09
published: true
tags: ['css', 'automation']
series: false
canonical_url: false
description: "For swapping between light mode and dark mode, you don't need JS. Using the `prefers-color-scheme`, that will automatically run the design code based on the user's preference."
layout: layouts/post.njk
---


Although I'm a pretty heavy user of Javascript, I do hate using JS for everything. If all you have is a hammer, then everything is a nail.

For swapping between light mode and dark mode, you don't need JS. (Although you WILL need JS to save preferences into session/local storage.)

Using the `prefers-color-scheme`, that will automatically run the design code based on the user's preference.

```css

@media (prefers-color-scheme: dark) {
  .day.dark-scheme   { background:  #333; color: white; }
  .night.dark-scheme { background: black; color:  #ddd; }
}

@media (prefers-color-scheme: light) {
  .day.light-scheme   { background: white; color:  #555; }
  .night.light-scheme { background:  #eee; color: black; }
}
```


But also giving them choice using a simple `:checked` selector.

```scss
#darkModeCheckbox:checked {
    body {
        background: darkgrey;
        color: whitesmoke;
    }
}
```

[MDN - prefers-color-scheme](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme)
[5 things you don't need Javascript for](https://lexoral.com/blog/you-dont-need-js)
