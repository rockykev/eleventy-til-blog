---
title: TIL Accessibility in Devtools
date: 2022-08-07
published: true
tags: ['accessibility', 'devtools']
series: false
canonical_url: false
description: "You can enable the Accessibility Devtool (currently in experiments mode) to do screenreader testing."
layout: layouts/post.njk
---


Having done accessibility testing using a bunch of very expensive external tools, it's kind of a pain. The average devteam doesn't have access to these tools. Heck, my current team doesn't have access to these tools! So finding this sweet devtools testing solution is amazing!

You can enable the Accessibility Devtool (currently in experiments mode) to do screenreader testing.

![](2022-08-07-accessibility-devtools_0.png)

![](2022-08-07-accessibility-devtools_1.gif)

It's a toggle. It doesn't remove the DOM inspector. You can just switch between DOM and accessibility tree this way with one click. Good for debugging how accessible your page is.

via [grumd on reddit](https://www.reddit.com/r/webdev/comments/vo14m8/comment/ieankq7/?utm_source=reddit&utm_medium=web2x&context=3)


Reference: [Enable "Full Accessibility Tree View" right now in dev tools](https://www.reddit.com/r/webdev/comments/vo14m8/enable_full_accessibility_tree_view_right_now_in/)

-----------
