---
title: TIL Clippy for generating clip-path art
date: 2022-08-02
published: true
tags: ['css', 'generator']
series: false
canonical_url: false
description: "Clip-path allows you to define a shape that will determine what parts of your HTML element are shown. You can use it to create all different kinds of shapes!"
layout: layouts/post.njk
---


Clip-path allows you to define a shape that will determine what parts of your HTML element are shown.

You can use it to create all different kinds of shapes!

There's a tool for creating these shapes in a handy UI called [Clippy](https://bennettfeely.com/clippy/).

![](2022-08-02-generator-clippy_0.png)

[MDN - clip-path](https://developer.mozilla.org/en-US/docs/Web/CSS/clip-path)
