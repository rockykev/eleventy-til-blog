---
title: TIL Avoiding promise chains
date: 2022-08-29
published: true
tags: ['javascript', 'promises']
series: false
canonical_url: false
description: "Callbacks are functions that are executed when another function finishes. It can lead to callback hell, which instead, use a promise! But don't make promise chains"
layout: layouts/post.njk
---


Callbacks are functions that are executed when another function finishes.

For example:

1. Go get data from a website.
2. When the data is fetched, then run this 'callback function' to take that data and do something with it.

But now that you understand javascript callbacks you'll soon find yourself in nested "callback hell."

Instead, use a promise!

[MDN promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then)

```js
// simple example of a promise.
// If successful -> resolve argument
// If fail -> reject argument
const myPromise = new Promise(function(resolve, reject) {
    setTimeout(function() {
        if (Math.random() < 0.9) {
            return resolve('Hooray!');
        }
        return reject('Oh no!');
    }, 1000);
});

// If successful, move to then()
// if fail, then move to catch()
myPromise
    .then(function(data) {
        console.log('Success: ' + data);
    })
    .catch(function(err) {
        console.log('Error: ' + err);
    });


```

But the main thing I wanted to point out is avoiding promise chains!

```js
// The wrong way
getSomeData.then(data => {
    getSomeMoreData(data).then(newData => {
        getSomeRelatedData(newData => {
            console.log(newData);
        });
    });
});

// The right way

// version 1
getSomeData
    .then(data => {
        return getSomeMoreData(data);
    })
    .then(data => {
        return getSomeRelatedData(data);
    })
    .then(data => {
        console.log(data);
    });

// version 2 - ES6
getSomeData
    .then(data => getSomeMoreData(data))
    .then(data => getSomeRelatedData(data))
    .then(data => console.log(data));


// version 3 - the shortest way
getSomeData
    .then(getSomeMoreData)
    .then(getSomeRelatedData)
    .then(console.log);
```

Version 3 works because, in layman terms, successful data moves to the next `then` statement.

via [A Collection of JavaScript Tips Based on Common Areas of Confusion or Misunderstanding](https://dev.to/nas5w/a-collection-of-javascript-tips-based-on-common-areas-of-confusion-or-misunderstanding-42j5#avoid-the-nesting-antipattern-of-promise-chaining)
