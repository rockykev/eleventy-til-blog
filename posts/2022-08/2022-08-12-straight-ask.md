---
title: TIL about going straight into the ask
date: 2022-08-12
published: true
tags: ['management','remote', 'teamwork']
series: false
canonical_url: false
description: "Don't ask to ask, just ask. Attention is a limited resource. If you work remotely, it's even more important to just get straight to the point."
layout: layouts/post.njk
---

Attention is a limited resource. Studies even show that each interrupt can cost a developer 1.5 hours a day. [Via Reducing the effect of email interuption on employees whitepaper](https://jamesclear.com/wp-content/uploads/2015/02/email-multitasking-study.pdf)

That's great for a developer who can solely focus on a ticket.

But when you manage a team, you need to be around for support.

> RandomGuy: Any Javascript experts can help?

This is bad form, for several reasons. What the person is actually asking here is:

> RandomGuy: Any Javascript experts who can dedicate the time help look into this vague problem for me, that could either be something with my code, my IDE, my browser, my build tools? It might be something on local, staging, production?

Via [Don't ask to ask, just ask](https://dontasktoask.com/)
