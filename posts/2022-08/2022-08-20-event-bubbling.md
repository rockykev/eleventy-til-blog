---
title: TIL event bubbling
date: 2022-08-20
published: true
tags: ['javascript','codepen', 'clicks']
series: false
canonical_url: false
description: "Event bubbling is when an event is in an element inside another element, and both elements have registered a handle to that event. That cause collisions."
layout: layouts/post.njk
---

Event bubbling is when an event is in an element inside another element, and both elements have registered a handle to that event.

For example: You have a button. Clicking that button makes it say "Hello". But ou also have events attached to the parent that says "goodbye". Both will trigger!

You can actually see it here:

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="YqNWpm" data-user="karolis" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/karolis/pen/YqNWpm">
  Event bubbling and capturing</a> by Karolis (<a href="https://codepen.io/karolis">@karolis</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


This stuff can get kinda hairy, and you'll end up crating work-arounds for work-arounds.

Instead, create a single event listener.

```js
// Listen for clicks on the entire window
document.addEventListener('click', function (event) {

	// If the clicked element has the `.click-me` class, it's a match!
	if (event.target.matches('.click-me')) {
		// Do something...
	}

});
```
The method above is an example. It's pretty heavy-handed as it's looking at the whole page. But you can scope it to just the section you want.


via [Listening for events on multiple elements using JavaScript event delegation](https://gomakethings.com/listening-for-events-on-multiple-elements-using-javascript-event-delegation/)
