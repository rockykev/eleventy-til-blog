---
title: TIL about Leaflet, an open-source Google Maps alternative
date: 2022-08-21
published: true
tags: ['library','open-source']
series: false
canonical_url: false
description: "I love how flexible Google Maps is for developers. But to use it for commercial products, it's a bit expensive. I learned about Leaflet — an open-source JavaScript library for interactive maps."
layout: layouts/post.njk
---

I think very highly of Google Products. They push the internet forward.

But Google has a 'people' problem.

It's quite possible to get your entire Google Account flagged or banned, without no real human intervention. Like when [Official Destiny 2 videos got copyright strikes](https://www.playstationlifestyle.net/2022/03/22/destiny-2-youtube-copyright-strikes-takedowns-bungie/), or when [Terraria developer had his whole account disabled](https://arstechnica.com/gadgets/2021/02/terraria-developer-cancels-google-stadia-port-after-youtube-account-ban/), or when a [Dad Photographs Son for Doctor. Google Flags Him as Criminal and Notifies Police](https://tech.slashdot.org/story/22/08/21/2148215/dad-photographs-son-for-doctor-google-flags-him-as-criminal-notifies-police)

So I always try to find a backup to a Google service.

I love how flexible Google Maps is for developers.

But to use it for commercial products, it's a bit expensive.

I learned about [Leaflet — an open-source JavaScript library for interactive maps](https://leafletjs.com/index.html), recently. It's a really attractive alternative.
