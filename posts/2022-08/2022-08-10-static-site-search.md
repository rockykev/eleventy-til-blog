---
title: TIL Searching a static site
date: 2022-08-10
published: false
tags: ['static-sites', 'search']
series: false
canonical_url: false
description: "Without a database to query for it, there's no built in-search query. Create your own search query system. (with a fallback where it opens up something like Duck Duck Go)."
layout: layouts/post.njk
---


Static Site Generators are just pure vanilla HTML files. Without a database to query for it, there's no built in-search query.

One method is to use a third-party system, like with Algolia or ElasticSearch.

The other method is to create your own search query system. (with a fallback where it opens up something like Duck Duck Go).

This is more of a briefer, explaining how to create it 'metaphorically'.

For the full instructions, check it out here: [How to create a search page for a static website with vanilla JS](https://gomakethings.com/how-to-create-a-search-page-for-a-static-website-with-vanilla-js/)

The method works as follows:

## 1. Create a search index.

This is typically a JSON or JS object on the search page, that contains information about every page.

```js
let searchIndex = [
	{
		title: "My awesome article",
		date: "December 18, 2018",
		url: "https://gomakethings.com/my-awesome-article",
		content: "The full text of the content...",
		summary: "A short summary or preview of the content (can also be a clipped version of the first few sentences)..."
	},
	// More content...
];

```

Ideally, you want to generate this index dynamically. (Especially with a blog like mine where there's hundreds of posts)

## 2. Create the frontend search function

This is a simple form with a click event. It takes the query input and runs through it.

## 3. Searching for results

We do a few strategies for searching. This is a very basic foundation of how big search engines do it.

Strategy 1: lowercase and split.

We want to make the search query lowercase, and turn every word into an array.

That allows us to:
* properly compare each word without worrying about case
* Search for every word in the search

```
// BEFORE
"A big Red Dog"

// AFTER
["a", "big", "red", "dog"]
```

Strategy 2: Stop words

> Stop words are common words like ‘the’, ‘and’, ‘I’, etc. that are very frequent in text, and so don’t convey insights into the specific topic of a document. We can remove these stop words from the text in a given corpus to clean up the data, and identify words that are more rare and potentially more relevant to what we’re interested in.

via [https://pythonspot.com/nltk-stop-words/](https://pythonspot.com/nltk-stop-words/)

[A list of English Stopwords](https://gist.github.com/sebleier/554280)


Strategy 3: Rate the priority of the search

Now that your search query is ready for review, you now compare each query word with your Search Index.

If it finds the occurance, give it a +100 and add that Index key to the pile.


## 4. Displaying the result

Now that you have the search results returned in object form, you now get to share it with the user.


Share the count, Sort the priority, display it on the frontend.

If there's no results, display that instead.

## 5. Lastly, update the URL.


[How to update the URL of a page without causing a reload using vanilla JavaScript](https://gomakethings.com/how-to-update-the-url-of-a-page-without-causing-a-reload-using-vanilla-javascript/)
