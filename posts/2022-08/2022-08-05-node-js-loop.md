---
title: TIL the difference in the JS Event Loop for Browsers and Node
date: 2022-08-05
published: true
tags: ['javascript','architecture']
series: false
canonical_url: false
description: "It took me years to understand this. Someone saying that 'they use Javascript' could mean they use Javascript on the web, or Node, or a different Javascript environment, of which there are MANY!"
layout: layouts/post.njk
---


It took me years to understand this. Someone saying that "they use Javascript" could mean they use Javascript on the web, or Node, or a different Javascript environment, of which there are MANY!

This is a small explainer of the differences in how Javascript functions get fired.

## The Event loop in the browser

![](2022-08-05-node-js-loop_0.png)

Heap - It stores all the object reference and variables that we define in our function.

**Call Stack** - This holds our function calls.

* When we hit a function in our code, it gets added to the Call Stack as First In, Last Out (FILO).
* Functions that call other functions get added.
* Events with SetTimeout get

**Web API's** - Special API's are provided by browser which provides additional functionality over V8 engine.

**Queues** - There's two types of queues. The macrotask queue (sometimes called the callback quque) and the Microtask queue (sometimes called the job queue).

Macro-Task Queue (Callback quque) - This is the main queue.

* On initialization, the JavaScript engine will first check if there's any events in the Microtask Queue. It will NOT RUN any Macro-tasks until the Microtasks are empty.
* Then, it pulls off the first task in the macrotask queue and executes the callback handler.
* The Macrotask queue executes async functions like DOM events, Ajax calls, requestAnimationFrame, and setTimeout, setInterval.
* In other words, Script -> promise -> setTimeout

Microtask Queue (Job Queue) - This queue executes async functions which uses promises and has higher precedence.

* Using the Microtask queue allows more consistent ordering of tasks.
* If you keep adding microtasks, they get priority. So essentially, you can 'starve' the Macro Task Queue. For example, a promise that creates promises.

## The Event Loop in Node

![](2022-08-05-node-js-loop_1.png)

![](2022-08-05-node-js-loop_2.png)

In Node, it's a bit simplier.

**Event Queue** - This is where events get added while they wait to be processed.

* On completion of the Thread Pool, a callback function is issued and sent to the event queue.


**The Event Loop** - This is the main loop (main thread).

* The Event loop has a order of operations.

```
   ┌───────────────────────────┐
┌─>│           timers          │
│  └─────────────┬─────────────┘
│  ┌─────────────┴─────────────┐
│  │     pending callbacks     │
│  └─────────────┬─────────────┘
│  ┌─────────────┴─────────────┐
│  │       idle, prepare       │
│  └─────────────┬─────────────┘      ┌───────────────┐
│  ┌─────────────┴─────────────┐      │   incoming:   │
│  │           poll            │<─────┤  connections, │
│  └─────────────┬─────────────┘      │   data, etc.  │
│  ┌─────────────┴─────────────┐      └───────────────┘
│  │           check           │
│  └─────────────┬─────────────┘
│  ┌─────────────┴─────────────┐
└──┤      close callbacks      │
   └───────────────────────────┘
```

via [NodeJs guide](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/)

**Thread Pool** - This is where the more "expensive" tasks. This is also known as the Worker Pool.

* The thread pool is composed of 4 threads (by default) which delegates operations that are too heavy for the event loop. I/O operations, Opening and closing connections, setTimeouts...

* The [libuv](https://docs.libuv.org/en/v1.x/) maintains a pool of threads to perform these operations behind the scenes, without blocking the main thread.


[Difference between the Event Loop in Browser and Node Js?](https://dev.to/jasmin/difference-between-the-event-loop-in-browser-and-node-js-1113?utm_source=pocket_mylist)

[The Node.js Event Loop, Timers, and process.nextTick()](https://nodejs.org/en/docs/guides/event-loop-timers-and-nexttick/)
