---
title: TIL John Romero had 74 games and 3 startups fail before making Wolfenstein 3D
date: 2022-08-25
published: true
tags: ['inspiration','quotes']
series: false
canonical_url: false
description: "Before Wolfenstein 3D, John Romero had given himself something new to learn: random maze generation; joystick input; music."
layout: layouts/post.njk
---

You may have heard this quote:

> "I've missed more than 9,000 shots in my career. I've lost almost 300 games. Twenty-six times I've been trusted to take the game-winning shot and missed." ~ Michael Jordan


In the dev space, John Romero (and John Carmack) are legendary for their work on Doom and Wolfenstein 3D.

But it wasn't luck.

> Romero’s first job in games was at Origin Systems in 1987, where he was employed to port the post-apocalyptic RPG 2400 A.D. to the Commodore 64. But by that time, he’d already made 74 games and burned through three startup companies: Capitol Ideas Software, Inside Out Software and Ideas from the Deep.


74 games and three startups!

The next piece is incredibly important for anyone learning:

> With every game, Romero had given himself something new to learn: random maze generation; joystick input; music. Each release in those early, prolific years represented a step forward in the self-taught programmer’s solitary education.

via [The life and times of John Romero, gaming’s original rockstar – part 1: “That’s classified”](https://www.pcgamesn.com/Doom/john-romero-interview-part-1)


----
The whole article is gold actually.

> When id received their very first royalty cheque – for $10,400, half of what Keen made in its first month – they immediately bought new computers. The four young developers quit Softdisk two weeks later.

> id was formally incorporated on February 1, 1991, and would make 12 games that year. They had names like Shadow Nights, Rescue Rover, Dangerous Dave and the Haunted Mansion. Each took a couple of months to make – but the team got into the habit of developing two games simultaneously. The four founders never built prototypes, instead polishing as they went. They could handle it: that intense decade prior had placed them all in good stead for the working pace of id.


12 games in one year, all before Wolfenstein 3D put them on the map.
