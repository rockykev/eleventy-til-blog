---
title: TIL Microservice vs Monolith
date: 2022-08-24
published: true
tags: ['architecture','dev']
series: false
canonical_url: false
description: "Final Verdict: You can do way more damage setting up a microservice architecture incorrectly in the long run than a really bad monolith."
layout: layouts/post.njk
---

I've been debating over the past few months how to approach a big project at work.

The major consideration is if I go Microservice, or if I go Monolith.

What are Microservices?

> Microservices are pieces of software (often just isolated functions) that do a single, tiny, independent part of a bigger application. If your application were a big company, each employee would be a microservice, each playing their own specific and small role, working alongside but independently from their coworkers. This lets you make changes to individual microservices without affecting the rest of the application. In a big company replacing or retasking one employee in a large team would probably not affect the rest of the company all that much.

via [API vs. Microservices: A Beginners Guide to Understand]
(https://snipcart.com/blog/microservices-vs-api)

The benefits:
* Microservices shine when you have to distribute the work across teams with different release schedules, different techs maybe, different processes. [ref](https://www.reddit.com/r/ExperiencedDevs/comments/sha6oy/comment/hv9vnt2/?utm_source=reddit&utm_medium=web2x&context=3)

* You can quickly depreciate and remove unused parts. Netflix keep the old instance with the previous version running until it doesn't receive any calls, then it gets dismantled. [ref](https://www.reddit.com/r/ExperiencedDevs/comments/sha6oy/comment/hv1ixy3/?utm_source=reddit&utm_medium=web2x&context=3)

* If one microservice goes down, the damage is isolated to only that instance.

* Microservices aka separate processes provides forced boundaries between code. [ref](https://www.reddit.com/r/programming/comments/rhmo92/comment/hotd37j/?utm_source=reddit&utm_medium=web2x&context=3)

* Geographically dispersed teams; differences in security and auditing requirements across services; differences in technology (certain domains have better tooling in particular languages); and differences in the pace of release cycles are all examples of where a micro service architecture might be useful, and I’m sure there are many more use cases. [ref](https://www.reddit.com/r/programming/comments/q8p4su/comment/hgr7jgt/?utm_source=reddit&utm_medium=web2x&context=3)



As for monoliths:

* When you have a small dev team, focus on modular components run as a monolith, and you will save yourself, time, money, and many headaches. If it's built modular enough, you'll be able to microservice it later as needed. [ref](https://www.reddit.com/r/programming/comments/q8p4su/comment/hgqpp29/?utm_source=reddit&utm_medium=web2x&context=3)



Not much needs to be said that wasn't covered above.


## The end result

> Microservices exist to scale developers. Only when you face issues from too many devs working on the same repo are microservices a viable option. Until then, until you hit those problems you should use a monolith. [ref](https://www.reddit.com/r/programming/comments/rhmo92/comment/horomqx/?utm_source=reddit&utm_medium=web2x&context=3)

I landed on this as my answer.

You can do way more damage setting up a microservice architecture incorrectly in the long run than a really bad monolith.

