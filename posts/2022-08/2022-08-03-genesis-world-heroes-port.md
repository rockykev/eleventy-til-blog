---
title: TIL a junior dev who programmed a World Heroes Genesis by himself
date: 2022-08-03
published: true
tags: ['gamedev', 'history']
series: false
canonical_url: false
description: " Yeah, World Heroes (even the original) wasn't anything to write home about. I had to squeeze 82M into a 16M cartridge. The source code was in assembly with absolutely no documentation. There were no testers. There was no one to help me port the artwork."
layout: layouts/post.njk
---


World Heroes Arcade was originally released for the Neo Geo MVS arcade cabinet on July 28, 1992.

It was first ported by Sunsoft to the SNES in Japan on August 12, 1993, in North America in September 1993, and PAL regions in 1993. Later, it was ported to the Sega Mega Drive / Genesis by Sega Midwest Studio (then known as Sega Midwest Development Division[4]) exclusively in North America on August 16, 1994.

His name is Jim Reichert did that Genesis port.

The source code was originally in assembly with no documentation.

And he was a junior dev who wanted to make it.

> JR: Well, as I was only the developer of the game, I can't really answer the question as to why an American development house was asked to do the port. As for myself, I'd never really heard of the game, and I was simply happy to enter into the game industry at the time -- I wasn't about to ask probing political questions.

> It was funny; the original "wunderkind" who was supposed to do the port, a British guy named "Steve," turned out to be all talk. But before he "left," he managed to bilk Sega Midwest out of a fair amount of money (he got a car as part of his deal). Ultimately, I came in to restart the port from the ground up and had very little time to do it. Thankfully, another guy at Sega, whose name was John <something> [Walsh?], helped out with certain parts.

> It was actually quite a feat to get all of the 8 characters, plus the end boss, in the game -- with all of the animations. Back then, cartridge ROM was expensive!

> Yeah, World Heroes (even the original) wasn't anything to write home about. I had to squeeze 82M into a 16M cartridge. The source code was in assembly with absolutely no documentation. There were no testers. There was no one to help me port the artwork.


[wayback machine - The Interview](https://web.archive.org/web/20150415110512/https://gdri.smspower.org/wiki/index.php/Interview:Jim_Reichert)
