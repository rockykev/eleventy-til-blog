---
title: TIL Animista for generating CSS animations
date: 2022-08-27
published: true
tags: ['css','generator']
series: false
canonical_url: false
description: "Animista is one of the best CSS tools you can use for animations. It gives you a collection of pre-made animations that you can use in your CSS."
layout: layouts/post.njk
---

I love tools which generates code.

It's satisfying to create your own for learning purposes. But it's the same reason we all use a IDE like VSCode, instead of coding in VIM or by banging rocks together.

I love Animista because I can quickly generate a sweet animation quickly.

![](2022-08-27-animista-css_0.png)


> Animista is one of the best CSS tools you can use for animations. It gives you a collection of pre-made animations that you can use in your CSS. You can choose any type of animation you like and edit it as you want. Once you’re done, you can generate the CSS code for the animation and use it on your project code.


Check it out -> https://animista.net/
