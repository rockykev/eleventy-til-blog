---
title: TIL generating fancy waves for HTML
date: 2022-08-23
published: true
tags: ['generator','css', 'design']
series: false
canonical_url: false
description: "Get Waves is another amazing tool that will allow you to create SVG waves for your projects using CSS. It makes it much easier, you just choose the options, then the tool generates the right CSS code for your wave design."
layout: layouts/post.njk
---

Waves are hard.

![](2022-08-23-generator-fancy-waves_0.png)

> Get Waves is another amazing tool that will allow you to create SVG waves for your projects using CSS. It makes it much easier, you just choose the options, then the tool generates the right CSS code for your wave design.


I mentioned it in another post that it's easier for me to open up Photoshop than to use clip-path. So any tool to help me is a win.

Check it out -> https://getwaves.io/

[9 Awesome CSS Tools That You Should Use in 2022](https://blog.devgenius.io/9-awesome-css-tools-that-you-should-use-in-2022-2155eed03c4c)
