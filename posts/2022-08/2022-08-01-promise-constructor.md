---
title: TIL Promise Constructor edge-case
date: 2022-08-01
published: true
tags: ['javascript', 'async']
series: false
canonical_url: false
description: "The promise constructor has to do a few more workarounds in order to catch errors. Just use a then statement"
layout: layouts/post.njk
---



```js
// Creating a promise with the promise constructor
// this will also catch the error
const createdPromise = new Promise((resolve, reject) => {
  somePreviousPromise.then(result => {
    // do something with the result
    resolve(result);
  }).catch(reject);
});


// This is the same function, only cleaner
const createdPromise = somePreviousPromise.then(result => {
  // do something with result
  return result;
});
```

The promise constructor has to do a few more workarounds in order to catch errors.

Via [3 most common mistakes when using Promises in JavaScript](https://dev.to/mpodlasin/3-most-common-mistakes-when-using-promises-in-javascript-oab)
