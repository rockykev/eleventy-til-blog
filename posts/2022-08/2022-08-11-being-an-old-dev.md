---
title: TIL being a old developer
date: 2022-08-11
published: true
tags: ['old', 'devstory']
series: false
canonical_url: false
description: "Every inspirational, blue-sky, brainstorming meeting or demo session has a person that keeps disrupting the flow. A battle-hardened, highly experienced developer with lots of memories of how things failed in the past. A person who a lot of people look up to and whose input counts as important. The even bigger problem is that this grumpy old person lives inside my head."
layout: layouts/post.njk
---


> Every inspirational, blue-sky, brainstorming meeting or demo session has a person that keeps disrupting the flow. A battle-hardened, highly experienced developer with lots of memories of how things failed in the past. A person who a lot of people look up to and whose input counts as important. The even bigger problem is that this grumpy old person lives inside my head.


via [Shut up, old man…](https://christianheilmann.com/2021/06/16/shut-up-old-man/)


I think about how I'm at this stage, where I walk on top of a road of errors.

So new ideas tend to get a heap of skepticism.

And often, it's just closing your mouth, biting your tongue, and waiting because the idea could be great.
