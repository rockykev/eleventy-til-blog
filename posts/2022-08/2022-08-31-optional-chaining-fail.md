---
title: TIL Optional chaining broke his mom's site
date: 2022-08-31
published: true
tags: ['apple', 'javascript', 'horror']
series: false
canonical_url: false
description: "Modern browsers get all fancy new features. But what about still-working technology like 1st-gen iPads and old chromebooks that no longer get updates?"
layout: layouts/post.njk
---


Optional Chaining looks like this:

```js
// before - version 1
if (foo && foo.bar && foo.bar.baz) {
	foo.bar.baz();
}

// before - version 2
foo && foo.bar && foo.bar.baz && foo.bar.baz();

// after - with optional chaining
foo?.bar?.baz?.()

```

It's cleaner and tighter.

In the post, [Refactoring optional chaining into a large codebase: lessons learned](https://lea.verou.me/2020/06/refactoring-optional-chaining-into-a-large-codebase-lessons-learned/), the author goes deep into various ways to refactor and use this 'fancy new feature'.

Unfortunately, we have a new problem.

Modern browsers get all fancy new features. But what about still-working technology like 1st-gen iPads and old chromebooks that no longer get updates?

[The Optional Chaining Operator, “Modern” Browsers, and My Mom](https://blog.jim-nielsen.com/2022/a-web-for-all/) talks about how Jim Nielsen's mother was unable to use a website that had optional chaining.

> The Nielsens weren’t aware that Chrome OS devices have a limited shelf life. Google’s Auto Update policy guarantees software updates and security support for a certain number of years. The Chromebox, which the Nielsens purchased in 2014, was past its August 2019 expiration date.

> In the Nielsens’ case, the malfunctioning website uses code that requires more-recent versions of the Chrome browser. This is a convenience for developers, who don’t then have to write longer code compatible with older software. The assumption is that pretty much everyone coming to the site will have an updated browser.

via [Before You Buy a Chromebook, Check the Expiration Date](https://www.wsj.com/amp/articles/before-you-buy-a-chromebook-check-the-expiration-date-11646538322)

