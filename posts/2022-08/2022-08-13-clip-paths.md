---
title: TIL a sweet SVG clip-path generator
date: 2022-08-13
published: true
tags: ['svg','generators', 'webtools']
series: false
canonical_url: false
description: "The clip-path CSS property creates a clipping region that sets what part of an element should be shown. Parts that are inside the region are shown, while those outside are hidden."
layout: layouts/post.njk
---

I'll be honest. Clip-paths confuse me. Honestly, I get so puzzled about how to approach them I just end up opening up Photoshop and fixing the image to fit.

> The clip-path CSS property creates a clipping region that sets what part of an element should be shown. Parts that are inside the region are shown, while those outside are hidden.

[MDN doc](https://developer.mozilla.org/en-US/docs/Web/CSS/clip-path)


I always hate devs who go "Clip-paths is easy bruh. They're just coordinates on a 2d plane and..."

![](2022-08-13-clip-paths_0.png)

Is that readable to you?

Well, you're in luck! There's a GUI that does this for you!

Check it out here: https://bennettfeely.com/clippy/

Clip away, bruh!

via [clip-path : An Awesome CSS Property You Should Be Using](https://acesmndr.medium.com/clip-path-an-awesome-css-property-you-should-be-using-eb24316d779c)

and [Useful CSS tools and resources to boost your productivity](https://blog.devgenius.io/9-awesome-css-tools-that-you-should-use-in-2022-2155eed03c4c)
