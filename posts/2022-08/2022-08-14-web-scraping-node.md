---
title: TIL Web scraping with node
date: 2022-08-14
published: true
tags: ['node','webscraping']
series: false
canonical_url: false
description: "Fetch the data, traverse the dom, and return the data."
layout: layouts/post.njk
---


Web Scraping is fun exercise for any frontend developer. Some great use-cases are:

1. Crawl a social media platform and get a list of posts.
2. Visiting a bunch of store product pages and grabbing their prices.
3. Quickly getting the top news from a collection of news sites.

And you can do that with just some basic node scripts!

## How it works

1. You `fetch` the data (webpage) from a website.
2. You traverse the DOM to grab what you're looking for and return it.

Pretty simple right?

## Fetching the data

Fetching is straightforward. Just like fetching an API.

You can do it in a dozen different ways.
But for example:

```js
async function fetch_endpoint() {
	const resp = await fetch('https://www.reddit.com/r/programming.json');

	console.log(await resp.json());
}


fetch_endpoint();

```

## Traversing the dom

THe next bit is extracting the bit you want.

With regular expressions: (super hard btw)
```js
const htmlString = '<label>Username: John Doe</label>'
const result = htmlString.match(/<label>Username: (.+)<\/label>/)

console.log(result[1])
// John Doe
```

But preferably, you're using something like [Cheerio](https://www.scrapingbee.com/blog/cheerio-npm/) (jquery-like syntax) or a bit more advanced is [jsdom](https://github.com/jsdom/jsdom).


jsdom's implementation
```js

const fs = require('fs');
const got = require('got');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const vgmUrl= 'https://www.vgmusic.com/music/console/nintendo/nes';

got(vgmUrl).then(response => {
  const dom = new JSDOM(response.body);
  console.log(dom.window.document.querySelector('title').textContent);
}).catch(err => {
  console.log(err);
});

```


via [Web Scraping with JavaScript and NodeJS](https://www.scrapingbee.com/blog/web-scraping-javascript/) and [Web Scraping and Parsing HTML in Node.js with jsdom](https://www.twilio.com/blog/web-scraping-and-parsing-html-in-node-js-with-jsdom)
