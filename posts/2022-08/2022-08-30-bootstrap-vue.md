---
title: TIL Cherry-picking out of bootstrap-vue
date: 2022-08-30
published: true
tags: ['javascript', 'vue']
series: false
canonical_url: false
description: "If you are using a component library that uses ES modules like bootstrap-vue, you can cherry pick its components instead of just registering all of them globally."
layout: layouts/post.njk
---

I found one of my old projects was importing the entirety a library. Bootstrap Vue, if you haven't used it, is pretty hefty.

A more performant solution would be to import only the pieces that I would need.

Just cherry-pick!

> via the blogpost: If you are using a component library that uses ES modules like bootstrap-vue, you can cherry pick its components instead of just registering all of them globally.


```js
// bad
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue);


// better
import {
 Modal,
 Table,
 Pagination,
 Button,
 Layout,
} from 'bootstrap-vue/es/components';

import bTooltipD from 'bootstrapvue/es/directives/tooltip/tooltip';

[Modal, Table, Pagination, Button, Layout].forEach(comp => {
  Vue.use(comp);
});

Vue.directive('b-tooltip', bTooltipD);
```


via [How to Boost Vue.js Performance](https://medium.com/@tugayyaldiz/how-to-boost-vue-js-performance-c7df027ff3f5](https://medium.com/@tugayyaldiz/how-to-boost-vue-js-performance-c7df027ff3f5)
