---
title: TIL CSS target everything but last element using Not
date: 2021-05-09
published: true
tags: ['css']
series: false
canonical_url: false
description: "Target everything but the last item in CSS"
layout: layouts/post.njk
---

## Don't do this:

```css

.item {
  margin-right: 1.6rem;
}

.item:last-child {
  margin-right: 0;
}
```

## Do this:

```css
/* Option 1 */

.item:not(:last-child) {
  margin-right: 1.6rem;
}

/* Option 2 */
.item:nth-child(n+2) {
  margin-left: 1.6rem;
}

/* Option 3 */
.item + .item {
  margin-left: 1.6rem;
}

```



REF:
https://betterprogramming.pub/5-css-practices-to-avoid-as-a-web-developer-1b7553c05131
