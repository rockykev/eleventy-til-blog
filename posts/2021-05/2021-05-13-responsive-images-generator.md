---
title: TIL this Responsive Image Generator
date: 2021-05-13
published: true
tags: ['html', 'optimization']
series: false
canonical_url: false
description: "With website optimization, it's better to deliver only the image that the browser size needs. Do that here."
layout: layouts/post.njk
---

With website optimization, it's better to deliver only the image that the browser size needs.

You can do that with [srcset](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#attr-srcset), which provides responsive image hints to the browser to select the exact file to deliver to the end user.

This dope site https://responsivebreakpoints.com/ from Cloudinary generates optimal responsive image dimensions.

How it looks:

![](2021-05-13-responsive-images-generator_0.png)

```html
<img
sizes="(max-width: 2100px) 100vw, 2100px"
srcset="
woman_glasses_c_scale,w_80.jpg 80w,
woman_glasses_c_scale,w_444.jpg 444w,
woman_glasses_c_scale,w_734.jpg 734w,
woman_glasses_c_scale,w_976.jpg 976w,
woman_glasses_c_scale,w_1151.jpg 1151w,
woman_glasses_c_scale,w_1304.jpg 1304w,
woman_glasses_c_scale,w_1486.jpg 1486w,
woman_glasses_c_scale,w_1624.jpg 1624w,
woman_glasses_c_scale,w_1838.jpg 1838w,
woman_glasses_c_scale,w_1864.jpg 1864w,
woman_glasses_c_scale,w_2024.jpg 2024w,
woman_glasses_c_scale,w_2100.jpg 2100w"
src="woman_glasses_c_scale,w_2100.jpg"
alt="">
```

