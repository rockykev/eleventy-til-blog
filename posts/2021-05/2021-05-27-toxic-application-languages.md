---
title: TIL toxic code language in Job Applications
date: 2021-05-27
published: true
tags: ['jobhunting']
series: false
canonical_url: false
description: "We're looking for a Rockstar developer!"
layout: layouts/post.njk
---


Probably the most famous one: "We're looking for a Rockstar developer!"
Nah tech, looks like you're looking for a code monkey to abuse.

Note: It's all opinionated. And it doesn't mean they're outright red flags danger. Maybe their HR person was forced to write it, and after applying, you discover they're actually a dope company. Your mileage may vary.

## The article

This article, [How to Spot Toxic Software Jobs From Their Descriptions](https://medium.com/swlh/how-to-spot-toxic-software-jobs-from-their-descriptions-c53cf224417f
) has 3 applications where the author rips them apart because of they're vagueness or coded language.

Example #2 really drives me insane. I've experienced them.

![](2021-05-27-toxic-application-languages_0.png)

Grabbing snippets from the blog post:

> “Own any and all parts of the Software Development Life Cycle” basically means, “We have no strong process here, and everything’s on fire so we need you wherever we can put you.”

> Participate and own all aspects of the development life cycle starting from design, estimation, development, DevOps and testing of both service and web components
> Yikes. You want me to be a software architect, a developer, a DevOps engineer, and a QA engineer at the same time?


---

The ones that really scream as red flags to me are:

### Ones that emphasize family.

I'm here to provide business value, not be a replacement son.

I've been in family businesses. They WILL give special treatment to family.

### Does not discuss money, finances, or benefits

I spent 80% of my energy applying for what looks like a cool company.
It felt like a shitty date, where you have to play "the game". After three weeks, then they offered me $54k.

I then applied to a job that starts you off at $80k. No games played. No BS.

### Asks for years of experience without why

This is something I'm working on explaining.

ROLE: Web development
REQUIRED: 3-10 years of experience with Javascript

Why? Why do you need that? I'm qualified. If it said something like, "We have a lot of ancient JQuery code" or "we write for browser built in 2001", then sure. But arbitrary years of dev is stupid.

### We work hard we play hard

Why do you play hard?

Is it because you just did a 60-hour week for your fifth straight week?

