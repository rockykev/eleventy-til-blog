---
title: TIL CSS Counter
date: 2021-05-20
published: true
tags: ['counter', 'css']
series: false
canonical_url: false
description: "CSS has a method to automagically generate numbers and pop it into the element itself."
layout: layouts/post.njk
---

I forget about this powerful CSS trick.

CSS has a method to automagically generate numbers and pop it into the element itself.

**Usecase 1**

Step 1, Step 2, Step 3... Step 50.

But what if you realize you need to inject another step between Step 22 and Step 23?

You can do Step 22a.
Or you can make that a NEW Step 23, and then bump EVERY SINGLE NUMBER afterwards.

**Usecase 2**
I saw this in the [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Lists_and_Counters/Using_CSS_counters#a_more_sophisticated_example)

Footer notes!
![](2021-05-20-css-counter_0.png)

Yikes, imagine doing that manually.

## What it is

```html

<style>
body {
  counter-reset: section;                       /* Set a counter named 'section', and its initial value is 0. */
}

h3::before {
  counter-increment: section;                   /* Increment the value of section counter by 1 */
  content: "Section " counter(section) ": ";    /* Display the word 'Section ', the value of
                                                   section counter, and a colon before the content
                                                   of each h3 */
}
</style>

<h3>Introduction</h3>
<h3>Body</h3>
<h3>Conclusion</h3>

```
![](2021-05-20-css-counter_1.png)

## Process

1. Create the counter with `counter-reset` somewhere in the parent.

2. Call the counter. `counter-increment` on your element. It'll increase by 1 every time it shows up.


Via
https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Lists_and_Counters/Using_CSS_counters
