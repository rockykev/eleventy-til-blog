---
title: TIL Adding your own custom panel in Devtools
date: 2021-05-26
published: true
tags: ['devtools']
series: false
canonical_url: false
description: "I was always curious how chrome extensions like `React Devtools` or `Vue Devtools` magically pop into the Devtools section."
layout: layouts/post.njk
---

In the "Woah this is neat but I'll never do it" side of things:

I was always curious how chrome extensions like `React Devtools` or `Vue Devtools` magically pop into the Devtools section.

Apparently, Chrome has documentation on how to do it:

https://developer.chrome.com/docs/extensions/mv3/devtools/
