---
title: TIL globalThis
date: 2021-05-14
published: true
tags: ['javascript', 'es2020']
series: false
canonical_url: false
description: "`this` doesn't mean the same things depending on environment. Use globalThis"
layout: layouts/post.njk
---

Via ES2020, `globalThis` is brand new.

To access the global object:

* In the browser, the global object refers to `window`. You can also use `self`, or `frames`

* In web workers, you must use `self`

* In NodeJS, you must use `global`

> The `this` keyword could be used inside functions running in non–strict mode, but `this` will be undefined in Modules and inside functions running in strict mode. You can also use `Function('return this')()`, but environments that disable `eval()`, like CSP in browsers, prevent use of `Function` in this way.
Via the MDN

`globalThis` allows us to always refer to the right global object no matter what type of environment you're working on.


REF:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/globalThis
