---
title: TIL Select boxes should be a last resort
date: 2021-05-29
published: true
tags: ['ux']
series: false
canonical_url: false
description: "I've always hated Select boxes (aka Dropdowns)"
layout: layouts/post.njk
---

I've always hated Select boxes (aka Dropdowns).

![](2021-05-29-select-boxes_0.png)

This picture shows the problem with Select boxes. Essentially, it takes multiple clicks to choose something.

> In short, select boxes are hard to use. Besides hiding options behind an unnecessary extra click, users generally don’t understand how they work. Some users try to type into them, some confuse focused options with selected ones. And, if that weren’t enough, users can’t pinch-zoom the options on certain devices.

## Things you can use to replace the Select box:

1. Steppers

![](2021-05-29-select-boxes_1.png)

2. Radios or Radio Groups

![](2021-05-29-select-boxes_2.png)

Also rule of thumb: If it's more than 6 radio buttons, maybe a Select box is the only alternative.

3. Breaking your Select box into more fields

![](2021-05-29-select-boxes_3.png)




