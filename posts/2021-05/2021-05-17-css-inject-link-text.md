---
title: TIL injecting links when the user wants to print thew ebpage
date: 2021-05-17
published: true
tags: ['css']
series: false
canonical_url: false
description: "A neat code snippet: If the user prints the page (for some reason), links get injected into the print."
layout: layouts/post.njk
---

If the user prints the page (for some reason), links get injected into the print.
Super helpful!

![](2021-05-17-css-inject-link-text_0.png)

```css

@media print {
  a[href^="http"]::after {
    content: " (" attr(href) ")";
  }
}

```



Via https://tomasz-smykowski.com/
