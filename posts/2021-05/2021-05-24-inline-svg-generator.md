---
title: TIL Copy/Paste SVG Icons
date: 2021-05-24
published: true
tags: ['html', 'svg', 'optimization', 'generator']
series: false
canonical_url: false
description: "And by inlining the SVG, you make one less call, and it loads faster. heroicons.dev is awesome"
layout: layouts/post.njk
---

One of the cool webpack configs that you can set up is to have inline SVG. SVG files themselves are `xml`, which can be read directly by the browser.

And by inlining the SVG, you make one less call, and it loads faster. But there's a cut-off. Typically, if the SVG is 1kb or smaller, it's better to inline them for the highest optimization.

Font Icons, like FontAwesome, require you get the entire file, just for a few icons.

This site, [Hero Icons](https://heroicons.dev/) is dope because it gives you a huge list of icons, and pastes the inline SVG code right into your clipboard.

Check it: https://heroicons.dev/
