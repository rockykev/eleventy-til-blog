---
title: TIL removing ELSE statements to write clearer code
date: 2021-05-19
published: true
tags: ['javascript', 'webdev']
series: false
canonical_url: false
description: "Programs are meant to be read by humans and only incidentally for computers to execute.  Donald Knuth"
layout: layouts/post.njk
---

I've been thinking about this dogma for years.

> “Programs are meant to be read by humans and only incidentally for computers to execute.” - Donald Knuth, The Art of Computer Programming.

## I hate Nestled If statements

I think we can all agree -- nestled `if` statements suck!

```js

function suckyCarChecker(isCar, howManyWheels) {

  if (isCar) {
    if (howManyWheels === 1) {
      return "You're a unicycle!";
    } else if (howManyWheels === 2) {
      return "You're a bike!";
    } else if (howManyWheels === 3) {
      return "You're a trike thing?";
    } else if (howManyWheels === 4) {
      return "You're a car or truck";
    } else if (howManyWheels > 4) {
      return "You're tractor trailer or something else?";
    } else {
      return "I don't think you have wheels.";
    }
  } else {
    return "You're not a car";
  }
}
```
NOBODY: "This code is soooo readable!" 🤢

And if you like that code above, then don't read the rest. It'll just annoy you.


## Being a better coder by not using Else

Some people feel very strongly about not using Else. Like "[Write better code and be a better programmer by NEVER USING ELSE statements](https://dev.to/dglsparsons/write-better-code-and-be-a-better-programmer-by-never-using-else-statements-4dbl)"

PROTIP: Like Drama? If you ever see the word NEVER, read the comments and watch a bunch of nerds fight. Because of course, take the advice with a grain of salt. You will always have edge-cases where you HAVE TO break the rules.

There'a few ways to solve the 'don't use Else' dogma, in no particular order. It's also not a definite list.

1. Guard Clauses (reverse the logic)

Also called `early exits` at my job.

It looks like this:
![](2021-05-19-removing-else-statements_0.png)

```js
 if (!isCar) {
  return "You're not a car";
 }
```

2. Helper functions

Abstract it to it's own function.

> It's good practice to extract the logic of the conditions in small functions that allow greater readability of the code (and, of course, to find bugs in them) since the responsibility of evaluating the condition is being delegated to a specific function.

Via https://betterprogramming.pub/refactoring-guard-clauses-2ceeaa1a9da

It doesn't solve the else completely, but it moves the logic to some place more manageable.

```js

function suckyCarChecker(isCar, wheels) {
  if (!isCar) {
    return "You're not a car";
  }

  return howManyWheels(wheels);
}

function howManyWheels(wheelCount) {

    if (wheelCount === 1) {
      return "You're a unicycle!";
    } else if (wheelCount === 2) {
      return "You're a bike!";
    } else if (wheelCount === 3) {
      return "You're a trike thing?";
    } else if (wheelCount === 4) {
      return "You're a car or truck";
    } else if (wheelCount > 4) {
      return "You're tractor trailer or something else?";
    }
}

```

3. Using data objects and map/filter/reduce

Your goal is to return a string.
Why not turn that whole if statement into a object?

```js
const wheelCount = {
  {
    wheels: 1,
    text: "You're a unicycle!";
  },
  ...
}
```

Then you can match the `wheelCount['wheels']` and output the text using some modern javascript.
(map, reduce, filter)[https://medium.com/poka-techblog/simplify-your-javascript-use-map-reduce-and-filter-bd02c593cc2d]

Heck, some [extreme programmers don't even want to use conditionals at all](https://medium.com/edge-coders/coding-tip-try-to-code-without-if-statements-d06799eed231)!


4. Sometimes you gotta use that [switch statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/switch)
