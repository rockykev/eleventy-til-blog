---
title: TIL planes having to restart to avoid overflow
date: 2021-05-10
published: true
tags: ['fun']
series: false
canonical_url: false
description: "Boeing planes generally have overflow happens after >200 days."
layout: layouts/post.njk
---

FROM THE ARTICLE:
According to the directive itself, if the aircraft is powered on for more than 51 days this can lead to "display of misleading data" to the pilots, with that data including airspeed, attitude, altitude and engine operating indications.


> Generally the overflow happens after >200 days. 51 days is just playing it safe as you would have to forget 4 times before an error comes up. So much redundancy. Wait till people hear how often some airbusses had to be restarted.
Via https://www.reddit.com/user/LeastCreativeDrawer/



REFERENCE:
https://www.reddit.com/r/todayilearned/comments/n96vi8/til_the_boeing_787_needs_to_be_rebooted_every_51/
