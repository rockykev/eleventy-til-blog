---
title: TIL System font stack
date: 2021-05-03
published: true
tags: ['css', 'fonts']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---


## System Font Stack
If you want to use the native font of the operating system of the user, do this:

```css
.system-font-stack {
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", Helvetica, Arial, sans-serif;
}
```

Via Educative.co course
