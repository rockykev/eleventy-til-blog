---
title: TIL outline-offset
date: 2021-05-18
published: true
tags: ['accessibility', 'css']
series: false
canonical_url: false
description: "Some sites remove :focus. Those sites suck. They should instead do this:"
layout: layouts/post.njk
---


## Focus

When using a keyboard, press ` ⭾ tab` key.

On well made sites, that is the `:focus` in action. Often it's a nice black box. It's for accessibility.
![](2021-05-18-css-outline-offset_0.png)


But it sure is ugly for the design heavy sites.

> An all too common violation that I have done myself in the past is to remove :focus outlines on links, buttons, and other interactive controls. Without providing an alternative :focus style, this is immediately a violation of the [WCAG Success Criterion 2.4.11: Focus Appearance](https://www.w3.org/WAI/WCAG22/Understanding/focus-appearance-minimum.html).

## Making it less ugly

Modern CSS now has `outline-offset`, which allows us to design it.

```
button:focus {
  outline: max(1px, 0.1em) dashed currentColor;
  outline-offset: -0.25em;
}
```
![](2021-05-18-css-outline-offset_1.png)

## What if you don't want it at all

> There is also a new pseudo-class that you can consider using in some circumstances. The :focus-visible pseudo-class will display an outline (or user-defined style) only when the device/browser (user agent) determines it needs to be visible. Typically this means it will appear for keyboard users upon tab key interaction but not for mouse users.

```
button:focus {
  outline: none;
}

button:focus-visible {
  outline: max(1px, 0.1em) dashed currentColor;
  outline-offset: -0.25em;
}
```
Remove the outline, but make it show only when  you keyboard into it.

This is via https://moderncss.dev/modern-css-upgrades-to-improve-accessibility/.

The site has working examples, which I yoinked and turned into screenshots above.

