---
title: TIL using live regions for autocomplete accessibility
date: 2022-02-21
published: true
tags: ['accessibility', 'javascript', 'autocomplete']
series: false
canonical_url: false
description: "However, the screen reader does not recognise this as an action to tell the user about. It will announce the header and the input box when they gain focus, but not the search results."
layout: layouts/post.njk
---

Every time a user types on the input box, the state of the page changes: the results from autocomplete are updated and presented to your user.

What's the problem:

> However, the screen reader does not recognise this as an action to tell the user about. It will announce the header ("Capital Search") and the input box when they gain focus, but not the search results.

![](2022-02-21-live-regions-accessibility_0.png)

The solution is to use "Live Regions".

tl;dr - you have JS update a hidden div, that speaks out the event.

```html
<div role="region" aria-live="polite">0 results found</div>
```

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="YzGJRRE" data-user="SavvStudio" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/SavvStudio/pen/YzGJRRE">
  Dev.to: Autocomplete - accessible</a> by Savvas Stephanides (<a href="https://codepen.io/SavvStudio">@SavvStudio</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


via [Savvas Stephanides's The problem with autocomplete and how to fix it](https://dev.to/savvasstephnds/the-problem-with-autocomplete-and-how-to-fix-it-2ill)
