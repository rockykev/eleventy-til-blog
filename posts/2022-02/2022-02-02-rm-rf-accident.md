---
title: TIL this bug that deleted all /usr stuff
date: 2022-02-02
published: true
tags: ['linux', 'commandline', 'terminal', 'woops', 'bug']
series: false
canonical_url: false
description: "Classic mistake where the dev wrote a rm -rf that deleted the folder"
layout: layouts/post.njk
---

This is from 2011 btw.
![](2022-02-02-rm-rf-accident_0.png)


> An extra space at line 351:
rm -rf /usr /lib/nvidia-current/xorg/xorg

> causes the install.sh script to do an rm -rf on the /usr directory for people installing in ubuntu.

> Totally uncool dude!!! The script deletes everything under /usr. I just had to reinstall linux on my pc to recover.

> Removing the space will fix this. Probably should do it quickly!!!

What's happening here?

The command is `rm -rf /usr /lib/nvidia-current/xorg/xorg`

`rm` - remove files
`rf` - the flags are recursive, and force. Force meaning, NO WARNINGS.

The next bit, that space says the target is `/usr`, not `/usr /lib/nvidia-current/xorg/xorg`



REFERENCE:
https://github.com/MrMEEE/bumblebee-Old-and-abbandoned/issues/123
