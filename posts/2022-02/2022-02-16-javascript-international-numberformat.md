---
title: TIL about the new Intl.NumberFormat browser API
date: 2022-02-16
published: false
tags: ['javascript', 'browser', 'mdn']
series: false
canonical_url: false
description: "No more stupid-ass hacks to format numbers correctly."
layout: layouts/post.njk
---

No more stupid-ass hacks to format numbers correctly.

## Numbers format
```js
const numberFormat = new Intl.NumberFormat('ru-RU');
console.log(numberFormat.format(654321.987)); // → "654 321,987"

```

If you don't pass a locale, then it defaults to the users' preferences!


## Currency Format

```js
const number = 123456.789;

console.log(new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number));
// expected output: "123.456,79 €"

// the Japanese yen doesn't use a minor unit
console.log(new Intl.NumberFormat('ja-JP', { style: 'currency', currency: 'JPY' }).format(number));
// expected output: "￥123,457"

// limit to three significant digits
console.log(new Intl.NumberFormat('en-IN', { maximumSignificantDigits: 3 }).format(number));
// expected output: "1,23,000"
```

## Units and Percentages

```js
new Intl.NumberFormat('en-US', {
    style: 'unit',
    unit: 'liter',
    unitDisplay: 'long'
}).format(amount);
// → '3,500 liters'


new Intl.NumberFormat("en-US", {
    style: "percent",
    signDisplay: "exceptZero"
}).format(0.55);
// → '+55%'
```


via [Jordan Finneran - Never use a number or currency formatting library again!](https://dev.to/jordanfinners/never-use-a-number-or-currency-formatting-library-again-mhb?utm_source=pocket_mylist)

[MDN link](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/NumberFormat)
