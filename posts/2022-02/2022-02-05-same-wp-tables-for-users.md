---
title: TIL you can share multiple users between WordPress installations
date: 2022-02-05
published: true
tags: ['wordpress', 'cms', 'user-tables']
series: false
canonical_url: false
description: " You can use the same userbase for all your blogs on the same domain by defining the CUSTOM_USER_TABLE and optionally the CUSTOM_USER_META_TABLE constants to point to the same wp_your_blog_users and wp_your_blog_usermeta tables."
layout: layouts/post.njk
---

Imagine you have multiple WordPress Installations.

* Course Platform (courses.website.com)
* Blog Platform (website.com/blog)
* Forum Platform (forum.website.com)
* Q&A Platform (qa.website.com)

What sucks is having to create 4 separate accounts to access each one.

Additionally, because there's no connection, a user can hypothetically create a:

1) John Jones (jjones@email.com)
2) John J Jones (johnjjones@email.com)

as two completely separate entities.

WordPress has thought of that!

> You can use the same userbase for all your blogs on the same domain by defining the `CUSTOM_USER_TABLE` and optionally the `CUSTOM_USER_META_TABLE` constants to point to the same wp_your_blog_users and wp_your_blog_usermeta tables.

```php
<?php
// in wp-config.php
define('CUSTOM_USER_TABLE', 'main_user_table');
define('CUSTOM_USER_META_TABLE', 'main_user_meta_table');
```

https://wordpress.org/support/article/installing-multiple-blogs/#multiple-databases-same-users
