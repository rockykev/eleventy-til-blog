---
title: TIL Numeric Separators
date: 2022-02-27
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "This is spicy ES2021, and super useful for big numbers."
layout: layouts/post.njk
---

This is spicy ES2021, and super useful for big numbers.

```js
// before
let number = 98234567

// new ES2021 format
let number = 98_234_567
```

via [Blessing Hirwa](https://dev.to/blessingartcreator/17-javascript-optimization-tips-3gil)
