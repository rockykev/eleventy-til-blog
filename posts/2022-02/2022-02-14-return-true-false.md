---
title: TIL of returning true/false directly
date: 2022-02-14
published: true
tags: ['booleans']
series: false
canonical_url: false
description: "return true or false directly bruh"
layout: layouts/post.njk
---

I've been guilty of this.

```php
<?php
function checkIsYouth($age) {
    if ($age >= 15 && $age <= 24) {
        return true;
    }

    return false;
}
```

So let's break it down!

If the age is:

* between 15 - 24, return `true`
* else return `false`

A one-line way that expresses the same thing is:

```php
<?php
function checkIsYouth($age) {
    return $age >= 15 && $age <= 24;
}
```

This works because it's going to return either `true` or `false` anyways. Might as well lift it to the return statement.

via [Amir Etemad - Some Tips For PHP developers](https://amir-etemad.medium.com/some-tips-for-php-developers-8f9f75fba31e)
