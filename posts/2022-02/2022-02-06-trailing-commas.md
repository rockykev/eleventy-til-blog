---
title: TIL trailing commas in Javascript
date: 2022-02-06
published: true
tags: ['mdn', 'linter', 'cleancode']
series: false
canonical_url: false
description: "JavaScript has allowed trailing commas in array literals since the beginning, and later added them to object literals, and more recently, to function parameters and to named imports and named exports."
layout: layouts/post.njk
---


[Trailing commas](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Trailing_commas) in Javascript is a interesting feature.

> JavaScript has allowed trailing commas in array literals since the beginning, and later added them to object literals, and more recently, to function parameters and to named imports and named exports.


One thing to note:
> `JSON`, however, disallows trailing commas.

```js


// this is fine
const list = ['ace', 'luffy', 'sabo']

// this is also fine
const list = ['ace', 'luffy', 'sabo', ]

```

On small lists, it's cleaner to just remove it. But when working with large arrays/objects/params, it starts to get really difficult.

For more specific details and use-cases, check out the MDN link above!

Finally - don't argue about this. Your linter should be handling if this rule is appropriate or not.
