---
title: TIL how to convert a nodeList into an array
date: 2022-02-24
published: true
tags: ['javascript', 'browser']
series: false
canonical_url: false
description: "You can use the spread operator to turn a nodelist into an array!"
layout: layouts/post.njk
---

You can use the spread operator to turn a nodelist into an array!

```js
const el = [...document.querySelectorAll('div')];
console.log(el); // (3) [div, div, div]

el.forEach(item => {
  console.log(item);
});
// <div></div>
// <div></div>
// <div></div>

```

via [Chris Bonger's 10 ways to use the spread operator in JavaScript](https://dev.to/dailydevtips1/10-ways-to-use-the-spread-operator-in-javascript-1imb#pass-unlimited-arguments-to-a-function)
