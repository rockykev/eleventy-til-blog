---
title: TIL how to get the location of the user with the Geolocation API
date: 2022-02-20
published: false
tags: ['mdn', 'browser', 'javascript', 'permissions']
series: false
canonical_url: false
description: "You can get the user's location in the browser. First, the user has to accept this. As it's a security request. Second, you use the Geolocation API."
layout: layouts/post.njk
---

You can get the user's location in the browser.

First, the user has to accept this. As it's a security request.

Second, you use the Geolocation API.

```js

function getLocation() {
  if (!navigator.geolocation) {
    console.log('Geolocation API not supported by this browser.');
  } else {
    console.log('Checking location...');

    //fire the geolocation with callback functions navigator.geolocation.getCurrentPosition(locationSuccess, locationError);
  }
}

// the two callbacks
function locationSuccess(position) {
  console.log(position);
}

function locationError(position) {
  console.error('Geolocation error!');
}

```

You can also set it to watch using the

`Geolocation.watchPosition()`



[the MDN](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API)

via [Nathan Pasko's How to Check a User's Location with JavaScript](https://dev.to/sprite421/how-to-check-a-user-s-location-with-javascript-142b)
