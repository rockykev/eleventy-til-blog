---
title: TIL of someone who missed all the JS pitfalls
date: 2022-02-17
published: true
tags: ['javascript', 'devstory']
series: false
canonical_url: false
description: "If JS doesn't do it the way they think it works, then... JaVaScRiPT is STUPID!"
layout: layouts/post.njk
---

I work with a lot of devs who treat Javascript like a second-class citizen... in that they learned and mastered another language, and then jumped right into Javascript with assumptions.

"If JS doesn't do it the way they think it works, then... JaVaScRiPT is STUPID!"

> Javascript is one of the most accessible languages. But between those who use it and those who master it, there is a clear difference. Javascript is full of nuances, fuzzy behaviors and hidden concepts. It'll drive you crazy if you don't know them.

via [Mehdi Zed's Javascript: what I didn't understand](https://dev.to/jesuisundev/javascript-what-i-didn-t-understand-14hl)

I like this post because:

1. The author stumbles on prototypical nature of of Javascript
2. The author accidentally blocks themselves in the event loop
3. The author acknowledges their own ego, and steps back to learn Javascript correctly.

That post again: [Mehdi Zed's Javascript: what I didn't understand](https://dev.to/jesuisundev/javascript-what-i-didn-t-understand-14hl)

