---
title: TIL of a nice ELI5 of code splitting
date: 2022-02-13
published: true
tags: ['programming', 'webpack', 'eli5']
series: false
canonical_url: false
description: "Code splitting is the splitting of code into various bundles or components which can then be loaded on demand or in parallel"
layout: layouts/post.njk
---


What is code splitting?

Via MDN:
> "Code splitting is the splitting of code into various bundles or components which can then be loaded on demand or in parallel."

ELI5:

> Airline luggage limits. You can only have 50 pounds in a bag. But you need to bring 150 pounds worth of stuff. So you split it up into three bags so each of them can be flown at the same time, but still be under the limit.

via [laurieontech](https://github.com/reactwg/react-18/discussions/46?utm_source=pocket_mylist#discussioncomment-848253)


This is helpful because SEO-wise, getting a functional webpage is a major signal to Google. They want to deliver visitors to the most accurate website that is also optimized for speed. And your site doesn't need all of that JS immediately.

```
// BEFORE
import MyScript from './my-script'

// WITH CODE SPLITTING
function myFunction() {
  if (condition) {
    import(`./my-script`).then(() => {
      // do something here
    })
  }
}
```

via [laurieontech's blog post](https://laurieontech.com/posts/code-splitting/)


Originally via [React-18's ELI5 Discussion thread](https://github.com/reactwg/react-18/discussions/46)
