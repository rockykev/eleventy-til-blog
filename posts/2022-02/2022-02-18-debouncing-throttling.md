---
title: TIL about Debouncing/throttling
date: 2022-02-18
published: true
tags: ['programming', 'eli5']
series: false
canonical_url: false
description: "Debouncing and throttling are too common techniques for dealing with things that happen 'too often', like typing."
layout: layouts/post.njk
---

## What is Debouncing, Throttling

(I'm yoinking from this [comment here](https://github.com/reactwg/react-18/discussions/46?utm_source=pocket_mylist#discussioncomment-847556)


> Debouncing and throttling are too common techniques for dealing with things that happen "too often". Imagine you're meeting a friend, and they are telling you a story, but they struggle to pause when talking. Let's say you want to accommodate them while also responding to what they have to say, when possible. (I know this might be a bit contrived, but bear with me!)

The GOAL:
Let's say you can never speak at the same time.

Method 1: Synchronous
You could respond to each sentence the moment they finish it...

The issue: If your responses are short, it doesn't disrupt your friend's story. But if they're long, it can cause a lot of confusion.

![](https://user-images.githubusercontent.com/810438/121427718-f3457b00-c96c-11eb-9515-0ab2974414c9.png)



Method 2: Debounced

You could wait for them to stop talking.

The issue:
It works if your friend is making pauses.
But if they aren't, then you can wait a long time before you get a chance.

If your friend makes pauses:
![](https://user-images.githubusercontent.com/810438/121427785-05271e00-c96d-11eb-88f2-b0c80221fe06.png)

If your friend just keeps going:
![](https://user-images.githubusercontent.com/810438/121428868-348a5a80-c96e-11eb-8b7d-c0b7bb45971c.png)


Method 3: Throttled

You could decide to respond at most once a minute. Here, you keep count of how long you haven't spoken for. Once you haven't spoken for a minute, you insert your response right after the friend's next sentence:

![](https://user-images.githubusercontent.com/810438/121428472-c2b21100-c96d-11eb-962c-ea1c190815cd.png)


This strategy is helpful if your friend wants you to respond as they go, but they don't ever create pauses for you to do it. But it's not great if they're creating pauses but you're still waiting out for no reason:

![](https://user-images.githubusercontent.com/810438/121429118-77e4c900-c96e-11eb-9ac1-88f3816487e6.png)

## How this matters in the real world

Instead of your friend's sentences, think of events like clicking, or keyboard typing.

And your responses is updating the screen.

> You use debouncing or throttling when the user is doing something too fast (e.g. typing), and updating the screen in response to each individual event is just too slow. So you either wait for the user to stop typing (debouncing) or you update the screen once in a while, like once a second (throttling).


Originally via [React-18's ELI5 Discussion thread](https://github.com/reactwg/react-18/discussions/46)
