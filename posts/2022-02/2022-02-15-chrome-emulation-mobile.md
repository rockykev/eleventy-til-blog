---
title: TIL Chrome can emulate mobile hardware
date: 2022-02-15
published: true
tags: ['browsers', 'emulation', 'mobile']
series: false
canonical_url: false
description: "Chrome can emulate device hardware in DevTools — choose Sensors from the More tools menu"
layout: layouts/post.njk
---

> Smart phones and tablets often include hardware such as Global Positioning Systems (GPS), gyroscopes, and accelerometers. These are not normally available in desktop PCs which can make development using APIs such as geolocation more difficult.

> Chrome can emulate device hardware in DevTools — choose Sensors from the More tools menu:

![](2022-02-15-chrome-emulation-mobile_0.png)

You can:

* Change the geolocation/city
* Change the device orientation
* switch to touch events
* set idle states to see how your app responds to the lock screen.

via [Craig Buckler's 15 DevTool Secrets for JavaScript Developers](https://blog.openreplay.com/15-devtool-secrets-for-javascript-developers)
