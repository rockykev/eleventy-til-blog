---
title: TIL the Bus Factor
date: 2022-02-07
published: true
tags: ['programming', 'idioms']
series: false
canonical_url: false
description: "The bus factor of a project is the number equal to the number of team members who, if run over by a bus, would put the project in jeopardy. If a project overly relies on the contributions or knowledge of one person, then you could say that the project’s bus factor is one."
layout: layouts/post.njk
---

> Bus factor
The bus factor of a project is the number equal to the number of team members who, if run over by a bus, would put the project in jeopardy. If a project overly relies on the contributions or knowledge of one person, then you could say that the project’s bus factor is one.

This is why I'm aggressively about documentation and training.

At some point - you will be gone. Either by choice or by force. (Maybe it's that murder bus?)

Help your team out by documenting.


A bonus one from the [reddit post I found this originally on](https://www.reddit.com/r/programming/comments/rhv8bz/software_engineering_idioms_you_should_know/):

> Death March - poorly scoped project, likely doomed from the start to crush all contributers with an impossible schedule/requirements/cost/etc.

REFERENCE:
(Software Engineering Idioms You Should Know)[https://endaphelan.me/posts/software-idioms-you-should-know/#bus-factor]
