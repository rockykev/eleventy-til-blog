---
title: TIL ELI5 for websockets
date: 2022-02-25
published: false
tags: ['eli5', 'browser']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

What are websockets?

tl;dr - It's part of the Web Workers API, and it keeps listening for a event from you to the server.

This is a really good ELI5 for Websockets, from [Akash](https://dev.to/buzzingbuzzer/comment/g0g):

You have a agreement with your best friend that she'll help with your math homework when you're stuck.

When you have a question, you call her, wait for her to answer, then ask your question, and listen while she replies. Then she hangs up and you carry on with your homework.

Most evenings, you call her a few times over the course of an hour or so. Sometimes she's too busy to answer, so you hang up, twiddle your thumbs and try in a few minutes.

But some evenings, homework is too hard, and you're practically calling her every five minutes. Although she doesn't mind helping you, neither of you are happy about the constant process of dialling her, waiting for her to answer, and hanging up, only to do the same thing a few minutes later.

Finally you settle upon a sane solution. You call her, and keep the line open. There's silence while you're working on your own, but when you have a question, you just have to start speaking and she can already hear you. When your homework is over, you can hang up for the evening.

Your friend is the server, you're the browser app, the call is a network connection and keeping the line open after calling is a websocket connection.

via [Jean-Michel Fayard's Best of #explainlikeimfive](https://dev.to/jmfayard/best-of-explainlikeimfive-3a0f)
