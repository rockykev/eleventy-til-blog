---
title: TIL the one-banana problem
date: 2022-02-09
published: true
tags: ['dev-history', 'developer', 'humor']
series: false
canonical_url: false
description: "Banana problem: Supposedly from the notion that a trained monkey could do what is required. At IBM, they divided the world into one, two, or three banana problems."
layout: layouts/post.njk
---

First, the banana problem.

> A problem, project, or task that requires little to no effort, expertise, or intelligence to solve or complete. Supposedly from the notion that a trained monkey could do what is required.

EXAMPLE:
A: "We have to add in some code so that users are able to remain logged into the website, even if they visit other pages."
B: "No worries, that's a one-banana problem."

At IBM:
> At IBM, folklore divides the world into one-, two-, and three-banana problems. Other cultures have different hierarchies and may divide them more finely; at ICL, for example, five grapes (a bunch) equals a banana. Their upper limit for the in-house sysapes is said to be two bananas and three grapes (another source claims it's three bananas and one grape, but observes “However, this is subject to local variations, cosmic rays and ISO”). At a complication level any higher than that, one asks the manufacturers to send someone around to check things.


REFERENCE:
http://www.catb.org/jargon/html/O/one-banana-problem.html
https://idioms.thefreedictionary.com/one-banana+problem#:~:text=A%20problem%2C%20project%2C%20or%20task,could%20do%20what%20is%20required.
