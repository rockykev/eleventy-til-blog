---
title: TIL World of Warcraft had a Developer Island in production code
date: 2022-02-03
published: true
tags: ['gamedev', 'hacks']
series: false
canonical_url: false
description: "This developer island was only for devs, and was used because some bugs couldn't be reproduced on dev servers"
layout: layouts/post.njk
---


> Developer Island, Programmer Isle and GM Island, who made these, and what were they made for? (I have heard GM island is/was the spawn point for Godtool logins).
asks stormsZero on Reddit.

Former WoWDev David Ray answers:

> GodTool was mostly about chatting, inventory, quests, and logs. What you describe about the GM walking away probably was an actual in game avatar, and not a GodTool thing. At least when I was there, when you logged your character in using GodTool it immediately turned you invisible and teleported you to GM island. You had to have an in game avatar to be able to chat, but GodTool didn't have any graphics in it, so you couldn't really walk around the world. It looked way more like something from "World Of Spreadsheets" than a game.

> I don't know who made the various islands. But the purpose of GM isle, was to have a place where all the invisible avatars of the GM's could be, while they chatted with the players, so they were out of the way of the game. They could also summon players there, to force them to pay attention, because there was no way off the island. Until it became a thing to try and see the island, and people were intentionally misbehaving to try and see it. Developer island was a place where we could create NPC's, and summon things, and use powers without players being able to see it. It was a necessity because in the early days we kept finding bugs that couldn't be reproduced on dev servers, they would only happen in live. So we needed a place to test bugs and things that would be away from the public, and were isolated so if we really messed things up it wouldn't affect the players.

Fantastic debugging solution!

VIA:
[stormsZero's Reddit convo](https://www.reddit.com/r/wow/comments/9huows/ama_former_wow_developers_kevin_jordan_classes/e6eoe4p/?context=3)
