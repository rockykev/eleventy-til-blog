---
title: TIL ELI5 for Smoke Testing
date: 2022-02-26
published: true
tags: ['testing', 'eli5']
series: false
canonical_url: false
description: "It's about doing some basic surface level tests. If any of them are goofy, then something is wrong."
layout: layouts/post.njk
---

tl;dr -
* It's about doing some basic surface level tests. If any of them are goofy, then something is wrong.

via [Marko Kruljac](https://dev.to/krukru/comment/14a0f):

For your upcoming birthday, you ask your parents to buy you a Lego 1989 Batmobile.

The day finally arrives, and you are presented with a wrapped package, but they don't tell you what's inside. It is a surprise.

Your excitement rises, but before you get your hopes up - you examine the package.

1 - Judging by the shape and size of the package, it matches the size of the 3306 piece box for the Lego 1989 Batmobile. Smoke test 1 passed.

2 - You take the present in your hand and feel its weight. Its not too heavy, not too light. Just about right for a box of Lego. Smoke test 2, passed.

3 - You decide to give it one final test before calling it. You shake the box and hear the familiar sound of Lego inside. Smoke test 3, passed!

Now you are fairly confident that the present is some kind of Lego, but you cannot really be sure if it is the Batmobile, so you start with user acceptance testing, unwrapping the present and validating the contents of the box.

Had any of the smoke tests failed, you would have immediately known that the present is not what you asked for, without investing time in detailed testing.

via [Jean-Michel Fayard's Best of #explainlikeimfive](https://dev.to/jmfayard/best-of-explainlikeimfive-3a0f)
