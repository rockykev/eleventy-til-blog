---
title: TIL Blogpost - my road to coding
date: 2022-02-10
published: true
tags: ['roadmap', 'journey', 'blog']
series: false
canonical_url: false
description: "Started from the bottom now im herr"
layout: layouts/post.njk
---

Read the post here: https://dev.to/rockykev/how-i-became-a-developer-in-about-15-years-552k


-----

Follow this roadmap and you too can be a dev in 15 years.

## Pre-2006: My road to coding

* I bought the book **Learn C++ in 24 hours**. I went to chapter 2 and got stuck.
* I made **Starcraft** maps and fumbled through the **Source Engine**.
* I wrote really crappy **HTML** on my xanga site. *Tables ftw.*
* I made movies using **RPG Maker**. I never saved any of them. 😔
* I decided to go to school for video game animation.

## 2006 - 2010: College years

* College. I took coding courses.
* I learned **Java**. *I discovered I suck with Java too*.
* My new life calling - this [fart app made $10,000 a day](https://www.wired.com/2008/12/iphone-fart-app/).
* I dropped out. My mate and I **hacked together mobile apps**, trying to score that fart app money.
* I am broken. I suck at coding. I suck at college. I owe a lot of money. 💀

## 2010 - 2013: I hate software developers 😡

* Started attending conferences. It was incredibly hostile and exclusive. 🔪 Very 'old guard'.
* **WordPress** development was hot. And community was super nice.
* Started **WordPress themes/plugins**. *Now I sucked at both Java AND PHP*.
* Still in severe debt. Worked with websites (I did **SEO** and **marketing**), but never as a bona fide developer.
* Wrote scripts to automate things like bots and social media.
* **Minecraft (Java)** was getting popular. Should I dig deeper in **Java**? *Haha no. Java is too traumatic*.
* Recieved a **Raspberry Pi**.

## 2013 - 2015: People give me money... for code?

* I hacked **Wordpress** templates together. *Genesis Framework*
* Someone paid me $200 to build them a *WordPress* site. *Am I a developer? No. I built iPhone apps, games and websites. But I'm no developer.*
* I fell in love with **jQuery**, **Bootstrap**, **FontAwesome**, and web languages. (ugh to **SQL**)
* People started talking about React, but it didn't make sense.
* Still building bots. Still creating scripts.
* I automated a huge chunk of my day job. You can *Javascript in Google docs, you know?*

## 2015 - 2017: I am developer now?

* Job learn I could **build websites**. *They chose to ignore me automating my job*. 🤷
* Job told me to **build websites**. *Yer a developer, Harry!*
* Job offloaded me to other people to **build websites**. I was now doing development for over 50+ hours.
* I **spoke at conferences** about marketing, seo. Now I was **speaking about web development**.
* Started fully understanding **Javascript**. And **jQuery** is starting to show it's age.
* Data science was getting big. *Should I play with Python?*
* **React is getting much bigger**. *I still don't get it?!?!*
* I helped on a **Angular** project. *Why are JS frameworks so confusing?*
* I started working on **Laravel** projects. *I like Laravel.*
* I returned to conferences/meetups. Much more inclusive now 🏳️‍🌈👨🏿👸! The old guard is dead? ☠️

## 2018 - 2020: I understand JS frameworks

* All my friends were into **React**, **Gatsby**, **GraphQL**. *Why is this so hard?!?!*
* Finally started learning **React** (through a Bootcamp), while also digging into **Vue**.
* 🙌 🙌 🙌 OMG JS FRAMEWORKS MAKES SENSE NOW. 🙌 🙌 🙌
* Still relying on smashing libraries together, like a crutch.
* Work was all **WordPress**, so I took freelancing jobs to do more JS framework things.
* I **lead WordPress meetups**, did **conf talks**, and ran **web development workshops**. *Heck yeah I'm going to make my spaces real ✨safe and inclusive✨. EVERYONE is invited 🏳️‍🌈👨🏿👸! Go suck a 🍋, old guard!* (*Yes I'm very spiteful.*)


## 2020 - 2022: The age of fundamentals

* Really find myself understanding the web fundamentals. **Vanilla HTML, CSS, JS**... real mastery instead of hacking things.
* I learned that the `<details>` element is magical.
* Spent the pandemic focusing on mental health.
* Started feeling framework fatigue. I could switch between **React** and **Vue**. Played around with **Svelte**.
* Started writing vanilla everything more, or trying to code things directly instead of finding a crutch like a library.
* After 4 years, I finally understood testing libraries.
* Still not going to touch **Java**.
* Finally found a real use-case for my **Raspberry pi**. *Pi-hole.*
