---
title: TIL How to determine what security group you're in
date: 2022-02-12
published: true
tags: ['linux', 'permissions', 'terminal', 'commandline']
series: false
canonical_url: false
description: "These commands will help you figure out what group you're in, and if you belong to the group that can modify files."
layout: layouts/post.njk
---

In Linux (and most OSes), you're sorted into user groups, and users.

For example:
The Admin group, which lets you delete the hard drive.
The Gamer group, which only lets you play video games, create save files, etc.

The Gamer group doesn't have permissions to delete the hard drive.

It's a very basic and powerful concept of giving limited permissions.

On linux servers, you may stumble on a situation where you can't modify a file.

Maybe only the `web` group can modify files.

Are you in it?

In your command line:

These commands will help you figure out what group you're in, and if you belong to the group that can modify files.

```bash
$ groups
$ groups nginx
$ grep 'web' in /etc/group
```

