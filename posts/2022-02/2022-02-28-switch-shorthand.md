---
title: TIL of this switch shorthand
date: 2022-02-28
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Using a switch statement to fire a function? Nah, just fire it."
layout: layouts/post.njk
---
This snippet is so freakin' awesome.

Say you had a switch statement fire a function.

```js
// Longhand
switch (data) {
  case 1:
    test1();
  break;

  case 2:
    test2();
  break;

  case 3:
    test();
  break;
  // And so on...
}

// Shorthand
var data = {
  1: test1,
  2: test2,
  3: test
};

data[anything] && data[anything]();
```

The shorthand does the following:

1. Check if the value exists in the object
2. then fires the value as a function `data[anything]()`

via [Blessing Hirwa's 17 Javascript optimization tips to know in 2021](https://dev.to/blessingartcreator/17-javascript-optimization-tips-3gil)
