---
title: TIL the word Sanitize is too overloaded
date: 2022-02-11
published: true
tags: ['xss', 'security', 'language', 'developer']
series: false
canonical_url: false
description: "Sanitize means too different things, depending on context. Use Filtering, Escaping, Validation, or Protect instead."
layout: layouts/post.njk
---

In a quest to clean up some potential XSS opportunities on a website, I kept hitting into the 'Sanitize your inputs' rhetoric.

I agree with this post [Sanitize Your Inputs? - Kevin Smith](https://kevinsmith.io/sanitize-your-inputs/), that the language is overloaded.

His proposal:

Instead of Sanitize --

IF YOU MEAN: "removing undesirable data while letting the good stuff through?"

RECOMMENDATION: "filtering"

---

IF YOU MEAN: "converting potentially harmful data into a harmless form"

RECOMMENDATION: “Escaping” user-supplied data — and making sure it only happens on output — is the way to go.

---

IF YOU MEAN: Flat-out rejecting a request when any invalid data is detected?

RECOMMENDATION: Opt for “validation” instead.

---

IF YOU MEAN: To protect the database from malicious input?

RECOMMENDATION: Remember that the only reliable solution is using prepared statements.

---




