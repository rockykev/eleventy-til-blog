---
title: TIL a easier way to check for multiple OR statements
date: 2022-02-19
published: true
tags: ['logic', 'booleans']
series: false
canonical_url: false
description: "Essentially, it's turning a multiple conditional into a array, then array to see if it matches."
layout: layouts/post.njk
---


I nerd out about this stuff.

Essentially, it's turning a multiple conditional into a array, then array to see if it matches.

```js
// longhand
if (x === 'abc' || x === 'def' || x === 'ghi' || x ==='jkl') {
    //logic
}

//shorthand
if (['abc', 'def', 'ghi', 'jkl'].includes(x)) {
   //logic
}

```

via [Blessing Hirwa](https://dev.to/blessingartcreator/17-javascript-optimization-tips-3gil)
