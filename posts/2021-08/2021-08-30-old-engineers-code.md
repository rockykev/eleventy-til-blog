---
title: TIL how the old generation of engineers thought about their code
date: 2021-08-30
published: true
tags: ['quotes', 'book', 'history']
series: false
canonical_url: false
description: "At it's core, all engineering was about making trade offs between the perfect and the workable."
layout: layouts/post.njk
---


This quote from the book [Where Wizards Stay Up Late: The Origins Of The Internet](https://www.amazon.com/Where-Wizards-Stay-Up-Late/dp/0684832674)

Chapter 4 - Head down in the bits

> "They were engineers and pragmatists... all of their lives, they had build things. They have connected wires, and made things real. Their ethos was utilitarian. At it's core, all engineering was about making trade offs between the perfect and the workable."
