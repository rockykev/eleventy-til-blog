---
title: TIL a clean way to refactor if/else statements
date: 2021-08-25
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Not using if-statements gets you closer to the code-as-data concept"
layout: layouts/post.njk
---

Not using if-statements gets you closer to the code-as-data concept, which opens the door for unique capabilities like modifying the code as it is being executed!

Here is a solution using an if-statement:

```js
const weekendOrWeekday = (inputDate) => {
  const day = inputDate.getDay();
  if (day === 0 || day === 6) {
    return 'weekend';
  }

  return 'weekday';
  // Or, for ternary fans:
  // return (day === 0 || day === 6) ? 'weekend' : 'weekday';
};
console.log(weekendOrWeekday(new Date()));

```

Here is a solution without if-statements:

```js
const weekendOrWeekday = (inputDate) => {
  const day = inputDate.getDay();
  return weekendOrWeekday.labels[day] ||
         weekendOrWeekday.labels['default'];
};
weekendOrWeekday.labels = {
  0: 'weekend',
  6: 'weekend',
  default: 'weekday'
};
console.log(weekendOrWeekday(new Date()));

```


VIA
[Coding Tip: Try to Code Without If-statements](https://medium.com/edge-coders/coding-tip-try-to-code-without-if-statements-d06799eed231)
