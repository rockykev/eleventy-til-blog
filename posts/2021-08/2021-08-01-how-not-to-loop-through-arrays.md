---
title: TIL how NOT to loop through arrays
date: 2021-08-01
published: true
tags: ['javascript', 'arrays']
series: false
canonical_url: false
description: "Don't use foreach for these use cases"
layout: layouts/post.njk
---


## Iterating an array to build another one
If you are iterating an array to build another array from it, you should use map. I've seen this anti-pattern so many times.

```js
// Don't do this:
const numbers = [1,2,3,4,5];
let doubled = [];
numbers.forEach((n, i) => { doubled[i] = n * 2 });


// Do this:
const numbers = [1,2,3,4,5];
const doubled = numbers.map(n => n * 2);
```

## Summing the array of numbers
You want to sum an array of numbers, you should use the reduce method.

```js
const numbers = [1,2,3,4,5];

// Don't do this
const sum = 0;
numbers.forEach(num => {
  sum += num;
});

// Instead
const sum = numbers.reduce((total, n) => total + n, 0);
```

Via https://stackoverflow.com/a/3010848/4096078
