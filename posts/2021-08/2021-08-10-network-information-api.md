---
title: TIL the Network Information API
date: 2021-08-10
published: true
tags: ['javascript', 'mobile']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

The Network Information API provides information about the system's connection in terms of general connection type (e.g., 'wifi', 'cellular', etc.).

As of 2021: This is experimental technology. Check [https://caniuse.com/netinfo](https://caniuse.com/netinfo)

Currently, it's implemented in mobile browsers.

`navigator.connection.downlink` can be used to select high definition content or low definition content based on the user's connection. You can easily determine the user's internet speed from JavaScript code using an object navigator.

MDN:
https://developer.mozilla.org/en-US/docs/Web/API/Network_Information_API
