---
title: TIL when to use local storage vs session storage
date: 2021-08-02
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "We already have the `local storage`, why would we want to use the `session storage`? "
layout: layouts/post.njk
---


Session Storage:
* It's tab specific. So opening up multiple tabs will create new sessions.
* It can only store strings. Other data types must be serialized before used.


We already have the `local storage`, why would we want to use the `session storage`?

1. Developers can use it to improves security, since the data will be deleted after the tab/ browser is closed (therefore, no third parties can access that data afterwards, unlike with local storage).

2. When we want the current state of an UI to be session specific (say the user changed the page theme from light to dark but the next time they access the website or open another tab, we want them to see the initial state of the page).

3. Any time you don't want the data to persist beyond a session, you should use session storage.


via https://dev.to/arikaturika/javascript-session-storage-beginner-s-guide-1i5e
