---
title: TIL functions should only do one thing
date: 2021-08-21
published: true
tags: ['cleancode', 'javascript']
series: false
canonical_url: false
description: "The most important rule of programming is - break your code into tiny bite-sized pieces. That makes it easier to compose, test, and add it."
layout: layouts/post.njk
---

The most important rule of programming is - break your code into tiny bite-sized pieces. That makes it easier to compose, test, and add it.

Bad:
```js
function emailClients(clients) {
  clients.forEach(client => {
    const clientRecord = database.lookup(client);
    if (clientRecord.isActive()) {
      email(client);
    }
  });
}
```

Good:

```js
function emailActiveClients(clients) {
  clients.filter(isActiveClient).forEach(email);
}

function isActiveClient(client) {
  const clientRecord = database.lookup(client);
  return clientRecord.isActive();
}
```

The `filter()` method creates a new array with all elements that pass the test implemented by the provided function.

`clients.filter(isActiveClient)` fires that callback function `isActiveClient`, passing in the `clients` array into it, and then returns only the successful ones.

Then, it runs the `forEach(email);` to finally send a email to those in that new array.

In a future update, if your boss considers all Active Clients to have the tag 'active' in the database, then that can be quickly updated in the `isActiveClient()` function you created.

Via [Clean Code Javascript](https://github.com/ryanmcdermott/clean-code-javascript#functions-should-do-one-thing)
