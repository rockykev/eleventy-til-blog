---
title: TIL this bash script
date: 2021-08-05
published: true
tags: ['bash', 'dev']
series: false
canonical_url: false
description: "I share this bash script because it's really easy to read, and a reminder that all langauges are kinda built the same."
layout: layouts/post.njk
---


I share this not because I'm trying to master bash, or I think this is a magical piece of code (which I mean, it's pretty cool).

I share it because it's really easy to read, and a reminder that all langauges are kinda built the same.

What the script does is:

Find all files that have `*.webp`.
Starting with the first `file`...
Check if they have either a `.jpg`, `.jpeg`, `.png`, `gif`
If it does, then move `mv` (which will also rename it).

## How to use it:

1. Save this script somewhere, like `replace-in-folder.sh`
```bash
#!/bin/bash
cd $1
for file in *.webp
do
  if test -f "${file%.webp}.jpg"
  then
    echo "${file%.webp}.jpg" EXISTS. Moving "$file" to "${file%.webp}.jpg.webp"
    mv "$file" "${file%.webp}.jpg.webp"
  fi

  if test -f "${file%.webp}.jpeg"
  then
    echo "${file%.webp}.jpeg" EXISTS. Moving "$file" to "${file%.webp}.jpeg.webp"
    mv "$file" "${file%.webp}.jpeg.webp"
  fi

  if test -f "${file%.webp}.png"
  then
    echo "${file%.webp}.png" EXISTS. Moving "$file" to "${file%.webp}.png.webp"
    mv "$file" "${file%.webp}.png.webp"
  fi

  if test -f "${file%.webp}.gif"
  then
    echo "${file%.webp}.gif" EXISTS. Moving "$file" to "${file%.webp}.gif.webp"
    mv "$file" "${file%.webp}.gif.webp"
  fi
done

```

2. Then in your command line, you run it:
```
find . -type d -exec ./replace-in-folder.sh {} \;
```

Via:
https://web.archive.org/web/20210814220346/https://help.shortpixel.com/article/277-how-to-rename-my-webp-images-from-single-to-double-extension


