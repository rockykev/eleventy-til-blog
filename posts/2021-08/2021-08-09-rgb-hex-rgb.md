---
title: TIL how to convert RGB to HEX to RGB again
date: 2021-08-09
published: true
tags: ['javascript', 'math']
series: false
canonical_url: false
description: "It's all just math! AAAAH!"
layout: layouts/post.njk
---


It's all just math! AAAAH!

Here are two helper functions to do that:

```js
const rgb2hex = ([r, g, b]) =>
  '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).substr(1);

const hex2rgb = hex =>
  [1, 3, 5].map((h) => parseInt(hex.substring(h, h + 2), 16));


rgb2hex([76, 11, 181]);
// #4c0bb5

hex2rgb("#4c0bb5");
// [76, 11, 181]
```

Via [11 JavaScript Tips and Tricks to Code Like A Superhero (Vol.2)](https://dev.to/orkhanjafarovr/11-javascript-tips-and-tricks-to-code-like-a-superhero-vol-2-mp6)
