---
title: TIL the cost of spaces on TypeScript compile
date: 2023-03-15
published: false
tags: ["prematureoptimization", "typescript"]
series: false
canonical_url: false
description: "One nice surprise we found from using esbuild was that on-disk size was reduced by more than we expected. It turns out that a big reason for this is that esbuild uses 2 spaces for indentation in output instead of the 4 spaces that TypeScript uses. When gzipping, the difference is very small; but on disk, we saved a considerable amount."
layout: layouts/post.njk
---


First, this isn't a war with "spaces vs tabs". I don't care about that bikeshedding.

Second, this is from the [Typescript's migration to ESModules blogpost](https://devblogs.microsoft.com/typescript/typescripts-migration-to-modules/).

tl;dr - Node now supports Modules (ES Modules). So Typescript is switching from CommonJS to the natively supported stuff.

MDN on Modules: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules


Anyways, the snippet:

> One nice surprise we found from using esbuild was that on-disk size was reduced by more than we expected. It turns out that a big reason for this is that esbuild uses 2 spaces for indentation in output instead of the 4 spaces that TypeScript uses. When gzipping, the difference is very small; but on disk, we saved a considerable amount.


There's no actions for anyone to take. I just thought it was fascinating.
