---
title: TIL fancy methods to transform Javascript Objects
date: 2023-03-26
published: true
tags: ["objects", "javascript", "mdn"]
series: false
canonical_url: false
description: "You can use Object.entries(), Object.keys(), Object.fromEntries()..."
layout: layouts/post.njk
---

We got a bunch of fancy methods to transform objects.

## Objects and Arrays

`Object.entries(originalObject)`
This object method takes an object and returns a new 2-dimensional array with each nested array containing the original object’s key and value as string.

```js
const fruitObject = {
  'banana': 'yellow',
  'strawberry': 'red',
  'tangerine': 'orange'
};

console.log(Object.entries(fruitObject));
// [["banana", "yellow"], ["strawberry", "red"], ["tangerine", "orange"]]
```

`Object.fromEntries(anIterable)`

```js
const fruitArray = [
  ['banana': 'yellow'],
  ['strawberry': 'red'],
  ['tangerine': 'orange'],
];

console.log(Object.fromEntries(fruitArray));
// {  banana: 'yellow', strawberry: 'red', tangerine: 'orange' }
```


## Object Keys

`Object.keys(anObject)` & `Object.values(anObject)`
This object method accepts an object as an argument and returns an array containing the object’s keys as elements.

```js
const fruitObject = {
  'banana': 'yellow',
  'strawberry': 'red',
  'tangerine': 'orange'
};

console.log(Object.entries(fruitObject));
// ["banana", "strawberry", "tangerine"]

console.log(Object.values(fruitObject));
// ["yellow", "red", "orange"]
```



via [The Coolest JavaScript Features from the Last 5 Years](https://dev.to/ppiippaa/some-cool-javascript-features-from-the-last-5-years-4alp)
