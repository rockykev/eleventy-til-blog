---
title: TIL Using pseudo-classes in your querySelector!
date: 2023-03-02
published: true
tags: ["css", "javascript"]
series: false
canonical_url: false
description: "let notTuna = document.querySelectorAll('.sandwich:not(.tuna)')"
layout: layouts/post.njk
---


I didn't know this, but you can use pseudo-classes in your querySelector!

> Let’s say you want to ignore buttons with the `.tuna` class. You can use the `:not()` pseudo-class in your selector string, like this…

```js
let notTuna = document.querySelectorAll('.sandwich:not(.tuna)');
```

via [](https://gomakethings.com/the-javascript-selector-methods-that-changed-everything/)
