---
title: TIL defining your video game in hooks
date: 2023-03-27
published: true
tags: ["videogame", "postmordem"]
series: false
canonical_url: false
description: "I like to think of a game’s potential in terms of 'hooks.' By this, I’m referring to the inherent momentum within a title to hook a buyer. I was just looking at the data on Steam, and there were 10,963 new game titles released in 2022. My strong suspicion is that most of them never generated a dime in revenue. Without some hook for your game, no one will ever play it, much less buy it."
layout: layouts/post.njk
---


I was reading this post [The Making of Colossal Cave – Part 1](https://www.colossalcave3d.com/2023/03/09/the-making-of-colossal-cave-part-1/), a point-and-click re-imagining of the original game.

Here is on Steam: https://store.steampowered.com/app/2215540/Colossal_Cave/

It's at 37 positive reviews, and about 0-20000 owners.

Very sad numbers unfortunately, where good games hit about 10,000 reviews

One piece of advice from the devs:

> I like to think of a game’s potential in terms of "hooks." By this, I’m referring to the inherent momentum within a title to hook a buyer. I was just looking at the data on Steam, and there were 10,963 new game titles released in 2022. My strong suspicion is that most of them never generated a dime in revenue. Without some hook for your game, no one will ever play it, much less buy it.

