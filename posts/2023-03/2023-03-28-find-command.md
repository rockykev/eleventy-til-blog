---
title: TIL using the find command
date: 2023-03-28
published: true
tags: ["terminal", "search", "command"]
series: false
canonical_url: false
description: "find [path] -user [username] [options]"
layout: layouts/post.njk
---

I can always improve my bash skills.

## Finding files

```bash
## syntax
$ find [path] -user [username] [options]


## example
$ find ./my-documents/ -user Rocky
```

## Advanced searching

```bash

## directories or files

$ find ./my-documents/ -type d -user Rocky ## directories
$ find ./my-documents/ -type f -user Rocky ## files

## Permissions

$ find ./my-documents/ -perm 755 ##  All files/directories inside subdirectory with permission 755

## Using Not
$ find ./my-documents/ -not -user Rocky ## All files/directories inside subdirectory not owned by Rocky

## Finding based on file extension
$ find . -type f -not -name "*.html" ## All files not ending .html extension

## Based on group
$ find . -group Developers ## files that are owned by the "Developers" group

## Based on changed within X time
$ find . -amin -30
```



via [How to Search Files Effectively in the Linux Terminal – Advanced Guide](https://www.freecodecamp.org/news/how-to-search-files-effectively-in-linux/?utm_source=pocket_reader)
