---
title: TIL Logical Properties
date: 2023-03-11
published: false
tags: ["css", "Internationalization"]
series: false
canonical_url: false
description: "For that sweet sweet Internationalization you want to avoid directional words like 'left', 'right', 'top', and 'bottom'."
layout: layouts/post.njk
---

> Logical, flow relative properties and values are linked to the flow of text, rather than the physical shape of the screen. Learn how to take advantage of this newer approach to CSS.

> Adjust your styles from physical properties to logical ones like padding-inline, margin-inline, inset-inline, and now the browser will do the adjusting work.

The benefit:


For that sweet sweet [Internationalization](https://web.dev/learn/design/internationalization/), you want to avoid directional words like "left", "right", "top", and "bottom".

> English is written from left to right and top to bottom, but not all languages are written this way. Some languages like Arabic and Hebrew read from right to left, and some Japanese typefaces read vertically instead of horizontally. To accommodate these writing modes, logical properties were introduced in CSS.

These shorthands for `*-start` and `*-end` are really nice.

```css

/* Don't */
label {
  margin-right: 0.5em;
}

/* Do */
label {
  margin-inline-end: 0.5em;
}
```

Via:

* https://developer.mozilla.org/en-US/docs/Web/CSS/inset-inline
* https://developer.mozilla.org/en-US/docs/Web/CSS/inset-block

* https://developer.mozilla.org/en-US/docs/Web/CSS/margin-inline
* https://developer.mozilla.org/en-US/docs/Web/CSS/margin-block

* https://developer.mozilla.org/en-US/docs/Web/CSS/padding-inline
* https://developer.mozilla.org/en-US/docs/Web/CSS/padding-block
