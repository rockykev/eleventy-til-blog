---
title: TIL how to JUICE up your UI
date: 2023-03-04
published: true
tags: ["videogame", "image"]
series: false
canonical_url: false
description: "The delightful chimes sound that plays when Mario collects a mushroom. The 1UP text that appears is essential. Communicating the player gained an extra life. The sound is the Juice."
layout: layouts/post.njk
---


I always bring up **JUICE** when I talk about to add more "feels" to when a user interacts with technology.

> the delightful chimes sound that plays when Mario collects a mushroom. The 1UP text that appears is essential. Communicating the player gained an extra life. The sound is the Juice.

Also, this delete button is fantastic.

(Not interactive, [visit the blog](https://garden.bradwoods.io/notes/design/juice?utm_source=pocket_reader))
![](2023-03-04_0.png)


I highly recommend reading this post: https://garden.bradwoods.io/notes/design/juice

and the talk that inspired me to use JUICE. https://www.youtube.com/watch?v=Fy0aCDmgnxg

