---
title: TIL why google recommends you avoid document.write()
date: 2023-03-03
published: true
tags: ["performance", "javascript", "browser", "corewebvital"]
series: false
canonical_url: false
description: "By injecting it with code, it bypasses the preload scanner. This means that the browser can’t request this file until it’s actually run the '<script>' block that inserts it, which is very much just-in-time (and too late)."
layout: layouts/post.njk
---




If you’ve ever run a Lighthouse test before, there’s a high chance you’ve seen the audit Avoid document.write():

via [Why Not document.write()?](https://csswizardry.com/2023/01/why-not-document-write/)

## document.write() Hides Files From the Preload Scanner

> The Preload Scanner is a secondary, inert, download-only parser that’s responsible for running down the HTML and asynchronously requesting any available subresources it might find, chiefly anything contained in src or href attributes, including images, scripts, stylesheets, etc. As a result, files fetched via the Preload Scanner are parallelised, and can be downloaded asynchronously alongside other (potentially synchronous) resources.


```html
<script>
  document.write('<script src=file.js><\/script>')
</script>
```

This means that the browser can’t request this file until it’s actually run the `<script>` block that inserts it, which is very much just-in-time (and too late).
