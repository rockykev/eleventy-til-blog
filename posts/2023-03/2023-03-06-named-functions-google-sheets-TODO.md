---
title: TIL Google Sheets has named functions
date: 2023-03-06
published: true
tags: ["function", "spreadsheet"]
series: false
canonical_url: false
description: "It's a way for you to make functions using built-in formulas."
layout: layouts/post.njk
---


Today I learned that Google Sheets has Named Functions!

![](https://i.imgur.com/X90PXMI.png)


It's a way for you to make functions using built-in formulas.

FOr example: Creating a function called STARCHART


`REPT(CHAR(11088),count)`

You have a `count` argument.

![](https://i.imgur.com/neoqPmQ.png)


via [A Guide To Named Functions In Google Sheets](https://www.benlcollins.com/spreadsheets/named-functions/)

more info: https://support.google.com/docs/answer/3094134?hl=en


