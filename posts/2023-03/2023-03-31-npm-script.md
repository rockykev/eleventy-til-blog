---
title: TIL what is npm Script
date: 2023-03-31
published: true
tags: ["javascript", "node"]
series: false
canonical_url: false
description: "Despite their high usage they are not particularly well optimized and add about 400ms of overhead. In this article we were able to bring that down to ~22ms."
layout: layouts/post.njk
---

When we run a npm script, what happens?

In the blog post [Speeding up the JavaScript ecosystem - npm scripts](https://marvinh.dev/blog/speeding-up-javascript-ecosystem-part-4/), the Author Marvin Hagemeister explains what's happening under the hood.

But also this amazing line:

> Despite their high usage they are not particularly well optimized and add about 400ms of overhead. In this article we were able to bring that down to ~22ms.

Some notes:

## Only use modules when they're needed

![](2023-03-31_0.png)

## F'en Classes.

> A recurring problem in various js tools is that they are composed of a couple of big classes that pull in everything instead of only the code you need. Those classes always start out small and with good intentions to be lean, but somehow they just get bigger and bigger.

I love this quote: "You wanted a banana but what you got was a gorilla holding the banana and the entire jungle." – Joe Armstrong


