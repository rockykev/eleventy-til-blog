---
title: TIL Static Blocks vs Dynamic Blocks
date: 2023-03-09
published: true
tags: ["blocks", "wordpress"]
series: false
canonical_url: false
description: "A static block is a piece of content whose markup is known when the page is saved. The block saves its content and markup directly in the post content. A dynamic block is a piece of content whose markup and exact content are not known when the page is saved."
layout: layouts/post.njk
---

Today I learned about Static Blocks and Dynamic Blocks.

The entire post is here: https://developer.wordpress.org/news/2023/02/static-vs-dynamic-blocks-whats-the-difference/



## Static block

The easiest way to explain this is that it's [dumb components/presentational components](https://medium.com/@thejasonfile/dumb-components-and-smart-components-e7b33a698d43). It simply taking data (the content and the settings) that the user enters in WordPress, and displays them.


> A static block is a piece of content whose markup is known when the page is saved. The block saves its content and markup directly in the post content. The Paragraph block is a simple example of a static block.

```
<!-- wp:paragraph -->
<p>If I had a boat, I'd go out on the ocean.</p>
<!-- /wp:paragraph -->
```

**Edge-cases**

> Block validation errors are most commonly caused when a block’s save() function is updated to change the markup produced by the block. Any small change, even to a class name, can trigger a validation error in the Editor.

> A block developer can mitigate these issues by adding a block deprecation to register the change in the block. The block deprecation keeps a record of previous versions of a block’s markup. The Post Editor can then compare a block’s current content with a previous version and (ideally) avoid a validation error.

Block deprecation: https://developer.wordpress.org/block-editor/reference-guides/block-api/block-deprecation/

```
// VERSION 1

<!-- wp:paragraph -->
<p>If I had a boat, I'd go out on the ocean.</p>
<!-- /wp:paragraph -->


// VERSION 2
<!-- wp:paragraph -->
<div><p>If I had a boat, I'd go out on the ocean.</p></div>
<!-- /wp:paragraph -->
```

In version 2, you should have add Block deprecation.

**Advantage**

> This approach is more performant. When a visitor views the page, the content comes from the database. There is no delay in displaying the block since no server-side rendering is required.



## Dynamic block

> A dynamic block is a piece of content whose markup and exact content are not known when the page is saved. This block could contain content that is timely or dependent on changes in other parts of the site. The contents of a dynamic block are expected to change without the intervention of a content editor. As a result, the markup of a dynamic block is rendered on the server-side.

> A simple example is the core Site Title block, which displays the site’s name. This block must be dynamic because its content cannot be known at the time the page is saved. The site title can change at any time via the site settings.

![](https://i.imgur.com/5qG019X.png)

This part is interesting:

> While the JavaScript side handles the editor experience, the PHP side handles the server-side rendering for the front-end markup.

> PHP block registration uses the register_block_type() function, which requires a render method to be defined. This can happen in one of two ways:

1. The registration function includes a render_callback argument.
2. A render property is added to block.json, whose value points to a separate PHP file.


**Advantage**
The markup of a dynamic block is expected to change. This is why that markup is not saved to the database. As a result, that markup is not subject to the Post Editor’s validation. This means that changes to a dynamic block’s markup cannot throw a validation error.


