---
title: TIL Dwarf Fortress Game Design
date: 2023-03-19
published: true
tags: ["gamedev", "gamedesign"]
series: false
canonical_url: false
description: "'Do not design for your experienced players' and some more tips from one of the most complicated games ever"
layout: layouts/post.njk
---


I love reading about game design and compiling tips from the pros.

This one is from the creators of Dwarf Fortress

> The game first went public on August 8, 2006, over 16 years ago. That it's still being worked on after all this time is an accomplishment in itself.

## New Players

Question: Something I wonder about with long-lived, still-popular games like NetHack and Dwarf Fortress is in their designs chasing the knowledgable player. Like, NetHack's quest seems like it's built atop a long sequence of responding to players' exploits, which makes the game more complete, but also makes it harder to get into.

Tarn Adams: We try not to do this, and it's pretty well-entrenched game design wisdom at this point. "Do not design for your experienced players!" etc. Some strategy games, some fighting games, some sports games, some RPGs, all genres probably, fall into this a lot, and they can be almost entirely impenetrable to new people after a while. Which isn't awful if you don't want too many new players and want something more like a club. But despite appearances, up to this point, we've always wanted a lot of people to be able to tell stories with our game, haha.

## Not using hitpoints

Question: One of the most obvious is the ubiquity of hit points as a measure of creature health. I remember you saying before that you disliked hit points as a concept and I find there are a lot of unexamined assumptions around that whole idea. Dwarf Fortress eschews them, replacing them with a wound status for each of an entity's major body parts. The result, it seems, is creatures that degrade in a meaningful way as they are wounded, and the possibility of even a strong monster getting hit in the head by a lucky blow. Does this line up with your experience? How else does this aspect of the sim affect the game?

Tarn Adams: ...hybrid systems are also interesting—even a straightforward HP + wounds system covers the degradation and lucky shots stuff you mentioned, without getting into some of the other complications. [There are] lots of things to do with damage systems (and spell systems improving on MP etc).

## Optimization Tricks

Question: What kinds of tricks does the simulation use to keep things moving at a playable rate? Are there any optimization tricks you might pass along?

Tarn Adams:  somebody told me about a project that kept the closed node list for pathfinding as a data object, storing each of the nodes. This is how the algorithms are described a lot of times in examples, but for a large map that's the kind of thing that just gets untenable, since it needs to be maintained, and that can get unwieldy or slow depending on how you do it, and it starts to hog resources. If you can just set a flag on a tile or otherwise store whatever values you need in existing structures, it might be faster and leaner. Might! You have to check, since a flag per tile is also expensive in its way.... You run into trouble very quickly if you just throw in some nested loops that are creating/destroying things every frame. Think about how often you actually need to do something and try to do it only that much.

... I've recently found it quite useful to add flags to objects like, say, an item flag called "might contain items"—the vast majority of items don't contain other items, and even the ones with this flag might not contain anything, but it lets me cut out a lot of checks and calls that might otherwise trigger and it has the benefit that I don't have to verify the state perfectly (which can be very expensive). If anything is put in an item, the flag gets set, but the flag doesn't have to be (expensively) checked when items are removed. The flag can be updated if some subsequent function finds the item doesn't in fact have anything inside, but it doesn't have to.

Generally, this type of flag is resilient against a future programmer's lack of attention, which is very handy, since that's often me, haha. It does cost a single bit on all those items that never have things inside, but for the savings in this example, it's well worth it, since the game has so many contents-related checks. You can of course organize your items so that certain never-container items (e.g. boulders) are excluded from checks in advance, and we do a lot of this too, but the flag still helps a lot, and we have others like it.


via [How Tarn Adams upgraded and optimized Dwarf Fortress for its official Steam release](https://www.gamedeveloper.com/programming/how-tarn-adams-upgraded-and-optimized-dwarf-fortress-for-its-official-steam-release)
