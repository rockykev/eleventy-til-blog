---
title: TIL how to build a chrome extension that steals everything
date: 2023-03-23
published: true
tags: ["security", "extension", "pii", "keystroke"]
series: false
canonical_url: false
description: "There's 3 components that will be used - background Service worker, Content script, and popup."
layout: layouts/post.njk
---



Today I learned how to [build a Chrome extension that steals everything](https://mattfrisbie.substack.com/p/spy-chrome-extension)


For the whole repo: https://github.com/msfrisbie/spy-extension


DISCLAIMER: This is educational obviously.

I love the ground rules from the post:

## Ground Rules
* The user shouldn’t be aware that anything is happening behind the scenes.
  * There must be no visual indication that anything is awry.
  * No extra console messages, warnings, or errors.
  * No additional browser warning or permission dialogs.
  * No extra page-level network traffic.
* Once the user agrees to the *ahem* ample permission warnings, that’s the last time they should have to think about the extension’s permissions.

You know those permission prompts from browsers? They look like this.

![](https://i.imgur.com/hcTtPYA.png)

via https://developer.chrome.com/docs/extensions/mv3/permission_warnings/

Welp, looks like they don't help after all.

## Terminology

There's 3 components that will be used.

> Background Service Worker
Event driven. Can be used as a “persistent” container for running JavaScript
Can access all* of the WebExtensions API
Cannot access DOM APIs
Cannot directly access pages

Service worker: https://developer.chrome.com/docs/extensions/mv3/service_workers/

> Popup Page
Only opens after user interaction
Can access all* of the WebExtensions API
Can access DOM APIs
Cannot directly access pages

Popup window: https://developer.chrome.com/docs/extensions/reference/browserAction/

> Content Script
Has direct and full access to all pages and the DOM
Can run JavaScript in page, but in sandboxed runtime
Can only use a subset of the WebExtensions API
Subject to same restrictions as page (CORS, etc)

Content Scripts: https://developer.chrome.com/docs/extensions/mv3/content_scripts/



## Obtaining Global Permissions

> Just for fun, our malicious extension will request all possible permissions. https://developer.chrome.com/docs/extensions/mv3/declare_permissions/ has a list of Chrome extension permissions, and we’ll take the lot.

In the blog post: https://mattfrisbie.substack.com/i/104158781/obtaining-global-permissions

is the entire list of how to set up your `manifest.json`

## Profit

With zero effort, you can get:


**Cookies**
`chrome.cookies.getAll({})` retrieves all the browser’s cookies as an array.

**History**
`chrome.history.search({ text: "" })` retrieves the user’s entire browsing history as an array.

**Screenshots**
`chrome.tabs.captureVisibleTab()` silently captures a screenshot of whatever the user is currently looking at.

**User Navigation and Page Traffic**

The Chrome webNavigation API lets you grab URLs timestamps of the visit.

Nice -- https://developer.chrome.com/docs/extensions/reference/webNavigation/

**Keylogger** & **Capture Input data**
You're using a content script, which can read the body of the page.

That makes reading keystrokes is dead easy.

Passwords. Emails. Why not.

**Clipboard**
Again, content script.

**Geolocation**

And Again! content script!


