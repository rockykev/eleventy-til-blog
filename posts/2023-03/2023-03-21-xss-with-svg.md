---
title: TIL executing a xss using a SVG image
date: 2023-03-21
published: true
tags: ["image", "svg", "security", "xss"]
series: false
canonical_url: false
description: "This user was able to upload a '.svg', that then executed a xss attack to steal local storage data."
layout: layouts/post.njk
---

This user was able to upload a `.svg`, that then executed a xss attack to steal local storage data.

The entire post is here: https://shahjerry33.medium.com/xss-the-localstorage-robbery-d5fbf353c6b0


Issues they discovered:

1. While uploading the file, the extension was not being checked
2. After the file was uploaded, Content-Type validation was not done
3. After the file was sent to the server, it was not being validated by the server


>  This program had an import feature where we can upload document files like .csv or .docx and when we clicked that import function to choose the file from our desktop, it showed "All Supported Types" because we can only import document files. So we changed "All Supported Types" to "All Files" on our desktop and uploaded a file with .svg extension which contained XSS payload in xml script.

> After looking at the authentication flow we noticed that an authentication token is generated on every login and is unique for every account. The token was getting stored in LocalStorage and that token was also protecting the website from CSRF attacks.

> Now the question was how should we steal the token from localstorage ? because if we manage to do so we can probably craft different other attacks. So in our .svg file we added a line to steal a token from the localstorage prompt(alert(localStorage.getItem("<item-name>"))); and uploaded the file. When we visited the path we were successfully able to steal the token from the localstorage.


The payload

```html
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">

<svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg">
<polygon id="triangle" points="0,0.0,50.50,0" fill="#009900" stroke="#004400"/>
<script type="text/javascript">
prompt('XSS-Attack');
prompt(document.domain);
prompt(document.cookie);
prompt(alert(localStorage.getItem("IsvSessionToken")));
</script>
</svg>
```
