---
title: TIL Indirection and Swallowing the error
date: 2023-03-13
published: false
tags: ["programming", "utilities"]
series: false
canonical_url: false
description: "Op shared a tryCatch util that is problematic."
layout: layouts/post.njk
---


From [this post -- Sharing some JS-recipes I've been using in projects](https://www.reddit.com/r/javascript/comments/11p4qx2/sharing_some_jsrecipes_ive_been_using_in_projects/?utm_source=pocket_mylist):


This snippet is problematic:

```js
const tryCatch = (tryExpression) => {
  try {
    const res = tryExpression();
    return { data: res, err: undefined };
  } catch (e) {
    return { err: e, data: undefined };
  }
};

```

[the snippet](https://github.com/chandru89new/js-cookbook/blob/main/try-catch-made-simpler.md)


> I like this callout by dmail06 and anacierdem:

> Not fan of hiding try/catch/finally behind an helper function. Creates indirection on standard js syntax making code magic

> You’ll need to check the error anyways, which is not any different than writing a catch block. Also it allows “swallowing” the error without any warning.

The defense by OP is interesting:

> I think I've found this pattern to be quite useful in preventing app crashes quite a bit (esp when one forgets to try-catch).

Which the rebuttal from PM_ME_YOUR_KNEE_CAPS:

> You dont’t want to prevent app crashes by swallowing all errors because it makes it impossible to debug.


## Indirection

>  It is a powerful technique for decoupling software units by introducing a layer between two existing units/layers, enabling the introduction of new behaviour, without directly impairing the existing units.

via https://medium.com/@nmckinnonblog/indirection-fba1857630e2

Aka a "Wrapper"!

I love wrappers, aka 'abstraction'!

Indirection isn't bad though. The root issue is bad abstraction.

These comment has a good explainer about when indirection gets weird: https://news.ycombinator.com/item?id=20259850


## Swallowing the error:

> In this example, the code does nothing with the exception and does not even tell the caller that something failed. I have spent many hours troubleshooting projects with this exact try/catch block and wondering why something that I thought should have worked was not, just to discover that deep down in the call stack it was swallowing the exception. Since the catch block is empty, there is no way to put a breakpoint in the catch block but even if you could, there is no way to get at the exception in the catch block since it was not passed into it.

```java
public void SomeMethod()
{
    try
    {
        //Some Code That Errors
    }
    catch
    {
    }
}

```

via https://digitaldrummerj.me/dont-swallow-the-exceptions/
