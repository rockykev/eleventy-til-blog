---
title: TIL Clear-Site-Data
date: 2023-03-17
published: false
tags: ["mdn", "cache"]
series: false
canonical_url: false
description: "The Clear-Site-Data header clears browsing data (cookies, storage, cache) associated with the requesting website. It allows web developers to have more control over the data stored by a client browser for their origins."
layout: layouts/post.njk
---

Today I learned about the `Clear-Site-Data` header.


> The Clear-Site-Data header clears browsing data (cookies, storage, cache) associated with the requesting website. It allows web developers to have more control over the data stored by a client browser for their origins.


```
// Single directive
Clear-Site-Data: "cache"

// Multiple directives (signing out of a website)
Clear-Site-Data: "cache", "cookies", "storage", "executionContexts"

// Wild card
Clear-Site-Data: "*"
```

Safari isn't fully supported yet. But it will!

Via MDN: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Clear-Site-Data
