---
title: TIL the alternate keyword
date: 2023-03-18
published: true
tags: ["stylesheet", "css", "darkmode", "image"]
series: false
canonical_url: false
description: "If 'alternate' appears along with the stylesheet keyword, the linked file is an alternative stylesheet. It won’t be applied to the document, but it will be ready for when we need it."
layout: layouts/post.njk
---

```html
<link rel="stylesheet" href="main-styles.css" />
<link rel="stylesheet alternate" href="dark.css" title="dark mode" />
```

If "alternate" appears along with the **stylesheet** keyword, the linked file is an alternative stylesheet. It won’t be applied to the document, but it will be ready for when we need it.

> In this case, the “dark-styles.css” file is loaded asynchronously and not applied to the document. This way, the styles will be available whenever we want to apply them, without having to wait for network or traffic. Some browsers, like Firefox, allow switching between styles, facilitating the process for users.

![Alternative](2023-03-18_img.png)
