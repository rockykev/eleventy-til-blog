---
title: TIL Play music in a bash script
date: 2023-03-07
published: true
tags: ["music", "bash", "fun"]
series: false
canonical_url: false
description: "On Macs, you have the afplay command. to play some sweet tunes."
layout: layouts/post.njk
---


In adventure games, sound effects clues the player to specific events. You might hear a harp sound when you're close to a secret. Or the music tempo might get fast when you are about to fight a boss.

I wanted to play music for a few seconds before I connect to a specific ssh server.

Maybe to hype me up. Or to remind me that I'm about to do something terrible.

On Macs, you have the [afplay](https://ss64.com/osx/afplay.html) command.

On Linux, you would use play and sox.

I couldn't get it to work on WSL. Sorry about that.

**Step 1**
`nano ~/.bashrc`

**Step 2**
In your bash script:

```bash

play_and_ssh() {
    # Set the path of the music file you want to play
    MUSIC_FILE_PATH="/music/Montell Jordan - This Is How We Do It.mp3"

    # Play the music file for 10 seconds using the 'afplay' audio player
    afplay "$MUSIC_FILE_PATH" & sleep 10 && kill $!

    # Set the server's IP address
    SERVER_IP="your_server_ip_address"

    # SSH into the server
    ssh user@$SERVER_IP
}

alias sshMontell=play_and_ssh

```

**Step 3**
`source ~/.bashrc`

to reload your script.

