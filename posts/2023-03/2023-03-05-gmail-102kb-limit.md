---
title: TIL  Gmail has a 102KB size-limit for HTML
date: 2023-03-05
published: true
tags: ["email", "html"]
series: false
canonical_url: false
description: PLACEHOLDER
layout: layouts/post.njk
---

Today I learned that Gmail has a 102KB size-limit for HTML.

> After that it will truncate the email with the words “[Message clipped]” and a link to view the rest of the email. Needless to say, it’s not a great user experience and most users probably won’t click the link, so some of your email’s content won’t be seen by many recipients. The markup for an HTML table layout is massively bulky, and moving beyond them will help avoid messages getting clipped in Gmail. Meanwhile, some contemporary email code is such a dense mess of nested tables it can crash Outlook.

via https://fullystacked.net/posts/modern-html-email/


How to optimize it:

* Minify it
* remove bloat (duh)

via https://www.emailonacid.com/blog/article/email-development/gmail-email-clipping/
