---
title: TIL about the insides of SVGs and the text element
date: 2021-04-12
published: true
tags: ['html']
series: false
canonical_url: false
description: "SVGs are like mini-html files!"
layout: layouts/post.njk
---

I had a SVG file that had funky text rendering.
The text looked different for everyone.

That's when I noticed that there was a `<text></text>` inside the SVG, that was using a font-family!
[MDN Reference](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/text)


To fix that, I opened up [photopea.com](http://photopea.com/) and one of the export options is to save text as vector graphics. That fixed things. But out of curiousity, I wanted to know how a SVG works.

And that's when I learned:
SVGs are like mini-html files!


If you open up a SVG, you'll get something like this:

```html
<svg width="300" height="200">
  <polygon points="100,10 40,198 190,78 10,78 160,198"
  style="fill:lime;stroke:purple;stroke-width:5;fill-rule:evenodd;" />
</svg>
```

Let's remove the inline style, and you get this:

```html
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 300 200" width="300" height="200">
  <style>
    polygon {
      transition: fill .1s ease-out, opacity .1s ease-out;
      fill: lime;
      stroke: purple;
      stroke-width: 5;
      fill-rule: evenodd;
    }
  </style>
  <polygon points="100,10 40,198 190,78 10,78 160,198" />
</svg>
```

Wait a minute, that looks really like html!

Darn right it does.


> NOTE: DON'T EVER DO THIS. This is terrible for debugging.


This is also valid!
I set it so if the window size is a certain width, change the color from `lime` to `orange`.
```html
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 300 200" width="300" height="200">
  <style>
    polygon {
      transition: fill .1s ease-out, opacity .1s ease-out;
      fill: lime;
      stroke: purple;
      stroke-width: 5;
      fill-rule: evenodd;
    }

    @media all and (max-width: 1000px) {
      polygon {
        fill: orange;
      }
    }
  </style>
  <polygon points="100,10 40,198 190,78 10,78 160,198" />
</svg>

```

So how do SVG animations works?

It's about targetting the SVG elements, and manipulating them with CSS, just like we did the media query!

REFERENCE:
Don't follow their example at all. But, it's super fascinating.
https://tympanus.net/codrops/2014/08/19/making-svgs-responsive-with-css/


