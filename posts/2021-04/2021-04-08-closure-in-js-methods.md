---
title: TIL a Closure example with class-like methods
date: 2021-04-08
published: true
tags: ['javascript', 'advanced']
series: true
canonical_url: false
description: "Closure in JS: Nesting functions"
layout: layouts/post.njk
---

1. JavaScript allows for the nesting of functions.

```js
const grievous = function a() {
  function b() {
    return "General Kenobi! You are a bold one.";
  }
  return b();
}

console.log(grievous());
```

2. It also allows inner functions full access to all the variables and functions defined the outer function (and all other variables and functions that the outer function has access to).

```js
const grievous = function a() {

  // outer
  const quote = "General Kenobi! You are a bold one.";

  function b() {
    // inner
    const quote = "Back away! I will deal with this Jedi slime myself.";
    return quote;
  }
  return b();
}

console.log(grievous());
```

3. However, the outer function does not have access to the variables and functions defined inside the inner function. A closure is created when the inner function is somehow made available to any scope outside the outer function.


```js
let starWarsCharacter = function(name) {
  let title;

  return {
    // SETTERS
    setName: function(newName) {
      name = newName;
    },
    setTitle: function(newTitle) {
      title = newTitle;
    },

    // GETTERS
    getName: function() {
      return name;
    },
    getTitle: function() {
      return title;
    },

  }
}
```

```js
// define the hero
var hero = starWarsCharacter("Luke");
hero.setTitle('Jedi Knight');
console.log(hero.getName());           // Luke
console.log(hero.getTitle());     // jedi Knight

// change the hero
hero.setName("Rey");
console.log(hero.getName());           // Rey
console.log(hero.getTitle());     // Jedi Knight
```

Notice that function 'saves' the occupation.

REFERENCE:
https://stackoverflow.com/a/31914232/4096078

SERIES:
* [Closure The Basics](/posts/2021-04/2021-04-08-closure-in-js)
* [Closure Example with class-like methods](/posts/2021-04/2021-04-08-closure-in-js-method)
* [Closure Example with private data](/posts/2021-04/2021-04-09-closure-in-js-private)
* [Closure Example as a backpack](/posts/2021-04/2021-04-10-closure-as-a-backpack)
