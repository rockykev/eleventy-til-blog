---
title: TIL Alt text for images that are important
date: 2021-04-25
published: true
tags: ['accessibility', 'html']
series: true
canonical_url: false
description: "Alt text is required if the image is a critical part of the content."
layout: layouts/post.njk
---

Via the HTML Spec docs themselves:

Alt text is required if the image is a critical part of the content. This could be the case, for instance, on a page that is part of a photo gallery. The image is the whole point of the page containing it.

## General Use-cases

When it is possible for detailed alternative text to be provided, for example if the image is part of a series of screenshots in a magazine review, or part of a comic strip, or is a photograph in a blog entry about that photograph, text that can serve as a substitute for the image must be given as the contents of the alt attribute.

```html
<figure>
 <img src="KDE%20Light%20desktop.png"
      alt="The desktop is blue, with icons along the left hand side in
           two columns, reading System, Home, K-Mail, etc. A window is
           open showing that menus wrap to a second line if they
           cannot fit in the window. The window has a list of icons
           along the top, with an address bar below it, a list of
           icons for tabs along the left edge, a status bar on the
           bottom, and two panes in the middle. The desktop has a bar
           at the bottom of the screen with a few buttons, a pager, a
           list of open applications, and a clock.">
 <figcaption>Screenshot of a KDE desktop.</figcaption>
</figure>
```

## If it's difficult to describe
Sometimes there simply is no text that can do justice to an image. For example, there is little that can be said to usefully describe a Rorschach inkblot test. However, a description, even if brief, is still better than nothing:

```html
<figure>
  <img src="/commons/a/a7/Rorschach1.jpg" alt="A shape with left-right
  symmetry with indistinct edges, with a small gap in the center, two
  larger gaps offset slightly from the center, with two similar gaps
  under them. The outline is wider in the top half than the bottom
  half, with the sides extending upwards higher than the center, and
  the center extending below the sides.">

  <figcaption>A black outline of the first of the ten cards
  in the Rorschach inkblot test.</figcaption>
</figure>
```

REFERENCE:
https://html.spec.whatwg.org/#a-key-part-of-the-content
