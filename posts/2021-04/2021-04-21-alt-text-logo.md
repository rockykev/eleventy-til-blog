---
title: TIL alt text for logos, the right way
date: 2021-04-21
published: true
tags: ['accessibility', 'html']
series: false
canonical_url: false
description: "Putting alt text on logos, the right way"
layout: layouts/post.njk
---


Via the HTML Spec docs themselves:

## Putting Alt Text for logos


**For your logo:**
OPTION 1: The icon is supplemental to a text label conveying the same meaning. In those cases, the alt attribute must be present but must be empty.

```html
<nav>
 <p><a href="/help/"><img src="/icons/help.png" alt=""> Help</a></p>
 <p><a href="/configure/"><img src="/icons/configuration.png" alt="">Configuration Tools</a></p>
</nav>
```

Notice the icon is next to text, which says the same thing.

OPTION 2: The icon has no text next to it describing what it means; the icon is supposed to be self-explanatory. In those cases, an equivalent textual label must be given in the alt attribute.

```html
  <h1>Ratatouille wins <i>Best Movie of the Year</i> award</h1>
  <p><img src="movies.png" alt="Movies"></p>

  <h1>Latest TWiT episode is online</h1>
  <p><img src="podcasts.png" alt="Podcasts"></p>
```

OPTION 3: If the logo is being used to represent the entity, e.g. as a page heading, the alt attribute must contain the name of the entity being represented by the logo. The alt attribute must not contain text like the word "logo", as it is not the fact that it is a logo that is being conveyed, it's the entity itself.

```html

  <h1><img src="XYZ.gif" alt="The XYZ company"></h1>
  <p>We have recently been looking at buying the <img src="alpha.gif" alt=""> ΑΒΓ company, a small Greek company
  specializing in our type of product.</p>

```

REFERENCES:
https://html.spec.whatwg.org/#a-short-phrase-or-label-with-an-alternative-graphical-representation:-icons,-logos
