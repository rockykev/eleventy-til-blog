---
title: TIL Immediately Invoked Function Expression
date: 2021-04-11
published: true
tags: ['javascript', 'advanced']
series: false
canonical_url: false
description: "This is a design pattern where you create a function expression that is immediately executed."
layout: layouts/post.njk
---

This is a design pattern where you create a function expression that is immediately executed.

```js
(function () {
    const jedi = "Qui-gon Jinn";
    console.log(jedi);
})();

// immediately fires Qui-gon Jinn
// note: jedi is not referencable
```

REFERENCE:
https://developer.mozilla.org/en-US/docs/Glossary/IIFE
