---
title: TIL Details element that creates a accordion
date: 2021-04-19
published: true
tags: ['html', 'codepen']
series: false
canonical_url: false
description: "HTML Clickable details element!"
layout: layouts/post.njk
---

HTML Clickable details element!

```html
<details>
  <summary>Click Here to get the user details</summary>
  <table>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Location</th>
            <th>Job</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Adam</td>
            <td>Huston</td>
            <td>UI/UX</td>
        </tr>
  </table>
</details>
```

<p class="codepen" data-height="265" data-theme-id="light" data-default-tab="html,result" data-user="rockykev" data-slug-hash="MWJLxJb" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="HTML only Clickable Details Element">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/MWJLxJb">
  HTML only Clickable Details Element</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

REFERENCE:
https://dev.to/atapas/10-useful-html5-features-you-may-not-be-using-2bk0?utm_source=digest_mailer&utm_medium=email&utm_campaign=digest_email
