---
title: TIL Alt Text for everything else
date: 2021-04-24
published: true
tags: ['accessibility', 'html']
series: true
canonical_url: false
description: "alt text for images with captions, that are decorative, part of image sets"
layout: layouts/post.njk
---

Via the HTML Spec docs themselves:

## If the image has a caption
If the image has a caption, then including alternative text avoids leaving the non-visual user confused as to what the caption refers to.

```html
<p>Ahead of today's referendum, the First Minister of Scotland, Alex Salmond,
wrote an open letter to all registered voters. In it, he admitted that all
countries make mistakes.</p>
<figure>
 <img src="alexsalmond.jpeg"
      alt="A high forehead, cheerful disposition, and dark hair round out Alex Salmond's face.">
 <figcaption> Alex Salmond, SNP. Photo © 2014 PolitiPhoto. </figcaption>
</figure>
```

Don't do this!
```html
<!-- This example is wrong. Do not copy it. -->
<figure>
 <img src="/commons/a/a7/Rorschach1.jpg" alt="A black outline
 of the first of the ten cards in the Rorschach inkblot test.">
 <figcaption>A black outline of the first of the ten cards
 in the Rorschach inkblot test.</figcaption>
</figure>
```


## If the image is purely decorative
If an image is decorative but isn't especially page-specific — for example an image that forms part of a site-wide design scheme — the image should be specified in the site's CSS, not in the markup of the document.

For example: If you are using shape images to separate blocks of content, that should be a CSS element.

## If the image has some relevance

However, a decorative image that isn't discussed by the surrounding text but still has some relevance can be included in a page using the img element. Such images are decorative, but still form part of the content. In these cases, the alt attribute must be present but its value must be the empty string.

For example:
Where the image is purely decorative despite being relevant would include things like a photo of the Black Rock City landscape in a blog post about an event at Burning Man, or an image of a painting inspired by a poem, on a page reciting that poem. The following snippet shows an example of the latter case (only the first verse is included in this snippet):

## If the image is part of a large set of images (these are links)

In the following example, a rating is shown as three filled stars and two empty stars. While the alternative text could have been "★★★☆☆", the author has instead decided to more helpfully give the rating in the form "3 out of 5".

That is the alternative text of the first image, and the rest have blank alternative text.

```html
<p>Rating:
  <meter max=5 value=3>
    <img src="star-colored.jpg" alt="3 out of 5">
    <img src="star-colored.jpg" alt="">
    <img src="star-colored.jpg" alt="">
    <img src="star-greyed-out.jpg" alt="">
    <img src="star-greyed-out.jpg" alt="">
  </meter>
</p>
```

## If it's not intended for the user

If an img element is being used for purposes other than showing an image, e.g. as part of a service to count page views, then the alt attribute must be the empty string.




REF:
https://html.spec.whatwg.org/#a-purely-decorative-image-that-doesn't-add-any-information
