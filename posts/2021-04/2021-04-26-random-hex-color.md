---
title: TIL how to generate a random color using JS
date: 2021-04-26
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Randomize Hex Color"
layout: layouts/post.njk
---

Need a color but can't figure out what?
Randomize it!

```js
(Math.random()*0xFFFFFF<<0).toString(16)
```

![](2021-04-26-random-hex-color_0.png)

This will give you a random color hex value.

So you can do something like this:

```js
const myColor = '#'+(Math.random()*0xFFFFFF<<0).toString(16);

body.style.background = myColor;
```


Via https://codepen.io/ixahmedxi/pen/VpLeEw
