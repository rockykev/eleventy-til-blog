---
title: TIL the difference between embed and iframe
date: 2021-04-29
published: true
tags: ['html']
series: false
canonical_url: false
description: "What the heck is a <iframe> and a <embed>"
layout: layouts/post.njk
---

In my [presentation slides for BangBangCon 2021](https://github.com/RockyKev/slides-bangbangcon2021/blob/main/index.html), I wanted to be able to interact directly with a website within the slides themselves.

My kneejerk reaction was to reach for `<iframe>`.

But then I remembered `<embed>`. And there's also a `<object>` tag as well!

What is the difference?

NOTE: Much of the research and differences comes from html4 and html5, stuff from nearly a DECADE ago. So all the concerns about one or the other is kind of moot. When in doubt, just go straight `<iframe>`.


## MDN notes

### `<iframe>`
The HTML Inline Frame element (<iframe>) represents a nested browsing context, embedding another HTML page into the current one.

```html
<iframe src="https://mdn-samples.mozilla.org/snippets/html/iframe-simple-contents.html"
        title="iframe Example 1" width="400" height="300">
</iframe>
```

via https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe

### `<embed>`
The HTML <embed> element embeds external content at the specified point in the document. This content is provided by an external application or other source of interactive content such as a browser plug-in.

```html
<embed type="video/quicktime" src="movie.mov" width="640" height="480" title="Title of my video">
```

via https://developer.mozilla.org/en-US/docs/Web/HTML/Element/embed

### `<object>`
> The HTML <object> element represents an external resource, which can be treated as an image, a nested browsing context, or a resource to be handled by a plugin.

```html
<!-- Embed a flash movie -->
<object data="movie.swf"
  type="application/x-shockwave-flash"></object>

<!-- Embed a flash movie with parameters -->
<object data="movie.swf" type="application/x-shockwave-flash">
  <param name="foo" value="bar">
</object>
```

via https://developer.mozilla.org/en-US/docs/Web/HTML/Element/object

## Conclusion
Note - this is super rough research, but this is kind of the bulletpoints.
* The `<object>` element is used to embed an object in an HTML document. It is commonly used to embed web page elements such as Flash and Java items that are handled by browser plugins.
* The `<object>` embed code is now considered to be deprecated. We would only recommend its use in situations where iframe embedding isn't possible. For all other uses, the iframe embed code will offer greater reliability and functionality.
* The `<embed>` attribute was originally used both for applications (like Flash) and also for media — movies and audio. You can now use HTML5’s new media elements, `<audio>` and `<video>`.
* With `<embed>` you can get the context of the child from the parent and vice versa. This means they you can use scripts in the parent to manipulate the child etc. That part is not possible with `<object>` or `<iframe>` where you would have to set up some other mechanism instead.
* The `<iframe>` creates an inline frame, which embeds an independent HTML document into the current document.

Overall - for my project, `<iframe>` is the best fit. And my use of `<embed>` is fine.

## Embed vs iframe vs Object References:
* https://stackoverflow.com/questions/16660559/difference-between-iframe-embed-and-object-elements

* https://web.archive.org/web/20150614171416/https://vzaar.com/help/kb/features/what-are-the-differences-between-the-iframe-and-object-embed-codes

* https://www.quora.com/What-is-the-difference-between-iframe-object-and-embed
