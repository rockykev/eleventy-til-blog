---
title: TIL CSS Text Stroke
date: 2021-04-18
published: stroke
tags: ['css']
series: false
canonical_url: false
description: "Making a outline on text, using text-shadow and the new text-stroke."
layout: layouts/post.njk
---

Creating a stroke around text is a challenging effect to do in CSS.
(By stroke, I'm referring to the outline around the text.)

The 'traditional' way to do it is through `text-shadow`. But you'll have to use a generator since it's difficult to just mentally do it in your head.

Text Stroke Generator (using text-shadow)
http://owumaro.github.io/text-stroke-generator/


But there's a better and cleaner way, that finally has the support of all the major browsers.

```
h1 {
  color: black;
  -webkit-text-fill-color: white; /* Will override color (regardless of order) */
  -webkit-text-stroke-width: 1px;
  -webkit-text-stroke-color: black;
}
```

You need to append `-webkit` in front of it.


REFERENCE:
https://caniuse.com/text-stroke
https://css-tricks.com/adding-stroke-to-web-text/
