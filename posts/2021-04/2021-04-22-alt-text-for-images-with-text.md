---
title: TIL Alt text for images that contain text
date: 2021-04-22
published: true
tags: ['accessibility', 'html']
series: true
canonical_url: false
description: "Alt text for images that contain text"
layout: layouts/post.njk
---


Via the HTML Spec docs themselves:

Alt text for images that contain text.

Some examples and how to approach it.

OPTION 1:
You have a image that says Earth Day. The purpose of the image is not to describe the the graphics, but the text itself.
So you don't have to put hands or blue background.

![Earth Day](2021-04-22-alt-text-for-images-with-text_0.png)

```html
<h1><img src="earthdayheading.png" alt="Earth Day"></h1>
```


OPTION 2:
If you're using capital letter graphics to create a neat effect.

![Manuscript capital letters](2021-04-22-alt-text-for-images-with-text_1.png)

```html
  <p><img src="initials/o.svg" alt="O">nce upon a time and a long long time ago, late at
  night, when it was dark, over the hills, through the woods, across a great ocean, in a land far
  away, in a small house, on a hill, under a full moon...
```

Note: If you just need a big letter, there's CSS ways to do it too, using the [:first-letter psuedoclass](https://css-tricks.com/almanac/selectors/f/first-letter/).

OPTION 3:
When an image is used to represent a character that cannot otherwise be represented in Unicode, for example gaiji, itaiji, or new characters such as novel currency symbols, the alternative text should be a more conventional way of writing the same thing, e.g. using the phonetic hiragana or katakana to give the character's pronunciation.

```html
<p>Only <img src="euro.png" alt="euro ">5.99!
```

Note: Web Fonts are a better solution than images.

REFERENCE:
https://html.spec.whatwg.org/#text-that-has-been-rendered-to-a-graphic-for-typographical-effect
