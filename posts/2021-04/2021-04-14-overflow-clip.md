---
title: TIL Overflow:clip
date: 2021-04-14
published: true
tags: ['css']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

Oh it's coming!

Using overflow: clip makes it possible for you to prevent any type of scrolling for the box, including programmatic scrolling. That means the box is not considered a scroll container; it does not start a new formatting context, which gives it better performance than overflow: hidden. And if you need it, you can apply clipping to a single axis via overflow-x and overflow-y.

```css
.overflow-clip {
  overflow: clip;
}
```

![](https://developer-chrome-com.imgix.net/image/0g2WvpbGRGdVs0aAPc6ObG7gkud2/cpWAwVTCytqgKryptFQh.png?auto=format&w=500)

There's also overflow-clip-margin, which allows you to expand the clip border. This is useful for cases where there is ink overflow that should be visible.
```css

.overflow-clip {
  overflow: clip;
  overflow-clip-margin: 25px;
}
```

![](https://developer-chrome-com.imgix.net/image/0g2WvpbGRGdVs0aAPc6ObG7gkud2/GSyCxP6F48a9tJ0U5HAh.png?auto=format&w=570)

## Status

CSS Overflow is in Working Draft.
https://www.w3.org/TR/css-overflow-3/


## Reference
https://developer.chrome.com/blog/new-in-chrome-90/

## Support
It's only supported on Edge, Firefox, and Chrome.
https://caniuse.com/?search=overflow

As usual, Safari lags behind.
