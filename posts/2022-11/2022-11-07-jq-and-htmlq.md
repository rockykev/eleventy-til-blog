---
title: TIL jq and htmlq
date: 2022-11-07
published: true
tags: ['commandline', 'curl']
series: false
canonical_url: false
description: "jq and htmlq, which are two command line libraries that make curl requests then return filtered data. jq is for json, and htmlq is for html!"
layout: layouts/post.njk
---

Today I learned about `jq` and `htmlq`, which are two command line libraries that make `curl` requests then return filtered data!

## jq

Repo: https://stedolan.github.io/jq/

How it looks:
```
// command line
jq '.[0] | {message: .commit.message, name: .commit.committer.name}'


// Result
{
  "message": "Merge pull request #162 from stedolan/utf8-fixes\n\nUtf8 fixes. Closes #161",
  "name": "Stephen Dolan"
}

```

## htmlq

Repo: https://github.com/mgdm/htmlq

Like jq, but for HTML. Uses CSS selectors to extract bits of content from HTML files.


How it looks
```
// command
curl --silent https://www.rust-lang.org/ | htmlq '#get-help'


// Result
<div class="four columns mt3 mt0-l" id="get-help">
        <h4>Get help!</h4>
        <ul>
          <li><a href="https://doc.rust-lang.org">Documentation</a></li>
          <li><a href="https://users.rust-lang.org">Ask a Question on the Users Forum</a></li>
          <li><a href="http://ping.rust-lang.org">Check Website Status</a></li>
        </ul>
        <div class="languages">
            <label class="hidden" for="language-footer">Language</label>
            <select id="language-footer">
                <option title="English (US)" value="en-US">English (en-US)</option>
<option title="French" value="fr">Français (fr)</option>
<option title="German" value="de">Deutsch (de)</option>

            </select>
        </div>
      </div>
 ```

