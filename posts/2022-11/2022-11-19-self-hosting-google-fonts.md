---
title: TIL self-hosting your own Google Fonts
date: 2022-11-19
published: true
tags: ['fonts', 'optimization', 'cdn', 'selfhost']
series: false
canonical_url: false
description: "I'm all for self-hosting. So seeing this question asked 'Should you self-host Google Fonts?', my immediate answer is ABSOLUTELY. But here's the edge-cases."
layout: layouts/post.njk
---


I'm all for self-hosting. So seeing this question asked "Should you self-host Google Fonts?", my immediate answer is ABSOLUTELY.  [Should you self-host Google Fonts?](https://www.tunetheweb.com/blog/should-you-self-host-google-fonts/)


But then this post provided some compelling arguments.

Argument #1 - Google Font is clever about how it serves the fonts

> So the Google Fonts CDN is backed by a clever script to serve the most appropriate fonts, and optimise for performance. By moving to self-hosted fonts you are taking on the responsibility to set this all up correctly, and may even be losing support for your fonts in some browsers if you don't.

Argument #2 - Font Display Swap
```html
<link href="https://fonts.gstatic.com" rel="preconnect" crossorigin>
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
```

> In the above font-face code you can see a font-display: swap; line. This is a relatively new instruction you can add to your font-face declaration which tells the browser to use the fallback, system font (sans-serif in this example) initially and then swap-in the real font once it has been downloaded. This means the content is not delayed waiting on the font, and so is seen as a good performance improvement.

Argument #3 - Future Enhancements

> Google Font does not serve the same CSS every time but instead basis it on the user-agent provided.

> Because of the way Google Fonts works, you can imagine it could drop in enhancements like these (or many others!) just by changing the CSS it returns to you. And it could be clever about when to do this. Obviously only when your browser supports it (like it does now with the font format - WOFF or WOFF2), or for other ways.

Argument #4 - Preloading fonts

> Once you self-host, and move away from Google's hashed URLs you have the option of preloading the font for further performance gains. This is one that has some potential downsides however, as discussed by Andy Davies in his Preloading Fonts and the Puzzle of Priorities blog post. Basically by preloading fonts, and moving them up the priority queue, you're implicitly deprioritising other critical loads (e.g. CSS), and due to a bug in Chrome, the fonts may even jump ahead of some of those.

---
But this article actually proposes that you do self-host! You just have to be aware of all the problems you have to solve yourself.

[Should you self-host Google Fonts?](https://www.tunetheweb.com/blog/should-you-self-host-google-fonts/)
