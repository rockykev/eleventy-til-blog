---
title: TIL the difference between partial application and currying
date: 2022-11-23
published: true
tags: ['advanced', 'javascript', 'closure']
series: false
canonical_url: false
description: "Partial application transforms a function into another function with smaller set of arguments. Currying creates nesting functions according to the number of the arguments of the function. Each function receives an argument. "
layout: layouts/post.njk
---

Currying and Partial Application are related (because of closure), but they are of different concepts.

**Partial application** transforms a function into another function with smaller set of arguments.

```js
// partial application
function add(a, b, c) {
  return a + b + c;
}

function partial(fn, ...args) {
  return function (...moreArgs) {
    return fn(...args, ...moreArgs);
  };
}

const addTwoNumbers = partial(add, 10, 20);

console.log(addTwoNumbers(30)); // Output: 60
```


**Currying** creates nesting functions according to the number of the arguments of the function. Each function receives an argument.


```js
function add(a) {
  return function (b) {
    return function (c) {
      return a + b + c;
    };
  };
}

const addThreeNumbers = add(10)(20);

console.log(addThreeNumbers(30)); // Output: 60
```



via [JavaScript Currying Currying in JavaScript](https://dev.to/suprabhasupi/currying-in-javascript-1k31)
