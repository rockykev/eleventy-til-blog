---
title: TIL about why Node was invented
date: 2022-11-15
published: true
tags: ['node', 'history', 'javascript']
series: false
canonical_url: false
description: "Node.js applications never buffer data, which dramatically reduces the processing time of uploading files, such as videos or audios. In other words, it simply outputs data in chunks, meaning that, for example, a user can watch videos without any interruption."
layout: layouts/post.njk
---

Today I learned about why Node was created.

Ryan Dahl, a PHP developer, faced some difficult issues developing features with intense input and output flow using synchronous programming and multi-threaded server-side platforms. Traditional server-side programming languages like Java and PHP - they were made to handle multiple requests simultaneously by spawning new threads or processes for each request. You can only grow as fast as how much 'metal' you throw in there.

Node.js was built to solve a specific problem: deal with intense asynchronous input and output events to scale. It is based on an event-driven, non-blocking I/O model, which allows it to handle large numbers of connections simultaneously using a single thread.

This makes Node.js ideal for building real-time web applications, streaming applications, and other network services that require high concurrency and low latency.

One of the key features of Node is that there's No Buffering.

Node.js applications never buffer data, which dramatically reduces the processing time of uploading files, such as videos or audios. In other words, it simply outputs data in chunks, meaning that, for example, a user can watch videos without any interruption.

[Basic Concepts of Node.js](https://dev.to/cassiocappellari/basic-concepts-of-node-js-pch?utm_source=pocket_reader)
