---
title: TIL how a game developer's obsession with karaoke paid off
date: 2022-11-28
published: true
tags: ['gamedesign', 'devstory']
series: false
canonical_url: false
description: "Initially, he says coworkers criticized him for going too far with the minigame – at the time, it was uncharacteristic to have the generally-stoic protagonist, Kiryu, doing something so goofy. But his instincts proved correct; it became one of the most popular features in the Yakuza series and went a long way in humanizing Kiryu."
layout: layouts/post.njk
---

Game Informer did a interview about Ryu Ga Gotoku Studio, and Ryosuke Horii's history interest me.

 Ryosuke Horii directed Yakuza: Like A Dragon, and now he’s directing its sequel. There's actually a LOT of Yakuza games out there. But Like a Dragon was the first that made the series wildly popular to the West.

I like this story because we have a lot of 'absolute rules' when it comes to game design. It seems like such a ironic twist, because we have rules about simply playing.

Essentially, Horii obsessed over a mini-game. And that obsession paid off.

> Horii’s life’s work is not the Yakuza series. It’s karaoke. And he has the stats to prove it.

> Horii keeps a spreadsheet of all the songs he can sing at karaoke. Once a year, he prints out the most up-to-date version and carries it around with him. Figuring he might need it today, he printed out a new one just in case. When he hands it over, I look over the meticulous details, spread across a staggering 7,964 songs.

> “[During my job interview] they asked me about myself. I said, ‘My hobby is karaoke,’” Horii told Denfaminicogamer in 2018 (translation via Carrie Williams). “When I had the final interview with Nagoshi, he said, ‘A lot of guys have karaoke as a hobby,’ which is, of course, true. So I had to find some way of showing him, ‘I’m not like those other guys.’

> “So I showed him my karaoke list I showed you before, saying, ‘Other guys don’t do this,’ with a bit of a smile, and I was offered the job.”

> ... Initially, he says coworkers criticized him for going too far with the minigame – at the time, it was uncharacteristic to have the generally-stoic protagonist, Kiryu, doing something so goofy. But his instincts proved correct; it became one of the most popular features in the Yakuza series and went a long way in humanizing Kiryu.

via [The Future Of Ryu Ga Gotoku Studio](https://web.archive.org/web/20221127121906/https://www.gameinformer.com/feature/2022/11/24/the-future-of-ryu-ga-gotoku-studio)
