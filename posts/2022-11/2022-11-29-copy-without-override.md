---
title: TIL copy without overriding in command-line
date: 2022-11-29
published: true
tags: ['terminal', 'files']
series: false
canonical_url: false
description: "The cp command has a -n flag that you can use to prevent the copied file from overriding another file if one with the same name exists"
layout: layouts/post.njk
---

Today I learned you can copy files without overriding!

If there's a naming collision occurs and the -n flag is present, the copy command does nothing!

> The cp command has a -n flag that you can use to prevent the copied file from overriding another file if one with the same name exists


via https://til.hashrocket.com/posts/0runelrmcp-copy-without-overriding-existing-files
