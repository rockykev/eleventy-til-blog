---
title: TIL HTTP "No-cache" headers
date: 2022-11-02
published: false
tags: ['cache', "server", "php", "apache"]
series: false
canonical_url: false
description: "Using PHP and apache .htaccess file to force reload"
layout: layouts/post.njk
---

I was working on a very old PHP project that was getting some weird caching issue on some client's browser.

Running out of ideas, I decided to go server-side.



```php
<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache"); // HTTP 1.0
header("Expires: Wed, 1 Jan 2020 00:00:00 GMT"); // Anytime in the past

// REST OF THE PAGE AS USUAL ?>
<!DOCTYPE html>
```

> This page is now set to “no-cache, always reload”. This will most likely cause the browser to reload everything on the page itself. For the sake of performance, you might want to turn off these headers after some time, after most of the users have gotten the updated page.

via [Force Reload Javascript CSS In Browsers (Simple Examples)](https://code-boxx.com/force-browsers-reload-javascript-css/?utm_campaign=Frontend%2BWeekly&utm_medium=email&utm_source=Frontend_Weekly_236)

This is a good idea for a single page. But sucks with a lot of pages.

Instead, you want to go to even higher. Into their Apache it is!

> Just add a small snippet in the .htaccess file.

```
<IfModule mod_expires.c>
  ExpiresActive on
  ExpiresByType text/css "access"
  ExpiresByType text/js "access"
  ExpiresByType text/javascript "access"
</IfModule>
```

> That will pretty much expire all CSS and Javascript files the moment users access them, and encourage browsers to reload them. I will somewhat recommend this method if you roll out updates on a regular basis, mass controls using this Apache module is much more flexible and powerful.
