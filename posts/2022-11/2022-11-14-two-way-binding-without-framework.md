---
title: TIL How to implement two-way binding without a framework
date: 2022-11-14
published: true
tags: ['javascript', 'vue', 'codepen', 'react', 'data']
series: false
canonical_url: false
description: "One of the reasons we use React or Vue is because of that sweet sweet two-way binding. It's the automatic syncing of data between the model (data) and the view (UI) in both directions. Any changes made to the data will automatically update the view, and any changes made to the view will automatically update the data."
layout: layouts/post.njk
---

One of the reasons we use React or Vue is because of that sweet sweet two-way binding.

It's the automatic syncing of data between the model (data) and the view (UI) in both directions. Any changes made to the data will automatically update the view, and any changes made to the view will automatically update the data.

How to do it without a framework:

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="jOePvQG" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/jOePvQG">
  Untitled</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

```html

<input id="inputEl" placeholder="Enter a number..." type="text" />
<span id="val"></span>
<button id="incrementVal">Increment</button>

<script>

const data = {
  value: ''
};

const el = document.getElementById('inputEl');

Object.defineProperty(data, 'prop', {
  get: function() {
    console.log('Getter called');
    return this.value;
  },
  set: function(value) {
    console.log('Setter called');
    this.value = value;
    el.value = value;
    printVal();
  }
});

function printVal() {
  const el = document.getElementById('val');
  el.innerText = data.prop;
}



// attaching the event listener on keyup events
el.addEventListener('keyup', (event) => {
  data.prop = event.target.value;
});

const btn = document.getElementById('incrementVal');
btn.addEventListener('click', () => {
 data.prop = Number(data.prop) + 1;
});
</script>
```



via [Two-way data binding in Vanilla JavaScript without Angular or React](https://medium.com/developers-arena/two-way-data-binding-in-vanilla-javascript-without-angular-or-react-223ddbb1252d)

