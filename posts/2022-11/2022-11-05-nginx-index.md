---
title: TIL how NGINX knows to look for index.html vs index.php
date: 2022-11-05
published: false
tags: ['servers', 'nginx', 'php']
series: false
canonical_url: false
description: " What happens when visitor hits /foo/bar URL?"
layout: layouts/post.njk
---

> What happens when visitor hits `/foo/bar` URL?

1. NGINX looks at the longest prefix-based location, which is / and chooses it for current request

    * It also checks for any regex locations but finds none that match to the URI

2. So it attempts to actually see if there is `root directive's value + /foo/bar` file on its system. That is, it checks using a stat system call, whether `/var/www/html/foo/bar` exists. If the file indeed exists, it will just serve it
If the file does not exist, then NGINX attempts to see if `/var/www/html/foo/bar/` directory exists. It it does, then NGINX will check the existence of files defined by index directive (e.g. index.html) in a defined order, and use those to satisfy requests (if they do exist), e.g. `/var/www/html/foo/bar/index.html`

3. If those do not exist, NGINX finally knows it needs to serve using `/index.php`. How the `try_files` works is that "reaching" its last argument will result in another location search, and this time it will match the regex location of `\.php$.`

* That's where the magic of talking to PHP-FPM will happen, as in NGINX will pass on the script filename which is constructed off root and current, internally rewritten request URI. How it is constructed can be configured using the `ngx_http_fastcgi_module` nginx module directives which are [documented here](https://nginx.org/en/docs/http/ngx_http_fastcgi_module.html)

https://stackoverflow.com/a/57548387/4096078
