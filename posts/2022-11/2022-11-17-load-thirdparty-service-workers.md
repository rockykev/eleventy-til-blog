---
title: TIL loading third-party scripts onto service workers
date: 2022-11-17
published: true
tags: ['serviceworker', 'javascript', 'cache', 'optimization']
series: false
canonical_url: false
description: "An alternative to self-hosting scripts would be using Service Workers to cache them. This can give you greater control over how often they are re-fetched from the network. This could also be used to create a loading strategy where requests for non-essential third parties are throttled until the page reaches a key user moment."
layout: layouts/post.njk
---



Web.Dev had a killer guide on how to load third-party scripts in a way to optimize page performance here: [Loading Third-Party JavaScript](https://web.dev/optimizing-content-efficiency-loading-third-party-javascript/) and [Efficiently load third-party JavaScript](https://web.dev/efficiently-load-third-party-javascript/#use-service-workers-to-cache-scripts-from-third-party-servers)

There's the usual tips like Resource Hints, async/defer, self-hosting, and lazy load.

But the one I was really interested in was using Service workers!

> An alternative to self-hosting scripts would be using Service Workers to cache them. This can give you greater control over how often they are re-fetched from the network. This could also be used to create a loading strategy where requests for non-essential third parties are throttled until the page reaches a key user moment.

Example code looks like this:


```js
// your main JS file
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js');
}  else {
    // Fallback to loading scripts normally
    var jqueryScript = document.createElement('script');
    jqueryScript.src = 'https://code.jquery.com/jquery-3.6.0.min.js';
    document.head.appendChild(jqueryScript);

    var bootstrapLink = document.createElement('link');
    bootstrapLink.rel = 'stylesheet';
    bootstrapLink.href = 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css';
    document.head.appendChild(bootstrapLink);
  }
```

```js
// sw.js - your service worker file

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open('my-cache').then(function(cache) {
      return cache.addAll([
        'https://code.jquery.com/jquery-3.6.0.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
      ]);
    })
  );
});

```
If the serviceWorker exists, then it loads the `sw.js` file.
