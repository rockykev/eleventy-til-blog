---
title: TIL Math.random() for design tricks
date: 2022-11-20
published: true
tags: ['codepen', 'art', 'random']
series: false
canonical_url: false
description: "Math.random() is awesome! However, if you dealing with sensitive applications and need a more secure method of randomization, I’d recommend WebCrypto."
layout: layouts/post.njk
---

I love this.

Random Particles:

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="mJzOWJ" data-user="towc" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/towc/pen/mJzOWJ">
  neon hexagon-forming particles</a> by Matei Copot (<a href="https://codepen.io/towc">@towc</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


Generative Art
<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="BaQrbx" data-user="tjoen" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/tjoen/pen/BaQrbx">
  Generative art from rectangleworld</a> by Theun (<a href="https://codepen.io/tjoen">@tjoen</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


Random Words
<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="OJRVXXG" data-user="css-tricks" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/team/css-tricks/pen/OJRVXXG">
  Random Words!</a> by CSS-Tricks (<a href="https://codepen.io/team/css-tricks">@css-tricks</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

Also this last bit:

> Is Math.random() the same as WebCrypto?

> As you’ve seen from this article, Math.random() is awesome! However, if you dealing with sensitive applications and need a more secure method of randomization, I’d recommend WebCrypto. Reasons you may want to use WebCrypto include temporary verification codes, random password generation, randomized lottery numbers, etc.


via [Lots of Ways to Use Math.random() in JavaScript](https://css-tricks.com/lots-of-ways-to-use-math-random-in-javascript/)

