---
title: TIL multiple ways to deal with extra 0s
date: 2022-11-24
published: true
tags: ['numbers', 'javascript']
series: false
canonical_url: false
description: "Dealing with lots of 0`s."
layout: layouts/post.njk
---


Dealing with lots of 0`s.

```js
❌
const SALARY = 150000000,
const TAX = 15000000;

✔️
const SALARY = 15e7,
const TAX = 15e6;

✔️
const SALARY = 150_000_000;
const TAX = 15_000_000;
```
