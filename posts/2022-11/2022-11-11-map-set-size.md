---
title: TIL that the max size of a Map/Set is 26843544
date: 2022-11-11
published: true
tags: ['javascript', 'mdn', 'v8', 'math']
series: false
canonical_url: false
description: " JS Maps and Sets are implemented by OrderedHashTable. OrderedHashTables double in size when they need to grow, and their max size is 26843544. After 16777216 elements, doubling that exceeds the max, so the allocation fails.Currently these data structures are not implemented to be usable for large in-memory datasets. Also note that the JS specification itself doesn't require that Maps and Sets must scale with available memory."
layout: layouts/post.njk
---



In JavaScript, there's the `Map` and the `Set` object.

Set: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
Map: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map

via https://bugs.chromium.org/p/v8/issues/detail?id=11852


According to this bug report on the Chromium's V8 board, which is Google's open-source Javascript engine, there's a limit to how large Set objects can be.

> Is there a specific reason why a Set has a limit of 2^24 elements?

(2^24 equals 16777216)

Map too!
```
const superSet = new Map();
for (let i = 0; i <= 16777216; i++) superSet.set(i, i);
```

The answer, [via syg@chromium.org](https://bugs.chromium.org/u/2333478852):

> JS Maps and Sets are implemented by OrderedHashTable, and the limit you're running into is in [1]. OrderedHashTables double in size when they need to grow, and their max size is 26843544. After 16777216 elements, doubling that exceeds the max, so the allocation fails.

> Currently these data structures are not implemented to be usable for large in-memory datasets. Also note that the JS specification itself doesn't require that Maps and Sets must scale with available memory.

Well dang, they're right. https://tc39.es/ecma262/#sec-set-objects

