---
title: TIL safe areas for mobile browsers
date: 2022-11-25
published: true
tags: ['css', 'mobile', 'mdn']
series: false
canonical_url: false
description: " The problem you quickly encounter is that the visible viewport is not per se the same as the interactive viewport. Mobile browsers tend to prioritize essential UI elements (like the home-bar on IOS) for interactivity at all times."
layout: layouts/post.njk
---

![](2022-11-25-mobile-safe-area_0.png)

Solution:

```css
/* env(safe-area-inset-bottom) or -top  */

.cookieNotice {
  position: fixed;
  right: 0px;
  left: 0px;
  bottom: 0px;
  padding-bottom: calc( env(safe-area-inset-bottom) + 20px )
}
```
> The problem you quickly encounter is that the visible viewport is not per se the same as the interactive viewport. Mobile browsers tend to prioritize essential UI elements (like the home-bar on IOS) for interactivity at all times.

> Browsers do this by implementing something called ‘safe-areas’. Area’s that can’t be touched with user-interactions inside the browser.


To learn more about [env()](https://developer.mozilla.org/en-US/docs/Web/CSS/env)

```
/* Using the four safe area inset values with no fallback values */
env(safe-area-inset-top);
env(safe-area-inset-right);
env(safe-area-inset-bottom);
env(safe-area-inset-left);

/* Using them with fallback values */
env(safe-area-inset-top, 20px);
env(safe-area-inset-right, 1em);
env(safe-area-inset-bottom, 0.5vh);
env(safe-area-inset-left, 1.4rem);
```


via [You have to start using this CSS property in your websites](https://bootcamp.uxdesign.cc/you-have-to-start-using-this-css-property-in-your-websites-7353f46def79)
