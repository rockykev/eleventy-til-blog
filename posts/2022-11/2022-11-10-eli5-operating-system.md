---
title: TIL ELI5 What an Operating system is
date: 2022-11-10
published: true
tags: ['os', 'eli5']
series: false
canonical_url: false
description: "If you traveled and wasn't able to speak the language, how would you get by? You would use a translator, who would help you around. But if everyone used a translator, tourists parties would quickly double in size."
layout: layouts/post.njk
---

I really like this ELI5 of what a Operating System is.

Explaining complex things in simple words is an art, and I really like this metaphor.

> Take this scenario: if you were to travel to a place that you had never been to before, where they speak a different language that you don't know, what would you need to help communicate with the local people?

> Well, you guessed it right – you’d need a translator. Now that’s what an OS does in your computer. It converts the binary input into a human understandable language.

> Without an OS, every program would need a means to handle all computer components and hardware. This means that it would need a method to accept user input using a keyboard and mouse, print documents, read and write on storage devices, keep track of time, send output to other screens and even communicate across a network. This would cause every single program to be very complex and heavy.

> That’s why one of the main purpose of an OS is to organize and control hardware and software so that the device it lives in behaves in a flexible but predictable way.

In other words, it's a system that provides key tools for all the other things to work.

The article goes to list what those key tools are in more detail.

* Processor Management - Ensuring that each process and application receives enough of the processor’s time. Keeping track of the status of processes.
* Memory Management - Keeping track of memory and allocating memory to others
* Device Management - Managing all the devices.
* File Management - Tracks where information is stored, settings, statuses, ownership.
* Job Scheduling - Determines the jobs/tasks that need to be processed. Also keeps track of the time and resources used by various tasks and users.
* Error Handling - Dealing with errors.

via [What Is An OS ? Operating System Definition For Beginners](https://www.deepcrazyworld.com/what-is-an-os-operating-system-definition-for-beginners/)
