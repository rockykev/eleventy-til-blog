---
title: TIL using jq to turn JSON into CSV
date: 2022-11-27
published: true
tags: ['terminal', 'csv', 'json']
series: false
canonical_url: false
description: "jq is like sed for JSON data - you can use it to slice and filter and map and transform structured data all within the command line."
layout: layouts/post.njk
---


I've been learning about [jq](https://stedolan.github.io/jq/) recently.

> jq is like sed for JSON data - you can use it to slice and filter and map and transform structured data with the same ease that `sed`, `awk`, `grep` and friends let you play with text.

One of the benefits is turning a JSON response into CSV.


```bash
jq -r '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
```


Via https://stackoverflow.com/a/32965227
