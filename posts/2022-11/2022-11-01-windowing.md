---
title: TIL Windowing in Browsers
date: 2022-11-01
published: true
tags: ['js', 'performance']
series: false
canonical_url: false
description: "Windowing only allow you to render what's available. Using twitter as a example, with tweets, when you scroll through hundreds of tweets and inspect the DOM, they just put a bunch of blank space above so there's not like 400 tweets above you."
layout: layouts/post.njk
---

TIL about "Windowing".

From the Syntax Podcast, around [7:30 seconds in this episode "Spooky Web Dev Stories 2022"](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkLnN5bnRheC5mbS9yc3M/episode/OGJhNzNkZmQtMzVhZS00MmQ4LWFjMDAtY2Y0ZWZiZTM4N2I3?ep=14).

So you have sites with Infinite Scrolling.

Windowing only allow you to render what's available. Using twitter as a example, with tweets, when you scroll through hundreds of tweets and inspect the DOM, they just put a bunch of blank space above so there's not like 400 tweets above you.

> Windowing or List virtualization is a concept of only rendering or write the visible portion in the current “ window ” to the DOM. The number of items that rendered at first time are smaller than the original one.

> The remaining items are rendered when you scroll down to it. The DOM nodes of items that exit the window are replaced by the new ones. This improves the performance of rendering a large list.

> Without windowing, the entire list is written to the DOM including items that are not in the current window. It means, you would have to wait until the entire list is written to see the first item.

via [What is windowing? Also I have heard about react-window and react-virtualized… ](https://praekiko.medium.com/what-is-windowing-also-i-have-heard-about-react-window-and-react-virtualized-c29dc843f4e0)


NPM: https://www.npmjs.com/package/react-window
