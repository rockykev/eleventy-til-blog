---
title: TIL what matters for JS optimization
date: 2022-11-12
published: true
tags: ['javascript', 'loops', 'memory']
series: false
canonical_url: false
description: "Optimizing your Javascript - Using getElementById() vs querySelector()? Doesn't matter. Loops? Doesn't matter. What matters: Render Blocking code, Repaints and Reflows, Memory allocation"
layout: layouts/post.njk
---

Two years ago, I remember arguing with a coworker over what loops to use for optimization. (Spoiler: congrats on saving milliseconds on something that might be optimized a few months later)

Via [Writing high-performance JavaScript](https://gomakethings.com/writing-high-performance-javascript/)

When writing High Performance Javascript, here's doesn't matter for Javascript Optimization:

## Doesn't matter:

Using getElementById() vs querySelector():

> For example, you may read that document.getElementById() is more than twice as fast as document.querySelector(), and start fretting about which selector method to use when.

> It’s true, by the way. The document.getElementById() method can run about 15 million operations a second, compared to “just” 7 million per second for the document.querySelector() method in the latest version of Chrome. But that also means that the document.querySelector() method runs 7,000 operations a millisecond. That’s really damn fast!


Loops:
> I also similarly see people worry about things like which loop method is faster.

Here's a blog post with data: https://blog.bitsrc.io/measuring-performance-of-different-javascript-loop-types-c0e9b1d193ed

And similar to the above comment... imagine shaving fractions of fractions of fractions of seconds off.

There's way better optimization you can look at.

## Matters

* Render Blocking code
* Repaints and Reflows, like repainting the UI
* Memory allocation: Two offenders - Lots of event listeners attached to individual elements & Store huge amounts of data in memory

