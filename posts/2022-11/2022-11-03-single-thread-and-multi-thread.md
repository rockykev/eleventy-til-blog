---
title: TIL the difference between single-threaded & multi-threaded architecture
date: 2022-11-03
published: false
tags: ['php', 'javascript', 'performance']
series: false
canonical_url: false
description: "For web dev, we don't need it. We're not bottle-necked by the processing power. We're instead bottlenecked by the ability to read files/databases. We can simulate multi-threading (and improve our app's performance) using async/await."
layout: layouts/post.njk
---

> Why is Node.js a single thread?

## What does it mean to be single-threaded/multi-threaded

Node's architecture is single-threaded, as its event loop runs one process at a time.
That also means it can only execute one function at a time. And since functions can have multiple instructions, the event loop will execute one instruction at a time.

Javascript is single threaded because you cannot have 2 sets of consecutive lines of code running interleaved or simultaneously on multiple cores.

In other programming languages (Like Java):

You can keep spawning threads to execute code, while the prior code is still being processed. You also need a way for both threads to communicate to each other and you have to manage both of their resources and allocate them.

> Imagine you have two people writing on the same whiteboard, ignoring each other and with no qualms about erasing what the other has written. The Javascript model would mean you have only one pen-eraser that each person can only pick up once the previous person has finished. In the “multiple threads” approach, each person has their own pen-eraser and doesn’t care about hat the others are doing by default; you have to add explicit code to make sure they don’t step on each other.

via https://qr.ae/prVTAe

In Javascript, there's a guarantee that your code will always execute in the same thread and all that memory is handled within that same thread too.

If you write a function that is 10000 lines long, and that function registers a callback on line 3 (the Javascript mechanism for concurrency), you have the guarantee that all 10000 lines of the current function will execute completely and finish before the callback runs. And, similarly, when the callback runs, it will run to completion (possibly registering more callbacks to be ran later) with no interruption, before any other code can run.

With Node, you can make it behave as if it were multi-threaded, using async/await and promises. You can have Javascript branch to another thread, complete the calculations (like with web workers), and come back with the results.

The con with multi-threading is additional management.
The con with single-threading is that you can block the event loop/call stack (Starving the Event lOop).


## Why not use multi-threading?

> The thing is, most web applications are not CPU bound. They're IO bound. For IO bound problems, depending on the problem, you may get substantial benefit from async IO (What Javascript/Node does, and what ReactPHP, AmPHP, and Fibers in 8.1 let you do) IFF the IO operations are independent.

> If you have to run 5 SQL queries and they could be run in any order, then async would help a lot there.

> If you need the results of query 1 to know what query 2 should be, async won't give you any meaningful benefit. (Multithreading would have the same maybe-benefit.)

via https://www.reddit.com/r/PHP/comments/xpeyk3/comment/iq5b2at/?utm_source=reddit&utm_medium=web2x&context=3

> There are a whole bunch of areas where language performance matters like image/video/audio processing (you wouldn't build photoshop or premiere in PHP or Ruby), machine learning (there's a reason Python is only used to glu together C algorithms), data processing (MySQL built in native PHP would be terrribly slow), 2D or 3D rendering engine (I mean Unity uses C#, but it's still OpenGL and DirectX doing the heavy lifting and those are developed in C), running on really cheap/small devices or integrated circuits (I don't even think you can run PHP on Arduino ?).

via https://www.reddit.com/r/PHP/comments/xpeyk3/comment/iq7egm5/?utm_source=reddit&utm_medium=web2x&context=3


ref:
https://stackoverflow.com/questions/17959663/why-is-node-js-single-threaded
https://dev.to/arealesramirez/is-node-js-single-threaded-or-multi-threaded-and-why-ab1

## TThe question: Why is Node.js a single thread?

Tl;dr - for web dev, we don't need it. We're not bottle-necked by the processing power. We're instead bottlenecked by the ability to read files/databases.

We can simulate multi-threading (and improve our app's performance) using async/await.

This also applies to PHP and other web-based languages!
