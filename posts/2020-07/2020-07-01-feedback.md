---
title: TIL The Address tag
date: 2020-07-01
published: true
tags: ['seo', 'html']
series: false
canonical_url: false
description: "JS you so crazy"
layout: layouts/post.njk
---

The HTML <address> element indicates that the enclosed HTML provides contact information for a person or people, or for an organization.

The <address> element should include the name of the person, people, or organization to which the contact information refers. It can also include URL, email address, phone number, social media handle, geographic coordinates, and so forth.

```
//wrong
<ADDRESS>Last Modified: 1999/12/24 23:37:50</ADDRESS>


//preferred
<address>
  <a href="mailto:jim@rock.com">jim@rock.com</a><br>
  <a href="tel:+13115552368">(311) 555-2368</a>
</address>

//also acceptable
<address>
  Written by <a href="mailto:hello@bitdegree.org">BitDegree</a>.
  Visit us at:
  https://www.bitdegree.org/<br>
</address>
```


[MDN Web docs](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/address)
[w3.org](https://www.w3.org/TR/2011/WD-html5-author-20110809/the-address-element.html)
