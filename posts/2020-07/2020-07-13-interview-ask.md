---
title: TIL ask about the most successful person
date: 2020-07-13
published: true2
tags: ['interview']
series: false
canonical_url: false
description: "Ask this one question"
layout: layouts/post.njk
---

ASK THIS:
“Can you tell me about the most successful person you’ve ever hired and what exactly they did to be successful?”

[reference](https://lifehacker.com/to-find-out-if-your-potential-boss-will-be-any-good-as-1844240788)
