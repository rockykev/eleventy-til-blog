---
title: TIL that Emojis are in the web🥺💡
date: 2020-07-10
published: true
tags: ['webdev']
series: false
canonical_url: false
description: "✨✨✨"
layout: layouts/post.njk
---

✨✨✨

If a website is using `<meta charset="UTF-8">`, then you can put emojis in your page.

This is Valid Code:
`<p>🍔</p>`

This is valid code:
```
map([🌽, 🐮, 🐔], cook)
=> [🍿, 🍔, 🍳]

filter([🍿, 🍔, 🍳], isVegetarian)
=>  [🍿, 🍳]

reduce([🍿, 🍳], eat)
=> 💩
```

This is valid code:
```
h1::before  {
  content: "🍔";
}
```

Note: I don't know the unicode. I visit the Emojipedia and copy/paste it.

[Tweet](https://twitter.com/steveluscher/status/741089564329054208?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E741089564329054208%7Ctwgr%5E&ref_url=https%3A%2F%2Fwww.tjvantoll.com%2F2016%2F06%2F10%2Femoji-and-coding%2F)
[reference](https://www.kirupa.com/html5/emoji.htm)
[Emojipedia](https://emojipedia.org/)
