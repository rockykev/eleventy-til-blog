---
title: TIL you can style console logs
date: 2020-07-07
published: true
tags: ['webdev']
series: false
canonical_url: false
description: "console logs wow"
layout: layouts/post.njk
---

I was goofing off and noticed `water.css` dev [Kognise](https://github.com/kognise) belonged to a team called doggo.ninja. Curious, I went to the site, and discovered something in the console.log.

This -> 2020-07-07-console-noice_0.png

That was done with the use of `%c`, a content specifier.

```
 console.log("%cHmmm, I wonder what $ is...", "color: white; background-color: black; font-size: 1.4rem; padding: 20px;");
 ```



 [reference](https://www.telerik.com/blogs/how-to-style-console-log-contents-in-chrome-devtools)
