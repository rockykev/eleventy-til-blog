---
title: TIL Different Workstations in VSCode
date: 2020-06-28
published: true
tags: ['vscode']
series: false
canonical_url: false
cover_image: ./images/vs-themes.png
description: "Work stations"
layout: layouts/post.njk
---

You may be using VSCode as your prefer editor for a lot of different types of projects.

For example:
* For Work
* For Hobby development
* For Open Source
* For Game Projects
* For Blogging

You may also keep a few of them open at the same time.

Your brain takes a few seconds to figure out what you're looking at. Reduce the mental strain by using different theme colors depending on the project.

To set a different theme based on the project:
1. File > Settings (or press the gear on the bottom-left corner)
2. Switch from "User" settings to "Workspace" settings
3. Type 'theme' in the searchbar, and change it.

[VSCode Settings](https://code.visualstudio.com/docs/getstarted/settings)
