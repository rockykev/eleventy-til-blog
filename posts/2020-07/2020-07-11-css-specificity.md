---
title: TIL CSS Specificity
date: 2020-07-11
published: true
tags: ['webdev', 'css']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

**Least to Most Specific**

`element`: h1, div
`attribute`: class, attribute [type="input"]
`id`: Higher. can cause trouble for styling.
`!important`: this means that there's a bigger problem with your css.

**Specific based on apply order**

Comparing Elements together
```
h1 {
  font-size: 1rem;
}

// this will apply because it's lower in the file
h1 {
  font-size: 2rem;
}
```

Comparing Class Vs Element
```
.header {
  font-size: 2rem;
}

// this is ignored, since .header is a class.
h1 {
  front-size: 1rem;
}
```

Comparing Class VS more specific Class
```
// this is ignored. This is 1 level of specificity.
.header {
  font-size: 1rem;
}

// this is applied. Because it's 3 levels of specificity.
.wrapper .container .header {
  font-size: 2rem;
}
```
