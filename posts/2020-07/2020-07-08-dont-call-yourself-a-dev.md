---
title: TIL about not calling yourself a programmer
date: 2020-07-08
published: true
tags: ['advice', 'development']
series: false
canonical_url: false
description: "Advice from Peter Drucker"
layout: layouts/post.njk
---

This is not to scare programmer. But a reminder that if you are just 'a programmer', then you are shooting yourself in the foot.

There's a great article by [Patrick McKenzie](https://twitter.com/patio11) which covers the why. I'll include the relevant parts below and in snippet forms.

## What's this about?
Within business, the company doesn't care how elegant your code is or how you spent two weeks solving technical debt. They want a result that helps them get more money.

Let's reverse this concept. You hire a plumber to fix your toilet. Would you rather:

1. Hire Red Plumber that can prefers to fix the pipes in your wall first to fix the toilet? ($500/day - 3 days of work)
2. Hire Blue Plumber that can fix your toilet. ($100/day - 1 day of work)

Chances are, you're going to hire the Blue Plumber. You need a working toilet... pronto! Now, maybe if you're going on vacation for a week and there's a potential emergency, maybe Red Plumber is a better hire. But right now, the priority is getting a working toilet. You can think about future problems if they come up. You're literally just trying to avoid having crap all over the floor.

Congrats - you are now thinking like a CEO!

Instead of `plumber`, it's `programmer`. I'm going to repeat this again: your job is to solve something that helps the company get more money.

Within the company, there are two types of people: `Profit Center` & `Cost Center`.

* `Profit Center` focuses on generating and maximizing revenue streams.
* `Cost Center` is everyone else that keeps the company operational.

Programmers are expensive `Cost Center` employees. A sales person can score a $1 million contract. We (programmers) can't.

What does that mean?
If you ONLY want to be a programmer, who wants to stare at code and make it pristine, you might one day find yourself out of a job. Remember the plumber analog? A CEO can on day decide to replace many Red Plumbers ($$$) with cheaper Blue Plumbers ($).

Programmers who work with `Profit Center` folks are more valuable, are given more responsibility (and status), and become difficult to replace.

You want to know how programmers become superstar CEOs?
Understanding the difference between the two is how.

Now go read the snippet below:

> Peter Drucker — you haven’t heard of him, but he is a prophet among people who sign checks — came up with the terms Profit Center and Cost Center.  Profit Centers are the part of an organization that bring in the bacon: partners at law firms, sales at enterprise software companies, “masters of the universe” on Wall Street, etc etc.  Cost Centers are, well, everybody else.  You really want to be attached to Profit Centers because it will bring you higher wages, more respect, and greater opportunities for everything of value to you.  It isn’t hard: a bright high schooler, given a paragraph-long description of a business, can usually identify where the Profit Center is.  If you want to work there, work for that.  If you can’t, either a) work elsewhere or b) engineer your transfer after joining the company.

> Engineers in particular are usually very highly paid Cost Centers, which sets MBA’s optimization antennae to twitching.  This is what brings us wonderful ideas like outsourcing, which is “Let’s replace really expensive Cost Centers who do some magic which we kinda need but don’t really care about with less expensive Cost Centers in a lower wage country”.  (Quick sidenote: You can absolutely ignore outsourcing as a career threat if you read the rest of this guide.)  Nobody ever outsources Profit Centers.  Attempting to do so would be the setup for MBA humor.  It’s like suggesting replacing your source control system with a bunch of copies maintained on floppy disks.

> Don’t call yourself a programmer: “Programmer” sounds like “anomalously high-cost peon who types some mumbo-jumbo into some other mumbo-jumbo”  If you call yourself a programmer, someone is already working on a way to get you fired.  You know Salesforce, widely perceived among engineers to be a Software as a Services company?  Their motto and sales point is “No Software”, which conveys to their actual customers “You know those programmers you have working on your internal systems?  If you used Salesforce, you could fire half of them and pocket part of the difference in your bonus.”

[reference](https://www.kalzumeus.com/2011/10/28/dont-call-yourself-a-programmer/)
