---
title: TIL the Spread Operator
date: 2020-07-27
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "The spread operator is pretty awesome."
layout: layouts/post.njk
---

The spread operator:

```
// spreads the array's items into the array
const arrCopy = [ ...arr ];

const obj1 = {'a': 1, 'b': 2};
const obj2 = {'c': 3};
const obj3 = {'d': 4};

// If we want an object to contain the combined properties of all these objects, we can do so with the simple code below:
const objCombined = {...obj1, ...obj2, ...obj3};
```


We can also iterate the characters in a string one at a time:

```
var greeting = "Hello world!";
var chars = [ ...greeting ];

console.log(chars);

// [ "H", "e", "l", "l", "o", " ",
//   "w", "o", "r", "l", "d", "!" ]
```
