---
title: TIL as of 2021, 1024x width is 2% of the world
date: 2020-09-01
published: true
tags: ["screensize"]
series: false
canonical_url: false
description: "1024x desktop is still a option on Google DevTools."
layout: layouts/post.njk
---

1024x desktop is still a option on Google DevTools.

But it's about 2% of the demographic. It should be abolished.

<div id="desktop-resolution-ww-monthly-202001-202101" width="600" height="400" style="width:600px; height: 400px;"></div><!-- You may change the values of width and height above to resize the chart --><p>Source: <a href="https://gs.statcounter.com/screen-resolution-stats/desktop/worldwide">StatCounter Global Stats - Screen Resolution Market Share</a></p><script type="text/javascript" src="https://www.statcounter.com/js/fusioncharts.js"></script><script type="text/javascript" src="https://gs.statcounter.com/chart.php?desktop-resolution-ww-monthly-202001-202101&chartWidth=600"></script>
