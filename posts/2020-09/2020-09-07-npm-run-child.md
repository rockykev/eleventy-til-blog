---
title: TIL how to run npm that's inside a child folder
date: 2020-09-07
published: true
tags: ["npm"]
series: false
canonical_url: false
description: "Running npm scripts on other package.json files inside child folders"
layout: layouts/post.njk
---

```
└── Root folder/
    ├── random-folder/
    │   └── files
    ├── app/
    │   ├── /dist
    │   ├── /src
    │   ├── webpack.js
    │   └── package.json
    ├── another-folder/
    │   └── other files
    ├── ...
    └── package.json

```

1. You're in your root folder.
2. You want to run the app/package.json
3. How?

```
    "scripts": {
        "postinstall": "npm install --prefix ./app",
        "build:app": "npm run build --prefix ./app",
        "serve:app": "npm run serve --prefix ./app"
    },
```

