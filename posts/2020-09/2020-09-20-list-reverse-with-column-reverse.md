---
title: TIL reversing HTML lists
date: 2020-09-20
published: true
tags: ["html", "css"]
series: false
canonical_url: false
description: "Today I learned about the reverse attribute"
layout: layouts/post.njk
---

Today I learned about the reverse attribute.

```html
<ol reversed>
  <li>Apples</li>
  <li>Oranges</li>
  <li>Pears</li>
  <li>Lemons</li>
</ol>
```
This will display:
> 4. Apples
> 3. Oranges
> 2. Pears
> 1. Lemons

Make it a flex-direction column-reverse, and boom --

This will display:
> 1. Lemons
> 2. Pears
> 3. Oranges
> 4. Apples


The codepen: https://codepen.io/elijahmanor/pen/zYqNxgE


![](https://elijahmanor.com/_next/static/chunks/images/pun-interview-28a112dd7fe5dd5d20e0ab3b9bde2971.png)


REFERENCE:
via https://elijahmanor.com/blog/pun-interview
