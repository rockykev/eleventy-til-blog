---
title: TIL about testing your android directly with Chrome Devtools
date: 2020-09-02
published: true
tags: ["testing", "mobile"]
series: false
canonical_url: false
description: "Web developers have a nice Chrome DevTools to do work in. How about Mobile dev"
layout: layouts/post.njk
---

Web developers have a nice Chrome DevTools to do work in.
But native testing is ideal.
Fortunately, it's pretty to set up for Android Phones.
(Can't say the same for iOS)

1. I plugged my phone directly into my Win10 computer.
2. I turned on Developer Mode on my Phone.
3. I went to `chrome://inspect#devices.`
4. Boom - phone was synced to and any changes I made in Devtools appeared in my phone.

REF:
https://developers.google.com/web/tools/chrome-devtools/remote-debugging
