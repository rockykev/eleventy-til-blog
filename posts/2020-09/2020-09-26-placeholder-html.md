---
title: TIL that you shouldn't use Placeholders as inputs
date: 2020-09-26
published: true
tags: ["accessibility", "ux", "html"]
series: false
canonical_url: false
description: "Placeholder texts aren't labels."
layout: layouts/post.njk
---

Placeholder texts aren't labels.
They are tiny support element to a label.

They make the fields lack clarity when they’ve been answered.

![](2020-09-26-placeholder-html_0.png)

Via Adam Silver: https://adamsilver.io/blog/placeholders-are-problematic/
