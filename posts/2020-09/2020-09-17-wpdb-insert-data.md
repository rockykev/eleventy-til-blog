---
title: TIL about using wpdb to insert data into the WordPress Database
date: 2020-09-17
published: true
tags: ["wordpress"]
series: false
canonical_url: false
description: "using wpdb to insert data into the WordPress Database"
layout: layouts/post.njk
---



```php
function shove_this_into_the_database() {

   global $wpdb;
    $tableName = $wpdb->usermeta;
    $tableData = [
        'user_id' => 1,
        'meta_key' => "cubby_cat",
        'meta_value' => "OH LAWD, he coming!"
];

    $wpdb->insert($tableName, $tableData);

    echo "The record has been set!";
}
```

![](2020-09-17-wpdb-insert-data_0.png)

REF:
https://stackoverflow.com/questions/26582193/insert-data-into-wordpress-database-table-from-a-custom-form
