---
title: TIL about the bullet ::marker
date: 2020-09-12
published: true
tags: ["css"]
series: false
canonical_url: false
description: "When you have a list element, you had to hack the css to change bullet items. Not anymore!"
layout: layouts/post.njk
---

When you have a list element, you had to hack the css to change bullet items.

DEFAULT:
* Item 1
* Item 2
* Item 3

You did that by hiding the bullet, then using a `::before` class with content to replace the bullet with something else.

Now, there's a CSS psuedo element called ::marker, which makes it a lot easier to modify bullets!

```css
ol li::marker {
  content: "🧡 ";
}

```



REF:
https://web.dev/css-marker-pseudo-element/
https://css-tricks.com/almanac/selectors/m/marker/
