---
title: TIL Waiting for video to load event listeners
date: 2020-09-04
published: true
tags: ["js"]
series: false
canonical_url: false
description: "I discovered the Media Events `loadedmetadata, loadeddata, canplay, canplaythrough`"
layout: layouts/post.njk
---


I wanted to fire an action in JS when there was enough video loaded that wouldn't cause a weird buffer.

I discovered the Media Events `loadedmetadata, loadeddata, canplay, canplaythrough`


```js

const video = document.querySelector("video");

video.addEventListener("loadeddata", () => {

  console.log("I have enough data to play now!");

});

```

So what's the difference between the Media Events?

(Media Events)[https://html.spec.whatwg.org/multipage/media.html#ready-states]

They return true when the readyState hits a certain threshold.

* loadedmetadata -> readyState is newly equal to HAVE_METADATA or greater for the first time.
* loadeddata -> readyState newly increased to HAVE_CURRENT_DATA or greater for the first time.
* canplay -> readyState newly increased to HAVE_FUTURE_DATA or greater.
* canplaythrough -> readyState is newly equal to HAVE_ENOUGH_DATA.

(Check their ready states)[https://html.spec.whatwg.org/multipage/media.html#ready-states]

HTML Documentation:
https://html.spec.whatwg.org/multipage/media.html#dom-media-have_current_data
