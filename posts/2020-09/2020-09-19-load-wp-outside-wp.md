---
title: TIL How to Summon WordPress in random PHP files
date: 2020-09-19
published: true
tags: ["wordpress", "php"]
series: false
canonical_url: false
description: "How to use WordPress Helper functions outside of WordPress"
layout: layouts/post.njk
---

Say you had a file that was outside of WordPress.
For example, when you submit a form, you can compile the data with pure PHP, and not use any WordPress.

But maybe you want to call a wp_options setting, like a API key that you saved.

```php
// THIS WON'T WORK
$sekrit_key = get_options('my_secret_api_key')
```

Well, then you NEED to get all the helper functions of WordPress.

You can do that with this snippet:

```php
define( 'WP_USE_THEMES', false ); // Don't load theme support functionality
require( './wp-load.php' );
```

> wp-load.php is the access to all functions of WordPress, that's all. The first line tells WordPress to load not the Theme files; maybe the files are necessary for your requirements, then remove the line.

Now, this works. `get_options('my_secret_api_key')`


REF:
https://wordpress.stackexchange.com/a/47052/132722
