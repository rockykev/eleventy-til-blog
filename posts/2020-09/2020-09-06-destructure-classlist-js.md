---
title: TIL how to check if a dom element has a specific class
date: 2020-09-06
published: true
tags: ["js", "css"]
series: false
canonical_url: false
description: "How to destructure the classList into an array so you can use include()"
layout: layouts/post.njk
---

Trying to figure out if your button has a `.red` or `.blue` class in JS?
Destructure the classList into an array so you can use include().

```js
const theButtonsClasses = [ ...document.querySelector("#theButton").classList];

if (theButtonsClasses.includes("red")) {
  console.log("This button has the red class!");
} else if (theButtonsClasses.includes("blue")) {
    console.log("This button has the blue class!");
}

```

MDN On Includes:
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes
