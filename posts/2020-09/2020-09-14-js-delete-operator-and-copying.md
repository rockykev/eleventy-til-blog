---
title: TIL Deleting Object Properties and Shallow Copy
date: 2020-09-14
published: true
tags: ["js"]
series: false
canonical_url: false
description: "Shallow copy objects to safely delete properties"
layout: layouts/post.njk
---

I wanted to loop through a object, but ignore a few object properties.

This is just an example:

```js
const theObject = {
  'James' : 20,
  'Sarah' : 20,
  'Steve' : 40,
  'Bill' : 30,
};
```
I wanted to remove `Sarah`, so the For...in loop would be a lot cleaner.

You can use the [delete](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/delete) operator.

`delete theObject['Sarah']`

BUT, this will mutate theObject, and Sarah will be gone forever.

Well, you can copy it.
```js

// WRONG WAY
// since it passes it by reference
// this will delete Sarah from passByReferenceObject AND theObject
const passByReferenceObject = theObject;
delete passByReferenceObject['Sarah'];

// PROPER WAY
// This is making a shallow copy
// And will delete Sarah only from the copy
const copyOfObject = Object.assign({}, data);
delete copyOfObject['Sarah']

```

If there were nestled objects, going into the deep copy is a better method.

REF:
https://www.javascripttutorial.net/object/3-ways-to-copy-objects-in-javascript/
