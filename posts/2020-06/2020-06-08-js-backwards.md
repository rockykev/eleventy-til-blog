---
title: TIL that JS is backwards compatible
date: 2020-06-08
published: true
tags: ['javascript', 'you-dont-know-js']
series: true
canonical_url: false
description: "JS you so crazy"
layout: layouts/post.njk
---

tl;dr: TC39 often proclaim, "We don't break the web!" with new advancements in JS.

Note: TC39 is the ECMA Technical Committee overseeing the standardization of ECMAScript.

Backwards Compatibility: (Thanks to transcompilers like Babel)
```
Backwards compatibility means that once something is accepted as valid JS, there will not be a future change to the language that causes that code to become invalid JS. Code written in 1995—however primitive or limited it may have been!—should still work today. As TC39 members often proclaim, “we don't break the web!”

The idea is that JS developers can write code with confidence that their code won't stop working unpredictably because a browser update is released. This makes the decision to choose JS for a program a more wise and safe investment, for years into the future.
```

Not Forward Compatability:

It doesn't skip over future additions, and insteads break. Preferrably, you're running that code through Babel to ensure that it works with old versions in JS.

```
JS is not forwards-compatible, despite many wishing such, and even incorrectly believing the myth that it is.

HTML and CSS, by contrast, are forwards-compatible but not backwards-compatible. If you dug up some HTML or CSS written back in 1995, it's entirely possible it would not work (or work the same) today. But, if you use a new feature from 2019 in a browser from 2010, the page isn't “broken” – the unrecognized CSS/HTML is skipped over, while the rest of the CSS/HTML would be processed accordingly.
```

Via [You Don't Know JS (2nd edition)](https://github.com/getify/You-Dont-Know-JS/blob/2nd-ed/get-started/README.md)
