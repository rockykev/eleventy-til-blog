---
title: TIL Higher Bootstrap screen resolutions
date: 2020-06-27
published: true
tags: ['webdev', 'bootstrap']
series: false
canonical_url: false
description: "Bootstrap"
layout: layouts/post.njk
---

Top 6 Desktop Screen Resolutions (via [Statcounter](https://gs.statcounter.com/screen-resolution-stats/desktop/worldwide))
(as of 2020-06-27)
* 1366x768 - 23.49%
* 1920x1080 - 19.91%
* 1536x864 - 8.65%
* 1440x900 - 7.38%
* 1280x720 - 4.89%
* 1600x900 - 4.01%

Why are [bootstrap breakpoints](https://getbootstrap.com/docs/4.5/layout/overview/) so low?
```
$grid-breakpoints: (
  xs: 0,
  sm: 576px,
  md: 768px,
  lg: 992px,
  xl: 1200px
);
```

Instead, use these breakpoints.
```
/* set the overriding variables */
$grid-breakpoints: (
  xxxs: 0,
  xxs: 320px,
  xs: 568px,
  sm: 667px,
  md: 768px,
  lg: 992px,
  xl: 1200px,
  xxl: 1440px,
  xxxl: 1600px
);
```
> With this method, we've added the new grid breakpoints, and ensured these new breakpoints work everywhere in Bootstrap including the grid, responsive utilities for spacing, display, flexbox, alignment, positioning, etc...

[via Stackoverflow](https://stackoverflow.com/questions/48924751/how-to-create-new-breakpoints-in-bootstrap-4-using-cdn/48976550#48976550)
