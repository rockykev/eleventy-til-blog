---
title: TIL Github workflows
date: 2020-06-21
published: true
tags: ['javascript', 'npm']
series: false
canonical_url: false
description: "Deploy your Github master files to FTP"
layout: layouts/post.njk
---

Automate deployment.

Issue: You have a ftp site. You also have Github. You wish that every time you push to master, the files on your production server get updated via FTP. (Note, there is a different setup if you have a SFTP setup.)

Solution:
This [workflow](https://github.com/marketplace/actions/ftp-deploy) will set that up for you:

Credentials you'll need:
```
* ftp-server address
* ftp-username
* ftp-password
```

Actions:
```
1. Select the repository you want to add the action to
2. Select the Actions tab
3. Select Blank workflow file or Set up a workflow yourself, if you don't see these options manually create a yaml file Your_Project/.github/workflows/main.yml
4. Paste the example above into your yaml file and save
5. Now you need to add a key to the secrets section in your project. To add a secret go to the Settings tab in your project then select Secrets. Add a new Secret for ftp-password
6. Update your yaml file settings
```

