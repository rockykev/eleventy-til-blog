---
title: TIL to Reset WP passwords
date: 2020-06-13
published: true
tags: ['wordpress']
series: false
canonical_url: false
description: "Reset WP Passwords"
layout: layouts/post.njk
---

While working locally, I always reset the WordPress username/password to something easy.

Of course, don't run this on your LIVE SITES. EVER.

In your myPhpAdmin(or favorite sql client) - access your WordPress database and add this query.
IF there's no `admin`, change it to something else.

SQL:
```
UPDATE wp_users SET user_pass = MD5('password') WHERE user_login = "admin";
```

