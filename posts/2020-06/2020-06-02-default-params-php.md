---
title: TIL Default Params in PHP
date: 2020-06-02
published: true
tags: ['php']
series: false
canonical_url: false
description: "Default Params"
layout: layouts/post.njk
---

In your markup file:

```
<?php //echo do_shortcode('[rockyShortcode color="red"]' . "BLAH BLAH BLAH" . "[/rockyShortcode]"); ?>
```

In your PHP file:
```
add_shortcode('rockyShortcode', 'rockysFunction'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Throwaway function
function rockysFunction($atts, $content = null)
{

    $atts = shortcode_atts(array(
        'color'  => 'blue',
        'size' => '24px'
    ), $atts);

    $textColor = 'color: ' . esc_attr($atts['color']) . ';';
    $textSize = 'font-size: ' . esc_attr($atts['size']) . ';';

    $element = '<div class="custom-class" style="' . $textColor . $textSize .  '">';
    $element .= $content;
    $element .= '</div>';

    return $element;

}
```
