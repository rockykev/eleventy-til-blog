---
title: TIL Axe for Accessibility
date: 2020-06-01
published: true
tags: ['webdev', 'accessibility']
series: false
canonical_url: false
description: "Be more accessible."
layout: layouts/post.njk
---

Axe is a software for web accessibility. It analyzes a page to determine if there's anything missing or not.

There's a [free chrome extension](https://chrome.google.com/webstore/detail/axe-web-accessibility-tes/lhdoppojpmngadmnindnejefpokejbdd/) and a paid service version.

Here's an example of me analyzing Little Ceasar's Contact Us using the free chrome extension.
![screenshot](/img/2020-06-01-axe-screenshot.png)
