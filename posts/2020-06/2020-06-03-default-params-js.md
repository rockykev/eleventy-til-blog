---
title: TIL Default Params in Javascript
date: 2020-06-03
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Default Params"
layout: layouts/post.njk
---

ES6 version:
```
function say(message='Hi') {
    console.log(message);
}

say(); // 'Hi'
say(undefined); // 'Hi'
say('Hello'); // 'Hello'

```

```
function createDiv(height = '100px', width = '100px', border = 'solid 1px red') {
    let div = document.createElement('div');
    div.style.height = height;
    div.style.width = width;
    div.style.border = border;
    document.getElementsByTagName('body')[0].appendChild(div);
    return div;
}
```
[reference](https://www.javascripttutorial.net/es6/javascript-default-parameters/)

