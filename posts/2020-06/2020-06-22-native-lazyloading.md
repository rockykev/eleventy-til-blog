---
title: TIL About Native Lazy Loading
date: 2020-06-22
published: true
tags: ['vanilla', 'html', 'optimization']
series: false
canonical_url: false
description: "Native Lazy Loading is a thing!"
layout: layouts/post.njk
---

Native Lazy Loading.

```
<img src="myimage.jpg" loading="lazy" alt="..." />
<iframe src="content.html" loading="lazy"></iframe>
```

[#1 on the list](https://www.sitepoint.com/five-techniques-lazy-load-images-website-performance/)

Note: Not every browser supports it. But it's a lightweight and frictionless way to set up lazyloading.

