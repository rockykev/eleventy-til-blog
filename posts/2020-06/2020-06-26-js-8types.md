---
title: TIL that JS has 8 data types
date: 2020-06-26
published: true
tags: ['javascript', 'basics']
series: false
canonical_url: false
description: "JS you so crazy"
layout: layouts/post.njk
---

| Data Type | Example | Notes |
| --- | --- | --- |
| Number | `let n = 123;` | Number.MAX_SAFE_INTEGER == (253 - 1) |
| BigInt | `const bigInt = 1234567890123456789012345678901234567890n;` | the "n" means it's a BigInt; larger than (253-1) |
| String | `let str = "Hello";` | --- |
| Boolean | `let nameFieldChecked = true;` | true or false |
| "null" | `let age = null;` | Note 1 |
| "undefined" | `let age;` | // shows "undefined" |
| Objects | `let o = new Object()` | [more info](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object) |
| Symbols | `let sym = Symbol('foo')` | Note 2 |

Note 1: In JavaScript, null is not a “reference to a non-existing object” or a “null pointer” like in some other languages. It’s just a special value which represents “nothing”, “empty” or “value unknown”. Similarly to undefined, null is the only value of its own type. However, null is also a liar. Due to a bug in JavaScript, it pretends to be an object.
`console.log(typeof(null)); // "object" (a lie!)`

Note 2: Symbols are often used to identify object properties. Often to avoid name clashing between properties, since no symbol is equal to another. Or to add properties that the user cannot overwrite, intentionally or without realizing.

[reference](https://javascript.info/types)
[reference for symbols](https://flaviocopes.com/javascript-symbols/#:~:text=Symbols%20are%20often%20used%20to,overwrite%2C%20intentionally%20or%20without%20realizing.)
