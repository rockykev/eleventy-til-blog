---
title: TIL Node Version Manager
date: 2020-06-19
published: true
tags: ['javascript', 'npm']
series: false
canonical_url: false
description: "Node Version Manager might be the reason everything's broken in your life."
layout: layouts/post.njk
---

Ever clone a project and suddenly get a `error` when you build?
One thing to investigate is the node version.

For example:
Say the developer of this amazing animation library is using Node 13.5.0 while you're still on 10.16.3. Their code is definitely using the latest features for Node (or has a dependency that is using something important.)

But you have a legacy project that you've been managing on 10.16.3. It's still good, and doesn't need any bells or whistles. And upgrading to anything or higher causes it to fail.

Node Version Manager is perfect for this!

You can install [nvm for windows](https://github.com/coreybutler/nvm-windows), or [nvm for linux/mac](https://github.com/nvm-sh/nvm)

Note: When you `npm i`, you are installing a bunch of packages based on the version of node you are currently using. If you're using 10.16.3 and `npm i` a project that's meant for node 13 or higher, you're going to have a bad time. Delete your `node_modules` and start over.
