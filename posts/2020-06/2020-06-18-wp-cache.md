---
title: TIL WordPress cache busting
date: 2020-06-18
published: true
tags: ['wordpress', 'caching']
series: false
canonical_url: false
description: "Importing scripts into WordPress causes it to cache. Cache bust them!"
layout: layouts/post.njk
---

If you import scripts in a php file, they'll be cached.

Example:
```
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/forms/forms.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/scripts/jquery.mask.min.js"></script>
```

If you ever modify forms.js, you'll need to add a versioning to it. Else it'll stay cached in the site.

```
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/forms/forms.js?v=<?php echo uniqid();?>"></script>
```
the `uniqid()` function will generate a random number, which gets tacked onto the versioning.

A better alternative is enqueueing js files via functions.php, so you can control what goes in and what goes out. [Why you should register scripts.](https://developer.wordpress.org/reference/functions/wp_register_script/)
```
 wp_register_script('form', get_template_directory_uri() . '/assets/js/scripts/forms.js', array(), uniqid());
```
