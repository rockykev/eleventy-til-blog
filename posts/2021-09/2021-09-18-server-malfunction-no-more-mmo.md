---
title: TIL A server malfunction took down a whole MMO permanently
date: 2021-09-18
published: true
tags: ['gaming', 'servers']
series: false
canonical_url: false
description: "This screams 'We didn't have source control or code management'."
layout: layouts/post.njk
---

This screams "We didn't have source control or code management".

> Players of the Japanese MMORPG M2, however, got no fond adieu when the game was taken down out of the blue, permanently, due to a simple server malfunction.

> Sankando and Hangame, the development company and operators respectively, released a statement that explained what happened. Basically, after a critical server error, Hangame jumped into action with emergency measures that were ultimately unsuccessful and all the game’s data was irretrievably lost. There were no survivors backups. Acknowledging that the game can not be restored in any meaningful way, Sankando and Hangame have issued an apology, for what that’s worth.


If there's a clear sign of why it's important to set things up properly, here it is.
Damn well crushed the entire business.

[Server Failure Permanently Ends Japanese MMORPG](https://www.themarysue.com/server-failure-kills-mmorpg/)
