---
title: TIL the vitality curve (aka Stack Ranking)
date: 2021-09-10
published: true
tags: ['management', 'work', 'amazon']
series: false
canonical_url: false
description: "A vitality curve is a performance management practice that calls for individuals to be ranked or rated against their coworkers."
layout: layouts/post.njk
---

A vitality curve is a performance management practice that calls for individuals to be ranked or rated against their coworkers. It is also called stack ranking, forced ranking, and rank and yank. Pioneered by GE's Jack Welch in the 1980s, it has had beneficial internal productivity effects, but has remained controversial due to job entitlement and fear of discrimination. Numerous companies practice it, but mostly covertly to avoid direct criticism.

[Via Wikipedia](https://en.wikipedia.org/wiki/Vitality_curve)

It ranks employees on a metric like this:
![](2021-09-10-vitality-curve_0.png)

What it leads to is a competitive environment. Teams will fire the bottom ranks, or refuse to work with low-ranking personell for fear of dropping their own points.


## Issues with the Vitality Curve

* Morale: It pits employees against each other.
* Cost: For companies, it's a lot of performance reviews and metrics.
* Discrimination: Lawsuits! "because they favor some groups of employees over others: white males over blacks and women, younger managers over older ones and foreign citizens over Americans".


> Travis Bradberry notes that "Some individual talents follow a natural bell-shaped curve, but job performance does not. When you force employees to fit into a pre-determined ranking system, you do three things: 1) incorrectly evaluate people’s performance, 2) make everyone feel like a number, and 3) create insecurity and dissatisfaction when performing employees fear that they’ll be fired due to the forced system.

> Rank and yank contrasts with the management philosophies of W. Edwards Deming, whose broad influence in Japan has been credited with Japan's world leadership in many industries, particularly the automotive industry. "Evaluation by performance, merit rating, or annual review of performance" is listed among Deming's Seven Deadly Diseases. It may be said that rank-and-yank puts success or failure of the organization on the shoulders of the individual worker.

> Amazon holds a yearly Organization Level Review, where managers debate subordinates’ rankings, assigning and reassigning names to boxes in a matrix projected on the wall. In recent years, other large companies, including Microsoft, General Electric and Accenture Consulting, have dropped the practice — often called stack ranking, or “rank and yank” — in part because it can force managers to get rid of valuable talent just to meet quotas.

It also puts a lot of weight on the employee.

> The employees who stream from the Amazon exits are highly desirable because of their work ethic, local recruiters say. In recent years, companies like Facebook have opened large Seattle offices, and they benefit from the Amazon outflow. Recruiters, though, also say that other businesses are sometimes cautious about bringing in Amazon workers, because they have been trained to be so combative. The derisive local nickname for Amazon employees is "Amholes" — pugnacious and work-obsessed.


