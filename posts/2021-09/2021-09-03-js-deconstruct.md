---
title: TIL Deconstructing Objects
date: 2021-09-03
published: true
tags: ['javascript', 'codepen', 'objects']
series: false
canonical_url: false
description: "Rather than create a big list of parameters in your function, pass the whole object and deconstruct it."
layout: layouts/post.njk
---

Rather than create a big list of parameters in your function, pass the whole object and deconstruct it.

```js
function getItem(args) {
    const {price, quantity, name, type} = args;
    const total = price * quantity;

  return `For the ${type} ${name}: you're getting ${quantity} ${name} at $${price} each. Final price is $${total}`
}

const buyingBananas = getItem({
    name: 'bananas',
    price: 10,
    quantity: 1,
    type: 'fruit'
});

console.log(buyingBananas);

```

<p class="codepen" data-height="300" data-slug-hash="ExXgvOK" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/ExXgvOK">
  </a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

REFERENCE:
Tip #5 in [7 Little Big JavaScript Techniques That Makes Your Code Better](https://medium.com/madhash/7-little-big-javascript-techniques-that-makes-your-code-better-7894d3f984c5)
