---
title: TIL How annoying interviews can be
date: 2021-09-26
published: true
tags: ['lol', 'algorithms', 'interviews']
series: false
canonical_url: false
description: "For web developers, the're a neat parlor trick. Interviews should just do away with them."
layout: layouts/post.njk
---

Ah, algorithms.

![](2021-09-26-lol-algorithms-for-interviews_0.png)


By Algorithms, I'm referring to:

[The linked list, tree sort, hash table, stack blah blah blah](https://github.com/trekhleb/javascript-algorithms)

If I ever had to use one:
1. I pulled it from a library
2. I found a code snippet

For web developers, the're a neat parlor trick.
Interviews should just do away with them.
