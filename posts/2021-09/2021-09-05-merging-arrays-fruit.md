---
title: TIL spreading an array into another array
date: 2021-09-05
published: false
tags: ['javascript', 'arrays']
series: false
canonical_url: false
description: "So to merge an array, use the spread operator. '...' <-- that guy"
layout: layouts/post.njk
---

So to merge an array:

```js
let apples = ['🍎', '🍏'];
let fruits = ['🍉', '🍊', '🍇', ...apples];
let fruitsAppleFirst = [...apples, '🥭', '🍌', '🍒'];

console.log( fruits ); //=> ["🍉", "🍊", "🍇", "🍎", "🍏"]

console.log( fruitsAppleFirst );//=> ["🍎", "🍏", "🥭", "🍌", "🍒"]
```


Via [10 Awesome JavaScript Shorthands](https://dev.to/palashmon/10-awesome-javascript-shorthands-4pek)
