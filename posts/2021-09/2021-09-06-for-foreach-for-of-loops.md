---
title: TIL the history of the for, forEach, and for...of loops
date: 2021-09-06
published: true
tags: ['arrays', 'javascript']
series: true
canonical_url: false
description: "for loop came first. Then forEach. With new iterables, for...of was adopted."
layout: layouts/post.njk
---


First, we had the `for` loop.

The problem with `for` loops are:
* off-by-one errors
* A chance to create infinite loops
* Statefulness
* hidden intent

![](2021-09-06-for-foreach-for-of-loops_0.png)

IT's explained here [4 Reasons Not to Use Programming Loops (and a Few Ways to Avoid Them)](https://thenewstack.io/4-reasons-not-to-use-programming-loops-and-a-few-ways-to-avoid-them/)


Then, we had the `forEach` loop.
This new version was introduced in 2009 with Ecmascript 5.

It solved a bunch of problems. It's a fine solution.

Finally, we have the `for ... of` loop.
As new objects like `set`, `map` were created, they needed a solution.

`forEach` only works on `arrays`.

`for ... of` works on:
* Array
* Map
* Set
* String
* TypedArray
* Other W3C classes

It supports all kinds of control flow in the loop body, like `continue`, `break`, `return`, `yield` and `await`.

Resources:
[https://stackoverflow.com/questions/50844095/should-one-use-for-of-or-foreach-when-iterating-through-an-array](https://stackoverflow.com/questions/50844095/should-one-use-for-of-or-foreach-when-iterating-through-an-array)
[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/for...of)
