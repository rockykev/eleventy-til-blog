---
title: TIL the difference between throttling and debouncing
date: 2021-09-30
published: true
tags: ['javascript', 'events']
series: false
canonical_url: false
description: "Throttling enforces a maximum number of times a function can be called over time. Debouncing enforces that a function not be called again until a certain amount of time has passed without it being called."
layout: layouts/post.njk
---

I always get these two confused.


`Throttling` enforces a maximum number of times a function can be called over time. As in “execute this function at most once every 100 milliseconds.”

A easy way to remember: throttling your car, your car can only go so fast.

`Debouncing` enforces that a function not be called again until a certain amount of time has passed without it being called. As in “execute this function only if 100 milliseconds have passed without it being called.”

A easy way to remember: When a ball bounces, when it lands then you can do something else.

[Via CSS Tricks](https://css-tricks.com/the-difference-between-throttling-and-debouncing/)
