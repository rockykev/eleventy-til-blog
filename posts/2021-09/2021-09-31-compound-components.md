---
title: TIL Compound Components in React
date: 2021-09-31
published: true
tags: ['react', 'kentdobbs', 'components']
series: false
canonical_url: false
description: "Compound components, where you combine parent/child data."
layout: layouts/post.njk
---

I learned about compound components, where you combine parent/child data.

> Think of compound components like the <select> and <option> elements in HTML. Apart they don't do too much, but together they allow you to create the complete experience. The way they do this is by sharing implicit state between the components. Compound components allow you to create and use components which share this state implicitly.— Kent C. Dodds



[5:30 seconds in the Codepen Podcast](https://podcasts.google.com/feed/aHR0cHM6Ly9ibG9nLmNvZGVwZW4uaW8vZmVlZC9wb2RjYXN0/episode/aHR0cHM6Ly9ibG9nLmNvZGVwZW4uaW8vP3A9MTU3Nzg?ep=14)


A better explainer here:
[Compound Components in React](https://medium.com/unibuddy-technology-blog/compound-components-in-react-b04772f9eb58)
