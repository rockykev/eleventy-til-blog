---
title: TIL how competitive shooters make maps
date: 2021-09-29
published: true
tags: ['gamedev', 'maps']
series: false
canonical_url: false
description: "For multiplayer fps games a la counterstrike, use node graphs."
layout: layouts/post.njk
---


![](2021-09-29-how-multiplayer-maps-are-made_0.png)
[source](https://old.reddit.com/r/GlobalOffensive/comments/8gqd6c/can_you_guess_the_map_from_the_graph_network/)

For multiplayer fps games a la counterstrike, I would use node graphs and come up with the shape later. Like come up with the amount of ways to get to A from spawn, amount of ways to get to b from spawn, if there are any connectors, what do they connect? Then using the nodes to define the amount of pathways you can start blocking out the level and play with timings based on walk speed to tweak choke points.

[Via this comment](https://www.reddit.com/r/gamedesign/comments/pjmt25/comment/hbxhke8/?utm_source=reddit&utm_medium=web2x&context=3)

