---
title: TIL using code-splitting to improve your First Contentful Paint (FCP) score
date: 2021-09-17
published: true
tags: ['javascript', 'seo', 'esmodules', 'webpack', 'vue', 'react', 'frameworks']
series: false
canonical_url: false
description: "Code splitting is a technique where you send only the necessary modules to the user in the beginning."
layout: layouts/post.njk
---


First Contentful Paint (FCP) measures how long it takes the browser to render the first piece of DOM content after a user navigates to your page. Images, non-white <canvas> elements, and SVGs on your page are considered DOM content; anything inside an iframe isn't included.

via the [web.dev](https://web.dev/first-contentful-paint/) on it.

Dynamic Code splitting is a technique where you send only the necessary modules to the user in the beginning. This would greatly impact the FCP score by reducing the size of the payload transmitted initially.

Rather than creating a loading screen, you send just enough JS to get the page to display. Then it continues to load the rest of it (LazyLoading) behind the scenes.

Doing it in React: [How to improve performance in your apps with Lighthouse, Webpack, and React Loadable Components](https://www.freecodecamp.org/news/taming-performance-in-todays-web-app-with-lighthouse-webpack-and-react-loadable-components-b2d3fa04e0ab/)

Doing it with Vue: [How to drastically reduce your bundle size and load time in Vue.js](https://medium.com/shard-labs/how-to-drastically-reduce-your-bundle-size-and-load-time-in-vue-js-54370d513fdf)


> All of your Vuex modules are most likely static modules, which means that they are declared during the initialization of your store. All of those static modules are imported and parsed during your initial load. That will likely lower your page load speed and increase the TTI (Time to Interactive).

> Dynamic modules are the ones that are registered after creating your Vuex store. By lazy loading dynamic modules, we can reduce our bundle size by quite a bit, depending on how much code we have in those modules. Inside your store.js, remove every module that you don’t need on your initial page load. Do you need a messaging module on your welcome page? Most likely not, remove it. Now you just need to think about where you will load your dynamic modules.


