---
title: TIL rimraf, a rm alternative
date: 2021-09-24
published: false
tags: ['node', 'terminal']
series: false
canonical_url: false
description: "rm is platform specific (and certainly will not work on Windows) and rimraf deals with this for us."
layout: layouts/post.njk
---


`rm` is platform specific (and certainly will not work on Windows) and `rimraf` deals with this for us.

https://www.npmjs.com/package/rimraf


[Via this blog comment about how to deploy to Github Pages from your terminal](https://dev.to/mcshaz/comment/10emn)
