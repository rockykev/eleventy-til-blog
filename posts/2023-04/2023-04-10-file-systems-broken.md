---
title: TIL File Systems are broken
date: 2023-04-10
published: true
tags: ["harddrive", "files", "os", "conference"]
series: false
canonical_url: false
description: "The real question: Why the file API so hard to use that even experts make mistakes?"
layout: layouts/post.njk
---


[Source: Files are fraught with peril](https://danluu.com/deconstruct-files/)

[Conference Talk](https://www.deconstructconf.com/2019/dan-luu-files)

I watched the presentation, read the transcript, read through comments and conversations, and I still feel like i only understand it at a surface level.

This is 'beyond my understandings', but that's how we learn, right?


## Tl;dr

It starts with this:

> But aren't most modern file systems about on par with each other?

> ...let's look at the top comments from Reddit r/ programming, are from when Dropbox decided to remove support for all but the most common file system on Linux. The top comment reads, I'm a bit confused. Why do these applications have to support these file systems directly? The only differences I could possibly see between different file systems are file size limitations and permissions.

The answer. NOPE. File systems is confusing.


## File API

Via the Presentation:
Let's say we want to write a file safely, so that we don't want to get data corruption. For the purposes of this talk, this means we'd like our write to be "atomic" -- our write should either fully complete, or we should be able to undo the write and end up back where we started. Let's look at an example from [Pillai et al., OSDI’14.](https://www.usenix.org/conference/osdi14/technical-sessions/presentation/pillai)

Pillai et al., OSDI’14 looked at a bunch of software that writes to files, including things we'd hope write to files safely, like databases and version control systems: Leveldb, LMDB, GDBM, HSQLDB, Sqlite, PostgreSQL, Git, Mercurial, HDFS, Zookeeper. They then wrote a static analysis tool that can find incorrect usage of the file API, things like incorrectly assuming that operations that aren't atomic are actually atomic, incorrectly assuming that operations that can be re-ordered will execute in program order, etc.

When they did this, they found that **every single piece of software they tested except for SQLite in one particular mode had at least one bug**.

This isn't a knock on the developers of this software or the software -- the programmers who work on things like Leveldb, LBDM, etc., know more about filesystems than the vast majority programmers and the software has more rigorous tests than most software.

But they still can't use files safely every time!

A natural follow-up to this is the question: why the file API so hard to use that even experts make mistakes?

## Why is this the case?

Again, via the presentation:


1 - Concurrent programming is hard

...we see bugs come from things like "incorrectly assuming operations are atomic" and "incorrectly assuming operations will execute in program order".

2 - Inconsistent APIs

even when using one filesystem, different modes may have significantly different behavior. Large parts of the file API look like this, where behavior varies across filesystems or across different modes of the same filesystem. For example, if we look at mainstream filesystems, appends are atomic, except when using ext3 or ext4 with data=writeback, or ext2 in any mode and directory operations can't be re-ordered w.r.t. any other operations, except on btrfs. In theory, we should all read the POSIX spec carefully and make sure all our code is valid according to POSIX, but if they check filesystem behavior at all, people tend to code to what their filesystem does and not some abtract spec.

3 - Unclear docs

It's theoretically possible to figure everything out from reading the source code, but this is pretty impractical for most people who don't already know how the filesystem works.

While the filesystem developers tend to be helpful and they write up informative responses, most people probably don't keep up with the past 6-8 years of LKML.

4 - Performance / correctness conflict

Another issue is that the file API has an inherent conflict between performance and correctness. We noted before that fsync is a barrier (which we can use to enforce ordering) and that it flushes caches. If you've ever worked on the design of a high-performance cache, like a microprocessor cache, you'll probably find the bundling of these two things into a single primitive to be unusual. A reason this is unusual is that flushing caches has a significant performance cost and there are many cases where we want to enforce ordering without paying this performance cost. Bundling these two things into a single primitive forces us to pay the cache flush cost when we only care about ordering.

## Conclusion

In conclusion, computers don't work (but you probably already know this if you're here at Gary-conf). This talk happened to be about files, but there are many areas we could've looked into where we would've seen similar things.

One thing I'd like to note before we finish is that, IMO, the underlying problem isn't technical. If you look at what huge tech companies do (companies like FB, Amazon, MS, Google, etc.), they often handle writes to disk pretty safely. They'll make sure that they have disks where power loss protection actually work, they'll have patches into the OS and/or other instrumentation to make sure that errors get reported correctly, there will be large distributed storage groups to make sure data is replicated safely, etc. We know how to make this stuff pretty reliable. It's hard, and it takes a lot of time and effort, i.e., a lot of money, but it can be done.


## More Explainers

[1](https://reddit.com/r/programming/comments/cduolb/dan_luu_deconstruct_files/)
[2](https://news.ycombinator.com/item?id=25090760)
[3](https://www.reddit.com/r/programming/comments/ydzlvx/files_are_fraught_with_peril/)

