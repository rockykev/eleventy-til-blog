---
title: TIL Fictitious Entries
date: 2023-04-23
published: true
tags: ["searchengine", "machinelearning", "seo", "maps"]
series: false
canonical_url: false
description: "Spoilers: a tale of a map making a fake town called Agloe, then a guy thinking it was real moved there and made the Agloe General Store."
layout: layouts/post.njk
---

I was thinking about how LLMs are great BS generators.

I'm reminded of paper towns.

> Fictitious or fake entries are deliberately incorrect entries in reference works such as dictionaries, encyclopedias, maps, and directories. There are more specific terms for particular kinds of fictitious entry, such as Mountweazel, trap street, paper town, phantom settlement, and nihilartikel

They're mostly used to detect patent issues. Like if a map company made `Faketown, OR` and rival map company suddenly has `Faketown, OR`, then there's a opportunity for a lawsuit.

it's called a [Fictitious entry](https://en.wikipedia.org/w/index.php?title=Fictitious_entry).



But there was also the time Bing was copying Google's data. via [Google’s “Hiybbprqag” Gets the Spotlight](https://www.searchenginejournal.com/googles-hiybbprqag-gets-the-spotlight/27703/)

> What is “hiybbprqag”? Prior to accusing Bing of copying their search engine results, Google wanted to be sure that those accusations could be backed up. While they noticed some abnormalities in existing search results that could be tracked and presented, they wanted to present something even more solid. To do this, they invented several words, including “mbzrxpgjys,” “juegosdeben1ogrande,” and of course “hiybbprqag.” Google then loaded an unrelated site manually into their SERP for these terms. After a couple months of no results from Bing, the Microsoft search engine result was topped with the loaded, bogus site.





And here's a situation where it became real! [THE FAKE TOWN THAT BECAME REAL (BRIEFLY)](https://nowiknow.com/reverse-cartography/)


> the General Drafting Company in 1937 did just this with the town of Agloe, creating it out of thin air at the intersection of two dirt roads just a few miles from Roscoe.... And a few decades later, Agloe appeared again, but this time on a map made by a different, unrelated company — Rand McNally. General Drafting thought they had caught Rand McNally red-handed, but Rand McNally had an incredibly good and surprising defense:

> The county clerk’s office had given them the information.

> It turns out that, in the early part of the 1950s, someone armed with the General Drafting map went to visit Agloe. Seeing nothing there, they figured that opportunity had knocked. This lost-to-history fellow, likely figuring that others would also come to Agloe — it was on the map, after all! — would expect to find something there. So he opened a small shop and called it the “Agloe General Store.”
