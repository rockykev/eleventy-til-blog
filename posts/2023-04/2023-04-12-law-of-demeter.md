---
title: TIL Law of Demeter
date: 2023-04-12
published: true
tags: ["classes", "oop"]
series: false
canonical_url: false
description: "A class should know only its direct dependencies. A better explainer: Don't make a web of classes. You're not a spaghetti chef."
layout: layouts/post.njk
---

Today I learned about the Law of Demeter for OOP.
Via https://www.sitepoint.com/introduction-to-the-law-of-demeter/

> Follow the Law of Demeter: A class should know only its direct dependencies.

This one breaks the law.
```php
<?php
class User {
    private $profile;

    public function __construct() {
        $this->profile = new Profile();
    }

    public function getProfile() {
        return $this->profile;
    }
}

class Profile {
    private $address;

    public function __construct() {
        $this->address = new Address();
    }

    public function getAddress() {
        return $this->address;
    }
}

class Address {
    public function getCity() {
        return 'New York';
    }
}

$user = new User();
$city = $user->getProfile()->getAddress()->getCity(); // Violates the Law of Demeter

```

Notice what's happening? We're getting the profile, which contains a method inside that calls ANOTHER class.



```php
<?php
class User {
    private $profile;

    public function __construct(Profile $profile) {
        $this->profile = $profile;
    }

    public function getCity() {
        return $this->profile->getCity();
    }
}

class Profile {
    private $address;

    public function __construct(Address $address) {
        $this->address = $address;
    }

    public function getCity() {
        return $this->address->getCity();
    }
}

class Address {
    private $city;

    public function __construct($city) {
        $this->city = $city;
    }

    public function getCity() {
        return $this->city;
    }
}

$address = new Address('New York');
$profile = new Profile($address);
$user = new User($profile);
$city = $user->getCity(); // Respects the Law of Demeter
```

Notice that it's not chaining. There's a clear separation.
