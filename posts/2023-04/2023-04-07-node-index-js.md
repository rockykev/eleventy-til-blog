---
title: TIL how Node index.js works
date: 2023-04-07
published: true
tags: ["engine", "node", "conference"]
series: false
canonical_url: false
description: "I always disliked `index.html` being a default file on servers. Node did something interesting. It made `index.js` a default!"
layout: layouts/post.njk
---

I always disliked `index.html` being a default file on servers.

Node did something interesting. It made `index.js` a default!


Ryan Dahl in a [conference talk](https://youtu.be/M3BM9TB-8yA?t=880) mentioned it was a mistake. Why?

via [lIIllIIlllIIllIIl on Reddit](https://www.reddit.com/r/node/comments/128in46/comment/jeja8r3/?utm_source=reddit&utm_medium=web2x&context=3)

> index.js is a mistake because it complicates the module resolution algorithm, and it relies on scanning a large number of files and directories, which isn't realistic on the web where each scan would be a slow network request, leading to incompatible code between Node and the browser.

For example, when you do import "./foo", Node needs to check:

* Is ./foo a file?
* If not, is ./foo.js a file?
* If not, is ./foo.mjs a file?
* If not, is ./foo.cjs a file?
* If not, is ./foo/index.js a file?
* If not, is ./foo/index.mjs a file?
* If not, is ./foo/index.cjs a file?
* And probably a few more checks I'm not too familiar with.

via [Why is `index.js` a mistake in node.js? Ryan Dahl mentions in JSConf](https://www.reddit.com/r/node/comments/128in46/why_is_indexjs_a_mistake_in_nodejs_ryan_dahl/)
