---
title: TIL the Object-View-Box
date: 2023-04-16
published: true
tags: ["images", "css"]
series: false
canonical_url: false
description: "The object-view-box property specifies a 'view box' over an element, similar to the `<svg viewBox>` attribute, zooming or panning over the element’s contents."
layout: layouts/post.njk
---

Problem - I want this:

![](https://i.imgur.com/bGM7jC5.png)


To solve that, you can:

METHOD 1 - Use a image editing software and resize the whole image. (bad)
METHOD 2 - Wrap in an additional element
METHOD 3 - Use the image as a background-image, and modify position/tag.

NEW METHOD 4 - Object-View-Box

## METHOD 1 - Image editing

You crop it.

## METHOD 2 - Wrap in an additional element ng
[How to](https://ishadeed.com/article/css-object-view-box/#wrapping-in-an-additional-element)

```html
<figure>
    <img src="img/brownies.jpg" alt="">
</figure>

<style>
figure {
    position: relative;
    width: 300px;
    aspect-ratio: 1;
    overflow: hidden;
    border-radius: 15px;
}

img {
    position: absolute;
    left: -23%;
    top: 0;
    right: 0;
    bottom: 0;
    width: 180%;
    height: 100%;
    object-fit: cover;
}
</style>
```

## METHOD 3 - Use the image as a background-image
```html
<div class="brownies"></div>

<style>
.brownies {
  width: 300px;
  aspect-ratio: 3 / 2;
  background-image: url("brownies.jpg");
  background-size: 700px auto;
  background-position: 77% 68%;
  background-repeat: no-repeat;
}
</style>
```

## METHOD 4 - Object-View-Box

> The object-view-box property specifies a “view box” over an element, similar to the `<svg viewBox>` attribute, zooming or panning over the element’s contents.

NOTE: It's not fully supported everywhere yet. But wow! It's so much cleaner.

```html
<img src="img/brownies.jpg" alt="">
<style>
img {
    aspect-ratio: 1;
    width: 300px;
    object-view-box: inset(25% 20% 15% 0%);
}
</style>

Via https://ishadeed.com/article/css-object-view-box/?ref=sidebar
