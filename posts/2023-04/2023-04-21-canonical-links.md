---
title: TIL about relationship canonical in links
date: 2023-04-21
published: false
tags: ["link", "blog", "html"]
series: false
canonical_url: false
description: "If I repost this to a different site, say Medium, there's a chance that the Search Engine might decide Medium is the original place and penalize me for 'copying' my own content."
layout: layouts/post.njk
---

I blog a lot.

This post is a blog.

If I repost this to a different site, say Medium, there's a chance that the Search Engine might decide Medium is the original place and penalize me for "copying" my own content.

This author shares his thoughts: https://dev.to/nfrankel/the-importance-of-relcanonical-for-content-writers-4o8f



> There's a single thing to do to set things right. All copies of a page should set the `<link rel="canonical">` attribute:

> A canonical link element is an HTML element that helps webmasters prevent duplicate content issues in search engine optimization by specifying the "canonical" or "preferred" version of a web page. It is described in RFC 6596, which went live in April 2012.

--

How to solve:

> To keep one's original content at the top of search results, a content writer should only crosspost to sites that allow setting the `<link rel="canonical">` attribute. I crosspost to a couple of other sites with this feature: dev.to, Hashnode, DZone (it finally added it), Medium, Foojay, and Hacker Noon.

> If you want to avoid your blog being flagged as duplicate by search engines, you must avoid content aggregators that don't allow setting the `rel=canonical` attribute
use it!

