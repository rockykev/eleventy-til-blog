---
title: TIL the Redneck Language
date: 2023-04-06
published: true
tags: ["language", "linux"]
series: false
canonical_url: false
description: "Today I learned that in 1998, RedHat had a redneck language option. Megabytes? Nah Meggerbytes."
layout: layouts/post.njk
---


Today I learned that in 1998, RedHat had a redneck language option!

![](https://i.imgur.com/W6VCaIf.png)


via [bd808 on Reddit](https://www.reddit.com/r/linux/comments/120c43j/comment/jdgtogt/?utm_source=reddit&utm_medium=web2x&context=3):

> Redneck, pirate, and pig Latin localizations are all helpers for monolingual English speaking developers to test the localization support of their software.

Via https://www.reddit.com/r/linux/comments/120c43j/just_learned_today_that_in_1998_redhat_had_a/


"Meggerbytes"

![](https://i.imgur.com/93SeDYF.png)

"whichun you want?"

![](https://i.imgur.com/LmUsBa4.png)


"nawp"

![](https://i.imgur.com/6eU5yEB.png)

