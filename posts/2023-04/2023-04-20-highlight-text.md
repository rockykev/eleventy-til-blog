---
title: TIL get user's highlighted text
published: true
tags: ['javascript', 'mdn']
series: false
canonical_url: false
description: "This one-liner lets you get the user's highlighted text!"
layout: layouts/post.njk
---


This one-liner lets you get the user's highlighted text!

![](2023-04-20-get-user-highlight-text_0.png)

MDN: https://developer.mozilla.org/en-US/docs/Web/API/Window/getSelection

```js
const getSelectedText = () => window.getSelection().toString();

getSelectedText();
```

via [20 Killer JavaScript One Liners](https://dev.to/saviomartin/20-killer-javascript-one-liners-94f)

