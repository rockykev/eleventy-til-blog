---
title: TIL cloning objects in Javascript
date: 2023-04-14
published: true
tags: ["objects", "javascript"]
series: false
canonical_url: false
description: "structuredClone() is a new function that will soon be supported by most browsers, Node.js and Deno. It creates deep copies of objects. This blog post explains how it works."
layout: layouts/post.njk
---




There's a new API called `structuredClone()`.

via [MDN](https://developer.mozilla.org/en-US/docs/Web/API/structuredClone)

>  `structuredClone()` is a new function that will soon be supported by most browsers, Node.js and Deno. It creates deep copies of objects. This blog post explains how it works.

via https://2ality.com/2022/01/structured-clone.html


Right now, [using the spread syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax) only creates a shallow clone.

Spreading has one significant downside – it creates shallow copies: The top levels are copied, but property values are shared. I repeat -- SHARED

Clone implies that it's a duplicate. So instead, use `structuredClone()`

