---
title: TIL Focusing on Readability
date: 2023-04-30
published: true
tags: ["readability", "cleancode", "principles"]
series: false
canonical_url: false
description: "You can say that understandability is achieved when developers on all levels can make updates to it in a way that is clear, safe, and predictable, all without breaking current functionalities."
layout: layouts/post.njk
---

> For example, take these two snippets. They are two versions of implementation for the FizzBuzz code challenge. The first one represents the most basic implementation, while the second one is trickier.

> In terms of performance, they are both the same O(n), where n is 100. So neither of these versions is better in terms of performance.

```js
function FizzBuzzMethodOne() {
  var x, y, a;
  var str = " ";

  for (var a = 1; a <= 100; a++) {
    x = a % 3 == 0;
    y = a % 5 == 0;

    if (x) {
      str += "fizz";
    }
    if (y) {
      str += "buzz";
    }
    if (x || y) {
      str += a;
    }
  }
}

function fizzBuzzMethodTwo() {
  for (let i = 1; i <= 100; i++) {
    let msg = i;

    if (i % 3 == 0 && i % 5 === 0) {
      msg = "fizzbuzz";
    } else if (i % 3 == 0) {
      msg = "fizz";
    } else if (i % 5 == 0) {
      msg = "buzz";
    }
  }
}
```

## The issue

`FizzBuzzMethodOne()` has a bug. :-)

Talking about readability:

For `fizzBuzzMethodTwo()`

- `fizzBuzzMethodTwo()` is smaller. (which doesn't mean better)
- `fizzBuzzMethodTwo()` also has clear 'exits'. If `i` has no remainder when dividing by 3 AND 5, `fizzbuzz`. Else check if it's divisible by 3, or divisible by 5.

Very clear.

`FizzBuzzMethodOne()`'s bug

```js
// broken
if (x || y) {
  str += a;
}

// fixed
if (!(x || y)) {
  str += a;
}
```

## Rules of Thumb

> You can say that understandability is achieved when developers on all levels can make updates to it in a way that is clear, safe, and predictable, all without breaking current functionalities.

> When you work with code that is tangled (spaghetti code) or more complex than it needs to be, you will have difficulty spotting any potential problems.

> Lastly, but not the least significant field that will be affected, is the budget. When there are issues with a team’s velocity, the first and easiest solution is to bring more developers on board. But this is just another patch on a broken bag until things go south again because the true problem was never treated.

KISS – keep it stupid simple or keep it simple, stupid. According to this principle, most systems perform best when they are kept simple rather than made complicated. That’s why simplicity should be a key goal if someone wants to achieve understandability across their codebase. Avoid any unnecessary complexity.

DRY – Don’t Repeat Yourself. This principle states that each piece of code must be unique and have a well-defined purpose. For example, if five lines of code are repeated several times across your program then, in order to be in accordance with this principle, you should take those lines of code and write a separate function which you call. Don’t rewrite the same thing every time.

YAGNI – You aren’t gonna need it. This principle is simple. Don’t add functionality until deemed necessary. Only implement things that are required.



[A Definitive Guide to Understandability](https://www.rookout.com/blog/understandability/)
