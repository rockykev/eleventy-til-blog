---
title: TIL how Netflix personalizes artwork
date: 2023-04-18
published: true
tags: ["algorithm", "netflix", 'images', 'machinelearning']
series: false
canonical_url: false
description: "The problem here is that A/B tests take a long time to collect data. So instead, they switched to 'contextual bandits'. They create controlled randomization based on the user. Then, Netflix evaluates the performance (Random, bandit, contextual bandit) to decide if they roll out that algorithm."
layout: layouts/post.njk
---


Today I learned how Netflix personalizes artwork just for you

![](https://i.imgur.com/DjwI5wG.png)

How Netflix looks without the personalization.

**The collection of Images**

![](https://i.imgur.com/ukW1Vwn.png)


## How does it work

![](https://i.imgur.com/SKfb4tT.png)

> Someone who has watched many romantic movies may be interested in Good Will Hunting if we show the artwork containing Matt Damon and Minnie Driver,

So Netflix will show Good Will Hunting using the 'romantic' image.

> whereas, a member who has watched many comedies might be drawn to the movie if we use the artwork containing Robin Williams, a well-known comedian.

So Netflix will show Good Will Hunting using Robin Williams.

![](https://i.imgur.com/7CmPj4H.png)

> A member who watches many movies featuring Uma Thurman would likely respond positively to the artwork for Pulp Fiction that contains Uma.

More Uma.

> Meanwhile, a fan of John Travolta may be more interested in watching Pulp Fiction if the artwork features John.

More Travolta.

## Challenges

**Challenge 1** - How to select it when you can only show a single piece?

Netflix collects a lot of data to find signals to help decide if one piece is better tha another.

**Challenge 2** - Will showing different art cause people to forget?

For example:

1. You saw the Uma version of the Pulp Fiction cover.
2. You watch half the movie but stopped because of sleep.
3. For lunch, you picked a John Travolta movie and now have enough signals to say you're a Travolta fan.
4. The Pulp Fiction cover becomes John Travolta.
5. Would you find Pulp Fiction again because originally you saw it as Uma variant?

**Challenge 3** - What about how art performs in relation to other images?

If you had 7 red images and 1 green image, it doesn't mean the green images was objectively better.

>  Maybe a bold close-up of the main character works for a title on a page because it stands out compared to the other artwork. But if every title had a similar image then the page as a whole may not seem as compelling. Looking at each piece of artwork in isolation may not be enough and we need to think about how to select a diverse set of images across titles on a page and across a session.


**Challenge 4** - Doing this at scale. 20 million requests per second with low latency.

> Our personalization algorithm also needs to respond quickly when a title launches, which means rapidly learning to personalize in a cold-start situation. Then, after launch, the algorithm must continuously adapt as the effectiveness of artwork may change over time as both the title evolves through its life cycle and member tastes evolve.


## Solutions

The problem here is that A/B tests take a long time to collect data.

So instead, they switched to "contextual bandits".

>  Rather than waiting to collect a full batch of data, waiting to learn a model, and then waiting for an A/B test to conclude, contextual bandits rapidly figure out the optimal personalized artwork selection for a title for each member and context.

tl;dr - Netflix trades cost of gathering training data to instead create controlled randomization based on the user. Then using that data, continues to evolve with each interaction "context" based on preferences, ratings, time of day.

Then, Netflix evaluates the performance (Random, bandit, contextual bandit) to decide if they roll out that algorithm. If there's a reasonable correlation between images + watching it, they push forward for everyone.


The entire post: https://netflixtechblog.com/artwork-personalization-c589f074ad76
