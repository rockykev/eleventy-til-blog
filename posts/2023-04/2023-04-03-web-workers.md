---
title: TIL more stuff about Web Workers
date: 2023-04-03
published: true
tags: ["javascript", "webworkers"]
series: false
canonical_url: false
description: "If it's a 200,000 line Javascript file with lots of calculations, it's going choke your webpage. The browser has to download HTML, CSS, then this massive chonk of JS code, all before loading what you actually need. You can offload much of the heavy lifting+calculations with Web Workers."
layout: layouts/post.njk
---

I wanted to learn more about how to communicate with Web Workers.

## What is a web worker?

When you have a website, you need Javascript. If it's a 200,000 line Javascript file with lots of calculations, it's going choke your webpage.

The browser has to download HTML, CSS, then this massive chonk of JS code, all before loading what you actually need.

You can offload much of the heavy lifting+calculations with Web Workers.


## How to use Web Workers?

1. Create a worker script, which is a separate JavaScript file containing the code you want to run in the background.

2. In your main script, create a new worker instance and start it by calling its postMessage() method.


```js
// Worker script
self.addEventListener('message', (e) => {
  const result = e.data.num1 + e.data.num2;
  self.postMessage(result);
});

// Main script
const worker = new Worker('worker.js');
worker.postMessage({ num1: 2, num2: 3 });
worker.onmessage = (e) => {
  console.log(`The result is: ${e.data}`);
};
```

1. Main script calls on the worker.
2. Worker does all the calculations and returns the message.
3. The Main script returns the results.


via [An Introduction to Web Workers: Run JavaScript in the Background](via https://medium.com/front-end-weekly/an-introduction-to-web-workers-run-javascript-in-the-background-86af50d280c5)

> Why use Web Workers in Video Games? 🎮

> Web Workers are especially useful for video games and other applications that require a lot of processing power. You can ensure a smooth and responsive user experience by running the heavy scripts in the background, without freezing the browser or slowing down the game.

> In my spare time, I’ve been experimenting with Web Workers, and the results are quite impressive. The game not only runs smoothly, but the code is also cleaner and easier to manage.


