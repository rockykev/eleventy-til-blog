---
title: TIL Regression Testing
date: 2023-04-11
published: true
tags: ['testing', 'typescript']
series: false
canonical_url: false
description: "Regression Testing is defined as a type of software testing to confirm that a recent program or code change has not adversely affected existing features."
layout: layouts/post.njk
---

What is Regression Testing?

> Regression Testing is defined as a type of software testing to confirm that a recent program or code change has not adversely affected existing features. Regression Testing is nothing but a full or partial selection of already executed test cases that are re-executed to ensure existing functionalities work fine.

via https://www.guru99.com/regression-testing.html

I was looking into some benefits of TypeScript, and [pontilanda](https://news.ycombinator.com/item?id=34359504) shares this:

> Are tests useful? Because this is what TypeScript gives you: it helps you avoid regressions.

> If I change a signature, tests fail. If I pass junk data, tests fail. It's like invisible live tests and people forget this.

It can't fully eliminate regression testing obviously, but it's helpful at detection.

```typescript
function sum(a: number, b: number): number {
  return a + b;
}

console.log(sum(2, 3)); // Output: 5
console.log(sum("2", 3)); // TypeScript compilation error
```

This simple example: We're expecting to get 2 numbers, and return a number. And that reduces a lot of issues of additional code that adds more features.

