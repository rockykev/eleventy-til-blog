---
title: TIL Super Tiny Compiler
date: 2023-04-17
published: true
tags: ["repo", "compiler"]
series: false
canonical_url: false
description: "Most compilers are broken into 3 stages: Parsing, Transformation, and Code Generation. I highly recommend reading it yourself. It's pretty short (10-15 minutes?)"
layout: layouts/post.njk
---


Today I learned about the Super Tiny Compiler!

The Repo: https://github.com/jamiebuilds/the-super-tiny-compiler

I highly recommend reading it yourself. It's pretty short (10-15 minutes?)

But for tl;dr --

## The Deets

Most compilers are broken into 3 stages: Parsing, Transformation, and Code Generation.

1. **Parsing** is taking raw code and turning it into a more abstract
   representation of the code.
2. **Transformation** takes this abstract representation and manipulates to do
   whatever the compiler wants it to.
3. **Code Generation** takes the transformed representation of the code and
   turns it into new code.

Say you had code like this:

## Parsing

Say you had code like this:
`add(2, 3)`

The Parsing will do this:
```
 [
    { type: 'name',      value: 'add'      },
    { type: 'paren',     value: '('        },
    { type: 'number',    value: '2'        },
    { type: 'seperator', value: ','        },
    { type: 'number',    value: '3'        },
    { type: 'paren',     value: ')'        },
  ]
```

Then it'll create a Abstract Syntax Tree (AST) from that.
```
 {
   type: 'Program',
   body: [{
     type: 'CallExpression',
     name: 'add',
     params: [{
       type: 'NumberLiteral',
       value: '2',
     },
     {
       type: 'NumberLiteral',
       value: '3',
     },
   }]
 }

```

## Transformation

Now we take the AST and translate it into a new language!

It starts from the root and goes through each node, until the entire code is processed.

It's a bit complicated to explain, and [I'd suggest reading the example here](https://github.com/jamiebuilds/the-super-tiny-compiler/blob/d8d40130459d1537f6117a927947cd46c83182b0/the-super-tiny-compiler.js#L222).



## Code Generation

Now we're at the same where it stringifies the code back out.

You might get reusable tokens from the Transformation stage. There will be some optimization.

>  Effectively our code generator will know how to “print” all of the different node types of the AST, and it will recursively call itself to print nested nodes until everything is printed into one long string of code.


## The Repo

https://github.com/jamiebuilds/the-super-tiny-compiler/blob/master/the-super-tiny-compiler.js

