---
title: TIL Different WP Marketplaces
date: 2023-04-27
published: true
tags: ["wordpress", "plugins"]
series: false
canonical_url: false
description: "I didn't realize this at all but apparently, there's more places to get WordPress plugin marketplaces than just the default WordPress.org plugins repository."
layout: layouts/post.njk
---

I didn't realize this at all but apparently, there's more places to get WordPress plugin marketplaces than just the default WordPress.org plugins repository.

## Marketplaces

* Envato/ThemeForest
* Mojo Marketplace

But what if you wanted to set up your own?

[How to self host plugin updates](https://rudrastyh.com/wordpress/self-hosted-plugin-update.html)
[Build your own WordPress plugin update server](https://www.macarthur.me/posts/serverless-wordpress-plugin-update-server)


via https://masterwp.com/a-response-to-open-letters-about-plugin-review-delays/
