---
title: TIL Rejection Sampling in Video Game Randomization
date: 2023-04-04
published: true
tags: ["gamedev", "diceroll", "math"]
series: false
canonical_url: false
description: "Rejection Sampling example: In No Man's Sky - the distribution of gravitino balls on each planet was not uniform. Players quickly figured out which planets had the highest density, allowing players were able to accumulate wealth much faster than others. Rejection Sampling allows you to make it 'random but fair'."
layout: layouts/post.njk
---

I was reading the book "500 Lines or Less", and came across Rejection Sampling in Video Game design.

This article: https://aosabook.org/en/500L/a-rejection-sampler.html

## What is Rejection Sampling

I asked our friend ChatGPT to explain:

> In Minecraft, players can find resources such as ores (like coal, iron, gold, and diamonds) by mining blocks underground. Each block has a certain probability of containing a resource, with some blocks having a higher probability than others.

> To simulate the process of finding resources, Minecraft uses rejection sampling. When a player mines a block, the game generates a random point within the probability distribution of finding resources in that block. The probability distribution is based on the type of block being mined and the type of resource being searched for.

> The game then evaluates the probability density function (PDF) at that point and compares it to a random number between 0 and the maximum value of the PDF. If the value of the PDF is greater than the random number, the player finds the resource and gets the corresponding item. If the value of the PDF is less than the random number, the player does not find any resources and gets nothing.

> By using rejection sampling, Minecraft creates a realistic and engaging experience for players who enjoy exploring and mining underground. The game also allows players to use different strategies to optimize their search for resources, such as focusing on blocks with higher probabilities or using certain tools to increase their chances of finding rare resources.

So what does that really mean?

Here is a example when you don't use Rejection Sampling.

> In the game No Man's Sky, players explore a procedurally generated universe with billions of planets. Each planet has its own unique terrain, creatures, and resources for the player to discover and collect.

> One of the resources in the game is called "gravitino balls", which can be found growing on certain types of plants on the surface of planets. However, when the game was first released, the distribution of gravitino balls on each planet was not uniform. This meant that some planets had a much higher density of gravitino balls than others.

> Players quickly figured out which planets had the highest density of gravitino balls and would flock to those planets to collect them. This created an imbalance in the game, as some players were able to accumulate wealth much faster than others simply by exploiting the non-uniform distribution of gravitino balls.

> To address this issue, the game's developers introduced a patch that re-balanced the distribution of gravitino balls using rejection sampling. Now, the density of gravitino balls on each planet is more uniform, which makes the game feel more fair and balanced for all players.



