---
title: TIL Writing a cover letter with AI
date: 2023-04-02
published: true
tags: ["resume", "ai"]
series: false
canonical_url: false
description: "You can use it to generate cover letters by copy & pasting the job description, uploading your CV/Resume, and changing a few settings. The app will then generate cover letters for you and, if you login with Google, you can manage your jobs, create multiple cover letters per job, and other fun stuff."
layout: layouts/post.njk
---

Let the AI Content generation wars begin!

Via [I open-sourced CoverLetterGPT.xyz -- over 500 users, featured in IndieHackers newsletter](https://www.reddit.com/r/webdev/comments/11uh4qo/comment/jco5ggp/?context=3&utm_source=pocket_saves)

> Hey Peeps!

> I've been working on coverlettergpt.xyz for the past week or so.

> You can use it to generate cover letters by copy & pasting the job description, uploading your CV/Resumé, and changing a few settings. The app will then generate cover letters for you and, if you login with Google, you can manage your jobs, create multiple cover letters per job, and other fun stuff.


Repo: https://github.com/vincanger/coverlettergpt

It was built in WASP, which is fascinating tool.

Via the repo:
> Wasp as the full-stack framework allows you to describe your app’s core features in the main.wasp config file in the root directory. Then it builds and glues these features into a React-Express-Prisma app for you so that you can focus on writing the client and server-side logic instead of configuring. For example, I did not have to use any third-party libraries for Google Authentication. I just wrote a couple lines of code in the config file stating that I want to use Google Auth, and Wasp configures it for me. Check out the main.wasp file for more.


And here's where the magic happens:

* The Content: https://github.com/vincanger/coverlettergpt/blob/main/src/server/actions.ts#L25
* The payload: https://github.com/vincanger/coverlettergpt/blob/main/src/server/actions.ts#L88


