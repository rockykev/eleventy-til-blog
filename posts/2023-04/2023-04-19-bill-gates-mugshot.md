---
title: TIL Bill Gates mugshot is the Outlook Profile pic
date: 2023-04-19
published: true
tags: ["profile", "microsoft"]
series: false
canonical_url: false
description: "Today I learned that Bill Gates used the outline of his mug shot in 1977 as the default Microsoft Outlook profile picture."
layout: layouts/post.njk
---

Today I learned that Bill Gates used the outline of his mug shot in 1977 as the default Microsoft Outlook profile picture.

![](https://i.imgur.com/hbur3cp.png)


Via https://skeptics.stackexchange.com/questions/48565/was-bill-gates-mugshot-photo-used-in-2010-as-a-template-for-outlook-contacts

