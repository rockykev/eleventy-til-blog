---
title: TIL Loop Unrolling
date: 2023-04-25
published: true
tags: ["loops", "antipattern"]
series: false
canonical_url: false
description: "Loop unrolling, also known as loop unwinding, is a loop transformation technique that attempts to optimize a program's execution speed at the expense of its binary size, which is an approach known as space–time tradeoff. The transformation can be undertaken manually by the programmer or by an optimizing compiler. On modern processors, loop unrolling is often counterproductive, as the increased code size can cause more cache misses."
layout: layouts/post.njk
---

Today I learned about Loop Unrolling!

> Loop unrolling, also known as loop unwinding, is a loop transformation technique that attempts to optimize a program's execution speed at the expense of its binary size, which is an approach known as space–time tradeoff. The transformation can be undertaken manually by the programmer or by an optimizing compiler. On modern processors, loop unrolling is often counterproductive, as the increased code size can cause more cache misses.

The example:

```js
// BEFORE
 int x;
 for (x = 0; x < 100; x++) {
     delete(x);
 }


// AFTER
 int x;
 for (x = 0; x < 100; x += 5 ) {
     delete(x);
     delete(x + 1);
     delete(x + 2);
     delete(x + 3);
     delete(x + 4);
 }
```

If you skimmed this, you might go "Oh how creative!"

It has some serious pros and cons. And as I frequently say, "Write code for humans".

via https://en.wikipedia.org/wiki/Loop_unrolling?utm_source=pocket_reader
