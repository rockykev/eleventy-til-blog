---
title: TIL WordPress Data Layer for Blocks
date: 2023-04-13
published: true
tags: ["api", "wordpress", "blocks", "courses"]
series: false
canonical_url: false
description: "In the devtools, check wp.data for some sweet endpoint calls."
layout: layouts/post.njk
---

Today I learned about the WordPress Data layer.

The course here: https://learn.wordpress.org/course/using-the-wordpress-data-layer/

My repo: https://github.com/RockyKev/using-wordpress-data-layer-blocks

Some of hte key things I learned:

Retrieving WP data records
* Using the wp.data.xxxxxx('core') object in devtools core-data
* Retrieving data from the WP store @wordpress/core-data, versus the API
* Using @wordpress/components like the search bar, the spinner
* Using getEntityRecord within the wp.data.select('core') data object.

Editing the WP data records
* Using wp.data.dispatch('core').editEntityRecord over useState
* Using wp.data.dispatch('core').saveEditedEntityRecord over useState
* Using wp.data.select('core').getEditedEntityRecord to see the changes
* Using getLastEntitySaveError for error checking

Creating new WP data records
* Using React useState for any brand new records (as opposed to editNewEntryRecord)
* Using wp.data.dispatch('core').saveEntityRecord to create new pages

Delete WP data records
* Using wp.data.dispatch('core').isDeletingEntityRecord for deleting pages
* Using getLastEntityDeleteError to for error checking
* Using the Snackbar
