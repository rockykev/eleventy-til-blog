---
title: TIL Code Smells
date: 2023-04-24
published: true
tags: ["readability", "cleancode"]
series: false
canonical_url: false
description: "Simply put, code smells are usually not bugs, they are not technically incorrect. But they indicate weaknesses in design that may slow down development or increase the risk of bugs. Our goal is to write readable, reusable and refactorable software."
layout: layouts/post.njk
---

Found some clean ways to code.

What is a code smell?

> Simply put, code smells are usually not bugs, they are not technically incorrect. But they indicate weaknesses in design that may slow down development or increase the risk of bugs. Our goal is to write readable, reusable and refactorable software.

Some ones that I love:

## Use searchable names

```js
// BAD
// What is 36000000 for?
setTimeout(restart, 36000000);

// GOOD
const MILLISECONDS_PER_HOUR = 60 * 60 * 1000; // 36000000
setTimeout(restart, MILLISECONDS_PER_HOUR);
```
## Unnecessary Context
```js
// BAD
type User = {
  userName: string;
  userLastName: string;
  userAge: number;
}

function print(user: User): void {
  console.log(`${user.userName} ${user.userLastName} (${user.userAge})`);
}

// GOOD
type User = {
  name: string;
  lastName: string;
  age: number;
}

function print(user: User): void {
  console.log(`${user.name} ${user.lastName} (${user.age})`);
}
```

## !something && function()

I get pretty annoyed at this pattern in React JSX code.

It looks like this:

```js
// if myVariable is true, then execute myFunction
myVariable && myFunction();

// This the same exact thing
if (myVariable) {
    myFunction()
}
```

So now the rule of thumb
```js
// BAD
const userAge = 19;
const ageLimit = 18;

userAge >= ageLimit && voteForPresident();


// GOOD
const userAge = 19;
const ageLimit = 18;

if (userAge >= ageLimit) {
  voteForPresident();
}
```

## Avoid negative conditionals
This one I struggle with as frequently, I'm working with a nasty codebase where I'm already working with negatives.

```js
// BAD
type UserStatus = 'online' | 'offline';

function isUserNotOnline(status: UserStatus): boolean {  // ...
}

if (isUserNotOnline(status)) {  // ...
}

// GOOD
type UserStatus = 'online' | 'offline';

function isUserOnline(status: UserStatus): boolean {  // ...
}

if (!isUserOnline(status)) {  // ...
}
```



via https://itnext.io/7-bad-code-smells-in-typescript-e1cb397723c6
