---
title: TIL ES6 Classes usecases
date: 2023-04-29
published: true
tags: ["oop", "classes", "javascript"]
series: false
canonical_url: false
description: "I prefer breaking apart functionality. It's where my mind goes. Out of curiosity, i wanted to know other people's thoughts on classes."
layout: layouts/post.njk
---

ES6 brought classes into Javascript. Unlike other programming languages, classes are 'syntactic sugar'.

These two things do the exact same thing:

```js
/*
 * Video player as a class
 */

class VideoPlayer {
  constructor(videoElementId) {
    this.video = document.getElementById(videoElementId);
  }

  play() {
    this.video.play();
  }

  stop() {
    this.video.pause();
    this.video.currentTime = 0;
  }

  pause() {
    this.video.pause();
  }
}

const myVideoPlayer = new VideoPlayer("myVideo");
myVideoPlayer.play();
myVideoPlayer.pause();
myVideoPlayer.stop();

/*
 * Video player as separate functions
 */
function playVideo(videoElementId) {
  const video = document.getElementById(videoElementId);
  video.play();
}

function stopVideo(videoElementId) {
  const video = document.getElementById(videoElementId);
  video.pause();
  video.currentTime = 0;
}

function pauseVideo(videoElementId) {
  const video = document.getElementById(videoElementId);
  video.pause();
}

// Example usage
const myVideoElementId = "myVideo";
playVideo(myVideoElementId);
pauseVideo(myVideoElementId);
stopVideo(myVideoElementId);
```

I prefer breaking apart functionality. It's where my mind goes. (But oddly enough, in this particular use-case, I honestly prefer the `class VideoPlayer`. )

Out of curiosity, i wanted to know other people's thoughts on classes.

Via [[AskJS] Am I missing out on anything truly important by avoiding classes entirely?
](https://www.reddit.com/r/javascript/comments/g4f8ca/askjs_am_i_missing_out_on_anything_truly/)

---

## Missing Abstraction

> You are missing something, but it's not something you will notice in many bodies of JS code. Many JS codebases are very much line by line, the entire program, logically in order. Using `Object.prototype` is identical to using classes in JS (except with extra semantics) and there is nearly nothing you can't create without them.

> What you are missing is abstraction. Good or bad, abstraction is what a class is. Reason a class is like a special type of object. I assume you DO you `Array.map\forEach\filter\etc`. `Array` is simply a class with methods on it. You could write the `Array` as an ojbect with keys and no instances, but then every single use of the object you create may end up sharing a state!A class is an abstraction to this pattern; creating an `instance` of and `object` that can maintain a state. It's generically the same as an instance of React Component.Many other abstractions can be made with a class (like a default get method that caches). Not using classes simply means that you are creating your own abstractions, good or bad, to solve something.

I agree with this to an extent.

For that video code example above, I prefer it. It's a video black box.

> I also find less syntactic noise to be a benefit. And classes add quite a lot of it in my experience.

> Classes are really nice if you use dependency injection, we use them to define Angular controllers. Also anytime you have more than one of something. For instance I will use them to wrap the values of complicated form elements. Each one has methods like validate(), format(), etc. It's sometimes easier to pass this little package of data + methods around than it is to pass the functions around and pass in data everywhere, especially if you have, say, an array of different types of elements

> I will say that I like building applications around classes, particularly with node, because it offers me the ability to essentially reference a single closure (which is essentially what a class instance is) across an entire codebase. For example, if I use a socket-emitter in my application, I likely want to ensure I'm referencing the correct instance. A real world use case I have of this is working with message queueing protocols - I don't want to be creating multiple copies of listeners as that would potentially duplicate the handling of those messages (which leads to a linear increase in resource usage).

I absolutely agree with this.

Again, using the `VideoPlayer` example. Wrap it in a nice black box.

## Type-checking?

> Typescript can be useful in defining shape of the data. You don't have to use classes.

> Typescript kinda eliminates any previous needs for classes. Earlier you still could find some use cases in model rich code bases. And there is a thing of throwing / catching errors.

TBH I didn't understand this comment. I'm guessing you can write a method that does a type-check before committing to the rest of the code? But then you can do that within the function itself so I dunno.

> Yeah if you're using TS it almost enforces a lot of the safety and structure that classes give you, so I would agree.

> Classes are useful when you're dealing w/ polymorphic types.

> One place where they sometimes show up in apps is error handling. Some people like to do class MyError extends Error {} and then do e instanceof MyError checks to handle specific types of errors differently than generic errors.

> Polymorphic types can also show up when you use interfaces. For example, say you want to do inversion of control via dependency injection (for example, so you can mock google analytics calls in your tests). Then it's useful to express the API of the thing you're mocking as an interface and then use classes to implement both the real API and the mock.

This is definitely where I'm the most weakest.

For me, I find it better to write a Error function, rather than rely on inheritance.

Seems cleaner? I dunno.

## History and Terminology

> Well my impression is that most people who write classes in javascript are folk who generally came to javascript as not their first langauge and have something like C#, Java, php etc as their background. Thus simply writing in patterns which they find familiar.

I find the people who defend OOP also tend to be the old guard, or have extremely rigid ways of thinking. The "Pluto is a planet" folks.

Yeah I said it. Fight me Dave.

> In my job we had this discussion too a few years ago. Well, least to say I was sort of overwhelmed by whole bunch of OOP proponents. However around year passed and even those who were insisting that we should prefer classes stopped writing them. We already had company wide practices of stateless services and composition over inheritance, so decline of class use in javascript code base was probably an inevitable eventuality.

Side-note: the fact that we need to explain state vs stateless and composition over inheritance is my main reason over not wanting OOP.

## Classes in Javascript

> Libraries might have some features being supported only through classes. React has a couple of those that are not usable with functional components, Angular is all about classes, etc.

> JavaScript's Error class extensions.

> HTML Custom components and various Worklets.

> Upcoming private class fields. There might be ways to have comparable functionality without classes, but reading/writing private class fields is only possible within classes.

I always found this argument weird. What is used as the foundation of a programming language's features shouldn't be what dictates how humans write code.

For example, engines do byte-shifting. But if I see a dev do byte-shifting in a PR, i'm raising a major red flag.

## Performance

> For people who are obsessed with performance like me, then I prefer Class when possible because of the structure it gives to the code, which allows the V8 engine to optimize for better performance. https://blog.sessionstack.com/how-javascript-works-inside-the-v8-engine-5-tips-on-how-to-write-optimized-code-ac089e62b12e

Argument here: Most devs shouldn't optimize for engines. For example, [the most performant loop](https://stackoverflow.com/a/7252102/4096078) is a `while` loop. Should you change everything to meet that micro-optimization and shave .00000001 seconds?

Key words: "people who are obsessed with performance like me"

Optimization is fine when it's a clear bottleneck. But in the wrong hands, it reeks of premature optimization.

> I've profiled classes vs factory functions myself several times and have never been able to discern a measurable difference. Unless it's computationally-heavy I wouldn't want to compromise on the readability and simplicity of factory functions. And even then, you'd have to ask yourself if you really should be using JavaScript for this task.

Readability and simplicity for the win.

> ... by not having classes, particularly if discussing something like React or React Native, decoupling them and using a functional approach really does decouple things nicely and you treat components as 'view only' (and using hooks makes the logic that is needed really nice to work with). If I had to make a choice and stick with it in this instance, it'd be functional components every single time.

This is where the arguments tend to happen.

(This is also where I started changing my opinions)

Are we talking about architecture of a entire system, or building design elements?

There's many times when I tried to fully turn a library from OOP to functional programming, only to make a mess of the code. And readability-wise, OOP was fine.

On the counterpoint, I've seen extreme OOP boomers try to OOP everything, turning a 4-line function into multiple classes in multiple files.

## Lastly -- Javascript is/isn't OOP

> This is incorrect. JavaScript is absolutely an object oriented language. It's just prototype based rather than class-based. It's true that JS's class syntax is syntactic sugar on top of prototypes, but it makes it much easier to define complex structures, especially when inheritance is involved. You can write all this without classes, but the language supports classes now in all major browsers, so why not use them when appropriate instead of hurting your brain trying to mess with the prototype?

Javascript is whatever you make it to be.
