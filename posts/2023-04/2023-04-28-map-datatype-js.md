---
title: TIL map in Javascript
date: 2023-04-28
published: false
tags: ["object", "javascript", "mdn"]
series: false
canonical_url: false
description: "Map: if you're using objects in JavaScript to store arbitrary key value pairs where you'll be adding and removing keys frequently, you should really consider using a map instead of a plain object."
layout: layouts/post.njk
---

I've been really looking into `map` data structure.

[MDN on MAP](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)


```js
const map1 = new Map();

map1.set('a', 1);
map1.set('b', 2);
map1.set('c', 3);
```

> if you're using objects in JavaScript to store arbitrary key value pairs where you'll be adding and removing keys frequently, you should really consider using a map instead of a plain object.

So try Maps!

## Setting/Deleting things

```js
// 🚩
const mapOfThings = {}
mapOfThings[myThing.id] = myThing
delete mapOfThings[myThing.id]

// ✅
const mapOfThings = new Map()
mapOfThings.set(myThing.id, myThing)
mapOfThings.delete(myThing.id)
```

`delete` actually has some bad performance in removal of data.

## Polluted Keys

Because everything in JS is an object!

```js
const myMap = {}

myMap.valueOf // => [Function: valueOf]
myMap.toString // => [Function: toString]
myMap.hasOwnProperty // => [Function: hasOwnProperty]
myMap.isPrototypeOf // => [Function: isPrototypeOf]
myMap.propertyIsEnumerable // => [Function: propertyIsEnumerable]
myMap.toLocaleString // => [Function: toLocaleString]
myMap.constructor // => [Function: Object]
```

## Iteration sucks
```js
// ISSUE 1
for (const key in myObject) {
  // 🚩 You may stumble upon some inherited keys you didn't mean to
}

// ISSUE 2
for (const key in myObject) {
  if (myObject.hasOwnProperty(key)) {
    // 🚩
  }
}

// SOLUTION
for (const key in myObject) {
  if (Object.hasOwn(myObject, key) {
    // 😐
  }
}

```

With maps, it's:

```js
for (const [key, value] of myMap) {
 // 😍
}
```


Via https://www.builder.io/blog/maps
