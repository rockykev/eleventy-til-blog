---
title: TIL How tweets are ranked
date: 2023-04-09
published: true
tags: ["search", "seo", 'weight']
series: false
canonical_url: false
description: "I love nerding out about the 'weight' of data. When you type search keys, Google 'weighs' certain terms higher than others. If you google 'Spade', does it give you the tool, the card logo, or David Spade?"
layout: layouts/post.njk
---

I love nerding out about the "weight" of data. When you type search keys, Google "weighs" certain terms higher than others. If you google "Spade", does it give you the tool, the card logo, or David Spade?

A few days ago, Twitter opened up about how they do their Algorithms. You can see here: https://github.com/twitter/the-algorithm


How does it rank Tweets?

You can read the takeaways here: https://www.reddit.com/r/19684/comments/129kjrv/twitter_algorithm_rule/ and https://aakashgupta.substack.com/p/the-real-twitter-files-the-algorithm

## key bits

### Adding Weight

Example: If you have a image, it gives you 2.0.

![](https://i.imgur.com/6WU0qXI.png)

You also get points taken away for Misinformation or Ukraine.

![](https://i.imgur.com/ehFTGMI.png)

You also get penalized for misspelling words.


### Grouping

After data aggregation comes feature formation. The algorithm turns all that data into four key feature buckets.

It groups people algorithmically. So if Selena Gomez, who is in the POP circle, tried to tweet about League of Legends, it probably won't go as well as her other tweets.

### Your metadata

Your followers to following ratio matter.

Who you interact with matter.

Getting muted/blocked/spam reports matter.






