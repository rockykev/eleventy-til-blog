---
title: TIL Principles from 10 years of development
date: 2023-04-26
published: true
tags: ["testing", "oop", "ruleofthree"]
series: false
canonical_url: false
description: "Some notes I picked up from this blog post."
layout: layouts/post.njk
---

Some notes I picked up from this blog post:

> Always write tests - If you're not writing tests, you're testing manually.
Solve 80% of use cases - You never gonna solve everyone’s problem.

Something I want to do better in general, especially unit testing.


> Prefer functional programming - It's easier to understand. If your code requires a Ph.D. to understand, you’re most likely doing it wrong.

The key word here is 'prefer'.

I've come around to recognizing that they're not mutually exclusive.

In Javascript, a lot of OOP concepts has been abstracted away compared to say C++/Java, making it easier to write functional-style programming without the OOP-style scaffolding.

> Do not generalize early - Wait until you have at least 3 duplicates of code before you make an abstraction (aka. Rule Of Three).

Something I'm currently teaching my juniors.

> Perfect code doesn't exist - It's better to accept this as a fact rather than wasting time and chasing the impossible.

Sometimes I'm teaching myself.

via [20 principles I learned from 10 years of developing software](https://dev.to/ondrejsevcik/20-principles-i-learned-from-10-years-of-developing-software-5354)
