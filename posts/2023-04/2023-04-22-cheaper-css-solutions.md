---
title: TIL Cheaper CSS Solutions
date: 2023-04-22
published: true
tags: ["shadow", "border", "performance"]
series: false
canonical_url: false
description: "The box-shadow property is a popular way to add a shadow effect to an element, and the the border-radius property is used to create rounded corners on an element. Both can be very costly in terms of performance"
layout: layouts/post.njk
---


Today I learned about cheaper performance solutions to various CSS things:

via https://dev.to/leduc1901/costly-css-properties-and-how-to-optimize-them-3bmd


## Box-Shadow

> The box-shadow property is a popular way to add a shadow effect to an element, but it can be very costly in terms of performance. When used on a large number of elements or with a large blur radius, it can significantly slow down your webpage.

Solutions:

(For details, click the link above)

1. Use a smaller blur radius

2. Use the inset keyword for inner shadows

3. Use the will-change property to improve performance when animating box shadows

I never knew about the `will-change` feature!
[MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/will-change)

> Warning: will-change is intended to be used as a last resort, in order to try to deal with existing performance problems. It should not be used to anticipate performance problems.


## Border-Radius

> The border-radius property is used to create rounded corners on an element, but it can be very costly in terms of performance. When used on a large number of elements or with a large radius, it can significantly slow down your webpage.

Solutions:

(For details, click the link above)

1. Use smaller border radius values

2. Use the border-image property instead of border-radius for complex border designs

3. Use SVG graphics for complex border designs

I had a complex set of images that caused the page to load a bit too long (probably from a bunch of network requests) But also had a lot of CSS code attached. After a few hours of wrestling, I argued with the design team to simply flatten it into a single SVG. Removed 15 images + 200 lines of CSS.



