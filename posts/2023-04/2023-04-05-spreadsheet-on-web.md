---
title: TIL Creating a Spreadsheet on the web
date: 2023-04-05
published: false
tags: ["excel", "javascript"]
series: false
canonical_url: false
description: "Here's how to make a Web spreadsheet (like Google Sheets)"
layout: layouts/post.njk
---

I was reading the book 500 lines of code, which shows solutions on how to create various projects in "under 500 lines". A lot of the code is kinda outdated (as it was written when jQuery was the mainstream and we didn't have a lot of good quality-of-life Javascript).

Here's how to make a Web spreadsheet (like Google Sheets)

* https://github.com/aosabook/500lines/tree/master/spreadsheet


That doesn't mean you can't re-create it in a modern format! (Which I highly recommend for mental exercise!)


Want to see it as pure Javascript?

https://github.com/aosabook/500lines/tree/master/spreadsheet/code/as-without-angularjs

