---
title: TIL Signals in Javascript
date: 2023-04-01
published: true
tags: ["messages", "observer", 'vue', 'javascript']
series: false
canonical_url: false
description: "Signals is a observer pattern. It's a way for different parts of a program to communicate with each other without directly talking to each other. A change gets sent and the observers are notified of state changes. Rather than update the whole parent component/components, it's just a granular update."
layout: layouts/post.njk
---

Today I learned about Signals in Javascript!

> The Problem: Almost all popular frameworks nowadays utilize a component based architecture, where components are responsible for managing their own state and describing the UI based off of that state. To get your application, you then compose all of your components together — and in doing so, naturally create a tree of interconnected components, each managing their own complexity.

> Wonderful, and in that wonder lies the problem. Often times, two or more components will rely on the same piece of state –- and often times, those components aren’t anywhere near each other in the component tree. The rule of thumb in these scenarios is to lift that state up to the nearest parent component and then pass it down to any component that needs it.

> In doing so, there’s a performance implication. If lifted up state changes in a framework like React, the component that manages that state as well as all of its children need to re-render (or you need to do the referential equality dance with memo and useMemo).

The solution is Signals! Signals is a observer pattern. It's a way for different parts of a program to communicate with each other without directly talking to each other. A change gets sent and the observers are notified of state changes. Rather than update the whole parent component/components, it's just a granular update.

* Component A publishes a signal indicating that some state has changed.
* Component B has subscribed to that signal and receives a notification that the state has changed.
* Component B can then update its own state or trigger a re-render based on the new information.

This is actually similar to Vue Compositions!

> Fundamentally, signals are the same kind of reactivity primitive as Vue refs. It's a value container that provides dependency tracking on access, and side-effect triggering on mutation.

via https://vuejs.org/guide/extras/reactivity-in-depth.html#connection-to-signals

via https://bytes.dev/archives/170



