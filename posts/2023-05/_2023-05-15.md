---
title: TIL PLACEHOLDER
date: 2023-05-15
published: false
tags: ["", ""]
series: false
canonical_url: false
description: PLACEHOLDER
layout: layouts/post.njk
---

https://web.dev/import-maps-in-all-modern-browsers/#:~:text=With%20import%20maps%2C%20importing%20ES%20modules%20now%20becomes%20a%20lot%20better.&text=This%20web%20feature%20is%20now,JavaScript%20code%20in%20web%20applications.

JavaScript import maps are now supported cross-browser

A modern way to use ES modules is with the &LTscript type="importmap"> tag. This tag allows you to define a mapping of external module names to their corresponding URLs, which makes it easier to include and use external modules in your code.
