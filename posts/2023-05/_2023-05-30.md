---
title: TIL PLACEHOLDER
date: 2023-05-30
published: false
tags: ["", ""]
series: false
canonical_url: false
description: PLACEHOLDER
layout: layouts/post.njk
---




Service worker caching and HTTP caching
https://web.dev/service-worker-caching-and-http-caching/?utm_source=pocket_reader


Overview of caching flow #
At a high-level, a browser follows the caching order below when it requests a resource:

Service worker cache: The service worker checks if the resource is in its cache and decides whether to return the resource itself based on its programmed caching strategies. Note that this does not happen automatically. You need to create a fetch event handler in your service worker and intercept network requests so that the requests are served from the service worker's cache rather than the network.
HTTP cache (also known as the browser cache): If the resource is found in the HTTP Cache and has not yet expired, the browser automatically uses the resource from the HTTP cache.
Server-side: If nothing is found in the service worker cache or the HTTP cache, the browser goes to the network to request the resource. If the resource isn't cached in a CDN, the request must go all the way back to the origin server.


Scenario: Long-term caching (Cache, falling back to network) #
When a cached resource is valid in the service worker cache (<= 90 days): The service worker returns the cached resource immediately.
When a cached resource is expired in the service worker cache (> 90 days): The service worker goes to the network to fetch the resource. The browser doesn't have a copy of the resource in its HTTP cache, so it goes server-side.
Pros and cons:

Pro: Users experience instant response as the service worker returns cached resources immediately.
Pro: The service worker has more fine-grained control of when to use its cache and when to request new versions of resources.
Con: A well-defined service worker caching strategy is required.
Scenario: Mid-term caching (Stale-while-revalidate) #
When a cached resource is valid in the service worker cache (<= 30 days): The service worker returns the cached resource immediately.
When a cached resource is expired in the service worker cache (> 30 days): The service worker goes to the network for the resource. The browser doesn't have a copy of the resource in its HTTP cache, so it goes server-side.
Pros and cons:

Pro: Users experience instant response as the service worker returns cached resources immediately.
Pro: The service worker can ensure that the next request for a given URL uses a fresh response from the network, thanks to the revalidation that happens "in the background."
Con: A well-defined service worker caching strategy is required.
Scenario: Short-term caching (Network falling back to cache) #
When a cached resource is valid in the service worker cache (<= 1 day): The service worker goes to the network for the resource. The browser returns the resource from the HTTP cache if it's there. If the network is down, the service worker returns the resource from the service worker cache
When a cached resource is expired in the service worker cache (> 1 day): The service worker goes to the network to fetch the resource. The browser fetches the resources over the network as the cached version in its HTTP cache is expired.
Pros and cons:

Pro: When the network is unstable or down, the service worker returns cached resources immediately.
Con: The service worker requires additional cache-busting to override the HTTP Cache and make "Network first" requests.

Service worker caching logic doesn't need to be consistent with HTTP caching expiry logic. If possible, use longer expiry logic in the service worker to grant the service worker more control.
HTTP caching still plays an important role, but it's not reliable when the network is unstable or down.
Revisit your caching strategies for each resource to make sure your service worker caching strategy provides its value, without conflicting with the HTTP cache.
