---
title: TIL Grid Layouts by Jen Simmons
date: 2020-05-21
published: true
tags: ["grid", 'layout']
series: false
canonical_url: false
description: "I absolutely love how Jen Simmons uses Grid to show what can be done. Two completely different usages of grid"
layout: layouts/post.njk
---

I absolutely love how Jen Simmons uses Grid to show what can be done.

![](2020-05-21-grid-layouts-jen-simmons_0.png)

```html
<ul class="grid-container">
  <li><img src="media/01-001/brooklynmuseum-o2310i000-1995.29.14_SL1.jpg" alt=""></li>
  <li><img src="media/01-001/brooklynmuseum-o755i000-44.65_SL1.jpg" alt=""></li>
  <li><img src="media/01-001/443197.jpg" alt=""></li>
  <li><img src="media/01-001/brooklynmuseum-o1085i000-52.166.5.jpg" alt=""></li>
  <li><img src="media/01-001/brooklynmuseum-o73318i000-57.91.3_open_PS2.jpg" alt=""></li>
  <li><img src="media/01-001/brooklynmuseum-o76375i000-59.205.3_IMLS_SL2.jpg" alt=""></li>
  <li><img src="media/01-001/brooklynmuseum-o69856i000-55.43_SL1.jpg" alt=""></li>
  <li><img src="media/01-001/brooklynmuseum-o49886i000-39.581_SL1.jpg" alt=""></li>
</ul>

<style>
  ul {
    display: grid;
    grid-template-columns: repeat(5, 1fr);
    grid-gap: 0.25rem;
    max-width: 100%;
  }
</style>

```




![](2020-05-21-grid-layouts-jen-simmons_1.png)

```html

<ul class="grid-container">
  <li><img src="media/01-001/brooklynmuseum-o1085i000-52.166.5.jpg" alt=""></li>
  <li><img src="media/01-001/380485.jpg" alt=""></li>
  <li><img src="media/01-001/brooklynmuseum-o4266i000-86.226.18_SL1.jpg" alt=""></li>
  <li><img src="media/01-001/brooklynmuseum-o44489i000-35.867_reference_SL1.jpg" alt=""></li>
  <li><img src="media/01-001/436667.jpg" alt=""></li>
</ul>

<style>

ul {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    max-width: 600px;
}

li:nth-child(1) {
    grid-column: 2 / 3;
    grid-row: 1 / 2;
}

li:nth-child(2) {
    grid-column: 4 / 5;
    grid-row: 2 / 3;
}

li:nth-child(3) {
    grid-column: 3 / 4;
    grid-row: 3 / 4;
}
li:nth-child(4) {
    grid-column: 1 / 2;
    grid-row: 5 / 6;
}

li:nth-child(5) {
    grid-column: 4 / 5;
    grid-row: 5 / 6;
}

</style>

```

REFERENCE:
[Labs, Jen Simmons](https://labs.jensimmons.com/2017/01-003.html)
