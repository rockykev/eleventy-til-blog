---
title: TIL clip-path and shape-outside
date: 2020-05-19
published: true
tags: ["css", 'image', 'shape']
series: false
canonical_url: false
description: "You know how you can shape a picture into a circle, and then wrap text around it? You can do that with two features."
layout: layouts/post.njk
---

You know how you can shape a picture into a circle, and then wrap text around it?

You can do that with two features.

## clip-path

The clip-path CSS property creates a clipping region that sets what part of an element should be shown. Parts that are inside the region are shown, while those outside are hidden.

For example:
Rather than round a circle by using a border-radius, you can instead `clip-path: circle(40%)`.

![](2020-05-19-shape-path-shape-outside_0.png)


## shape-outside
You can then have content wrap around it using the shape-outside css.



[mdn - clip-path](https://developer.mozilla.org/en-US/docs/Web/CSS/clip-path)
[mdn - shape-outside](https://developer.mozilla.org/en-US/docs/Web/CSS/shape-outside)
