---
title: TIL datalist input suggestions
date: 2020-05-13
published: true
tags: ["html", "forms"]
series: false
canonical_url: false
description: "There's a cool way to have a dropdown and type in to autocomplete the answer you want."
layout: layouts/post.njk
---


There's a cool way to have a dropdown and type in to autocomplete the answer you want.

> Input suggestions and autocomplete are fairly common these days and you must have noticed it on sites like Google and Facebook.

> Using the <datalist> tag.

> Remember that this tag’s ID attribute must be the same as the input fields list attribute.

```html
<label for="country">Choose your country from the list:</label>
<datalist id="countries">
  <option value="UK">
  <option value="Germany">
  <option value="USA">
  <option value="Japan">
  <option value="India">
</datalist>
```

via [5 HTML Tricks Nobody is Talking About](https://javascript.plainenglish.io/5-html-tricks-nobody-is-talking-about-a0480104fe19
)
