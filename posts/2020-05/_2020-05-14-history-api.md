---
title: TIL the History API
date: 2020-05-14
published: false
tags: [""]
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---


I never knew about the `History API`.

> The History interface allows manipulation of the browser session history, that is the pages visited in the tab or frame that the current page is loaded in.

via [MDN - History](https://developer.mozilla.org/en-US/docs/Web/API/History)

So to create a way to go back a page:

```js
const navigateBack = () => history.back();

// Or
const navigateBack = () => history.go(-1);
```


