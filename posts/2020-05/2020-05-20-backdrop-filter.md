---
title: TIL applying a filter directly to a background using backdrop-filter
date: 2020-05-20
published: true
tags: ["caniuse", "experimental", "css", "backgrounds"]
series: false
canonical_url: false
description: "backdrop-filter applies to everything behind the element, to see the effect you must make the element or its background at least partially transparent."
layout: layouts/post.njk
---


The `backdrop-filter` CSS property lets you apply graphical effects such as blurring or color shifting to the area behind an element.

Because it applies to everything behind the element, to see the effect you must make the element or its background at least partially transparent.

These are the same as [filter](https://developer.mozilla.org/en-US/docs/Web/CSS/filter).
But it's targeting backgrounds.

```css
/* <filter-function> values */
backdrop-filter: blur(2px);
backdrop-filter: brightness(60%);
backdrop-filter: contrast(40%);
backdrop-filter: drop-shadow(4px 4px 10px blue);
backdrop-filter: grayscale(30%);
backdrop-filter: hue-rotate(120deg);
backdrop-filter: invert(70%);
backdrop-filter: opacity(20%);
backdrop-filter: sepia(90%);
backdrop-filter: saturate(80%);

/* Multiple filters */
backdrop-filter: url(filters.svg#filter) blur(4px) saturate(150%);
```

As of May 2020  - Firefox does not have this feature, unless you turn on the flag.

UPDATE:
Apparently it's still buggy on Firefox.
https://stackoverflow.com/q/63789769/4096078


REFERENCE:
[MDN - Backdrop-filter](https://developer.mozilla.org/en-US/docs/Web/CSS/backdrop-filter)
[caniuse - Backdrop-filter](https://caniuse.com/css-backdrop-filter)
