---
title: TIL difference between justify-content vs justify-items
date: 2020-05-22
published: true
tags: ["css", 'grid', 'layouts', 'stack-overflow']
series: false
canonical_url: false
description: "The 'justify-content' property aligns columns within the container. The 'justify-items' property aligns grid items within their tracks (not the entire container)"
layout: layouts/post.njk
---

**justify-content vs justify-items**

The `justify-content` property aligns columns within the container.
The `justify-items` property aligns grid items within their tracks (not the entire container)

![](2020-05-22-justify-content-justify-items_0.png)

![](2020-05-22-justify-content-justify-items_1.png)



REFERENCE:
[Stack Overflow](https://stackoverflow.com/questions/48535585/what-is-difference-between-justify-self-justify-items-and-justify-content-in-cs)
