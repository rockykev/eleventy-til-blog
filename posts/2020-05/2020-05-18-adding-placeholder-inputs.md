---
title: TIL How to add a placeholder cursor in an input
date: 2020-05-18
published: true
tags: ["inputs", 'placeholder', 'css']
series: false
canonical_url: false
description: "How to add a placeholder cursor in an input"
layout: layouts/post.njk
---

How to add a placeholder cursor in an input

```css
  li {line-height: 2; clear: both;}
  label {display: inline-block; width: 200px;}
  .shell span {color: pink;}
  li {font-family: helvetica; font-size: 0.93rem;}
```

```html
<ul>
  <li>
    <label for="expiration">Credit Card Expiration Month</label>
    <span class="shell"><span aria-hidden="true" id="expirationMask"><i>01/2</i>Y</span><input id="expiration" type="tel" class="masked" pattern="(1[0-2]|0[1-9])\/\d\d" data-valid-example="11/18" title="2-digit month and 2-digit year greater than 01/15" maxlength="5" data-placeholder="MM/YY"></span>
  </li>

<li>
  <label for="zip">Zip Code</label>
  <span class="shell"><span aria-hidden="true" id="zipMask"><i></i>XXXXX</span><input id="zip" type="tel" name="zipcode" pattern="\d{5}" class="masked" title="5-digit zip code" maxlength="5" data-placeholder="XXXXX"></span>
</li>
<li>
  <label for="zipca">Canadian Zip Code</label>
  <span class="shell"><span aria-hidden="true" id="zipcaMask"><i></i>XXX XXX</span><input id="zipca" type="text" name="zipcodeca" pattern="\w\d\w \d\w\d" class="masked" data-charset="_X_ X_X" title="6-character alphanumeric zip code in the format of A1A 1A1" maxlength="7" data-placeholder="XXX XXX"></span>
</li>
<li>
  <label for="tel">Telephone</label>
  <span class="shell"><span aria-hidden="true" id="telMask"><i></i>(XXX) XXX-XXXX</span><input id="tel" type="tel" name="phone" pattern="\(\d{3}\) \d{3}\-\d{4}" class="masked" title="10-digit number" maxlength="14" data-placeholder="(XXX) XXX-XXXX"></span>
</li>
<li>
  <label for="cc">Credit Card Number</label>
  <span class="shell"><span aria-hidden="true" id="ccMask"><i></i>XXXX XXXX XXXX XXXX</span><input id="cc" type="tel" name="ccnumber" pattern="\d{4} \d{4} \d{4} \d{4}" class="masked" title="16-digit number" maxlength="19" data-placeholder="XXXX XXXX XXXX XXXX"></span>
</li>
</ul>
```
![](2020-05-18-adding-placeholder-inputs_0.png)


Estelle made a library here that also does more accessibility with javascript.
https://github.com/estelle/input-masking
