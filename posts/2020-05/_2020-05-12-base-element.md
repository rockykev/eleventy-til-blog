---
title: TIL the Base element
date: 2020-05-12
published: true
tags: ["mdn", "html"]
series: false
canonical_url: false
description: "HTML has a '<base>' tag which enables you to set a base URL as shown below:"
layout: layouts/post.njk
---

While fixing code from a decade ago (like HTML 4.1), I found this base element.

HTML has a `<base>` tag which enables you to set a base URL as shown below:

```html
<head>
  <base href="https://www.twitter.com/" target="_blank">
</head>


<body>
  <img src="billgates.jpg" alt="Bill Gates' twitter">
  <a href="BillGates">Bill Gate</a>

</body>

```

[MDN - base](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base)
