---
title: TIL First Class functions
date: 2020-05-10
published: false
tags: ["javascript", "programming"]
series: false
canonical_url: false
description: "First Class Functions simply means that functions are treated like any other variable. First Class Functions can be passed As Arguments to other functions!"
layout: layouts/post.njk
---


Today I learned about First class functions!

First Class Functions simply means that functions are treated like any other variable. First Class Functions can be passed As Arguments to other functions!

First class functions are incredibly powerful, but also somewhat difficult to grasp.

For example:
```js
// the function
function printHi() {
  console.log("Hi I am function passed as an argument so I am a first class Function");
}

// the function that takes a function
function executeFunction(theFunctionBeingCalled) {
  theFunctionBeingCalled();
}

// this function executes the argument function
executeFunction(printHi); // passing printHi function as an argument

```


[The (Most Comprehensive) JavaScript Design Principles Guide](https://dev.to/cleancodestudio/the-most-comprehensive-javascript-design-principles-guide-7i3)
