---
title: TIL Box-sizing
date: 2020-05-23
published: true
tags: ["css", "layout"]
series: false
canonical_url: false
description: "Setting box-sizing to border-box is a neat trick to avoid any unexpected padding issue. It tells the browser to include any border and padding in the values you specify in that element’s width and height itself. For example, if your element width is 125px, then any padding you add will be included in that width(125px) itself."
layout: layouts/post.njk
---

Setting box-sizing to border-box is a neat trick to avoid any unexpected padding issue. It tells the browser to include any border and padding in the values you specify in that element’s width and height itself. For example, if your element width is 125px, then any padding you add will be included in that width(125px) itself.

The box-sizing CSS property sets how the total width and height of an element is calculated.

```css
div#box-1 {
  box-sizing: content-box;
  width: 100%;
  border: solid #5B6DCD 10px;
  padding: 5px;
}

div#box-2 {
  box-sizing: border-box;
  width: 100%;
  border: solid #5B6DCD 10px;
  padding: 5px;
}
```

Box-1
![](2020-05-23-box-sizing-css_0.png)

Box-2
![](2020-05-23-box-sizing-css_1.png)

Rules of thumb:

Set `box-sizing: border-box` to layout elements. This makes dealing with the sizes of elements much easier, and generally eliminates a number of pitfalls you can stumble on while laying out your content.

 But, using `position: relative` or `position: absolute`, use of `box-sizing: content-box` allows the positioning values to be relative to the content, and independent of changes to border and padding sizes, which is sometimes desirable.



[MDN - box-sizing](https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing)
