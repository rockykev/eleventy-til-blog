---
title: TIL that Xbox has a accessibility Guidelines
date: 2022-03-23
published: true
tags: ['gamedev', 'accessibility']
series: false
canonical_url: false
description: "The guideline also provides `gamer personas', types of gamers who are affected by that specific issue and how your accessibility solution will help them."
layout: layouts/post.njk
---

During the early days of video games, developers shipped games of various quality. Some games were good. Others like Atari's ET was garbage-ware.

To avoid that problem, Nintendo had a guideline that all developers must follow in order to receive the Nintendo Seal of Approval.

That Seal of Approval still exists in different ways. Some practices became standard, and others became guidelines.

So I was pleasantly surprised by the [Xbox Accessibility Guidelines (XAGs)](https://docs.microsoft.com/en-us/gaming/accessibility/guidelines).

> These guidelines are intended for designers as a catalyst for generating ideas, for developers as guardrails when developing their games, and for test teams as a checklist to validate the accessibility of their titles. The XAGs aren't intended to act as a checklist to validate any type of compliance or legal requirements. Rather, they seek to ensure that the user experience in a game is enjoyable and playable for everyone. When everyone plays, we all win.

The guideline provides questions & actions, as well as examples of how to solve a specific problem.

The guideline also provides `gamer personas', types of gamers who are affected by that specific issue and how your accessibility solution will help them.

For example:

Implementing a time limit.

> There are many reasons that a player might need more than the alloted time to complete a task. For example, a younger player who can't read might need time to get a parent from another room to read the instructions to them. Players with disabilities such as blindness, low vision, dexterity impairments, and cognitive limitations might require more time to read content or to physically perform tasks such as filling out online forms. If functions are time-dependent, it can be difficult for some players to perform the required action before a time limit is up. This might result in negative consequences like lost progress, being signed out of an account unintentionally, or even rendering the game unusable.


The following gamer personas benefit by considering best implementations of the guideline.

* Gamers without vision	X
* Gamers with low vision	X
* Gamers without hearing	X
* Gamers without limited hearing	X
* Gamers with limited cognitive skills	X
* Gamers with limited reach and strength	X
* Gamers with limited manual dexterity	X
* Gamers with prosthetic devices	X
* Gamers with limited ability to use time-dependent controls	X
* Other: casual gamers, younger gamers, those new to gaming
