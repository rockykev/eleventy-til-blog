---
title: TIL that 2^24 items will cause a interesting error
date: 2022-03-07
published: true
tags: ['math', 'javascript', 'errors']
series: false
canonical_url: false
description: "Obviously there's not much real use-cases. But that's the fun of finding limitations."
layout: layouts/post.njk
---

Obviously there's not much real use-cases. But that's the fun of finding limitations.

## As a map
```
const myObject = new Map();

// 50_000_000 is a ES2021 syntax
for (let i = 0; i <= 50_000_000; i++) {
    myObject.set(i,i);
    if(i%100000==0) { console.log(i) }
}
```

What will happen?

```
0
100000
200000
…
16400000
16500000
16600000
16700000
```

Then it will crash after adding approx 16.7M elements and say

`Uncaught RangeError: Value undefined out of range for undefined options property undefined`

3 undefined?

## As a Object

What if you made it an object?

```
const myObject = {};

// 50_000_000 is a ES2021 syntax
for (let i = 0; i <= 50_000_000; i++) {
    myObject["myobj_" + i] = i;
    if(i%100000==0) { console.log(i) }
}
```
The result:
```
0
100000
200000
…
8000000
8100000
8200000
8300000
```

Then it freezes!


For the Map: 2^24=16,777,216

For the Object it is around 2^23=8,388,608

Not useful. Not fun trivia!

Via [searchvoidstar's post](https://searchvoidstar.tumblr.com/post/659634228574715904/an-amazing-error-message-if-you-put-more-than-2-24)
