---
title: TIL about wrapping a variable in curly brackets
date: 2022-03-19
published: false
tags: ['devtools', 'javascript']
series: false
canonical_url: false
description: "Output the key:value pair using curly brackets"
layout: layouts/post.njk
---


I learned this trick a while back and realized not everyone knows about it.

![](2022-03-19-wrap-variable-javascript_0.png)

Via [Christian Heilmann's Developer Tools secrets that shouldn’t be secrets](https://christianheilmann.com/2021/11/01/developer-tools-secrets-that-shouldnt-be-secrets/)
