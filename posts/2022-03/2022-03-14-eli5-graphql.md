---
title: TIL ELI5 for GraphQL
date: 2022-03-14
published: true
tags: ['browser', 'graphql', 'apis']
series: false
canonical_url: false
description: "ELI5? It's like a lunch tray, properly portioned and asking for a specific food type in each slot."
layout: layouts/post.njk
---

This is a really good ELI5 for GraphQL, from [Ben Halpern](https://dev.to/buzzingbuzzer/comment/g0g):

NOTE: I modified it a bit to make it easier for me.

It's lunchtime at school and they're serving your favorite meal: Meatloaf and mashed potatoes.

You really like the meatloaf and mashed potatoes part the most, but you're a good kid so you know you need to eat your peas and carrots. You are excited about the corn and of course, the gravy.

The current setup (REST APIs), you have to go to each cook, who each will give you a single plate. So to get a full plate of mash potatoes, meatloaf, corn, peas & carrots... you'll need to make 4 trips, one to each cook.

But you don't want 4 single plates! You're not going to eat all those veggies! So you need to be specific about what you ask for. You need two scoops of mashed potatoes, a big slice of meatloaf, half an ear of corn, and about a fistful of peas and carrots. But communicating all this to the cafeteria chef is a mouthful.

That's why you use one of these:

![](2022-03-14-eli5-graphql_0.png)

It's a GraphQL tray. Instead of dealing with four chefs, you can just go over to a single food window. Inside the kitchen, the cooks scoop out the portions from their big Mongo vats of meat, potatoes and the rest. Some chefs use Postgres tubs. Maria and Cassandra brand tubs are also popular in some school districts. But none of that really matters to you. It might matter if you had to give the chef specific instructions, but it's his job to figure out how to scoop out the mashed potatoes and fit the into the shape of your tray.

via [Jean-Michel Fayard's Best of #explainlikeimfive](https://dev.to/jmfayard/best-of-explainlikeimfive-3a0f)
