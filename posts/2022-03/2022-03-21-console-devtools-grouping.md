---
title: Grouping console messages together
date: 2022-03-21
published: true
tags: ['javascript', 'devtools']
series: false
canonical_url: false
description: "You can wrap console messages together using console.group('name') and console.groupEnd('name')"
layout: layouts/post.njk
---

You can wrap console messages together using

`console.group('name')` and `console.groupEnd('name')`
![](2022-03-21-console-devtools-grouping_0.png)

The [MDN](https://developer.mozilla.org/en-US/docs/Web/API/console):

`console.group()`
Creates a new inline group, indenting all following output by another level. To move back out a level, call groupEnd().

`console.groupCollapsed()`
Creates a new inline group, indenting all following output by another level. However, unlike group() this starts with the inline group collapsed requiring the use of a disclosure button to expand it. To move back out a level, call groupEnd().

`console.groupEnd()`
Exits the current inline group.

Via [Christian Heilmann's Developer Tools secrets that shouldn’t be secrets](https://christianheilmann.com/2021/11/01/developer-tools-secrets-that-shouldnt-be-secrets/)
