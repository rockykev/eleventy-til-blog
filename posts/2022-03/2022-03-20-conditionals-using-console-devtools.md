---
title: TIL about console.assert()
date: 2022-03-20
published: true
tags: ['javascript', 'devtools']
series: false
canonical_url: false
description: "You can instead use console.assert() to show conditional errors"
layout: layouts/post.njk
---


We've all been there. We did something like this.
```js
if (value != 10) {
    console.log("value does not equal 10");
}
```

You can instead use `console.assert()` to show that message.

```js
console.assert(value < 10, "value is greater than 10");
```

What does it do? Via [MDN](https://developer.mozilla.org/en-US/docs/Web/API/console):

> console.assert() - Log a message and stack trace to console if the first argument is false.


Via [Christian Heilmann's Developer Tools secrets that shouldn’t be secrets](https://christianheilmann.com/2021/11/01/developer-tools-secrets-that-shouldnt-be-secrets/)
