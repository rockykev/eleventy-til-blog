---
title: TIL about the Image Capture API
date: 2022-03-06
published: false
tags: ['browser', 'apis', 'javascript']
series: false
canonical_url: false
description: "The Image Capture API which helps us to capture an image or grab a frame from the video devices(like webcam). Not only that, you can also perform actions on capturing an image or grabbing a frame."
layout: layouts/post.njk
---

The Image Capture API which helps us to capture an image or grab a frame from the video devices(like webcam). Not only that, you can also perform actions on capturing an image or grabbing a frame.

NOTE: The end user does need to give the code permission to do so.

```js
navigator.mediaDevices.getUserMedia({video: true})
  .then(mediaStream => {
     document.querySelector('video').srcObject = mediaStream;
     const track = mediaStream.getVideoTracks()[0];
     setTrack(track);
  }).catch(error => {
     console.error(` ${error} is not yet supported`);
     setError(error);
});
```

In the blog post, there's also other use-cases.
Like taking screenshots, or streaming the video.

You can check out the demo: https://demo.greenroots.info/web-apis/web-apis-image-capture/

via [Tapas Adhikary's 10 lesser-known Web APIs you may want to use](https://blog.greenroots.info/10-lesser-known-web-apis-you-may-want-to-use)
