---
title: TIL about Concurrency
date: 2022-03-05
published: true
tags: ['programming', 'react']
series: false
canonical_url: false
description: "It means tasks can overlap."
layout: layouts/post.njk
---

What is Concurrency?
It means tasks can overlap.

(Yoinked this from the comment)

Let's use phone calls as an analogy.

No concurrency means that I can only have one phone conversation at a time. If I talk to Alice, and Bob calls me, I have to finish the call with Alice before I can talk to Bob.

**Non-overlapping calls**
![](https://user-images.githubusercontent.com/810438/121394782-9be1e380-c949-11eb-87b0-40cd17a1a7b0.png)



Concurrency means that I can have more than one conversation at a time. For example, I can put Alice on hold, talk to Bob for a bit, and then switch back to talking to Alice.

**Overlapping calls**
![](https://user-images.githubusercontent.com/810438/121394880-b4ea9480-c949-11eb-989e-06a95edb8e76.png)

*Note that concurrency doesn't necessarily mean I talk to two people at once. It just means that at any moment, I may be in multiple calls, and I choose who to talk to. For example, based on which conversation is more urgent.*



In React's case "phone calls" are your setState calls. Previously, React could only work on one state update at a time. So all updates were "urgent": once you start re-rendering, you can't stop. But with startTransition, you can mark a non-urgent update as a transition.

![](https://user-images.githubusercontent.com/810438/121396132-f760a100-c94a-11eb-959c-b95a6647d759.png)

You can think of "urgent" setState updates as similar to urgent phone calls (e.g. your friend needs your help) while transitions are like relaxed conversations that can be put on hold or even interrupted if they're no longer relevant.




[original](https://github.com/reactwg/react-18/discussions/46#discussioncomment-846786)


via [React-18's ELI5 Discussion thread](https://github.com/reactwg/react-18/discussions/46)
