---
title: TIL Blogpost, What New Hires think about
date: 2022-03-10
published: true
tags: ['blogpost', 'culture', 'newhire', 'job']
series: false
canonical_url: false
description: "New hires come in with a lot of unknowns. They also don't have psychological safety to open up."
layout: layouts/post.njk
---


See the post here: https://dev.to/rockykev/what-new-hires-think-about-3i99

---
Shoshin.

It's a concept in Zen Buddhism, which means "beginner's mind".

The concept of cycles/returning back to the beginning is prevalent in Eastern Philosophy. In Martial Arts, well-worn black belts sheds back to white.  There's the cycle of rebirth. In the dev industry - switching jobs is a new cycle.

That perspective (and that empathy) is important, especially for new hires.

My team grows from 6 devs to more than a dozen! And I hear the chatters of hazing new hires (😡), talking down to them (😡), or not even taking the time to think about their perspective.

New hires come in with a lot of unknowns. They also don't [have psychological safety](https://dev.to/rockykev/these-6-rules-created-a-really-dope-culture-at-my-job-5fj5) to open up.

As one of the **cat wranglers** bringing in these new hires, I reflect on Shoshin to help me see things from their perspective. (Heck, [it took me 15 years to get here](https://dev.to/rockykev/how-i-became-a-developer-in-about-15-years-552k))


This is what I've come up with.

## Things new hires thing about:

1. **Not knowing where to go for help.** Is it the culture to reach out to the whole team? Should I DM someone privately? Or should I just 'read the documentation'?

2. **Who/what is a [WORD HERE]?** Every company has their in-house terms. From Gazorpazorps to [chumps](https://til.heyitsrocky.com/posts/2022-03/2022-03-17-bioshock-chumps/). What does any of it mean?

3. **Org Chart** A decade ago, I once emailed a CEO to tell them that the bathroom was broken. I wish I knew who the right person was.

4. **Who is responsible for what?** Small companies, devs deal with everything. Big companies have clear lines of responsibility. That website footer, is that my team? Is that your team?

5. **Culture and environment.** What type of people work here? Can I throw a Parks and Rec reference? Am I in a workspace with casette tapes, or CDs, or Spotify?

## Things new hires go through:

1. **The Emotional Cycle of Change**. The [Emotional Cycle of Change](https://www.infocusleadership.ca/blog/five-stages-move-emotionally-changing-behavior/) is used to explain learning, and also applies here. When new hires join a company, they're in the *Uniformed Optimism* stage (a honeymoon phase). That honeymoon phase disappears, and things get harder. Soon, they drop into the Valley of Despair. Everything sucks. All the time. Can we create processes to allow our new hires to cross this bridge quickly?


2. **When 💩 hits the fans - the Unknown unknowns.** Every company has a period where chaos reigns. It's inevitable. New hires have to wonder, "When will the other shoe drop? What will happen to me?" Will a weak leader become power hungry and fire everyone who disagrees with them? What culture can we create to mitigate that fear?

3. **Am I doing this right?"** This question that will be asked for months. About everything! From little details like code style or formatting, to 'am I allowed to use this library?' It takes months for new hires to gain the confidence to draw outside the lines.

4. **Am I safe to even ask questions?** This is the question new hires rarely openly express. "Is everyone a phoney and secretly judging me?" I talk about [Psychological Safety in my workplace](https://dev.to/rockykev/these-6-rules-created-a-really-dope-culture-at-my-job-5fj5) here.

## What do we do with this

Whatever annoyances and frustrations you snagged on during your on-boarding, stamp them out! Can't remember? Adopt the beginner's mindset with Shoshin. Gain some empathy, take a step back, and think about how you can smooth out your onboarding of new hires.
