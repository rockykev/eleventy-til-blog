---
title: TIL about the battery status API
date: 2022-03-12
published: true
tags: ['mdn', 'browsers', 'api']
series: false
canonical_url: false
description: "Do you want to know all about the battery of your laptop, PC or, devices? This API helps in knowing all the information like, the battery is charging or not, how much charge is left and also the handlers to handle the charge related state changes."
layout: layouts/post.njk
---

NOTE: This only works in Chrome/Chromium (like Edge)
[MDN](https://developer.mozilla.org/en-US/docs/Web/API/Battery_Status_API)

Check out [What can the web do today](https://whatwebcando.today/battery-status.html) for more compatibility.


Do you want to know all about the battery of your laptop, PC or, devices? This API helps in knowing all the information like, the battery is charging or not, how much charge is left and also the handlers to handle the charge related state changes.

```js
navigator.getBattery().then(function (battery) {

   // handle the charging change event
   battery.addEventListener("chargingchange", function () {
      console.log("Battery charging? " + (battery.charging ? "Yes" : "No"));
   });

   // handle charge level change
   battery.addEventListener("levelchange", function () {
      console.log("Battery level: " + battery.level * 100 + "%");
   });

   // handle charging time change
   battery.addEventListener("chargingtimechange", function () {
      console.log( "Battery charging time: " + battery.chargingTime + " seconds");
   });

   // handle discharging time change
   battery.addEventListener("dischargingtimechange", function () {
      console.log("Battery discharging time: " + battery.dischargingTime + " seconds");
   });

});
```

via [Tapas Adhikary's 10 lesser-known Web APIs you may want to use](https://blog.greenroots.info/10-lesser-known-web-apis-you-may-want-to-use)
