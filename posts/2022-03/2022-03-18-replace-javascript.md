---
title: TIL how to replace many characters in javascript
date: 2022-03-18
published: false
tags: ['mdn', 'regex', 'javascript']
series: false
canonical_url: false
description: "You can use the replace() method to swap one string pattern for another."
layout: layouts/post.njk
---

You can use the [replace()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace) method to swap one string pattern for another.

For example, to remove all the periods and replace them with dashes, you do this.
```
var result = str.replace(".", "-");
```

But what if you want to replace spaces, commas, and periods?

You can put it in a regex like this:

```
var result = str.replace(/[ ,.]/g, "");
```


via [redneb on StackOverflow](https://stackoverflow.com/a/39345669/4096078)
