---
title: TIL Types of hacks and how to defend against them
date: 2022-03-30
published: true
tags: ['network', 'certifications']
series: false
canonical_url: false
description: "On the Network+ exam, there were a few different methods of attacks. This is my notes on them."
layout: layouts/post.njk
---

## Types of Tech-based Attacks

**On-path attack (man-in-the-middle attack)**

**DNS Poisoning (DNS Spoofing)**
This is when hackers redirect web traffic. Like instead of `yahoo.com`, it's `yahoo-com.io` It's like someone changing all the house addresses/street addresses to redirect your pizza order to a different house.

This is done by gaining access to a DNS server. The hacker can then get credentials/private info.

Via https://www.keyfactor.com/blog/what-is-dns-poisoning-and-dns-spoofing/

**ARP Spoofing**

This is when hackers modify a ARP message.

The hacker can then enable DoS attacks or on-path (man in the middle) attacks.

**MAC Spoofing**

The hacker changes their MAC address. You can spoof your MAC address to hide it in public LAN/WLAN networks. OR you can spoof your MAC address to gain access to a large network to pretend to be a authorized user.

via https://www.giac.org/paper/gsec/3199/mac-spoofing-an-introduction/105315

**IP Spoofing**

The hacker disguises their data as coming from a trusted IP address. They're modifying the packet to say it's from a trusted source address (rather than their address).

They can then enable a DDos attack, or redirect information.

For DDos attack, they disguise themselves as innocent users.

For redirecting, the GameOver Zeus attack, where hackers took control of user's devices to capture banking credentials. Then they re-direct wire transfers.

Via https://www.keyfactor.com/blog/what-it-is-ip-spoofing-how-to-protect-against-it/

**VLAN Hopping**



**Rogue DHCP**
As a device enters the network, it tells every device it needs (broadcasts a request for) an IP address. A dedicated DHCP server, listening for kinds of requests, responds with a DHCP response: assigning an IP address to the device from its pool of addresses.

Rogue DHCP servers are not authorized to give out IP Addresses.

If users suddenly cannot connect to the network, it's possible that a Rogue DHCP occurred.

DHCP Snooping is useful for blocking DHCP responses that are questionable, and block those ports.


Via https://www.auvik.com/franklyit/blog/rogue-dhcp-server/
Rogue Access Point

Evil Twin


## Social Hacks

There's a few. But the ones I'm surprised by:

**Tailgating:**
Tailgating represents the situation, when an individual without access authorization closely follows an authorized person in a reserved area. The malefactor takes advantage of the moment, when the authorized one opens the door with his badge – and sneaks inside before the door closes.


Piggybacking represents the situation, when someone accesses a reserved area with the permission obtained by deception of an authorized person.

Piggyback on someone's security access. Like, in Hitman, where you change clothes.

Tailgating is walking behind them. Like in Metal Gear Solid, where you follow them through a lock door.


## Hardening techniques
Router Advertisement Guard
Port Security
DHCP Snooping

Dynamic ARP Inspection

Controlk Plane Policing

Private VLANs


## Using ACLs to block things

Say your NIDS (Network Intruder Detection system) gave this packet:
```
23:12:23:153234 IP 86.18.10.3:34312 > 71.168.10.45:3389
```
You discovered there's unauthorized service running on this host.

1. The host is `71.168.10.45`.
2. The port being used `3389` is your RDP port, which is for remote desktop. Yikes.
3. You want to block all traffic to that host & post.

That's done with this:
`DENY TCP ANY HOST 71.168.10.45 EQ 3389`

## Specific terms with recovery

RTO - Recovery Time Objective
RPO - Recovery Point Objective
MTBF - Mean Time before Failure
MTTR - Mean Time to Recover
