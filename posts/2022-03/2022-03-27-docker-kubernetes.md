---
title: TIL ELI5 for Docker & Kubernetes
date: 2022-03-27
published: true
tags: ['docker', 'eli5']
series: false
canonical_url: false
description: "Kubernetes is the service that manages all the docker containers to ensure there's enough resources for everything to operate. "
layout: layouts/post.njk
---

tl;dr:
* Kubernetes is the service that manages all the docker containers to ensure there's enough resources for everything to operate.

Via [Miguel Mota](https://dev.to/miguelmota/comment/filh)

Note: I modified it to better help me understand.

This is a futuristic farm. We can now clone animals using our handy cloning machine.

Docker images: They're blueprints, for example a blueprint for creating a cow.

Docker daemon: This is the corral (an enclosure) for letting the cows run wild.

Docker swarm (and Kubernetes): This is the rancher that manages the cows.

Let's say you create many cows (docker containers) with the same blueprint (docker image) and let the cows do their thing in the corral (docker daemon).

You have all the dairy cows in one place but it's getting pretty crowded and they're eating all the stuff around them (resources) and you need to redistribute them to other areas or they will die.

You hire the rancher named Kubernetes and tell him of all the other corrals (nodes). The rancher checks each corrals capacities (resources) that they can handle. The rancher will take care of moving the cows around when the corrals are low on food to more abundant areas and the rancher will also take care of creating new cows for you if cows die for any reason.

The rancher is responsible optimizing your cattle ranch as efficient as possible and making it scale as long as you tell him of all the locations that he's allowed to move the cows to. You can also tell him to only grow the ranch to a certain size or to dynamically scale larger to produce more milk based on the dairy consumption demand by the population (auto-scaling).

via [Jean-Michel Fayard's Best of #explainlikeimfive](https://dev.to/jmfayard/best-of-explainlikeimfive-3a0f)
