---
title: TIL about getting how long it takes for JS to execute
date: 2022-03-13
published: true
tags: ['performance', 'javascript']
series: false
canonical_url: false
description: "This post took me 7.22 minutes to write."
layout: layouts/post.njk
---

You know how Google is able to show off this?

![](2022-03-13-js-execute-speed_0.jpg)

```js
const firstTime = performance.now();
something();
const secondTime = performance.now();
console.log(`The something function took ${secondTime - firstTime} milliseconds.`);
```

via [8 JavaScript Tips & Tricks That No One Teaches](https://dev.to/worldindev/8-javascript-tips-tricks-that-no-one-teaches-24g1)
