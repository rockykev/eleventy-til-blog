---
title: TIL about 'chumps' in BioShock
date: 2022-03-17
published: true
tags: ['gamedev', 'hacks']
series: false
canonical_url: false
description: "In Bioshock Infinite, they have 'chumps' without AI, without the ability to pathfind around - just bodies looping animations with a few flourishes like voice lines."
layout: layouts/post.njk
---


I always love little hacks that are quick to implement and do something pretty darn effective.

![](2022-03-17-bioshock-chumps_0.png)

The reason for this?

Performance cost!

At Irrational (the company that made Bioshock Infinite), a "chump" was an in-house term for NPCs that didn't have AI.

So they have "chumps" without AI, without the ability to pathfind around, just bodies looping animations with a few flourishes like voice lines.

> Wanting to add more motion to the scene but not having the performance leeway for another AI walking around, she "figured a chump running in a circle around that cylinder could work since I could just expand the collision of it to prevent the player from running through them." She hit upon the idea to re-use an NPC animation from the scene where NPCs dance around Elizabeth in Battleship Bay, using kids because "a couple randomly dancing seemed dumb."

> But, she explains, "the kids had different proportions than the adults, so the kids' feet were clipping through the ground and their hands were going through each other," she said. And trying to use inverse kinematics to make their arms reach the same positions as the adults made them raise their hands way up high. A simple solution:

> "So I deleted the boy's dancing partner and attached a baguette to his hands. Bam! Boy dances with baguette! Ship it!

via Rock Paper Shotgun:
https://www.rockpapershotgun.com/bioshock-infinites-dancing-bread-boy-is-the-start-of-some-good-game-dev-tales
