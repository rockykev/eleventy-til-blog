---
title: TIL Spreading a object into another object
date: 2022-03-11
published: true
tags: ['codepen', 'javascript']
series: false
canonical_url: false
description: "It's copying an object into another object, while also adding a new input."
layout: layouts/post.njk
---

Sometimes, I do stuff for a long time and tiny details like this blow me away. This neat trick is one of those times.

```js

const user = {
    firstName: "Rocky",
    lastName: "Kev"
}

const betterUser = { ...user, superPower: "taking notes" };
```

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="MWQWqOr" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/MWQWqOr">
  spread-array-test</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>
