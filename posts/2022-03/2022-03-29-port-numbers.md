---
title: TIL Port Numbers
date: 2022-03-29
published: true
tags: ['network', 'certifications']
series: false
canonical_url: false
description: "This is my own cheatsheet for remembering port numbers."
layout: layouts/post.njk
---


How to remember port numbers

The default ones to remember:

Websites:
FTP - 20/21
SSH/SFTP - 22
Telnet - 23
DNS - 53
DHCP 67/68
HTTP - 80
HTTPS - (TLS & SSL) 443

To remember:
* 20, 21 is FTP
* 22 is Secure
* 23 is not secure

* DNS -- the D is the fourth letter. So up one, down one. 5 and 3. 53.
* DHCP - right before NICE. 67/68.
* HTTPS: HTTP is 4 letters. So 443.

* For Sure Tomahawk. Don't do High hits.

Utilities:
TFTP - 69
NTP (Network Time Protocol) - 123
LDAP (Lightweight Directory) - 389/636
SMB (Server Message Block) - 445
Syslog - 514
RDP (Remote Desktop) - 3389
SIP - 5060/5061

To remember:
* TFTP - nice.
* NTP, is easy as 123
* LDAP - Like 369/636
* SMB - Super mario brothers, Giant World 4
* Syslog - From age 5 to 14, keep an eye on them.
* RDP - Like LDAP, but starts with 3.
* SIP - 50, 60

* Tomahawk, Ninjas like short songs

Mail is: (the number after is the TLS/SSL version)
SMTP - 25/587
POP3 - 110/995
IMAP - 143/993

To remember:
SMTP - twenty five. It's sending email.
POP3 - almost 100. It's post office.
IMAP - almost 150. It's mapping.

Sequel:
SQL Server - 1433
SQLnet - 1521
MySQL - 3306

To remember:
* SQL - Q is the 17th letter. That's 14 + 3. And then another 3.
* SQLnet - age 15. Age 21.
* MySQL - Being 33 in June.
