---
title: TIL about joke variables names in WordPress
date: 2022-03-03
published: true
tags: ['wordpress', 'jokes']
series: false
canonical_url: false
description: "The vast majority of users are likely unaware of this joke unless they enjoy trawling through the 6,000+ lines of code in '/wp-includes/formatting.php'. There is a good chance that any developer who has looked up the 'wpautop()' function has stumbled upon it."
layout: layouts/post.njk
---

> The vast majority of users are likely unaware of this joke unless they enjoy trawling through the 6,000+ lines of code in `/wp-includes/formatting.php`. There is a good chance that any developer who has looked up the `wpautop()` function has stumbled upon it.

> What is a `$pee`, exactly? It is text. Therefore, `$text` makes sense as a variable name, which is one of recent code changes. Other modifications include:

`$pee_parts` to `$text_parts`
`$last_pee` to `$last_part`
`$pees` to `$paragraphs`
`$tinkle` to `$paragraph`

> That complex `wpautop()` function and its 150+ lines of code start making a bit more sense when things are named for clarity.

As a dev who uses the placeholder `$butt` everywhere, I get it. Any named variable that forces a user to 'think' about its intention should be scrutinized.

Write code for your future devs.

(As for naming, there's some good [examples of how to name things here](https://github.com/kettanaito/naming-cheatsheet) and [how to name things here](https://www.freecodecamp.org/news/javascript-naming-conventions-dos-and-don-ts-99c0e2fdd78a))

REFERENCE:
https://wptavern.co
m/wordpress-ditches-pee-and-tinkle-jokes-in-favor-of-more-inclusive-language-and-clearer-code
