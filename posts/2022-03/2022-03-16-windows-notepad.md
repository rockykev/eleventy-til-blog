---
title: TIL about some neat notepad tricks
date: 2022-03-16
published: true
tags: ['windows', 'timestamps']
series: false
canonical_url: false
description: "'F5' generates a timestamp in notepad. And adding a timestamp using .LOG on the 1st line of a text file."
layout: layouts/post.njk
---


Tip #1 - 'F5' generates a timestamp in notepad

Via [IronicTelos's comment on Reddit](https://www.reddit.com/r/sysadmin/comments/si5qy8/til_f5_generates_a_timestamp_in_notepad/?utm_source=share&utm_medium=web2x&context=3)

Tip #2 - Put .LOG on the 1st line of a text file. When you open it in notepad it adds a timestamp to the bottom of the file when you opened it.

Via [Sovos' comment on Reddit](https://www.reddit.com/r/sysadmin/comments/si5qy8/comment/hv8clv1/?utm_source=share&utm_medium=web2x&context=3)
