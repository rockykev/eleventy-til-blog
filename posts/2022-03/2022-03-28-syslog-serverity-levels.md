---
title: TIL about Syslog Severity Levels
date: 2022-03-28
published: true
tags: ['network', 'certifications']
series: false
canonical_url: false
description: "Syslog is a protocol to manage your network devices.It's a score from 0 to 7 to determine severity."
layout: layouts/post.njk
---

Syslog is a protocol to manage your network devices.

> Syslog itself relies heavily upon having a Syslog server of some kind to receive, store, and interpret Syslog messages because, after all, a device or application being able to send messages is of little use if there's nothing to receive them!

via https://www.ittsystems.com/what-is-syslog/

It's a score from 0 to 7 to determine severity.

0 - Emergency - System is unusable, dead.
1 - Alert - Danger. Fix immediately
2 - Critical - A core file has shut down.
3 - Error - It's failing.
4 - Warning - a error can occur.
5 - Notice - unusual events.
6 - Information - give standard info
7 - Debug - for dev

Reference:
https://support.solarwinds.com/SuccessCenter/s/article/Syslog-Severity-levels?language=en_US
