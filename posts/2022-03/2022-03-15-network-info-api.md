---
title: TIL about the Network Information API
date: 2022-03-15
published: true
tags: ['browser', 'api', 'networks']
series: false
canonical_url: false
description: "The Network Information API provides information about the network types(e.g., 'wifi', 'cellular', etc.), save data mode, bandwidth and many more."
layout: layouts/post.njk
---

NOTE: This only works in Chrome/Chromium (like Edge)
Safari and Firefox have experimental versions that they're working on. But some requires the flag to be turned on.

[MDN](https://developer.mozilla.org/en-US/docs/Web/API/Network_Information_API)

The Network Information API provides information about the network types(e.g., 'wifi', 'cellular', etc.), save data mode, bandwidth and many more.

```js

let preloadVideo = true;
var connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
if (connection) {
  if (connection.effectiveType === 'slow-2g') {
    preloadVideo = false;
  }
}

```

Direct link to the demo: https://demo.greenroots.info/web-apis/web-api-network-info/

via [Tapas Adhikary's 10 lesser-known Web APIs you may want to use](https://blog.greenroots.info/10-lesser-known-web-apis-you-may-want-to-use)
