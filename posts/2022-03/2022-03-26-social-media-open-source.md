---
title: TIL alternative open-source viewers for social media
date: 2022-03-26
published: false
tags: ['bypass', 'open-source']
series: false
canonical_url: false
description: "A lot of websites require you to log in to use their services. Reading a twitter thread, and suddenly, you get blocked and have to log in. Not anymore!"
layout: layouts/post.njk
---

A lot of websites require you to log in to use their services.

For example, reading a twitter thread, and suddenly, you get blocked and have to log in.

Using a random example (Shaq is probably not a good example, as he mostly shares basic quotes and lots of NFTs)

The regular version:
https://twitter.com/SHAQ

The not-having-to-login version:
https://nitter.net/SHAQ

Surpringly, nitter.net is WAAAAY faster too.

Some other alternatives:

YouTube → Piped, Invidious, FreeTube, Yattee

Twitter → Nitter

Instagram → Bibliogram

TikTok → ProxiTok

Reddit → LibReddit, Teddit

Via [Elemnut's Reddit comment](https://www.reddit.com/r/InternetIsBeautiful/comments/t44z8f/comment/hywixc9/?utm_source=share&utm_medium=web2x&context=3)
