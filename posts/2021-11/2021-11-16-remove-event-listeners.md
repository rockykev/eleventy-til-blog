---
title: TIL how to remove a event listener
date: 2021-11-16
published: true
tags: ['js', 'events']
series: false
canonical_url: false
description: "Maybe you have a situation where you only want the event to fire once. "
layout: layouts/post.njk
---


Maybe you have a situation where you only want the event to fire once.

## To Remove the Event
```js
const removeEventOffElement = (el, evt, fn, opts = false) => el.removeEventListener(evt, fn, opts);

const testFunction = () => console.log('My function has been called');

document.body.addEventListener('click', testFunction);// Call remove method

removeEventOffElement(document.body, 'click', fn);
```

## To turn off the event after it's fired
```js
const myButton = document.getElementById("myBtn");

const myClickFunction = () => {
  console.log('this click will only get called once')
}

myButton.addEventListener('click', myClickHandler, {
  once: true,
});

```

REFERENCE:
[21 Useful JavaScript Snippets For Everyday Development](https://medium.com/@danthedev/21-useful-javascript-snippets-for-everyday-development-9e66e33bfb86)
