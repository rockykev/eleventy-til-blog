---
title: TIL Passing Object in Params instead of direct params
date: 2021-11-29
published: true
tags: ['clean-code', 'objects', 'javascript']
series: false
canonical_url: false
description: "The more arguments you add, the harder it'll be to read your code. Switch to a object approach."
layout: layouts/post.njk
---

The more arguments you add, the harder it'll be to read your code.

```js
// BEFORE:
function showStatistics(name, team, position, average, homeruns, rbi) {
  // code
}

showStatistics("Mark Teixeira", "New York Yankees", "1st Base", .284, 32, 101);
```

Switch to a object approach.

```js
// AFTER - much cleaner
function showStatistics(args) {
 // code
}

showStatistics({
  name: "Mark Teixeira",
  team: "New York Yankees",
  position: "1st Base",
  average: .284,
  homeruns: 32,
  rbi: 101
});
```

REF:

[6 Advanced JavaScript Concepts You Should Know](https://dev.to/codehunterbd/6-advanced-javascript-concepts-you-should-know-4lfe?utm_source=pocket_mylist)
