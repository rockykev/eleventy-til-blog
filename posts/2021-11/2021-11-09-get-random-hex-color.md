---
title: TIL how to get a random Hex color
date: 2021-11-09
published: false
tags: ['mdn', 'math', 'random']
series: false
canonical_url: false
description: "Math.random()*16777215 then turn it into a "
layout: layouts/post.njk
---

```js
// METHOD 1:
const generateRandomColor = () =>   "#" + Math.floor(Math.random()*16777215).toString(16);

// METHOD 2:
const randomHexColor = () => '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).slice(1, 6);

generateRandomColor(); // Result: #fec150
randomHexColor(); // Result: #abba22
randomHexColor(); // Result: #304060

// Shove it on your body!
// document.getElementsByTagName("body")[0].style.color
```

`toString(16)` is using a radix param, so it's a hexadecimal.


```js
let baseTenInt = 10;
console.log(baseTenInt.toString(2));
// Expected output is "1010"
```

2 for binary numbers,
8 for octal numbers,
10 for decimal numbers,
16 for hexadecimal numbers.


REFERENCE:
[21 Useful JavaScript Snippets For Everyday Development](https://medium.com/@danthedev/21-useful-javascript-snippets-for-everyday-development-9e66e33bfb86)
[toString](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/toString)
[Method 2 via](https://medium.com/dailyjs/nine-javascript-one-liners-for-the-beginner-2021-developer-792872ad6137?source=email-f85b306cec29-1628848595117-digest.weekly-f5105b08f43a-792872ad6137----1-1------------------92f5b90c_6ca7_444b_84d5_9fd5c3ca1a55-31-d4ef77c8_4f72_46fc_8584_03bb6934f4b1)
