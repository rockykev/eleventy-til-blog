---
title: TIL the DOM and CSSOM
date: 2021-11-13
published: true
tags: ['css', 'html', 'web-development']
series: false
canonical_url: false
description: "DOM is the html. CSSOM is the design. Together, they become... WEBPAGE-PERSON."
layout: layouts/post.njk
---


The server transfers over bytes to your computer.

The this happens:

`Bytes → characters → tokens → nodes → object model`

The object model is the DOM & CSSOM.


## DOM (Document Object Model)

![](2021-11-13-dom-and-cssom_0.png)

It's the tree structure of all your sweet HTML elements.

## CSSOM (CSS Object Model)

![](2021-11-13-dom-and-cssom_1.png)

It's the tree structure of how you lay out your elements, and then appends CSS on it.

Finally, the render tree combines them together to make that web page!

REFERENCE:
[Constructing the Object Model ](https://developers.google.com/web/fundamentals/performance/critical-rendering-path/constructing-the-object-model?utm_source=pocket_mylist)
