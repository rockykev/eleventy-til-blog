---
title: TIL Adds days to a date
date: 2021-11-11
published: true
tags: ['javascript', 'dates']
series: false
canonical_url: false
description: "Maybe you just need some quick date math, and don't want to use a date library."
layout: layouts/post.njk
---

Maybe you just need some quick date math, and don't want to use a date library.

```js
const addDaysToDate = (date, n) => {
  date.setDate(date.getDate() + n);
  return date.toISOString().split('T')[0];
};
addDaysToDate('2021-0-10', -10); // '2020-12-31'
```


REFERENCE:
[21 Useful JavaScript Snippets For Everyday Development](https://medium.com/@danthedev/21-useful-javascript-snippets-for-everyday-development-9e66e33bfb86)
