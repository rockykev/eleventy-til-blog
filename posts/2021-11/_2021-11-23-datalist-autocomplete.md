---
title: TIL the datalist element has autocomplete
date: 2021-11-23
published: true
tags: ['html', 'autocomplete', 'browser']
series: false
canonical_url: false
description: "Autocomplete your inputs with datalist"
layout: layouts/post.njk
---

Input suggestions and autocomplete are fairly common these days and you must have noticed it on sites like Google and Facebook.

Method 1:
JavaScript to add input suggestions by setting an event listener on the input field and then matching the term searched for with the predefined suggestions.

Method 2:
The `<datalist>` tag does it right out of the box!

```html
<label for="country">Choose your country from the list:</label>
<input list="countries" name="country" id="country">

<datalist id="countries">
  <option value="UK">
  <option value="Germany">
  <option value="USA">
  <option value="Japan">
  <option value="India">
</datalist>
```


REFERENCE:
[5 HTML Tricks Nobody is Talking About](https://javascript.plainenglish.io/5-html-tricks-nobody-is-talking-about-a0480104fe19)
[<datalist>: The HTML Data List element MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist)
