---
title: TIL how Wing Commander shipped with a clever hack
date: 2021-11-07
published: false
tags: ['video-game-history']
series: false
canonical_url: false
description: "Error when you quit? Change the error message!"
layout: layouts/post.njk
---

I'm 99% sure I shared this already but I love this story.

![](2021-11-07-wing-commander-history_0.jpg)

Deadlines, my friend!
