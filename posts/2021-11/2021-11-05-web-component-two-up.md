---
title: TIL Web Component two-up
date: 2021-11-05
published: true
tags: ['web-components', 'html']
series: false
canonical_url: false
description: "I think there's still a world where it can exist. We just have to find a right way to do it."
layout: layouts/post.njk
---

Web Components were a big thing for a while, and then it fizzled.
I think there's still a world where it can exist. We just have to find a right way to do it.

![](2021-11-05-web-component-two-up_0.png)

Google's Two-up example still holds true as the most pristine example of a world with Web Components.
https://github.com/GoogleChromeLabs/two-up

```html
	<two-up>
		<div class="light">
			<h1>The Light Side</h1>
		</div>
		<div class="dark">
			<h1>The Dark Side</h1>
		</div>
	</two-up>
</div>
```

You import a little web component script that deals with that `<two-up>` component.
```html
<script src="https://unpkg.com/two-up-element@1"></script>
```


<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="qBdbNLK" data-user="developit" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/developit/pen/qBdbNLK">
  &lt;two-up&gt; (two-up-element)</a> by Jason Miller (<a href="https://codepen.io/developit">@developit</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>
