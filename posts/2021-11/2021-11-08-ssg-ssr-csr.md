---
title: TIL SSG vs SSR vs CSR
date: 2021-11-08
published: true
tags: ['serverside', 'serverless', 'nextjs']
series: false
canonical_url: false
description: "SSG means Static Site Generation. SSR means Server Side Rendering. CSR means Client Side Rendering."
layout: layouts/post.njk
---

In [NextJS](https://nextjs.org/), you have a bunch of 3-letter acronyms.

SSG means Static Site Generation.

SSR means Server Side Rendering.

CSR means Client Side Rendering.


Okay lots of words.

A visual example:
![](2021-11-08-ssg-ssr-csr_0.png)
## SSG (Static Site Generation)

It means that when you `npm run build`, it takes all of your files and attempts to flatten it out so there's no 'reactiveness', as much as possible. It's pre-built. It'll still hydrate your app. This is great for blogs/sites that don't change much.

Tl;dr: Starbucks makes the drink in a warehouse somewhere for maximum efficiency. You, the consumer, can just pick it out of the fridge.
## SSR (Server Side Rendering)

This is where the server manages all the business logic. Backend languages like Ruby on Rails and PHP do this well, for over 10-20+ years.

In Javascript land, it means that data and the frontend gets figured out on the server, then once a HTML file is generated, it gets sent to the user.

Tl;dr: You, the consumer, visits the counter and points to a product. You may also say, "3 shots, not 2." The barista disappears and comes back with a drink.


## CSR (Client Side Rendering)

This is where the server gives you all the code that you need, and on your computer, you do some of the computation. We'll call this an "app", and it's showing bitcoin data. Maybe data changes. The browser grabs that new data, interprets it through the "app" (which you already cached), and updates it. Now it changes the view from 1 week to 1 month.

tl;dr: You, the consumer, orders a black coffee. The barista gives you a black coffee. You now remix that drink with whiskey.

REFERENCE:
[How to Think in Next.js](https://dev.to/benjaminwfox/how-to-think-in-next-js-2dh5?utm_source=pocket_mylist)
