---
title: TIL How to check if the string is uppercase
date: 2021-11-21
published: true
tags: ['js', 'text']
series: false
canonical_url: false
description: "The magic is comparing str === str.toUpperCase()"
layout: layouts/post.njk
---


How to check if the string is uppercase.

```js
const isUpperCase = str => str === str.toUpperCase();

console.log(isUpperCase("string")); // false

console.log(isUpperCase("STRING")); // true

console.log(isUpperCase("5TR1NG")); // true
```




REFERENCE:
[21 Useful JavaScript Snippets For Everyday Development](https://medium.com/@danthedev/21-useful-javascript-snippets-for-everyday-development-9e66e33bfb86)
