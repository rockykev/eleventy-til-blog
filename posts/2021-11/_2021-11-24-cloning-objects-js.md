---
title: TIL The many ways of cloning a object
date: 2021-11-24
published: true
tags: ['objects', 'javascript']
series: false
canonical_url: false
description: "Quick and dirty, shallow clone, or deep clone"
layout: layouts/post.njk
---

Maybe you're curious about performance?

[Run the benchmark!](https://jsben.ch/bWfk9)

![](_2021-11-24-cloning-objects-js_0.png)



## The Quick and dirty I don't care method

Good ol’ JOSN.stringify() is the most used method for cloning an object when you don’t care about `data loss` or `shallow clone` is sufficient for your use case.

```js
let some_obj = {
 name: "Shadid Haque",
 age: 26,
 items: ["health portion", "bow", "arrow"]
}

let copy = JSON.parse(JSON.stringify(some_obj));
console.log(copy)
```

You really shouldn't be doing that anymore. I mean, unless you're traveling back in time to 2012 to do web development.

## The Spread Operator

This is also shallow cloning.

```js
let A1 = {a: "2"};
let A3 = {...A1};  // Spread Syntax
```


## Deep Cloning Function

You can use the Lodash [Clone Deep function](https://lodash.com/docs/#cloneDeep).

```js
var objects = [{ 'a': 1 }, { 'b': 2 }];

var deep = _.cloneDeep(objects);
console.log(deep[0] === objects[0]);
// => false
```

Or their code directly [github](https://github.com/lodash/lodash/blob/master/cloneDeep.js)



Or steal [this SO Answer](https://stackoverflow.com/questions/122102/what-is-the-most-efficient-way-to-deep-clone-an-object-in-javascript/5344074#5344074)

```js
function clone(obj) {
    if (obj === null || typeof (obj) !== 'object' || 'isActiveClone' in obj)
        return obj;

    if (obj instanceof Date)
        let temp = new obj.constructor();
    else
        let temp = obj.constructor();

    for (let key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            obj['isActiveClone'] = null;
            temp[key] = clone(obj[key]);
            delete obj['isActiveClone'];
        }
    }
    return temp;
}

```


REFERENCE:
[The Most Efficient Ways to Clone objects in JavaScript](https://dev.to/shadid12/the-most-efficient-ways-to-clone-objects-in-javascript-1phe?utm_source=pocket_mylist)
