---
title: TIL JS commas evaluate operands
date: 2021-11-20
published: try
tags: ['javascript', 'interview', 'tricky']
series: false
canonical_url: false
description: "The comma operator evaluates each of its operands (from left to right) and returns the value of the last operand. var a=(2,3,4,5,6,7), result is 7."
layout: layouts/post.njk
---

This is indeed some tricky Javascript.

```js
var a=(2,3,4,5,6,7);

console.log(a); // 7

```
The comma operator evaluates each of its operands (from left to right) and returns the value of the last operand.



REFERENCE:
(Sometimes JavaScript is Tricky.)[https://dev.to/rajatarya007zxc/sometimes-javascript-is-tricky-67j]
