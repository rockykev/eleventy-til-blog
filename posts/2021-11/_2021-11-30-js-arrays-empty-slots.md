---
title: TIL JS arrays create empty slots when you jump indexes
date: 2021-11-30
published: true
tags: ['javascript', 'arrays', 'mdn']
series: false
canonical_url: false
description: "Javascript will create 'empty slots' for those numbers."
layout: layouts/post.njk
---


What happens with this in the dom?

```js
const numbers = [1, 2, 3];

numbers[10] = 11;

console.log(numbers);

```

![](_2021-11-30-js-arrays-empty-slots_0.png)

The result is that Javascript will create 'empty slots' (MDN Web Doc terms, not mine!) for those numbers.

But when you unravel it, it's not shown in the Console tab!

But we still see that it exists. Look at `empty x 7`, and the `length: 11`

> Note: this implies an array of 7 empty slots, not slots with actual undefined values

Don't get it twisted. Not undefined.



REFERENCE:
[Sometimes JavaScript is Tricky.](https://dev.to/rajatarya007zxc/sometimes-javascript-is-tricky-67j)
[Array.of()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/of)
