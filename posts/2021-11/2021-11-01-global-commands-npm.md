---
title: TIL how to get a list of all globally installed npm commands
date: 2021-11-01
published: true
tags: ['npm', 'terminal']
series: false
canonical_url: false
description: "Well, you probably installed it globally. I was curious on what other global commands I installed. 'npm list -g --depth 0'"
layout: layouts/post.njk
---


New Project.

You type in `vue create my-project`.

Magic.

But then you think to yourself. How the heck is `vue` a command line?

Well, you probably installed it globally. I was curious on what other global commands I installed.


The magic command is: `npm list -g --depth 0`

REF:
[npm tricks part 1: Get list of globally installed packages](https://medium.com/@alberto.schiabel/npm-tricks-part-1-get-list-of-globally-installed-packages-39a240347ef0)
