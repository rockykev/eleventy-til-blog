---
title: TIL detect dark mode
date: 2021-11-22
published: true
tags: ['js', 'system', 'darkmode']
series: false
canonical_url: false
description: "Your OS may switch theme colors when it's night time. Heck, Chrome switches theme colors when it's night time."
layout: layouts/post.njk
---

Your OS may switch theme colors when it's night time.

Heck, Chrome switches theme colors when it's night time.

What if your site can do it too?

```js
// Result: True or False
const isDarkMode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
```

REFERENCE:
[Nine JavaScript One-Liners For The Beginner 2021 Developer](https://medium.com/dailyjs/nine-javascript-one-liners-for-the-beginner-2021-developer-792872ad6137)
