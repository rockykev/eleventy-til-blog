---
title: TIL Event Bubbling vs Event Capture
date: 2021-11-26
published: true
tags: ['javascript', 'events', 'mdn']
series: false
canonical_url: false
description: "During event propagation, there are 3 phases: capturing, target, and bubbling."
layout: layouts/post.njk
---

**What do you think will happen with this code?**

```html

<div onclick="console.log('div')">
  <p onclick="console.log('p')">
    Click here!
  </p>
</div>

```

OPTION 1: `div p`
OPTION 2: `p div`
OPTION 3: `p`
OPTION 4: `div`

**The result:**

`p div`

**Here's why:**

During event propagation, there are 3 phases: capturing, target, and bubbling.

By default, event handlers are executed in the bubbling phase.

With bubbling, the event is first captured and handled by the innermost element and then propagated to outer elements.


if you switch to useCapture, this will happen.

> Events that are bubbling upward through the tree will not trigger a listener designated to use capture.


```js
target.addEventListener(type, listener);
target.addEventListener(type, listener, options);
target.addEventListener(type, listener, useCapture);
```




REFERENCE:
[Sometimes JavaScript is Tricky.](https://dev.to/rajatarya007zxc/sometimes-javascript-is-tricky-67j)
[EventTarget.addEventListener() MDN](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener)
