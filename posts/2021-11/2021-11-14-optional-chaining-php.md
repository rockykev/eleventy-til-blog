---
title: TIL Optional Chaining in PHP8
date: 2021-11-14
published: true
tags: ['php', 'js']
series: false
canonical_url: false
description: "Optional Chaining in PHP looks very similar to optional chaining in JS"
layout: layouts/post.njk
---

Hot dog!

This is totally legit in php now.

```php

$country = $session?->user?->getAddress()?->country;

```

It looks like this:
```js

const countryAddress = session?.user?.getAddress()
const country = countryAddress['country']

```

REFERENCE:
[PHP 8 features I wish also existed in JavaScript](https://devmount.medium.com/php-8-features-i-wish-also-existed-in-javascript-a4037230a1f0)
