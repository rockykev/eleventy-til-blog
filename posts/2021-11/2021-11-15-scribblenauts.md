---
title: TIL Scribblenauts isn't math, it's hand-crafted
date: 2021-11-15
published: true
tags: ['game-design']
series: false
canonical_url: false
description: " Most people believed that this impressive system was the result of complicated math, but according to one designer, the real secret was hand-crafting."
layout: layouts/post.njk
---


Scribblenauts is a game franchise where you write words to solve problems. The words become physical objects in the game.

For example: To beat the level, you must unlock a cage.

You can unlock the cage by:

* Spawning a key.
* Getting a bulldozer to destroy the cage.
* Call a Robot Trex to crush the cage.

In other words, your imagination runs wild.

![](2021-11-15-scribblenauts_0.png)


How was this made?

> Back in 2009, a little game called Scribblenauts blew minds across the world with a single ambitious concept: you could make anything. By typing a noun on the in-game keyboard, you’d conjure that object into the game’s world. The game could recognize just about anything, from common (dinosaurs) to esoteric (Shoggoth). Most people believed that this impressive system was the result of complicated math, but according to one designer, the real secret was hand-crafting.

> Although the design team at 5th Cell was able to categorize every item in the game and give many of them shared (or inherited) properties, there were always exceptions. For example, England pointed out, all humans could eat food, but vegetarians shouldn’t be able to eat hamburgers—which meant going into the database and changing things by hand.

Madness. From what I gathered [in this amazing post: The Secret Behind Scribblenauts: Making Objects By Hand (And Lots Of Crunch)](https://kotaku.com/the-secret-behind-scribblenauts-making-objects-by-hand-1796262472):

The team started by giving objects a few traits, which had some core 'models'.

Then they extended the objects with custom elements. A "wooden shield" would extend a shield model, and give it a material: wood.

Finally, they did some manual hacking. (Axes destroys wooden shields)

We see this style of dynamic development in games like Breath of the Wild too, where the game is a playground of objects interacting with each other.


