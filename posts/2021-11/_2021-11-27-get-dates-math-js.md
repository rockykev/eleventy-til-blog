---
title: TIL How to get the next set of days using JS
date: 2021-11-27
published: false
tags: ['javascript', 'dates']
series: false
canonical_url: false
description: "A pretty cool trick that returns an array containing the last 7 days, including the current day."
layout: layouts/post.njk
---
A pretty cool trick that returns an array containing the last 7 days, including the current day.

You can swap the - for a + and you’ll get the next 7 days!


```js
const pastSevenDays = [...Array(7).keys()].map(days => new Date(Date.now() - 86400000 * days));
console.log(pastSevenDays); // Result: [Array with 7 Date Objects]

const comingSevenDays = [...Array(7).keys()].map(days => new Date(Date.now() + 86400000 * days));
console.log(comingSevenDays);
```

![](_2021-11-27-get-dates-math-js_0.png)



REFERENCE:
[Nine JavaScript One-Liners For The Beginner 2021 Developer](https://medium.com/dailyjs/nine-javascript-one-liners-for-the-beginner-2021-developer-792872ad6137)
