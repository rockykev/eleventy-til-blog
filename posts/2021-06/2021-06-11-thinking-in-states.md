---
title: TIL thinking in States
date: 2021-06-11
published: true
tags: ['states', 'programming']
series: false
canonical_url: false
description: "This is a great reminder of how to approach a problem."
layout: layouts/post.njk
---

This is a great reminder of how to approach a problem.

"Sorry, we're super-duper, mega-out of milk."

To a programmer, that's an odd statement. You're either out of milk or you're not.

* has milk
* does not have milk

There's no "super duper" state. There's no 'So out of milk there's chaos'.

Using a example from a e-commerce platform:

If the user paid, then ship it. If it's shipped, then the whole order is complete.

This function smells funny:

```js
function isComplete() {
     return isPaid() && hasShipped();
}
```

Why? That whole function is encompassing two states.

When we think in state machines:

State machine rules:
![](2021-06-11-thinking-in-states_0.png)
[img source](https://medium.datadriveninvestor.com/state-machine-design-pattern-why-how-example-through-spring-state-machine-part-1-f13872d68c2d)

You can ONLY move to each state after completion of the prior one.

An order can only be in one of three distinct states:

* In progress: Can add or remove items. Can't ship.
* Paid: Can't add or remove items. Can be shipped.
* Shipped: Done. No more changes accepted.

The order if finalized when `hasShipped()` has occured.

So that function should be:
```js
function isComplete() {
     return hasShipped();
}
```


Via:
https://97-things-every-x-should-know.gitbooks.io/97-things-every-programmer-should-know/content/en/thing_84/
