---
title: TIL One-by-one errors
date: 2021-06-06
published: true
tags: ['math', 'loops', 'javascript', 'codepen']
series: false
canonical_url: false
description: "An off-by-one error is when you expect something to be of value N, but in reality it ends up being `N-1` or `N+1`."
layout: layouts/post.njk
---

## Off by one errors

An off-by-one error is when you expect something to be of value N, but in reality it ends up being `N-1` or `N+1`.

For example, you were expecting the program to perform an operation 10 times, but it ends up performing 9 or 11 times (one too few or one too many times).

In programming this is most commonly seen happening when dealing with "for" loops.

```js

let n = 10;

// for (int i = 1; i < n; ++i) { ... }
let countStartAtOne = "0";
for (let i = 1; i < n; ++i) {
  countStartAtOne += `, ${i}`;
}

// RESULTS: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9

// for (int i = 0; i <= n; ++i) { ... }
let countStartAtZeroGreaterEqual = "0";
for (let j = 0; j <= n; ++j) {
  countStartAtZeroGreaterEqual += `, ${j}`;
}

// RESULTS: 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

```

<p class="codepen" data-height="300" data-default-tab="result" data-slug-hash="MWmgLVP" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/MWmgLVP">
  </a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

REF:
https://en.wikipedia.org/wiki/Off-by-one_error
https://stackoverflow.com/questions/2939869/what-is-an-off-by-one-error-and-how-do-i-fix-it
