---
title: TIL RTL writing
date: 2021-06-30
published: true
tags: ['rtl', 'accessibility']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---

In English, the beginning of the line is on the left (LTR).
In Arabic, the beginning of the line is on the right (RTL).



```css
/* INCORRECT */
.text {
  text-align: left;
}

.text {
   text-align: right;
}

/* CORRECT */
.text {
  text-align: start;
}

.text {
  text-align: end;
}
```

VIA:
https://developer.mozilla.org/en-US/docs/Web/CSS/direction
and
https://dev.to/melnik909/3-simple-css-tricks-to-improve-ux-4g29
