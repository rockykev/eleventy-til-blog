---
title: TIL Starting with Yes
date: 2021-06-12
published: true
tags: ['lead-dev', 'people']
series: false
canonical_url: false
description: "At my previous job, I had the opportunity to work with a really brilliant tech nerd. He was my boss, and he's been in the tech space for over 30+ years, and would frequently remind me of this tip."
layout: layouts/post.njk
---

At my previous job, I had the opportunity to work with a really brilliant tech nerd. He was my boss, and he's been in the tech space for over 30+ years, and would frequently remind me of this tip.

So I was pleased to see it in **97 Things Every Developer Should Know**.

The tip:

"Start from Yes"


## How to start from Yes

During a meeting, the CEO of my company went to me directly (instead of my boss), and laid out a bunch of things he wanted to see. For about 10 minutes straight, he listed feature after feature.

I immediately became defensive, and that defensiveness became anger.

Everyone in the meeting was surprised, and I was so furious. So now I had like 20 priorities?!

My boss took me aside and shared that with tip with me.

"Say yes, and..." (which is also a improv trick)

For example:

* "Yes, I can do that! Of those 20 things, which one has the highest priority?"
* "Yes, now out of those things, which of my current work would you like me to de-prioritize?"
* "Yes, I can put it on my todo list and put it at high priority - absolutely! But these two tasks seem to require a lot of work that I can't work on both at the same time."
* "Yes, I agree they're both important! In order to meet your expectations, based on my throughput, I can do X, and parts of Y by next week, only if I drop Z."

Do you see where this is going?

To wrap it up:

> Starting from yes means working with your colleagues, not against them.


I highly recommend you read the post to reinforce it.

Then get a poster with that statement and never ever forget it.

VIA **97 Things Every Developer Should Know**:
https://97-things-every-x-should-know.gitbooks.io/97-things-every-programmer-should-know/content/en/thing_77/
