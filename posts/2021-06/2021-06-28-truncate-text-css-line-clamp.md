---
title: TIL Truncating text with Ellipsis
date: 2021-06-28
published: true
tags: ['css']
series: false
canonical_url: false
description: "You know you get a body of text, but you only want a specific amount to be shown?"
layout: layouts/post.njk
---


You know you get a body of text, but you only want a specific amount to be shown?

Example:
> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec iaculis enim. Nam sagittis laoreet tellus, ac dapibus erat. Aliquam venenatis auctor elit eget sollicitudin. Nullam vulputate varius ante eu hendrerit. Nullam malesuada sem sapien, ut mollis nisi efficitur in. Nullam metus velit, aliquam non posuere non, vulputate quis diam. Aenean efficitur, mi non mollis sollicitudin, nulla nulla convallis ante, id scelerisque nisi arcu at est. Integer efficitur turpis nec tortor finibus, eu vehicula libero aliquam. Vivamus consequat lorem ac massa maximus, vel semper nibh vehicula.

becomes

> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut nec iaculis enim. Nam sagittis laoreet tellus, ac dapibus erat. Aliquam venenatis auctor elit eget sollicitudin. Nullam vulputate varius ante eu hendrerit. Nullam malesuada sem sapien, ut mollis nisi efficitur in. Nullam metus velit, aliquam non posuere non...

You can do that here:

```css
.line-clamp {
  display: -webkit-box;
  -webkit-line-clamp: 3;
  overflow: hidden;
  -webkit-box-orient: vertical;
}
```

If your build tool is removing `webkid-box-orient`,
that's because your tool sees [webkit-bot-orient as outdated](https://stackoverflow.com/questions/46152850/webkit-box-orient-styling-disappears-from-styling), and is automatically removing it.

But, this specific process is implemented in all browsers (hacky and all).


To make it work, do this:
```css
/* autoprefixer: off */
-webkit-box-orient: vertical;
/* autoprefixer: on */
...
```



via:
https://css-tricks.com/line-clampin/
