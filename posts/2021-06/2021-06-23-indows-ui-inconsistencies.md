---
title: TIL that Windows 10 has code all the way back to Windows 3.x
date: 2021-06-23
published: true
tags: ['windows']
series: false
canonical_url: false
description: "This dev digs real deep to see how far Windows has been re-using UI elements."
layout: layouts/post.njk
---

This dev digs real deep to see how far Windows has been re-using UI elements.

They go through each layer of UI and what still exists.

Did you know that the spinning dots were introduced in Windows 8?
And the installing a driver still uses the Windows XP dialog box?
Or how the Run dialog box looks very similiar to the Windows 95 version?

Layer 1: Fluent Design
Layer 2: Metro
Layer 3: Windows 8 Win32 elements
Layer 4: Windows 7 UI elements
Layer 5: Windows Vista.
Layer 6: Windows XP --
Layer 7: Windows 2000
Layer 8: Windows 95/NT 4.0 elements.

Via:
https://ntdotdev.wordpress.com/2021/02/06/state-of-the-windows-how-many-layers-of-ui-inconsistencies-are-in-windows-10/
