---
title: TIL about all the times devs broke things
date: 2021-06-20
published: true
tags: ['dev']
series: false
canonical_url: false
description: "HBO Intern breaks something. Wholesome."
layout: layouts/post.njk
---


HBO Intern breaks something.
Devs know all about that.

![](2021-06-20-dev-breaks-things_0.jpeg)

![](2021-06-20-dev-breaks-things_1.jpeg)

![](2021-06-20-dev-breaks-things_2.jpeg)

![](2021-06-20-dev-breaks-things_3.jpeg)



VIA:
https://imgur.com/gallery/aR3IxrY
