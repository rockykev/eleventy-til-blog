---
title: TIL Lexical Scoping
date: 2021-06-17
published: false
tags: ['javascript', 'closure']
series: false
canonical_url: false
description: "Lexical scoping describes how a parser resolves variable names when functions are nested."
layout: layouts/post.njk
---

Lexical scoping is a part of Closures.


Lexical scoping describes how a parser resolves variable names when functions are nested. Lexical refers to the fact that lexical scoping uses the location where a variable is declared within the source code to determine where that variable is available. Nested functions have access to variables declared in their outer scope.


```js
function initialize() {

  var name = 'Mozilla'; // name is a local variable created by initialize

  function displayName() { // displayName() is the inner function, a closure
    alert(name); // the variable declared in the parent function
  }

  displayName(); // will fire the displayName function

}

initialize();
```

This is the example in the MDN.
The variable `name` will stay stay and remain available for us.


Via:
[MDN Web Docs: Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)
