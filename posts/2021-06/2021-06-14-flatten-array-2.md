---
title: TIL flattening arrays
date: 2021-06-14
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "How to flatten out arrays"
layout: layouts/post.njk
---

Have an array inside your array inside your array?

```js

let theArray = [1, 2, [3, 4, [5, 6]]];


// flat() method
theArray.flat(0); // DOES NOTHING: [1, 2, [3, 4, [5, 6]]]

theArray.flat();  // DEFAULT: [1, 2, 3, 4, [5, 6]]
theArray.flat(1); // [1, 2, 3, 4, [5, 6]]
theArray.flat(2); // [1, 2, 3, 4, 5, 6]
theArray.flat(3); // [1, 2, 3, 4, 5, 6]
```



