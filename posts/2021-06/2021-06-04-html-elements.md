---
title: TIL HTML elements like Blockquote, datalist, details, meter, & progress
date: 2021-06-04
published: true
tags: ['html', 'codepen', 'mdn']
series: false
canonical_url: false
description: "HTML Elements I forget exist. Blockquote, datalist, details, meter, progress"
layout: layouts/post.njk
---

HTML Elements I forget exist

Live examples:
<p class="codepen" data-height="300" data-default-tab="result" data-slug-hash="abWoQMw" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/abWoQMw">
  HTML Elements I forget exist</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

## Blockquote

The `<blockquote>` HTML element indicates that the enclosed text is an extended quotation. Usually, this is rendered visually by indentation (see Notes for how to change it). A URL for the source of the quotation may be given using the cite attribute, while a text representation of the source can be given using the `<cite>` element.

[Via MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/blockquote)

## Datalist

The `<datalist>` HTML element contains a set of `<option>` elements that represent the permissible or recommended options available to choose from within other controls.

[via MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist)


## Details

The `<details>` HTML element creates a disclosure widget in which information is visible only when the widget is toggled into an "open" state. A summary or label must be provided using the `<summary>` element.

A disclosure widget is typically presented onscreen using a small triangle which rotates (or twists) to indicate open/closed status, with a label next to the triangle. The contents of the `<summary>` element are used as the label for the disclosure widget.

[via MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details)


## Meter

The `<meter>` HTML element represents either a scalar value within a known range or a fractional value.

[via MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meter)


## Progress

The `<progress>` HTML element displays an indicator showing the completion progress of a task, typically displayed as a progress bar.

[via MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/progress)


