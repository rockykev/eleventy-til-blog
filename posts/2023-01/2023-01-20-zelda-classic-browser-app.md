---
title: TIL porting Zelda Classic from a Windows App to the browser
date: 2023-01-20
published: true
tags: ["videogames", "assembly", "compiling"]
series: false
canonical_url: false
description: "He learned about each layer of the codebase, and how to transpile it down to the next layer until finally hitting Web APIs."
layout: layouts/post.njk
---

Real fascinating read about how this developer ported Zelda Classic, a Windows application to the web browser using WebAssembly.


Porting ZQuest Classic:

> ZQuest Classic started as a classic 80s-style dungeon-crawler game engine, that over time has grown into a more modern engine. It allows much easier creation of dungeon-crawler style games than other big game engines, due to the number of features that are available without the need for any form of programming knowledge. Since 2012, it has scripting capabilities via the ZScript language, developed specifically for the program. While no scripting is required for many features, use of scripting can allow much more complex and fancy interactions, and allows much more creativity in mechanics and design- at the cost of requiring much more work.
https://www.zeldaclassic.com/about/

> However, the most recent version of Zelda Classic only supports Windows... until now!
> I spent the last two months (roughly ~150 hours) porting Zelda Classic to run in a web browser.

via [Porting Zelda Classic to the Web](https://hoten.cc/blog/porting-zelda-classic-to-the-web/?utm_source=pocket_reader)

## Getting it working

![](_2023-01-20-zelda-classic-browser-app_0.png)

Emscripten to build C/C++ to WebAssembly

> Zelda Classic is written in C++ and uses Allegro, a low-level cross platform library for window management, drawing to the screen, playing sounds, etc. Well, it actually uses Allegro 4, released circa 2007. Allegro 4 does not readily compile with Emscripten, but Allegro 5 does. The two versions are vastly different but fortunately there is an adapter library called Allegro Legacy which allows an Allegro 4 application to be built using Allegro 5.

> So that's the first hurdle–Zelda Classic needs to be ported to Allegro 5, and its CMakeLists.txt needs to be modified to build allegro from source.

tl;dr - he learned about each layer of the codebase, and how to transpile it down to the next layer until finally hitting Web APIs.

## Takeaways

> As soon as you run into what seems like an intractable bug, stop trying to debug it from the context of your application and try to make a minimial reproduction. It will become easier to reason about the problem, and if the bug belongs to a dependency you will have a ready-made repro to provide in bug report.

> File bug reports and upstream bug fixes when possible! But also, have some way to tweak your dependencies, be it with hard forks or a patching system. You can't allow a bug in a dependency that you know how to resolve stall progress.

> Break down problems with unknown solutions. For example, my first attempt at porting Zelda Classic was over a year ago, and that failed miserably because I jumped right into the end task of "actually port it", without first taking the time to really learn the tools involved, which resulted in me spinning my wheels. This time around, I avoided that by making my first task to fully understand how to port the simplest Allegro program.


