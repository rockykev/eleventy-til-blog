---
title: TIL Libuv
date: 2023-01-12
published: true
tags: ["javascript", "node"]
series: false
canonical_url: false
description: "Libuv also implements two extremely important features of Nodejs which are the event loop and the thread pool and in simple terms, the event loop is responsible for handling easy tasks like executing callbacks and network I/O while the thread pool deals with heavy work like file access or compression."
layout: layouts/post.njk
---


Today I learned about Libuv!

Node is a JS environment so we can run JS server-side, and built on top of Google's V8 Javascript engine.

## What about Livuv?

Libuv is a library that provides the underlying event loop and I/O functionalities used by Node.js. Node.js is built on top of libuv!

[Libuv](https://github.com/libuv/libuv) is also open-source!

Libuv also implements two extremely important features of Nodejs which are the event loop and the thread pool and in simple terms, the event loop is responsible for handling easy tasks like executing callbacks and network I/O while the thread pool deals with heavy work like file access or compression.


## Written in
libuv is written in C++, Google's v8 engine is primarily written in C++.

Node.js is primarily written in C and C++, with some portions of the core written in JavaScript. The Node.js project uses C and C++ for its core functionality, such as the event loop and low-level I/O operations, while JavaScript is used for higher-level abstractions and user-facing APIs.

## Performance and the Thread Pool

Some tasks are really heavy and expensive to be executed in the event loop because they will then block the single thread.

So that's where we use the Thread Pool.

The thread pool is used to execute operations that are CPU-bound or that require blocking I/O operations, such as file system access or network communication.

The use of a thread pool in combination with the event loop allows Node.js to handle a large number of concurrent operations efficiently, without blocking the event loop or requiring the use of multiple threads.

via [Node.js Behind the Scenes](https://medium.com/@SigniorGratiano/node-js-behind-the-scenes-c6c7eb966833)
