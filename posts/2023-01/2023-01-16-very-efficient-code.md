---
title: TIL Very Efficient Code
date: 2023-01-16
published: true
tags: ["cleancode", "bigo"]
series: false
canonical_url: false
description: "This is a great solution. Easy to understand. Executes really fast. No real-time string concatenation. No over-engineering. String appearance is easily customized."
layout: layouts/post.njk
---


I absolutely love this:

![](_2023-01-16-very-efficient-code_0.png)

via https://www.reddit.com/r/ProgrammerHumor/comments/10dh6x1/very_efficient_code/j4lm8eg?context=3


## Why I like it

via [IntentionallyBadName](https://www.reddit.com/r/ProgrammerHumor/comments/10dh6x1/comment/j4lm8eg/?utm_source=reddit&utm_medium=web2x&context=3)

> This code is part of the Dutch DigiD App which is an authentication app for Dutch citizens to log in to government websites for taxes and other government related stuff.

It's O(1)

via [sebbdk](https://www.reddit.com/r/ProgrammerHumor/comments/10dh6x1/comment/j4mp91h/?utm_source=reddit&utm_medium=web2x&context=3)

> at some point discovered that nitpicking isolated bad code only serves to suck the soul out of my juniors.



via [kalesandwichsincity](https://www.reddit.com/r/ProgrammerHumor/comments/10dh6x1/comment/j4ly6kf/?utm_source=reddit&utm_medium=web2x&context=3)

> There are no awards for short code. Two types of coders would use this solution. Those who've written code for one month, and those who've done it for a lifetime. In-betweens can't resist over-engineering.

> This is a great solution. Easy to understand. Executes really fast. No real-time string concatenation. No over-engineering. String appearance is easily customized.

> Unless called 1000 times per second, there’s no reason for optimizing this further. It’s better to have readable code and spend time where it matters.



via [Glitch29](https://www.reddit.com/r/ProgrammerHumor/comments/10dh6x1/comment/j4mzhpt/?utm_source=reddit&utm_medium=web2x&context=3)

> It's EXTREMELY easy to read. It's easy to verify that there aren't any bugs. It's not that long. And while performance is unlikely to matter here, it runs faster than any solution which involves string operations.
