---
title: TIL finding the browser being used
date: 2023-01-14
published: true
tags: ["javascript", "browser"]
series: false
canonical_url: false
description: "How to detect the browser you're using, with javascript."
layout: layouts/post.njk
---


My biggest pain the 🍑 client had me fixing something that was a issue on his kid's iPhone 5, using Firefox.

I could not recreate it ANYWHERE ELSE.

To determine their browser, I had to use this.

```js
function whichBrowser() {
  if (isFirefox()) {
    return "Firefox";
  } else if (isEdge()) {
    return "Edge";
  } else if (isIE()) {
    return "Internet Explorer";
  } else if (isOpera()) {
    return "Opera";
  } else if (isVivaldi()) {
    return "Vivaldi";
  } else if (isChrome()) {
    return "Chrome";
  } else if (isSafari()) {
    return "Safari";
  } else {
    return "Unknown";
  }
}
function agentHas(keyword) {
  return navigator.userAgent.toLowerCase().search(keyword.toLowerCase()) > -1;
}
function isIE() {
  return !!document.documentMode;
}
function isSafari() {
  return (
    (!!window.ApplePaySetupFeature || !!window.safari) &&
    agentHas("Safari") &&
    !agentHas("Chrome") &&
    !agentHas("CriOS")
  );
}
function isChrome() {
  return agentHas("CriOS") || agentHas("Chrome") || !!window.chrome;
}
function isFirefox() {
  return agentHas("Firefox") || agentHas("FxiOS") || agentHas("Focus");
}
function isEdge() {
  return agentHas("Edg");
}
function isOpera() {
  return agentHas("OPR");
}
function isVivaldi() {
  return agentHas("Vivaldi");
}

```

