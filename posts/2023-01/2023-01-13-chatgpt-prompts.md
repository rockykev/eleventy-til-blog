---
title: TIL ChatGPT prompts
date: 2023-01-13
published: true
tags: ["ai", "chatgpt", "machinelearning"]
series: false
canonical_url: false
description: "Don’t ask it to write an essay about how human error causes catastrophes. The AI will come up with a boring and straightforward piece that does the minimum possible to satisfy your simple demand."
layout: layouts/post.njk
---


Like everyone, I've been loving ChatGPT as a BS generator to quickly turn a blank white canvas into a pile of words.

But, one thing I struggled with was generating good prompts.

Some tips via [How to... use ChatGPT to boost your writing](https://oneusefulthing.substack.com/p/how-to-use-chatgpt-to-boost-your)


## Details in your prompt. Don't ask it a question

> Don’t ask it to write an essay about how human error causes catastrophes. The AI will come up with a boring and straightforward piece that does the minimum possible to satisfy your simple demand. Instead, remember you are the expert and the AI is a tool to help you write. You should push it in the direction you want.

> So try asking for it to be concise or wordy or detailed, or ask it to be specific or to give examples. Ask it to write in a tone (ominous, academic, straightforward) or to a particular audience (professional, student) or in the style of a particular author or publication (New York Times, tabloid news, academic journal).

Example:

![](_2023-01-13-chatgpt-prompts_0.png)

---

## Play with memory and length

> Sometimes the memory is useful, you can (and should) ask it to revise previous work: change the third paragraph to be more professional or use a different example in the middle and it will provide a revised version. However, sometimes it forgets what you were working on, and you have to remind it. You may, for example, want to tell it revise the third paragraph on the essay on catastrophes so it doesn’t lose track.

I do this a lot where I argue with pieces of the content it shares. I ask for clarity and re-write sections.

## Play with personas and style

> You can ask the AI to use specific styles for writing. You will get different results from asking for an academic essay versus a persuasive article versus a blog post versus a corporate memo. The results are often surprising. So you should consider trying several writing types when experimenting with prompts.

> “I want you to act as a storyteller. You will come up with entertaining stories that are engaging, imaginative and captivating for the audience. It can be fairy tales, educational stories or any other type of stories which has the potential to capture people’s attention and imagination. You should provide lots of detail and make the story memorable.


I also use this:
https://prompts.chat/#using-promptschat


For example:

> Act as NodeJS Interviewer

> I want you to act as an interviewer. I will be the candidate and you will ask me the interview questions for the position position. I want you to only reply as the interviewer. Do not write all the conservation at once. I want you to only do the interview with me. Ask me the questions and wait for my answers. Do not write explanations. Ask me the questions one by one like an interviewer does and wait for my answers. My first sentence is “Hi”

or
> Act as a Dentist

> I want you to act as a dentist. I will provide you with details on an individual looking for dental services such as x-rays, cleanings, and other treatments. Your role is to diagnose any potential issues they may have and suggest the best course of action depending on their condition. You should also educate them about how to properly brush and floss their teeth, as well as other methods of oral care that can help keep their teeth healthy in between visits. My first request is “I need help addressing my sensitivity to cold foods.”
