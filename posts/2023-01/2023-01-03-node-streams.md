---
title: TIL Node Streams
date: 2023-01-03
published: true
tags: ["node", "streams", "promise"]
series: false
canonical_url: false
description: "Imagine if you did some data-extensive fetch request getting a 1 gig file. 1 gig can take a while, and it's fetching it in memory."
layout: layouts/post.njk
---

Today I learned about Node Streams!

via [Node Streams](https://nodejs.org/api/stream.html)

Without streams, you'll write the full content to the file before returning the control back to your program (in the async version, this means executing the callback).

Imagine if you did some data-extensive fetch request getting a 1 gig file. 1 gig can take a while, and it's fetching it in memory.

In this case, a better option is to write the file content using streams.

## Why streams?

Streams basically provide two major advantages using other data handling methods:

* Memory efficiency: you don’t need to load large amounts of data in memory before you are able to process it

* Time efficiency: it takes way less time to start processing data as soon as you have it, rather than waiting till the whole data payload is available to start


## Four Stream Types

* Writable: streams to which data can be written (for example, `fs.createWriteStream()`).
* Readable: streams from which data can be read (for example, `fs.`createReadStream()).
* Duplex: streams that are both Readable and Writable (for example, `net.Socket`).
* Transform: Duplex streams that can modify or transform the data as it is written and read (for example, `zlib.createDeflate()`).

