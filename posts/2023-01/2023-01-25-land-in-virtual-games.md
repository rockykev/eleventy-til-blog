---
title: TIL Land in Virtual Games
date: 2023-01-25
published: true
tags: ["mmo", "videogames", "economy"]
series: false
canonical_url: false
description: "in [Star Wars] Galaxies … there was ample room for everyone to own a house if they chose – but people did things like build player cities atop the entrances to dungeons to monopolize access to resources."
layout: layouts/post.njk
---


I was playing V Rising, and in that game, you can 'claim' a chunk of land to set up your castle. The servers are small, roughly 40 people. That means, roughly 40 vampire castles.

In V Rising, you simply accept that land is scarce, and either make a new server or troll people enough that they never return back and then you steal their land.

But what about a MMO? A game like World of Warcraft would quickly fall apart if everyone shared the same server, and had thousands of players demanding virtual land.

And that's where I landed on this hilarious tidbit:

> Here’s a practical example witnessed by Raph Koster, lead designer of Star Wars Galaxies:

> “in [Star Wars] Galaxies … there was ample room for everyone to own a house if they chose – but people did things like build player cities atop the entrances to dungeons to monopolize access to resources.”

> The above example is pretty similar to the behavior of the medieval Robber Barons, who famously stretched chains across the Rhine so they could extract tolls from passing ships.


via [Land value tax in online games and virtual worlds: A how-to guide](https://www.gamedeveloper.com/design/land-value-tax-in-online-games-and-virtual-worlds-a-how-to-guide)
