---
title: TIL node modules fs and os
date: 2023-01-04
published: true
tags: ["node", "streams", "os"]
series: false
canonical_url: false
description: "with fs - you can access files, read files, make directories, chmod. With os - get stats, machine, version, hosts."
layout: layouts/post.njk
---

Today I learned about the NodeJS OS module and folder structure

## Working with folders in Node.js

The Node.js fs core module provides many handy methods you can use to work with folders.

Use-cases:

* Use `fs.access()` to check if the folder exists and Node.js can access it with its permissions.

* `fs.readFile()` method to read the contents of a file and the `fs.writeFile()` method to write data to a file. These methods are useful for working with text files, JSON files, and other types of data.

* `fs.mkdir()` to create a new directory, `fs.readdir()` to list the contents of a directory, and `fs.rmdir()` to remove a directory.

* `fs.watch()` method, which allows you to monitor a file or directory for changes. When a change occurs, you can execute a callback function to perform some action, such as updating a cache or reloading a web page.

* `fs.chmod()` method allows you to set the permissions for a file or directory.

* `fs.createReadStream()` and `fs.createWriteStream()`. These methods are useful for working with large files or for streaming data over a network.

* `fs.stat()` method allows you to retrieve information about a file or directory, such as its size, last modified date, and file type.

via [Node.js Files](https://nodejs.org/api/fs.html)

## The Node.js os module

This module provides many functions that you can use to retrieve information from the underlying operating system and the computer the program runs on, and interact with it.

* Retrieving system information, such as the amount of free memory `os.freemem()`, the CPU architecture `os.machine()`, the hostname `os.hostname()`, and the operating system version `os.version()`.

* `os.cpus()` to retrieve information about the system's CPUs, `os.loadavg()` to retrieve the system's load average, and `os.uptime()` to retrieve the system's uptime.

* `os.userInfo([options])` Returns information about the currently effective user. On POSIX platforms, this is typically a subset of the password file. The returned object includes the username, uid, gid, shell, and homedir. On Windows, the uid and gid fields are -1, and shell is null.

* `os.tmpdir()` to retrieve the system's temporary directory and `os.homedir()` to retrieve the current user's home directory.
via [Node.js OS](https://nodejs.org/api/os.html)
