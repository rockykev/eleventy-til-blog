---
title: TIL Computed Properties in Javascript
date: 2023-01-19
published: true
tags: ["naming", "javascript", "mdn"]
series: false
canonical_url: false
description: "Today I learned about Computed Property names."
layout: layouts/post.njk
---


Today I learned about Computed Property names.

This is useful for a lot of reasons, like creating a Factory Pattern where you need to shape objects with unique keys.

```js
const yourKeyVariable = "happyCount";
const someValueArray= [...];

const obj = {
    [yourKeyVariable]: someValueArray,
}
```

another example:
```js
// Computed property names
let i = 0;
const a = {
  [`foo${++i}`]: i,
  [`foo${++i}`]: i,
  [`foo${++i}`]: i,
};

```

[MDN on Computed Property Names](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer#computed_property_names)

and via [StackOverflow](https://stackoverflow.com/questions/11508463/javascript-set-object-key-by-variable)
