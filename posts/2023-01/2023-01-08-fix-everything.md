---
title: TIL the fix everything script
date: 2023-01-08
published: true
tags: ["bash", "script"]
series: false
canonical_url: false
description: "A script that fixes everything for end-users"
layout: layouts/post.njk
---

> I think one of my favorite stories on here was a 1 man IT guy putting a "fix everything" script on all desktops that would run ipconfig and several other commands to spam the screen with worthless text and then finish by rebooting the system. He would then just advise everyone that the "script" he wrote fixed a lot of common issues and they should try it first before calling him.

via https://www.reddit.com/r/sysadmin/comments/107pi1j/every_ticket_that_came_in_today_has_been_solved/j3o7efw?context=3


How this script can look like:

1. Remove browser cache
2. Get IP address
3. Do some weird matrix stuff
4. reboot
5. FIXED!

```bash

#!/bin/bash

# Remove browser cache files (for Firefox, Chrome, and Safari)
rm -rf ~/.cache/mozilla/
rm -rf ~/.cache/google-chrome/
rm -rf ~/Library/Caches/com.apple.Safari/

# Get IP address using the ip command
IP=$(ip route get 1.2.3.4 | awk '{print $7}')

echo "Your IP address is: $IP"


# Script to display a Matrix-like effect in the terminal for 15 seconds
# Function to generate a random 0 or 1
function rand01 {
    echo $((RANDOM%2))
}

# Set the start time and end time
start=$(date +%s)
end=$((start+15))

# Loop to display the Matrix effect until 15 seconds are up
while [ $(date +%s) -lt $end ]
do
    for ((i=1;i<=50;i++))
    do
        for ((j=1;j<=100;j++))
        do
            echo -n $(rand01)
        done
        echo
    done
    sleep 0.1
done

# Script to reboot the computer after a specified delay
# Delay in seconds before rebooting
DELAY=10

sleep $DELAY

echo "Rebooting the system now..."
reboot


```

 that creates a Matrix-like 0101011 effect, then after a few seconds,
