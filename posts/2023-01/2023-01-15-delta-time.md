---
title: TIL delta time
date: 2023-01-15
published: true
tags: ["time", "javascript"]
series: false
canonical_url: false
description: "Delta time, also known as 'time delta,' refers to the difference between the 'current time' and the 'last time' a piece of code was executed. In JavaScript, delta time can be calculated using the Date object or by using the performance.now() method."
layout: layouts/post.njk
---

I had to calculate how long it took for data to get from multiple fetch requests, generating it into a file, and then completing the entire action.

```js
  const timestampStart = process.hrtime.bigint();
// A bunch of code

// End Timer
const timestampEnd = process.hrtime.bigint();
const timestampElapsed = Number(timestampEnd - timestampStart) / 1_000_000_000; // convert nanoseconds to seconds

// RESULT
// WHen promise is finished for everything
console.log(response.text);
console.log(`This took ${timestampElapsed.toFixed(2)} seconds to run.`);
```


What is Delta time (or Time Delta)?
https://developer.mozilla.org/en-US/docs/Web/API/Performance/now

Via ChatGPT:

> Delta time, also known as "time delta," refers to the difference between the "current time" and the "last time" a piece of code was executed. In JavaScript, delta time can be calculated using the Date object or by using the performance.now() method.

> Delta time is often used in game development and animation to ensure that animations and movements are consistent regardless of the computer's processing speed. Instead of relying on a fixed time interval for animations, which may result in choppy or inconsistent movement on slower computers, delta time can be used to calculate the appropriate amount of movement for each frame based on the time elapsed since the last frame was rendered.

> For example, if a game is running at 60 frames per second, and the delta time between the current frame and the last frame is 16 milliseconds, the game can calculate the appropriate amount of movement to apply to game objects to maintain a smooth and consistent frame rate.


