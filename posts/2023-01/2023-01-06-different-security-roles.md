---
title: TIL all the different security roles
date: 2023-01-06
published: true
tags: ["roles", "security", "certifications"]
series: false
canonical_url: false
description: "In appsec most notably offense vs defense. They are complimentary of course but the skills differ. For offense you need specific tools, need to get used to testing applications sometimes without source code access."
layout: layouts/post.njk
---


This question Reddit: [Devs who transitioned into a Security Engineering role, how’s it going?](https://www.reddit.com/r/ExperiencedDevs/comments/104xwuq/)

I never knew about the different Security roles.

> There are many paths in security. In appsec most notably offense vs defense. They are complimentary of course but the skills differ. For offense you need specific tools, need to get used to testing applications sometimes without source code access.

> For defense it’s similar to software architect, except it’s the security part of it, which requires most of the software architect know how.


via [andersonmvd](https://www.reddit.com/r/ExperiencedDevs/comments/104xwuq/devs_who_transitioned_into_a_security_engineering/j399mqt?context=3)

> For security engineers, you have today some separation of devsecops (defense) , pentester (offsec), cloud security engineer, IAM/WAF engineer, reverse engineer. Maybe I am missing some more.

> This would be my answer for security jobs in general:

> In security consultancies: pentest (appsec / netsec), social engineering, reverse engineering

> In education: overall security knowledge trainings, training for certifications (ISC2, ISACA, Offensive Security, SANS, etc), security awareness trainings (employees, executives, developers, etc)

> In companies: cloud security engineer, devsecops engineer, IAM engineer, security architect, security officer (think access control, manage machines w/ antivirus, etc), compliance officer, fraud prevention (some people are responsible for both security & fraud, sometimes it's separate), network engineer, IAM/WAF engineer

> In auditing: audit PCI, ISO27001, etc
