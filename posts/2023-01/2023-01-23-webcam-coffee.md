---
title: TIL the first webcam
date: 2023-01-23
published: true
tags: ["history", "webcam", "coffee"]
series: false
canonical_url: false
description: "The Trojan Room coffee pot was a coffee machine located in the Computer Laboratory of the University of Cambridge, England. Created in 1991 by Quentin Stafford-Fraser and Paul Jardetzky, it was migrated from their laboratory network to the web in 1993 becoming the world's first webcam."
layout: layouts/post.njk
---

Today I learned about the origin of the webcam

A few months ago, I was sick of not knowing when my coffee was finished being prepared. So I decided to hook up a webcam to the kitchen.

That's when someone shared this tidbit:

> The Trojan Room coffee pot was a coffee machine located in the Computer Laboratory of the University of Cambridge, England.

> Created in 1991 by Quentin Stafford-Fraser and Paul Jardetzky, it was migrated from their laboratory network to the web in 1993 becoming the world's first webcam.

Looks like I'm using the webcam for it's original intention! :-)

Via [Trojan Room Coffee Pot](https://en.m.wikipedia.org/wiki/Trojan_Room_coffee_pot)
