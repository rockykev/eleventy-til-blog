---
title: TIL if TypeScript is worth it
date: 2023-01-17
published: true
tags: ["typescript", "javascript"]
series: false
canonical_url: false
description: "The answer: No, according to HackerNews. But HackerNews audience aren't exactly representative of average developers? From my perspective, they're the 'startup' and 'ship fast and break things' folks. (I mean, I am too!)"
layout: layouts/post.njk
---

I've been struggling over this for a few weeks, wondering if TypeScript is worth incorporating.

It started with this HackerNews article: [Ask HN: Is TypeScript worth it?](https://news.ycombinator.com/item?id=34359504) where they were going hard at ripping TypeScript apart.

But, is the HackerNews audience aren't exactly representative of average developers? From my perspective, they're the 'startup' and 'ship fast and break things' folks. (I mean, I am too!)

So I wanted to source from Reddit too.

My results:

## Typescript benefits

via Baturinsky

> Types are not for debugging. It's for intellisense and refactoring.

via Ozymandias0023

> I wish every day that our code base was written in typescript. The amount of time I spend figuring out the shape of objects passed to methods I need to refactor is too damn high

via TRASH--BOAT

> Typescript is worth it on ANY size team. Typing your data improves readability for yourself and your team. It also improves your IDEs ability to help you code faster. Structured data also means you know exactly what is being exposed and where.

via jmking

> TS is very useful when you're trying to refactor things. It'll tell you that you forgot to update the parameters of a certain function when you've updated the object you've been passing around, for example.

> Also, you don't have to do TS all at once. In fact, you can write .ts files with vanilla JS and the compiler won't complain.

> I've given this advice recently elsewhere, but the easiest way to get started with TS is to start typing your function return types, then start typing function parameters.

> If you're dealing with a seam between JS and TS, don't be afraid of typing stuff as any as a stopgap.

> As you get more comfortable with TS, you'll find more language features that are useful. Also keep in mind that TS doesn't fundamentally change JS. A lot of people errantly think that TS magically makes JS behave like a classic object-oriented language. TS is ultimately just a tool to tell you at compile time if what your code expects is what you've committed to supply.

> TS doesn't work at runtime. So if you write code that says it passes certain types, but, say, the server returns a response that doesn't match that type, TS isn't going to save you.

> TS is very useful, even in a small team to make your editor autocomplete work, and makes it so you can document your API contracts.

> If you've ever found yourself digging around in other files trying to figure out what the shape of the object being passed in looks like, TS will be an immediate win once you type that stuff out.

via Eux86

> Typescript is as important as unit tests. The more typescript you add, the less unit test you need to check that you’re not confusing oranges and potatoes. Typescript helps you define what shapes your data takes flowing in the logic of your code and makes sure that you don’t change it inadvertently.

> I strongly believe that the feeling of speed just writing plain JavaScript is an illusion that shatters as soon as the code becomes complex enough or that enough developers start working on the same code.


## Typescript Hate


**The TypeScript Tax**

via madeofpalk

> I think whatever tax you pay in writing typescript (which, as someone reasonably experienced with it, I believe is none or exceptionally minimal) you easily get back from improved efficiencies of not requiring memorizing the entire shape of your application, looking up in seperate documentation, or a run/inspect/write-code just to see what things are.

> I think that typescript's type refinement is extremley useful to know whether you've covered all the cases for the types of data as it flows through your system.

---
via chucklenorris

> As i said before, a lot of people expect that writing typescript is as easy as writing js, which is not. I estimate the typescript tax to be around 40-100% more time, especially if you want to do typescript right and not use `any` all over the place. And besides, typescript is a poor fit for someone who is used to writing highly dynamic, lambda based code, which is one of the main niceities of js. My guess is that a lot of people with a background in java and c# found themselves writing server side js and were really unconfortable with this paradigm, so they tried again to turn js into what they knew best - an imperative object oriented programming language. Now, i'm not saying that types are a bad thing, i for one like typescript especially in projects with a lot of people, but, it's funny to see new people struggling to finish a task that should take a few hours in days.

---

via puritanner


> * TS is slightly more difficult to read (more LOC, more indirection, complexity of code structure statements can outrun the complexity of actual code)
* Pre-Mature Optimization is a bigger issue with TS than it is with JS.
* Third Party Code integration without Types can lead to a lot of unproductive work.
* Integration in Node is still a few extra steps
* Generally speaking: The 5% of use-cases where TS just failed to figure out Third-Party Code or React Components rendering loop could lead to hours spend re-arranging code without any actual use.
* Tech Debt with TS is generally heavier although it incurs at a slower rate. UNLESS specs, techstack or architecture changes midway through a project.


via marty_byrd_

> I fundamentally don’t agree with typescript. I think it should be an ide tool. Coercion is very powerful in JavaScript same with the dynamic types. It’s not something to be corrected.

via chuck_niespor

> I use both .js and .ts depending on particular project. Using Typescript all the time usually leads to over-engineering even simple projects.

via [[AskJS] If you don't use TypeScript, tell me why (2 year follow up)](https://www.reddit.com/r/javascript/comments/o8n3uk/comment/h37h9rz/?utm_source=reddit&utm_medium=web2x&context=3)


via [Is Typescript worth it in a small team?](https://www.reddit.com/r/vuejs/comments/vlkbzd/is_typescript_worth_it_in_a_small_team/)

via [After 800 hours I finally saw why people like TS](https://www.reddit.com/r/typescript/comments/12f8x97/after_800_hours_i_finally_saw_why_people_like_ts/)
