---
title: TIL Scoping with Cypress
date: 2023-01-24
published: false
tags: ["javascript", "testing", "cypress"]
series: false
canonical_url: false
description: "So you wanted to find a specific element called `heading`. But you have a BUNCH of elements called `heading`. By using `within()` helper, you can force it to only look inside `main` elements."
layout: layouts/post.njk
---

Today I learned about how to scope with Cypress:

```js
cy.findByRole("main").within(() => {
  // Everything in here is scoped to the "main" element only
  cy.findByRole('heading', { name: "Dashboard" });
});
```

So you wanted to find a specific element called `heading`.

But you have a BUNCH of elements called `heading`.

By using `within()` helper, you can force it to only look inside `main` elements.

[Cypress within](https://docs.cypress.io/api/commands/within)

```html
<form>
  <input name="email" type="email" />
  <input name="password" type="password" />
  <button type="submit">Login</button>
</form>

<script>
cy.get('form').within(($form) => {
  // you have access to the found form via
  // the jQuery object $form if you need it

  // cy.get() will only search for elements within form,
  // not within the entire document
  cy.get('input[name="email"]').type('john.doe@email.com')
  cy.get('input[name="password"]').type('password')
  cy.root().submit()
})
</script>
```

via https://dev.to/devteam/what-cypress-e2e-testing-has-taught-us-about-our-code-5aco

