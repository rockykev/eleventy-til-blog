---
title: TIL a mod dedicated to his fiance
date: 2023-01-21
published: true
tags: ["readme", "sad"]
series: false
canonical_url: false
description: "The best way to avoid DVT is simple: Save your game, stand up, and walk around for 2-3 minutes every 3 or 4 hours. Go to the fridge, pee, look out the window, play with your pet...anything, just MOVE. That simple action would have saved her life: it could save yours."
layout: layouts/post.njk
---


via [Comedy And Tragedy: I Read Readme Files](https://www.rockpapershotgun.com/mod-readme-files)

Mod readme files are interesting.

Get ready to get hit in the heart.

```
[Fishing in Skyrim for The Elder Scrolls V: Skyrim. 2012.]

DEDICATION - and a warning to gamers...

This mod is dedicated to my fiance, Kiersten, who passed away in April, one week before her 22nd birthday. She loved Skyrim and sushi, so what began as a birthday present evolved into a memorial to her life. The new species of fish this mod adds are named after things she loved, and I am in the process of adding a small fishing camp populated by her Skyrim toons, so she can live on in the fantasy world she loved. A book will be added in a future version explaining all the symbolism added in the module, for those who care about the backstory.

Kiersten died from something called Deep-Vein Thrombosis (DVT), which is a blood clot you get from sitting in one position for too long. Gamers are at high risk for DVT - she died after playing MW3 for 11 hours. The only warning sign she had was a cramp in her leg. Two hours later, she was dead.

The best way to avoid DVT is simple: Save your game, stand up, and walk around for 2-3 minutes every 3 or 4 hours. Go to the fridge, pee, look out the window, play with your pet...anything, just MOVE. That simple action would have saved her life: it could save yours.

When deep vein thrombosis symptoms occur, they can include:
• Swelling in the affected leg, including swelling in your ankle and foot.
• Pain in your leg; this can include pain in your ankle and foot. The pain often starts in your calf and can feel like cramping or a charley horse.
• Warmth over the affected area.
• Changes in your skin color, such as turning pale, red or blue.

When to see a doctor
If you develop signs or symptoms of deep vein thrombosis, contact your doctor for guidance.

If you develop signs or symptoms of a pulmonary embolism — a life-threatening complication of deep vein thrombosis — seek medical attention immediately.

The warning signs of a pulmonary embolism include:
• Unexplained sudden onset of shortness of breath
• Chest pain or discomfort that worsens when you take a deep breath or when you cough
• Feeling lightheaded or dizzy, or fainting
•Rapid pulse
•Sweating
•Coughing up blood
•A sense of anxiety or nervousness

Should you encounter a Khajit named Lillani in your travels, know you just met the spirit of a truly good person. She will be missed.
```
