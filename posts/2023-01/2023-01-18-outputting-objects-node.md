---
title: TIL outputting objects in Node
date: 2023-01-18
published: true
tags: ["logging", "node"]
series: false
canonical_url: false
description: "In your Node projects -- sometimes you just need to output the object. Stringify it."
layout: layouts/post.njk
---

In your Node projects -- sometimes you just need to output the object.

If you `console.log(myObject)` normally, you'll get something like

`{ a: 'a', b: { c: 'c', d: { e: 'e', f: [Object] } } }`

If you want it all showing, go with:


`console.log(JSON.stringify(myObject, null, 4));`


```json
{
    "a": "a",
    "b": {
        "c": "c",
        "d": {
            "e": "e",
            "f": {
                "g": "g",
                "h": {
                    "i": "i"
                }
            }
        }
    }
}
```

via [How can I get the full object in Node.js's console.log(), rather than '[Object]'?](https://stackoverflow.com/a/10729391/4096078)
