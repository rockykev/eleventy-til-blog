---
title: TIL why you should always use textContent
date: 2023-01-31
published: true
tags: ["js", "text"]
series: false
canonical_url: false
description: "Today I learned the difference between 'Node.textContent'. It handles all elements, even hidden ones. It also prevents XSS attacks since it strips tags."
layout: layouts/post.njk
---


Today I learned the difference between `Node.textContent` and all the others.



## Versus `HTMLElement.innerText`.

* `Node.textContent` gets content of any element, including hidden elements, `<script></script>` and `<style></style>` elements.

* `innerText` only shows "human-readable" elements.
* `innerText` also triggers a reflow.


## Versus `HTMLElement.innerHTML`

* `innerHTML` can cause XSS attacks as it runs the HTML.
* `Node.textContent` strips tags.

```js
var el = document.createElement('div');
el.innerHTML = 'A<p>B<span>C</span>D</p>D';
el.textContent; // "ABCDD" (HTML tags stripped successfully)
```

via the [MDN](https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent#differences_from_innertext)
via https://stackoverflow.com/a/31466405/4096078
