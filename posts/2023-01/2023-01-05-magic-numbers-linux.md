---
title: TIL Trovald's Magic Numbers
date: 2023-01-05
published: true
tags: ["magic", "linux"]
series: false
canonical_url: false
description: "These magic numbers are intended to safeguard against a typo or potential bit flips in the syscall number. Since reboot is a destructive and non-syncing operation, it's important to make absolutely sure that it isn't called accidentally due to a typo, a misbehaving program, or memory error"
layout: layouts/post.njk
---


Today I learned (from Reddit) that Linux kernel's reboot syscall accepts the birth dates of Torvald's and his three daughters (written in hexadecimal) as magic values.

The post: https://www.reddit.com/r/programming/comments/104igz8/til_the_linux_kernels_reboot_syscall_accepts_the/

This is so low-level that I'm not even fully following. But boy is it fascinating.

```
/*
 * Magic values required to use _reboot() system call.
 */

#define	LINUX_REBOOT_MAGIC1	0xfee1dead
#define	LINUX_REBOOT_MAGIC2	672274793
#define	LINUX_REBOOT_MAGIC2A	85072278
#define	LINUX_REBOOT_MAGIC2B	369367448
#define	LINUX_REBOOT_MAGIC2C	537993216
```

> The reboot call instantly reboots the machine and leaves no evidence. Requiring a magic number in a register means it can't be called accidentally because of memory corruption.

> These magic numbers are intended to safeguard against a typo or potential bit flips in the syscall number. Since reboot is a destructive and non-syncing operation, it's important to make absolutely sure that it isn't called accidentally due to a typo, a misbehaving program, or memory error.

> Every type of "system call" a program/process can make into Linux kernel has an integer associated with it. One flipped bit due to cosmic rays or whatever could lead to a completely different syscall being made than what the program/process intended. These additional magic values ensure that reboot is never accidentally performed due to such a thing by having the code also check for these magic values being passed in as part of the syscall.



