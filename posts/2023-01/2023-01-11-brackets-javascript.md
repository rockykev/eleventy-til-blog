---
title: TIL brackets breaking elements in JavaScript
date: 2023-01-11
published: true
tags: ["javascript", "linter"]
series: false
canonical_url: false
description: "The 'automatic semicolon insertion' it does is an error correction process"
layout: layouts/post.njk
---


I always forget about this edge-case:

```js

// returns the object
return {
hello : "world"
}

// undefined, because of the
return
{
hello : "world"
}
```

Because of automatic semicolon insertion.

> The "automatic semicolon insertion" it does is an error correction process.

:grimace:

Via [Potential_Ant_9890 on Reddit](https://www.reddit.com/r/node/comments/109v6mj/why_different_curly_brace_position/j40ke6d?context=3)
