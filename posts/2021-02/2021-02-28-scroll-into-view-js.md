---
title: TIL how to scroll to an element in JS
date: 2021-02-28
published:
tags: ['javascript']
series: false
canonical_url: false
description: "The normal HTML way is with an anchor link. But sometimes, you need to do it with JS."
---

Today I learned about the `.scrollIntoView` method.

The normal HTML way is with an anchor link.

```html
<a href="#ScrollToMe">LINK</a>
```

But sometimes, you need to do it with JS.

```
document.querySelector('#ScrollToMe').scrollIntoView({
     behavior: 'smooth'
});
```

You can also set it to jump to it's vertical and horizontal location, which is really nice.

> block Optional
Defines vertical alignment.
One of start, center, end, or nearest. Defaults to start.

> inline Optional
Defines horizontal alignment.
One of start, center, end, or nearest. Defaults to nearest.


Can I use is at 98.15%:
https://caniuse.com/?search=scrollIntoView

REFERENCE:
https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollIntoView
