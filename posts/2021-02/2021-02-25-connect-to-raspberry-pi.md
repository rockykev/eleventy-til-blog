---
title: TIL How to connect to your Raspberry pi (home network)
date: 2021-02-25
published: true
tags: ['raspberrypi', 'linux']
series: false
canonical_url: false
description: "Connecting to your Raspberry Pi"
layout: layouts/post.njk
---

Raspberry Pis are interesting. I never know what to do with them except for play classic video games or build adblocking tools.

What happens when you need to modify files?
For example:

* You need to update software
* You want to add new files (like games)
* You need to change settings

In order to modify the content on your Raspberry Pi, you can:

1) Pull out the MicroSD card, move it to your computer, and upload your files

2) Upload them via SSH or using a FTP client like FileZilla

The first method is fine. But it gets super annoying if you have to do it multiple times.

The second method, you can do from any computer.

## How to do it:
First, you need to find your Raspberry Pi's IP address:

On Windows:
`arp -a`

on Linux:
`nmap`

Using that command will help you find what devices are connected on your network.

How it looks on Windows:
![](2021-02-25-connect-to-raspberry-pi_0.png)

For example, Pi-hole has a admin portal that I can access. So I can dial right into it.


## To Connect into Pi-hole

```
ssh pi@192.168.x.xxx
```
password is `raspberrypi` or `raspberry` (depending on the version)

[How to connect to your Raspberry pi](https://www.raspberrypi.org/documentation/remote-access/ssh/windows10.md)


## Pi Hole
To Update Pi Hole:
if it's a pi-hole, `pihole -up` (https://discourse.pi-hole.net/t/how-do-i-update-pi-hole/249)

if it's a retro thing.
https://retropie.org.uk/docs/Transferring-Roms/

Protocol: SFTP
IP address: To find the IP address of your RetroPie, go into RetroPie options from the main menu, and select the last option Show IP address. You can also find this information from the terminal on retropie in the bash info or with the command ifconfig
Username: pi (default)
Password: raspberry (default)

Simply drop the files in the ~/RetroPie/roms/$CONSOLE folder, where $CONSOLE is the name of the target console, e.g. snes or arcade.
