---
title: TIL Web Performance with Javascript
date: 2023-02-10
published: true
tags: ["javascript", "webworker", "performance", "criticaljs", "image"]
series: false
canonical_url: false
description: "Overall, we should be loading the most important JS first to get the website working. Then load all your third-party scripts onto your web worker, which then responds with 'Success' once it loads, so you can then interact with it."
layout: layouts/post.njk
---

From [Everything you need to know about Web Performance (in 5 Minutes)](https://dev.to/vue-storefront/everything-you-need-to-know-about-web-performance-as-a-dev-in-5-minutes-450l?utm_source=pocket_reader)


## Web Performance Tip 1:

![](2023-02-10_0.png)

Overall, we should be loading the most important JS first to get the website working.

Then everything else can be loaded later.

## Web Performance Tip 2:

I've been thinking about Web Workers for a while.

Web Workers communicate to each other through messages.
So you can load all your third-party scripts onto your web worker, which then responds with 'Success' once it loads, so you can then interact with it.

![](2023-02-10_1.png)

