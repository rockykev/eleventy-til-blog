---
title: TIL How many Fucks there are in Linux
date: 2023-02-13
published: true
tags: ["comments", "git", "image"]
series: false
canonical_url: false
description: " As of today, in the Linux Repo, there are comments that contains the following mentions: 1,651 for hacks, 2,863 for workarounds, and 4,102 for fixme"
layout: layouts/post.njk
---

I saw this post ["Code with swearing is better code."](https://www.jwz.org/blog/2023/02/code-with-swearing-is-better-code/),

and a reddit commenter shared a sweet tool that measures the word count of specific words in the linux kernel.

Check the whole thing out at https://www.vidarholen.net/contents/wordcount/

![](2023-02-13-TODO_0.png)

## "Fuck"

* "Fuck" first showed up on Sept 17th, 1991
* The next "Fuck" would occur on June 6, 1995
* Peak "Fuck" would be July 7th, 2005 at 68 counts
* The great "Fuck Purge" was from Jan 31, 2018 (39) to March 5, 2019 (15).
* As of Today (Feb 2, 2023), we are steady at 13 "Fucks"

## Hack, Workaround, todo, and Fix me

![](2023-02-13-TODO_1.png)

The one made me laugh.

As of today:

* 1,651 hacks
* 2,863 workarounds
* 4,102 fixme



via [Open source code with swearing in the comments is statistically better than that without](https://www.reddit.com/r/programming/comments/110mj6p/open_source_code_with_swearing_in_the_comments_is/j8akdtv?context=3)
