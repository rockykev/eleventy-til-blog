---
title: TIL the history of fonts and COLRv1
date: 2023-02-23
published: true
tags: ["image", "browser", "font"]
series: false
canonical_url: false
description: "COLRv1 fonts, have a smaller footprint, vector-scalable, reposition-able, gradient-featuring, and blend-mode powered fonts that accept parameters to customize the font per use case or to match a brand."
layout: layouts/post.njk
---

Today I learned about COLRv1 Fonts! (and font-palette)

> Before COLRv1 fonts, the web had OT-SVG fonts, also an open format for fonts with gradients and built-in colors and effects. These could grow very large though, and while they allowed editing the text, there wasn't much scope for customization.

COLRv1 fonts, have a smaller footprint, vector-scalable, reposition-able, gradient-featuring, and blend-mode powered fonts that accept parameters to customize the font per use case or to match a brand.

Also look at that size!

![](2023-02-23_0.png)



How to use it:
```
// declare
@import url(https://fonts.googleapis.com/css2?family=Bungee+Spice);

// modify the palette
@font-palette-values --colorized {
  font-family: "Bungee Spice";
  base-palette: 0;
  override-colors: 0 hotpink, 1 cyan, 2 white;
}

// add the palette
.spicy {
  font-family: "Bungee Spice";
  font-palette: --colorized;
}

```

via State of CSS 2022 - https://web.dev/state-of-css-2022/
MDN https://developer.mozilla.org/en-US/docs/Web/CSS/font-palette
