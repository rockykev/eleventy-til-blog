---
title: TIL what DOCTYPE means
date: 2023-02-15
published: true
tags: ["moderndevelopment", "html"]
series: false
canonical_url: false
description: "tl;dr: DOCTYPE declaration in the first line of the HTML file, to instruct the browser to run the code in Standard mode."
layout: layouts/post.njk
---

Today I learned what the hell is `<!DOCTYPE html>` is.

tl;dr: DOCTYPE declaration in the first line of the HTML file, to instruct the browser to run the code in Standard mode.

Via this:

```html
<!DOCTYPE html> <!-- What the hell is this?? -->
<html lang="en">
<head>
    <title>Document</title>
</head>
<body>
</body>
</html
```

> Prior to HTML5 (for e.g. in HTML 4.01), all HTML documents would have to contain a “Doctype” declaration on its first line along with a reference to its type definition. This would look something like:

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
```

Enter HTML5!

> See, earlier versions of HTML (prior to HTML5) were SGML (Standard Generalized Markup Language) based, and therefore required a reference to DTDs. HTML5 onwards, this reference was no longer needed since this version of HTML is not SGML based.

> Now how do browsers identify which mode it needs to use? Well, just add a valid DOCTYPE declaration in the first line of the HTML file, to instruct the browser to run the code in Standard mode. Anything other than that will trigger the Quirks mode in IE9 or older. This is exactly what <!DOCTYPE html> does HTML5 onwards. If you fail to add this line to your HTML file, the browser would interpret this as an instruction to run your code in Quirks mode, and you could end up getting inconsistent results across different browsers.


via [What the hell is <!DOCTYPE html>?](https://dev.to/aman894/what-the-hell-is-doctype-html-32om)
