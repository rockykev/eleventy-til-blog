---
title: TIL Shape-Outside to wrap text
date: 2023-02-08
published: true
tags: ["css", "shapes", "codepen"]
series: false
canonical_url: false
description: "Shape-outside provides a way to customize this wrapping, making it possible to wrap text around complex objects rather than simple boxes."
layout: layouts/post.njk
---

Shape-outside is a CSS property that allows setting shapes.
It also helps define areas where text flows!

MDN: https://developer.mozilla.org/en-US/docs/Web/CSS/shape-outside

> Shape-outside provides a way to customize this wrapping, making it possible to wrap text around complex objects rather than simple boxes.


```css

.left,
.right {
  width: 40%;
  height: 12ex;
  background-color: lightgray;
}

.left {
  shape-outside: polygon(0 0, 100% 100%, 0 100%);
  float: left;
  clip-path: polygon(0 0, 100% 100%, 0 100%);
}

.right {
  shape-outside: polygon(100% 0, 100% 100%, 0 100%);
  float: right;
  clip-path: polygon(100% 0, 100% 100%, 0 100%);
}


```



<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="JjpBzGP" data-user="OMGZui" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/OMGZui/pen/JjpBzGP">
  shape-outside</a> by omgzui (<a href="https://codepen.io/OMGZui">@OMGZui</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>
