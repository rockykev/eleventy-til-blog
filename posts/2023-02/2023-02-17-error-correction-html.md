---
title: TIL how Error correction works in HTML
date: 2023-02-17
published: true
tags: ["error", "html", "table", "browser"]
series: false
canonical_url: false
description: "You never get an 'Invalid Syntax' error on an HTML page. Browsers fix any invalid content and go on."
layout: layouts/post.njk
---

[How browsers work](https://web.dev/howbrowserswork/)

HTML is super forgiving. Forget to close a tag, and HTML may still display it.

You never get an "Invalid Syntax" error on an HTML page. Browsers fix any invalid content and go on. [Browsers Error Tolerance](https://web.dev/howbrowserswork/#browsers-error-tolerance)

Note that the error handling is internal: it won't be presented to the user.

## Example 1 - Break
`</br>` instead of `<br>`

Some sites use `</br>` instead of `<br>`. In order to be compatible with IE and Firefox, WebKit treats this like `<br>`.

The code:

```
if (t->isCloseTag(brTag) && m_document->inCompatMode()) {
     reportError(MalformedBRError);
     t->beginTag = true;
}
```


### Example 2 - Stray Table

A stray table is a table inside another table, but not inside a table cell.

For example:

```html
<table>

  <table>
    <tr><td>inner table</td></tr>
  </table>

  <tr><td>outer table</td></tr>
</table>
```

WebKit will change the hierarchy to two sibling tables:
```html
<table>
  <tr><td>outer table</td></tr>
</table>
<table>
  <tr><td>inner table</td></tr>
</table>
```

The code:
```
if (m_inStrayTableContent && localName == tableTag)
        popBlock(tableTag);
```

WebKit uses a stack for the current element contents: it will pop the inner table out of the outer table stack. The tables will now be siblings.

There's a few other crazy error corrections. That includes CSS too!
