---
title: TIL ":has" selector
date: 2023-02-22
published: true
tags: ["css", "organization"]
series: false
canonical_url: false
description: "Before :has(), the subject of a selector was always at the end. Like 'ul > li:hover' or 'ul > li:not(.selected)'. Now we can do ul:has(> li)"
layout: layouts/post.njk
---

The `:has()` selector is so sweet!

Using this example:  `ul > li`

Before `:has()`, the subject of a selector was always at the end.

If you wanted to edit the `li` item, you would do:
`ul > li:hover` or `ul > li:not(.selected)`


After `:has()`:

`ul:has(> li)`

Some more examples:
`.parent:has(.child) {...}`

An example where a `<section>` element is the subject, but the selector only matches if one of the children has :focus-visible:

`section:has(*:focus-visible) {...}`

via [State of CSS: Has](https://web.dev/state-of-css-2022/#has)
