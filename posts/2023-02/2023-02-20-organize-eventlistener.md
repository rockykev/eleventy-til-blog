---
title: TIL ways to organize eventListeners
date: 2023-02-20
published: true
tags: ["cleancode", "naming"]
series: false
canonical_url: false
description: "Some code spaghetti I made recently was creating a lot of eventListeners for multiple buttons, and then forgot what they all connected to."
layout: layouts/post.njk
---


Some code spaghetti I made recently was creating a lot of eventListeners for multiple buttons, and then forgot what they all connected to.

How do I write cleaner code to avoid that?

## IDEA 1 - Prefixing

> To help prevent naming collisions, it’s a good idea to prefix custom events with your library or project name.

```js
// Namespaced to the Calculator library
emit('calculator-add');
```

## IDEA 2 - Colon

The one I really like is this:

> Another convention is to put a colon (:) between the library name and the event type. I’m personally quite fond of this approach, as I think it makes events more readable.

> I call this style prefix-kebab.

```js
// prefix-kebab naming
emit('calculator:add');
```

Via [Custom event naming conventions](https://gomakethings.com/custom-event-naming-conventions/)
