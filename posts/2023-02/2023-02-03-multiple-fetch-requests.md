---
title: TIL Multiple Fetch Requests
date: 2023-02-03
published: false
tags: ["javascript", "api"]
series: false
canonical_url: false
description: "I've been frequently dealing with this scenario in the past few weeks. I wanted to make multiple fetch requests, and then merge everything together when it's all done. To make multiple/parallel fetch requests:"
layout: layouts/post.njk
---

I've been frequently dealing with this scenario in the past few weeks.

I wanted to make multiple fetch requests, and then merge everything together when it's all done.

To make multiple/parallel fetch requests:


```js
async function fetchMoviesAndCategories() {
  const [moviesResponse, categoriesResponse] = await Promise.all([
    fetch('/movies'),
    fetch('/categories')
  ]);
  const movies = await moviesResponse.json();
  const categories = await categoriesResponse.json();
  return [movies, categories];
}
fetchMoviesAndCategories().then(([movies, categories]) => {
  movies;     // fetched movies
  categories; // fetched categories
}).catch(error => {
  // /movies or /categories request failed
});


```

via [How to Use Fetch with async/await](https://dmitripavlutin.com/javascript-fetch-async-await/#5-parallel-fetch-requests)
