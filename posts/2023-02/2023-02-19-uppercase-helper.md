---
title: TIL a helper to make things uppercase
date: 2023-02-19
published: true
tags: ["javascript", "helper", "string"]
series: false
canonical_url: false
description: "ucWords function converts the first character of each word in a string to uppercase. This function is the same as PHP’s ucWords function."
layout: layouts/post.njk
---


Needed a helper function to make things uppercase?

`ucWords`

> ucWords function converts the first character of each word in a string to uppercase. This function is the same as PHP’s ucWords function.

```js
export const ucWords = string => {
    return String(string).toLowerCase()
        .replace(/\b[a-z]/g, (l) => l.toUpperCase())
}
```

via [JavaScript String Helpers That I Use Every Day](https://medium.com/@parvej.code/javascript-string-helper-that-i-use-every-day-d16154e6279a)
