---
title: TIL why jpeg vs jpg
date: 2023-02-25
published: true
tags: ["image", "extension", "history"]
series: false
canonical_url: false
description: "In DOS systems, a filename extension was a different part of the name stored in a different field that can only be 3 characters. You still see this legacy today in Microsoft's .NET software, where most system calls that use the word 'filename' in their name don't really mean the whole filename. They mean just the part without the extension."
layout: layouts/post.njk
---


Sometimes you see files with `.jpeg` but then other times, you see it as `.jpg`.

Why is that?

Older OSes, like DOS couldn't handle 4-letter extensions.

It's not a problem today. But for backward-compatibly, the 3-letter system stuck.

via [Dunbaratu on reddit](https://www.reddit.com/r/explainlikeimfive/comments/12a4ibc/eli5_why_do_jpg_and_jpeg_both_exist/jeqnm9h?context=3):

> In UNIX and Mac systems, a filename extension meant nothing and in fact wasn't even really a thing. You could place a period in a filename if you felt like it but the system didn't see it as meaning anything special. As far as the OS was concerned, a filename like abc.def is just a 7 character filename where the third character happens to be a period for some reason. The def wasn't even stored in a separate field.

> In DOS systems, a filename extension was a different part of the name stored in a different field that can only be 3 characters. You still see this legacy today in Microsoft's .NET software, where most system calls that use the word "filename" in their name don't really mean the whole filename. They mean just the part without the extension.

Via [zero_z77 on Reddit](https://www.reddit.com/user/zero_z77/)

> When microsoft office 2007 came out, they changed the format for office files from a proprietary binary format, to an xml based format. To distinguish these files from legacy office files, an 'x' was added to the file extension. So .doc became .docx, .xls became .xlsx, .ppt became .pptx, and so on. They also did this when asp.net (.aspx) was introduced to distinguish it from classic asp (.asp).


Via [Santacroce](https://www.reddit.com/r/explainlikeimfive/comments/12a4ibc/comment/jeselb9/?utm_source=reddit&utm_medium=web2x&context=3)

There are plenty of answers as to why now, but it's actually not that rare of a thing. There is also:

* .htm and .html
* .mpg and .mpeg
* .mid and .midi
* .tif and .tiff

and also .mp4 and .mpeg4
