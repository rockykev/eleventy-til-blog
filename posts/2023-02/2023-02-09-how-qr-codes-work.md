---
title: TIL how QR codes work
date: 2023-02-09
published: true
tags: ["barcode", "qr", "image"]
series: false
canonical_url: false
description: "The Quick Response code was invented by a subsidiary of Toyota to track parts across the manufacturing process."
layout: layouts/post.njk
---

> The Quick Response code was invented by a subsidiary of Toyota to track parts across the manufacturing process.

How QR codes work:

The full post is fantastic. [QR codes](https://typefully.com/DanHollick/qr-codes-T7tLlNi)

## Finder Patterns

![](2023-02-09_0.png)


## Timing Patterns

This row helps them know how large the QR code is.

> It's stored twice so its readable even when QR code is partially obscured. (You'll notice that this is a recurring theme.)

![](2023-02-09_1.png)


## Error Correction

![](2023-02-09_2.png)

## The Data

![](2023-02-09_3.png)

![](2023-02-09_4.png)
