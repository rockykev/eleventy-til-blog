---
title: TIL Container Queries are live!
date: 2023-02-24
published: true
tags: ["container", "css"]
series: false
canonical_url: false
description: "After @container, elements can respond to a parent container size or style! The only caveat is the containers must declare themselves as possible query targets, which is a small requirement for a large benefit."
layout: layouts/post.njk
---

via State of CSS 2022 - https://web.dev/state-of-css-2022/

Container queries are live!

> Before` @container`, elements of a webpage could only respond to the size of the whole viewport. This is great for macro layouts, but for micro layouts, where their outer container isn't the whole viewport, it's impossible for the layout to adjust accordingly.

> After `@container`, elements can respond to a parent container size or style! The only caveat is the containers must declare themselves as possible query targets, which is a small requirement for a large benefit.


```css
@container sidebar (width > 400px) {
  /* <stylesheet> */
}

.day {
    container-type: inline-size;
    container-name: calendar-day;
}

or
.day {
  container: sidebar / inline-size;
}
```

Then you declare:
```


MDN: https://developer.mozilla.org/en-US/docs/Web/CSS/@container
