---
title: TIL keywords in package.json
date: 2023-02-07
published: true
tags: ["node", "keywords"]
series: false
canonical_url: false
description: "Today I learned what keywords are in a package.json file! It's a collection of keywords about a module. Keywords can help identify a package, related modules and software, and concepts."
layout: layouts/post.njk
---

Today I learned what keywords are in a `package.json` file!

It's a collection of keywords about a module. Keywords can help identify a package, related modules and software, and concepts.

```json
    "keywords": [
        "metaverse",
        "virtual reality",
        "augmented reality",
        "snow crash"
    ]
```

[The Basics of Package.json in Node.js and npm](https://nodesource.com/blog/the-basics-of-package-json-in-node-js-and-npm)
