---
title: TIL detecting if a user is online with JS
date: 2023-02-28
published: true
tags: ["fingerprint", "javascript", "mdn"]
series: false
canonical_url: false
description: "You can use the navigator.onLine api."
layout: layouts/post.njk
---

Today I learned how to detecting if the user is online with JavaScript

```js
if (navigator.onLine) {
  console.log("online");
} else {
  console.log("offline");
}


window.addEventListener("offline", (e) => {
  console.log("offline");
});

window.addEventListener("online", (e) => {
  console.log("online");
});

```

Edge-cases:

> In Chrome and Safari, if the browser is not able to connect to a local area network (LAN) or a router, it is offline; all other conditions return true. So while you can assume that the browser is offline when it returns a false value, you cannot assume that a true value necessarily means that the browser can access the internet. You could be getting false positives, such as in cases where the computer is running a virtualization software that has virtual ethernet adapters that are always "connected." Therefore, if you really want to determine the online status of the browser, you should develop additional means for checking.

> In Firefox, switching the browser to offline mode sends a false value. Until Firefox 41, all other conditions returned a true value; testing actual behavior on Nightly 68 on Windows shows that it only looks for LAN connection like Chrome and Safari giving false positives.

[MDN Navigator: onLine](https://developer.mozilla.org/en-US/docs/Web/API/Navigator/onLine)

via [Detecting if the user is online with JavaScript](https://dev.to/dailydevtips1/detecting-if-the-user-is-online-with-javascript-58ne)


I like this comment via [Uploadcare](https://dev.to/uploadcare/comment/23m2a)

> There are better ways to detect offline:

1. Long-pooling request to the server.
2. Periodical check for a dummy image/resource without caching.
3. Showing such a status only when the user's request fails due to unstable network.
