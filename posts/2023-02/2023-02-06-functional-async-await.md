---
title: TIL functional async/await
date: 2023-02-06
published: true
tags: ["promises", "javascript", "node"]
series: false
canonical_url: false
description: PLACEHOLDER
layout: layouts/post.njk
---

I struggle with getting async/await written correctly.

This is a simple example of async/await used to run a function asynchronously:

```js
const doSomethingAsync = () => {
  return new Promise((resolve) => {
    setTimeout(() => resolve('I did something'), 3000)
  })
}

const doSomething = async () => {
  console.log(await doSomethingAsync())
}

console.log('Before');
doSomething();
console.log('After');
```


The above code will print the following to the browser console:

```
Before
After
I did something // after 3s
```

So the line of thinking is:

1. We hit the console.log('Before');
2. We fire off the `doSomething` function, which is going to return a Promise with a setTimeout.
3. We hit the console.log('After');
4. We finally get the data back from `doSomething`, as it resolves it's Promise.

via [The definitive Node.js handbook – Learn Node for Beginners](https://www.freecodecamp.org/news/the-definitive-node-js-handbook-6912378afc6e?utm_source=pocket_mylist)

