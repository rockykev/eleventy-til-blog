---
title: TIL individual transform CSS
date: 2023-02-11
published: true
tags: ["transform", "css"]
series: false
canonical_url: false
description: "We now have individual transform CSS!"
layout: layouts/post.njk
---

We now have individual transform CSS!

Currently, in order to apply `transforms`, you do something like this:

```css
div {
  /* These transform functions are applied from left to right. */

  transform: translate(50%, 0) scale(1.5);
}

.square {
  /* scale -> translate */
  transform: scale(1.5) translate(50%, 0);
}
```
By moving these into individual transforms, they look like this and follow a execution order.
```css
div {
  /* Follows a pattern: translate -> rotate -> scale */
  translate: 50% 0;
  scale: 1.5;
}

.square {
  /* translate -> scale */
  translate: 50% 0;
  scale: 1.5;
}
```


To ensure support, you would use:

```css
@supports (translate: 0) {
  /* Use `translate` */
}

@supports not (translate: 0) {
  /* Use `transform: translate()` */
}
```

via [Order in CSS transformations – transform functions vs individual transforms](https://www.stefanjudis.com/blog/order-in-css-transformation-transform-functions-vs-individual-transforms)
