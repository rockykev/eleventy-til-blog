---
title: TIL prioritizing Javascript
date: 2023-02-27
published: true
tags: ["performance", "javascript", "corewebvitals"]
series: false
canonical_url: false
description: "... async and defer attributes should be used where possible to avoid render blocking of the DOM. Render blocking is when the browser must halt all rendering of the page in order to process a resource that the page depends on."
layout: layouts/post.njk
---

Today I learned about the details about prioritizing Javascript, and the whole 'Put Javascript in the `<head></head>`

## JavaScript in the `<head>`

> An old and often-touted best practice for performance has been to load your JavaScript in the footer of the document to avoid render blocking of scripts and to ensure the DOM is constructed before your scripts have a chance to run.

> In recent times, however, it has been more commonplace in certain architectures to place `<script>` elements in the document `<head>`.

Instead, we should be using `async` for that: https://web.dev/efficiently-load-third-party-javascript/#use-async-or-defer

Why?

> ... async and defer attributes should be used where possible to avoid render blocking of the DOM. Render blocking is when the browser must halt all rendering of the page in order to process a resource that the page depends on.

> We found that 77% of mobile pages have at least one render-blocking script in the document `<head>`, whereas 79% of desktop pages do this. This is a concerning trend, because when scripts block rendering, page content is not painted as quickly as it could be.

I like this alternative:

> Where possible, render-critical JavaScript can be placed in the footer and preloaded so the browser can get a head start on requesting those resources. Either way, the state of render-blocking JavaScript in tandem with how much JavaScript we ship is not good, and web developers should make more of an effort to curb these issues.


## Injected scripts

> Script injection is a pattern where an HTMLScriptElement is created in JavaScript using document.createElement and injected into the DOM with a DOM insertion method. Alternatively, `<script>` element markup in a string can be injected into the DOM via the innerHTML method.

First, yikes. It looks like this.

```html
	<h1>Injecting Bootstrap CDN link with innerHTML</h1>
	<script>
		window.onload = function() {
			var targetElement = document.getElementById("target");
			targetElement.innerHTML = '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">';
		}
	</script>
```

Second, this:

> Script injection is a fairly common practice used in a number of scenarios, but the problem with it is that it defeats the browser’s preload scanner by making the script undiscoverable as the initial HTML payload is parsed.

> This can affect metrics such as Largest Contentful Paint (LCP) if the injected script resource is ultimately responsible for rendering markup, which itself can kick off long tasks to parse large chunks of markup on the fly.


via [Part 1 Chapter 2 JavaScript Web Almanac](https://almanac.httparchive.org/en/2022/javascript)
