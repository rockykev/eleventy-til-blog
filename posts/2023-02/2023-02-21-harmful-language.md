---
title: TIL Harmful language
date: 2023-02-21
published: true
tags: ["words", "speaking"]
series: false
canonical_url: false
description: "We’ve talked before, we’ve used that phrase “words matter” because words matter, words are the way we communicate with one another, it is how we perceive our self value in our own head."
layout: layouts/post.njk
---


I was listening to this excellent podcast episode [Harmful Language You May Be Using](https://www.underrepresentedintech.com/harmful-language-you-may-be-using/) with Michelle Frechette and Allie Nimmons.

> We’ve talked before, we’ve used that phrase “words matter” because words matter, words are the way we communicate with one another, it is how we perceive our self value in our own head.

Some are pretty straight-forward.

* whitelist means to allow, and blacklist means to not allow
* Master/Slave, in relations to Hard Drives.
* Black Hat (bad actors) vs White Hat (good actors)

Here's some that have a questionable history!

And as usual - dude... this is a blog. Let's call a spade a spade (which funny enough, is on the list), these are words that have a origin that if they make you feel a funny way, find a better word and convince others of the same.


## Grandfather Clause

> rules, put in place after the civil war here in America, by the Southern states... was to block African Americans or ex-slaves from voting while exempting white voters from taking literacy tests and paying poll taxes required to vote.

> It was you grandfathered in the white people. So the white people who always had the right to vote, whether or not they had literacy, they were grandfathered in because their grandfather could vote. And it was a way to gate keep out any black and African American people from being able to vote because they didn’t have grandfathers here that did that.

## Peanut Gallery

> Peanut gallery often refers to the people who are observing and laughing. We would say, “Hey, let’s ask the peanut gallery,” meaning all the people who aren’t the principles on stage that also has racist roots.

> So it originates from late 1800s vaudeville area of theater. It was usually the cheapest section of seats usually occupied by people with limited means, which can be racial minorities.


## Cakewalk

> So the cakewalk was actually an event. It was a pre-Civil war, so during the time of slavery, everything before civil war in America is slavery, it was a pre-Civil war dance originally performed by slaves on plantation grounds. It’s a uniquely American thing. First known as the prize walk and the prize was an elaborately decorated cake. I do remember reading a little bit about this and I believe it was sort of like, “Oh, it’s a nice thing for the masters to do for the slaves. You get a prize, you get some delicious cake.” But it was also very much for their amusement to just make the slaves dance for them.

## Spirit Animal

Oftentimes we’ll hear people say, “Oh my spirit animal is this or my spirit animal is that,” just in an offhanded kind of way. But, again, that is downplaying. What is truly a spiritual experience for people who are native Americans and who have that as part of their culture.

## Spade

I'm guilty of this. :-(

> Call spade a spade one is actually kind of complex. That phrase has a really long history going back to the Greeks. It changes throughout and so the more racist history with that begins in the late 1920s. And spade was code for a black person. Eventually the phrase, “Black as the ace of spades,” also became widely used.

> It was just another way to isolate and to identify, “This person is different, this person is black, they’re as black as a spade, as black as a shovel.” Spade lit was used to refer to black babies, black children. And at a certain time we did try to reclaim it sort of like the N word.


## No Can do

>  If I were to speak Spanish, I would put words in different order than we would hear in the United States. And also in other languages, you can drop the word I. So I could just say, “Estoy consado.” I’m tired. But I didn’t say, “Yo estoy cansado.”

 > ...Chinese-Americans into the railroad building and the gold rush culture of California is where a lot of this came from... people would say "no can do" which would be a mocking of how Chinese people translated directly into English from the way that their language is formatted.
