---
title: TIL Find Command
date: 2023-02-04
published: true
tags: ["search", "terminal", "commands"]
series: false
canonical_url: false
description: "I wanted to look for files with a specific extension. You can do that with ls *.(mp3|exe|mp4)"
layout: layouts/post.njk
---

I wanted to look for files with a specific extension.

```bash

## top answer
ls *.{mp3,exe,mp4} ## will throw a error if any of them don't have a result
ls *.(mp3|exe|mp4) ## Return all files that match, even if there's 0 results


## alternate
ls *.mp4 *.mp3 *.exe
```

The `*` means any
Then the brackets of elements


Via https://stackoverflow.com/a/1447974/4096078
