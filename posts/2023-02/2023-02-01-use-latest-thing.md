---
title: TIL wanting to use the latest thing
date: 2023-02-01
published: true
tags: ["mdn", "latest", "jquery", "github"]
series: false
canonical_url: false
description: "In evaluating a solution, be sure to consider the library dying out or becoming a maintenance burden after 5 years (e.g., everyone trying to get off jQuery onto Web Components)."
layout: layouts/post.njk
---

I like this because whenever we (and maybe I'm projecting) learn a new thing that looks great, we want to use it immediately.

David Flanagan, the author of O'Reilly's JavaScript: The Definite Guide, made this story request for the MDN docs.

> As an MDN developer, I want to rewrite the MDN page header using React, so I can gain experience using React for a non-trivial page component, and so that I can reduce our the dependencies on jquery.

Let's address some of the language:

What -  Wants to rewrite the MDN page header using React
Why - SO ge can gain experience using React and reduce dependencies on jQuery.

Vanilla JS has adopted all the features of jQuery. So the second part is a good idea. But there's so much more packed in. And WHY react?

The top answers that really address it:

> Just my 2c, as someone involved with browsers and standards for the last ~13 years. Libraries come and go, and generally all libraries/frameworks have a lifespan of about 5 years (there was some data published by GitHub about this, I believe, that made a pretty compelling case). In evaluating a solution, be sure to consider the library dying out or becoming a maintenance burden after 5 years (e.g., everyone trying to get off jQuery onto Web Components).

> Before choosing React, or any library, please consider if the MDN project will be around in 10-20 years (I believe it will be) - so these technology choices REALLY matter. Please choose wisely.

via [marcoscaceres](https://github.com/marcoscaceres)

> As representatives of Mozilla, standing for and promoting the use of open web standards, we should be more diligent about our choice of technology. It comes under increased scrutiny and it sets an example to the people we intend to educate on the importance of open web standards.

> Regardless of the technology used now, it will be eventually rewritten in the future. Frameworks come and go. Being closer to the web platform matters more in the long term.

via [oslego](https://github.com/oslego)

> @gaearon I don't think any of the above is criticising React or is necessarily misinformed. It's simply pointing out that it isn't required in this instance. It's like using a steel-mill hammer to knock a thumb-tack into putty. Sure, it'll work – and it might be the most poweful hammer in the world – but it's overkill.

> Sites like MDN are a standard bearer for technologies that are used on content-focused websites like itself. If we start adding React for reasons related exclusively to the development experience, then we risk sending the message people should be using React – or other similar tools that are intended to solve problems complex web apps suffer from – for other content-focused websites.

via [benfurfie](https://github.com/benfurfie)

https://github.com/mdn/sprints/issues/967?utm_source=pocket_mylist
