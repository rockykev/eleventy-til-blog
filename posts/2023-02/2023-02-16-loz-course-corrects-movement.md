---
title: TIL Legend of Zelda course corrects movement
date: 2023-02-16
published: true
tags: ["movement", "videogames", "pixel"]
series: false
canonical_url: false
description: "If you play The Legend of Zelda for a while, you’ll notice that Link doesn’t ever get hung up on obstacles due to unexpected collisions, nor do you ever just miss a bad guy by a pixel or two when attacking him."
layout: layouts/post.njk
---


Today I learned that Legend of Zelda's movements course corrects!

> If you play The Legend of Zelda for a while, you’ll notice that Link doesn’t ever get hung up on obstacles due to unexpected collisions, nor do you ever just miss a bad guy by a pixel or two when attacking him.

> Yet at the same time, you’ll notice that Link moves fluidly through the gameworld; even tough the environment is tile-based, you can tap the directional pad and link will move a single pixel in that direction. If you’re moving to the left and decide to go up, down or right, Link instantly moves in that other direction when you change directions on the gamepad

![](2023-02-16-link-walking.gif)

via [gfycat](https://gfycat.com/ko/giftedlightheartedibisbill)

It's a very smooth movement.

How does that work?

What’s happening here is a very neat trick. While Link can move a single pixel at a time, in any direction, the longer he continously moves in any direction the more he gravitates toward aligning himself with the underlying grid of the screen.

* The tile grid for LoZ is 16 tiles wide by 14 tiles high (including 3 tiles for the status display at the top of the screen).
* Each tile is 16×16 pixels.
* Link operates on a half-tile grid, though (32×28 tiles, 8×8 pixels each).

As Link moves, if he’s not currently aligned with the half-tile grid, he is adjusted, one pixel at a time, toward the closest correction. As a result, if Link is 4 pixels off alignment he’ll line back up with the grid after moving 4 pixels.

What does this correction do for you?

> The correction prevents the subtle but annoying problem wherein the player would “snag” on the corners of objects that he anticipated passing by. The more the player moves contiously the more aligned Link becomes, which has the same effect as speed-sensitive steering in a car.

> The correction also has benefits for attacking as well. When Link attacks (with his sword, for example), the “kill zone” lies in the tile(s) immediately along his facing direction. Since enemies align along a similar half-grid as Link, the correction serves to line up Link with his enemies (as opposed to missing the enemy by a few frustrating pixels).

via [Troy Gilbert Movement Mechanics](https://web.archive.org/web/20230408211200/https://troygilbert.com/deconstructing-zelda/movement-mechanics/)
