---
title: TIL how Levels.fyi used Google Sheets
date: 2023-02-14
published: true
tags: ["database", "google", "quickanddirty", "image"]
series: false
canonical_url: false
description: " Our philosophy to scaling is simple, avoid premature optimization."
layout: layouts/post.njk
---

I love finding creative workarounds to complex problems.

Via [How Levels.fyi scaled to millions of users with Google Sheets as a backend](https://www.levels.fyi/blog/scaling-to-millions-with-google-sheets.html)

This quote:

> Our philosophy to scaling is simple, avoid premature optimization

How does it work?

## Write Flow:

**Version 0**

> V0 had no UI, add salary form was a Google Forms UI 😱

LOL, so literally a embedded Google Form. Hell yeah.

![](2023-02-14-TODO_0.png)

**Version 1**

> Our own UI with necessary validations on the frontend

> 1. Frontend calls publicly visible API Gateway
2. API Gateway triggers and spawns a lambda function
3. Lambda function processes and appends new salary to the sheet

Form does a post request, which sends it to a serverless function.
Then the serverless function adds that data to that Google Sheet.

I LOVE IT.

![](2023-02-14-TODO_1.png)

## Read Flow

How does data show up on their website?

Remember that their 'database' is Google Sheets. So they had to do something creative to generate that data.

> Our recipe for building a read flow was as follows:

> 1. Process data from Google Sheet and create a JSON file
2. Use AWS Lambda for processing and creating new JSON files
3. Upsert JSON files on S3
4. Cache JSON files using a CDN like AWS Cloudfront

![](2023-02-14-TODO_2.png)

Btw, upsert is short for `Update` and `Insert`. No I never heard of it until today.

So really, it's literally just turning the Google Sheets into a JSON file using serverless functions, upload it to S3, then use a CDN to handle server load.



