---
title: TIL Forever Seinfeld
date: 2023-02-02
published: true
tags: ["fun", "ai"]
series: false
canonical_url: false
description: " Nothing, Forever is a parody of '90s sitcoms, done in the style of '90s point-and-click PC games (but, you know, in 3D). We set out to build something weird, new, and novel, and this is what we ended up with."
layout: layouts/post.njk
---

Over the past few days, I've been obsessed with NothingForever, using AI to generate episodes of Seinfeld, non-stop, for 24/7.

What is it?

Via the creators: Hey everybody, we're finally launching our always-on, generative show -- Nothing, Forever -- streaming on Twitch. Nothing, Forever is a parody of '90s sitcoms, done in the style of '90s point-and-click PC games (but, you know, in 3D). We set out to build something weird, new, and novel, and this is what we ended up with.
Aside from the artwork and the laugh track you'll hear, everything else is generative, including: dialogue, speech, direction (camera cuts, character focus, shot length, scene length, etc), character movement, and music.


https://www.twitch.tv/watchmeforever

Not certain if it's still alive for the future users.

## How was it made?

> AI-generated (aside from the artwork) parody of '90s sitcoms, running forever (24/7/365). We worked on this w/ a very small team for the past four years, in-between our day jobs. When started, OpenAI didn't have an API, and Stable Diffusion definitely wasn't a thing, so we had to come up with novel methods to thread cohesive content together. Most of the "creative" details e.g., laugh track, dialogue, frequency of dialogue, camera shots, and so on, are all tunable on a per scene basis.

> We're in sort of a holding pattern right now -- no clear path to monetization for the project, and it hasn't garnered enough attention for us to probably get funding based on the technology backbone. Hope you enjoy it! Labor of love. :)

via [hyperhopper](https://www.reddit.com/r/videos/comments/10rhjwg/nothingforever_an_infinite_ai_generated_episode/j6wav6o?context=3)



>  Nothing, Forever is built using a combination of machine learning, generative algorithms (we use 'generative' here in a non-academic sense), and cloud services. Our stack is mostly comprised of Python + TensorFlow for our ML models, TypeScript + Azure Functions and Heroku for our backend, and C# + Unity for the client, with some neural voice APIs thrown into the mix.

Heading into the future, we plan to leverage OpenAI's davinci models for our dialogue -- we actually have an integration already, but the cost is too prohibitive to run full time -- as well as leveraging Stable Diffusion for art generation.

via [HackerNews Comment](https://news.ycombinator.com/item?id=33833645)


