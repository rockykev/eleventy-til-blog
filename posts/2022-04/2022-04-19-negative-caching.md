---
title: TIL about Negative Caching
date: 2022-04-19
published: true
tags: ['dns', 'sandbox', 'stackoverflow', 'servers']
series: false
canonical_url: false
description: "If you're ever working on a site and get a 'Server Not Found', then fix it (so that it totally works), and you STILL get a 'Server Not Found', that's negative catching!"
layout: layouts/post.njk
---


If you're ever working on a site and get a "Server Not Found", then fix it (so that it totally works), and you STILL get a "Server Not Found"?

That's called Negative caching!

How long does it live?

> The TTL for negative caching is not arbitrary. It is taken from the SOA record at the top of the zone to which the requested record would have belonged, had it existed.

via [This StackExchange answer](https://serverfault.com/a/426810/956135)


I learned about this on this sweet DNS sandbox [https://messwithdns.net/](https://messwithdns.net/)

I hope this site still works. I saw things changing from even a year ago.


Weird Experiment #1:

Did you know that just visiting a domain can make it not work? This is weird but true! In this experiment, we'll:

Create a DNS record for a subdomain, and then visit it after (this will work!)

Visit a subdomain, and create the DNS record for it, and then visit it again (this won't work!)

Let's go!

1. Create a CNAME record with
```
Name: visit-after
Type: CNAME
Target: orange.jvns.ca
TTL: 3600
```

2. Go to visit-after.shark98.messwithdns.com in your browser. You should see an orange site -- everything works!

3. Now, go to visit-before.shark98.messwithdns.com in your browser. You should get an error like "Server Not Found", since we haven't created the record yet.

4. Create a CNAME record with:
```
Name: visit-before
Type: CNAME
Target: orange.jvns.ca
TTL: 3600
```

With visit-before, it doesn't work because of something DNS resolvers do called negative caching, where they cache the absence of a record.

* Browser: where's visit-before.shark98.messwithdns.com?
* DNS server: doesn't exist!
* DNS resolver: "oops it doesn't exist! Better cache that!"
* DNS resolver: Doesn't exist

You create record
* Request: where's visit-before.shark98.messwithdns.com?
* DNS resolver: "I saw this before!! It doesn't exist!"
* DNS resolver: doesn't exist
* You: :( :( :( IT EXISTS THOUGH
* You wait 1 hour and try again, everything works because the cache expired


Not all DNS resolvers do negative DNS caching! For example, Cloudflare doesn't, so this experiment won't work for you if you're using Cloudflare DNS!

Here's the full instructions in case the site changed: 2022-04-19-negative-caching_0.png
