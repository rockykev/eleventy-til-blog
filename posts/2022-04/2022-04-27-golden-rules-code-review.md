---
title: TIL of the Golden Rules of Code Review
date: 2022-04-27
published: true
tags: ['programming', 'teamwork']
series: false
canonical_url: false
description: "Just read it."
layout: layouts/post.njk
---

1. Remember that there is a human being on the other end of the review.

2. Never tell someone that the code needs to be fixed without giving suggestions or recommendations on what to fix or how to fix it.

3. Assume good intent.

4. Clarify the action and the level of importance.

5. Don't forget that code feedback – and all feedback – includes praise.

[Code Reviews 101 - The Basics](https://www.semasoftware.com/blog/code-reviews-101-the-basics#the-golden-rules-of-code-reviews)
