---
title: TIL about Cypress Custom Commands
date: 2022-04-13
published: true
tags: ['testing', 'cypress']
series: false
canonical_url: false
description: "Today I learned adding custom commands to Cypress to abstract common patterns, using the Cypress.Commands.add()"
layout: layouts/post.njk
---

I really like Cypress as a testing suite. It's really pwowerful and has a lot of features.

Today I learned adding custom commands to Cypress to abstract common patterns:

```js
Cypress.Commands.add('login', () => {
    cy.visit('/login');
    cy.get('input[name=email]').type('foo@bar.com');
    cy.get('input[name=password]').type('password');
    cy.get('form').submit();
});

// Anywhere else
cy.login();

```

via [Cypress or how I learned to stop worrying and love E2E](https://madewithlove.com/blog/software-engineering/cypress-or-how-i-learned-to-stop-worrying-and-love-e2e/)
