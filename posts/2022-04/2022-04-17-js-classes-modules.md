---
title: TIL about switching out of JS Classes and into JS Modules
date: 2022-04-17
published: true
tags: ['javascript', 'oop', 'classes', 'advanced']
series: false
canonical_url: false
description: "A classes are just blueprints for creating objects. A class encapsulates data and functions that manipulate data. A module is a file that exports features, such as variables, objects, and functions, that can be used elsewhere."
layout: layouts/post.njk
---

## What is a class?

A classes are just blueprints for creating objects. A class encapsulates data and functions that manipulate data.

In other programming languages such as Java and C#, classes allow developers to create a parent-child, class based inheritance.

JavaScript classes are syntactic sugar over the prototypal inheritance. In other words, ES6 classes are just special functions.

[Explore the MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes)


## Prototypal Inheritance?

When we say `prototypal inheritance`, we're referring to that hidden property known as `__proto__`, or Dunder Proto (short for double underline prototype).

This guy:
![](2022-04-17-js-classes-modules_0.png)

The object arr (and basically every object you create in JS), has the [[Prototype]]:Array property, inside which lies `__proto__`. If we expand this `[[Prototype]]: Array` property in our example, we see a huge list of methods like every, forEach, map, splice, etc.

The point to be noted here is that each object we create has a different set of key-value pairs in the `__proto__` property.

Whenever we try to call/access a property that does not exist in the defined object, the JS engine goes down the `__proto__` chain (or a rabbit 🐇 hole), to search for that property. In the above case, we tried to compute the `map()` method on an array (which is an object), and it went down the `__proto__` chain to look for the same.

[ref](https://ivinoop.hashnode.dev/oops-in-javascript-what-is-dunder-proto)

## So why does that matter?

Because that's not inheritance. That's not a parent-child relationship. And that's not how classes is supposed to work!

And since it's all syntactic sugar, why not just write it the Javascript way without the class mumbo jumbo?

Well - there's still a lot of benefits with JS Classes.

* You can still bundle your data/function/objects together.
* There's still some benefits like scope & namespace.
* It does a lot of heavy lifting behind the scenes.

This [SO post does a good job of comparing the code](https://stackoverflow.com/a/30783368/4096078)

When I need to create a lot of instances of a object, I like reaching for a JS Class!

But whenever I see people use it, often the class is instatiated once. Of which I ask: Why use a class? There are better patterns out there for code organization!

> When deciding between what patterns to use, the team should pick the pattern that best addresses the problem being solved in the language being used. But if the OOP pattern of classes is the only way you know how to or like to code, then of course there will be times when using it will make the code harder for others to understand because the pattern is not a natural fit for the problem and/or programming language.

via [Stop Using JavaScript Classes!](https://medium.com/giant-machines/stop-using-javascript-classes-d0b6890ef097)

## What about JS Modules

So what got me on my rant in the first place.

My usage of JS Classes is minimum - I just don't have that many use-cases that I can't write a cleaner version using functions.

In JavaScript, a module is a file that exports features, such as variables, objects, and functions, that can be used elsewhere. They are defined using the export syntax, and code within the module will have scope access to all variables declared in the file in which it was defined. The exported features can then be imported into any other file or module. But code outside of the module will not have access to code in the module that hasn’t been exported.


```js
// some js file
const a = 1;
const b = 2;
export function add() {
 return a + b;
}

// some other js file that imports the add() function
import { add } from '/some-js-file.js';
add(); // 3
```

Suddenly, you can bundle your data, you have scope, and you can instatiate many instances of this. And it looks just like regular code! (because it is!)

You don't even need the `new` keyword!
```
// old: db created or imported here and passed into Class
const library = new Library(db);
// new: db created or imported here and passed into Module
const library = createLibrary(db);
```

So... bye JS Classes.

[Stop Using JavaScript Classes!](https://medium.com/giant-machines/stop-using-javascript-classes-d0b6890ef097)
