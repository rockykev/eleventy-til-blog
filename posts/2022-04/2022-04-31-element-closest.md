---
title: TIL about the element.closest() method
date: 2022-04-31
published: true
tags: ['javascript', 'codepen', 'eventlisteners']
series: false
canonical_url: false
description: "Using a single event listener is super efficient, but has edge-cases. So using closest instead of match."
layout: layouts/post.njk
---

You create a button and the event listener for that button:

```html
<button class="click-me">
	<span class="text-large">Click Me!</span>
	<br>
	<span class="text-small">(limited time offer)</span>
</button>

<script>
document.addEventListener('click', function (event) {

	// Only run on .click-me buttons
	if (!event.target.matches('.click-me')) return;

	// Do stuff...

});
</script>
```

Right now, it LOOKS like the code will work. If the button has a `.click-me`, then it should work, right?

Well, your click events are ACTUALLY happening on those `span` elements.

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="ZEvQQdd" data-user="cferdinandi" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/cferdinandi/pen/ZEvQQdd">
  Event Delegation</a> by Chris Ferdinandi (<a href="https://codepen.io/cferdinandi">@cferdinandi</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

To get around that, we use the `element.closest()` trick.

```js
document.addEventListener('click', function (event) {

	// Only run on .click-me buttons
	if (!event.target.closest('.click-me')) return;

	// Do stuff...

});
```
Our click events that happen in `span` now check if the nearest element contains `.click-me`, which is TRUE!

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="QWayyeJ" data-user="cferdinandi" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/cferdinandi/pen/QWayyeJ">
  Event Delegation - Fixed</a> by Chris Ferdinandi (<a href="https://codepen.io/cferdinandi">@cferdinandi</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


via [Climbing up the DOM tree with vanilla JavaScript](https://gomakethings.com/climbing-up-the-dom-tree-with-vanilla-javascript/)
