---
title: TIL Ethernet wire colors
date: 2022-04-04
published: true
tags: ['network', 'certifications']
series: false
canonical_url: false
description: "Ethernet wires are more than just 'a cord'. It's 8 tiny wires."
layout: layouts/post.njk
---

Ethernet wires are more than just 'a cord'.

## Ethernet Adapter Type:
There's a Type A & Type B.

NOTE: Many devices now have Auto MDI-X, which will automatically fixes A->B if needed.

Pin Types:
![](2022-04-04-ethernet-wires_0.png)
via https://www.startech.com/en-us/faq/network-cables-a-vs-b-pinout

A easy way to remember:

1. It's mixed, solid, mixed, solid... (so white/green, green)
2. At mixed, it's always starts with white.
3. It's easier to remember all the colors, then remember the mixed.

T568A:
Green Gremlins Or Blue Bats Or Brown Bears.

green
green
orange
blue
blue
orange
brown
brown


white/green
green
white/orange
blue
white/blue
orange
white/brown
brown


T568B:
Odor On Goats bring back grizzly Brown Bears.

white/orange
orange
white/green
blue
white/blue
green
white/brown
brown

orange
orange
green
blue
blue
green
brown
brown



## Ethernet Cord Type:
patch cable (or straight-through cable)
Default

How to determine which one to use?
A basic rule of thumb is: If it's the same device, you'll need a crossover cable.



## Ethernet wire categories:
They all use the RJ45 connectors, except Cat7.

Cat5e: 1Gigabit
Cat6: 10Gigabit
Cat6a: 10Gigabit
Cat7: 10Gigabit. Different type of connector, and not IEEE approved.
Cat8: 25-40Gigabits.

## Key terms

Open - There's no connection between two ends of a cable or wire. Like if a wire is cut in half.

Short - It's when current is flowing to a place where it's not supposed to. Like a damaged cable where two+ conductors are connected.

Crosstalk - When voltage is sent to adjacent line, like when pairs of wires become untwisted, or no more shielding/insulation remains.

Electrostatic Discharge - A sudden flow of electrically charged objects.
