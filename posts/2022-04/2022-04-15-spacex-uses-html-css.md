---
title: TIL SpaceX ships run Chromium
date: 2022-04-15
published: true
tags: ['space', 'browsers']
series: false
canonical_url: false
description: "HTML/CSS is good enough for space."
layout: layouts/post.njk
---

A serious TIL.

The touchscreens on SpaceX is running Chromium browsers.

See? HTML/CSS is good enough for space.

(SpaceX spacecraft are close cousins to your Chrome browser and Android phone - CNET)[https://www.cnet.com/culture/spacex-rockets-fly-with-software-you-can-find-on-your-android-phone/]

