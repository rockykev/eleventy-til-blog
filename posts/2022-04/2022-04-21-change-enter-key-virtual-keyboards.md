---
title: TIL about changing the enter key on virtual keyboards
date: 2022-04-21
published: true
tags: ['mobile', 'html', 'codepen']
series: false
canonical_url: false
description: "use 'enterkeyhint'."
layout: layouts/post.njk
---

On mobile devices - you get a virtual keyboard to interact with input field. Did you know you can change what the `enter` key says (the button to the right of the L)?

![](2022-04-21-change-enter-key-virtual-keyboards_0.png)

The `enterkeyhint` attribute is a global attribute that can be applied to form controls or elements that have contenteditable set to true. This attribute assists users on mobile devices that use a virtual on-screen keyboard.

`<input type="text" enterkeyhint="done">`

You can change that `enterkeyhint` to any of the following:

* enter
* done
* go
* next
* previous
* search
* send

You can try it yourself in this codepen:

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="bGadMyz" data-user="smashingmag" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/smashingmag/pen/bGadMyz">
  Using the enterkeyhint Attribute</a> by Smashing Magazine (<a href="https://codepen.io/smashingmag">@smashingmag</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


Via [Those HTML Attributes You Never Use via Louis Lazaris](https://www.smashingmagazine.com/2022/03/html-attributes-you-never-use/#the-enterkeyhint-attribute-for-virtual-keyboards)
