---
title: TIL of what happens when you put a random url in your DNS records
date: 2022-04-20
published: true
tags: ['dns', 'servers', 'redirect']
series: false
canonical_url: false
description: "So when you use a 'A' record, it points to the IP address. If you put a site you don't own, and it gets redirects, it'll 404 because the header metadata!"
layout: layouts/post.njk
---


So when you use a `A` record, it points to the IP address.

> The "A" stands for "address" and this is the most fundamental type of DNS record: it indicates the IP address of a given domain. For example, if you pull the DNS records of cloudflare.com, the A record currently returns an IP address of: 104.17.210.9.

> A records only hold IPv4 addresses. If a website has an IPv6 address, it will instead use an "AAAA" record.

via [Cloudflare](https://www.cloudflare.com/learning/dns/dns-records/dns-a-record/#:~:text=What%20is%20a%20DNS%20A,records%20only%20hold%20IPv4%20addresses.)


But you can't just shove any URL in there.

For example:

example.com's IP address is `93.184.216.34` (which you can find using `dig +short example.com`

```
Name: bad-ip
Type: A
Target: 93.184.216.34
TTL: 60
```

This would assume your site `bad-ip.website-example.com` would point to example.com, right?

NAH! It'll 404!

Why?

When your browser requests a website, it sends a Host header with the name of the domain you typed in. If the server (like Apache/nginx) doesn't recognize that name, it'll often return a 404. Here's more detail about how that works.

When you request example.com and everything works, here's what happens:

* Type `example.com` into your browser
* Browser asks: where's `example.com`?
* Resolver responds: 93.184.216.34
* Browser (to 93.184.216.34): an HTTP request for Host: `example.com`!
* Example.com server: I know that site! Here's a response!

But when you request bad-ip.shark98.messwithdns.com and you get a 404, here's how it goes:

* Type example.com into your browser
* Browser asks: where's `bad-ip.website-example.com`?
* Resolver responds: 93.184.216.34

* (everything is the same so far!)

* Browser (to 93.184.216.34): an HTTP request for Host: `bad-ip.website-example.com`!
* Example.com server: I've never heard of that site!! 404!


I learned about this on this sweet DNS sandbox [https://messwithdns.net/](https://messwithdns.net/)

I hope this site still works. I saw things changing from even a year ago.

