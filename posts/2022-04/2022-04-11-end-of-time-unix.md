---
title: TIL about the End of Time! (for Unix)
date: 2022-04-11
published: true
tags: ['trivia', 'datetime', 'javascript']
series: false
canonical_url: false
description: "On January 19th, 2038 at precisely 03:14:07 UTC, instead of the clock going to 03:14:08 UTC, it will overflow and go to 20:45:52 UTC of December 13th, 1901."
layout: layouts/post.njk
---

This is known as the Year 2038 problem!

On January 19th, 2038 at precisely 03:14:07 UTC, instead of the clock going to 03:14:08 UTC, it will overflow and go to 20:45:52 UTC of December 13th, 1901.

The reason is that for systems which measure Unix time, the number of seconds elapsed since the Unix epoch (00:00:00 UTC on 1 January 1970), the data is stored in a signed 32-bit integer.

via [Wikipedia Year 2038 Problem](https://en.wikipedia.org/wiki/Year_2038_problem)

The year 2038 problem applies to signed 32 bit timestamps only, which PHP and some other systems use.

Javascript doesn't have integers but only floats, which don't have an inherent maximum value (but in return have less precision).

[Jonas Schubert Erlandsson's SO comment](https://stackoverflow.com/a/19980881/4096078)
