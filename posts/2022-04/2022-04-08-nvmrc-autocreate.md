---
title: TIL how to quickly create a .nvmrc file
date: 2022-04-08
published: true
tags: ['node', 'buildtools']
series: false
canonical_url: false
description: "all you need is 'node -v > .nvmrc' and let the magic happen"
layout: layouts/post.njk
---

In my job, we switch node versions a lot, using something like `Node Version Manager` to handle all the heavy lifting.

Which means, lots of weird hacks to figure out which node version is running in a specific project.

Weird hacks we've done:

1. We tried to put the node version in the readme file. (Nobody ever reads it)

2. We tried to lock down the node version in `package.json` (sometimes it works, sometimes it doesn't?)

3. We start every project by going `nvm i`, which looks for a `.nvmrc` file. (BOOM! This works for unix devices! Windows... nah. [See this convo](https://github.com/coreybutler/nvm-windows/issues/16))

To explain, your `.nvmrc` file saves your node version into the project. Node Version Manager finds that file, installs that version if it doesn't exist, and switches to that version. It's fantastic.

And to automatically generate that file: `node -v > .nvmrc`

That `>` is a bash command to send data from the left, to the riht.
