---
title: TIL how my team used web components
date: 2022-04-22
published: true
tags: ['html', 'spec', 'blog']
series: false
canonical_url: false
description: "I'm sure this site is from like 2002 or something (I don't exactly know). This was a super quick task. And from the looks of it's age, we just wanted to get it done quickly."
layout: layouts/post.njk
---

See the post here: https://dev.to/rockykev/solving-a-2002-website-issue-with-web-components-2hmf

----
My team inherited a site that was written in `asp.net` and vanilla HTML/CSS/JS.

There was roughly 100+ `.html` files. And the stakeholder wanted the site moved off of an old microsoft server.

I'm sure this site is from like 2002 or something (I don't exactly know).

This was a super quick task. And from the looks of it's age, we just wanted to get it done quickly.

## The Problem

These `.html` files were fascinating!

All 100+ files sort look something like this:
```html
<body>
<!-- asp.net code here for navigation -->

<div class="content"> ... </div>

<!-- asp.net code here for footer -->
</body>
```

I don't know `asp.net`. My hypothesis was that the original site used `asp.net` to pull a navigation file and a footer file for all of their pages.

All I know is that the `asp.net` code didn't work when we tested it on a new server. So we needed a way to get the nav/footer back.

Honestly, it seems pretty sweet that the snippet of code allowed for DRY-ness.

## Different ways to solve it

### Idea 1: We can roll up a JS framework.

The snippet of code? What does that remind you of? That's right -- Components!

I can quickly `create-react-app` or `vue-create` and build out a header/footer component. Should take like 30 minutes.

**Problems with that approach**

Issue 1 - It'll leave a trail of dependencies. Like I said, this site is OLD. It's older than my career! Some developer in 2040 will be cursing my name if I used

Issue 2 - The entire payload of this site is like 20kb. According [to this gist](https://gist.github.com/Restuta/cda69e50a853aa64912d), all the frameworks would literally double the size of this page. (Except Preact! Wow, look at that file size!)

![table of minified files](2022-04-22-blogpost-web-components_0.png)

(This screenshot is 61kb.)

Is there a way to avoid that overhead?

### Idea 2: Quick and dirty

Why not just copy the `html` for the navbar and footer, and paste it where `asp.net` code was?

```html
<body>
<!-- asp.net code for header is now replaced -->
<nav><ul class="menu">... </ul></nav>

<div class="content"> ... </div>

<!-- asp.net code for footer is now replaced -->
<footer><ul class="copyright">... </ul></footer>
</body>
```

The extra lines of code doesn't add much weight to the site. And it works?

**Problems with that approach**
Issue 1 - We'll have to copy/paste that code 50x times. So 50 individual `.html` changes.

Issue 2 - It feels bad and makes me feel bad.

### Winning Idea: Web Components

The more I reflected on it, the more I remembered a thing called 'Web Components'!

> Web Components is a suite of different technologies allowing you to create reusable custom elements — with their functionality encapsulated away from the rest of your code — and utilize them in your web apps.

[Via MDN](https://developer.mozilla.org/en-US/docs/Web/Web_Components)

It essentially looks like this:

```html
<body>
<!-- asp.net code for header is now replaced -->
<navbar-section></navbar-section>

<div class="content"> ... </div>

<!-- asp.net code for footer is now replaced -->
<footer-section></footer-section>
</body>
```

I don't need props or anything, and styling is already handled.

Now we can:

1. Have a single place to house the navbar and the footer

2. Keep the payload lightweight.

3. Have it work for the next million years! (Hello Google Ultron!)

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="LYQJNgv" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/LYQJNgv">
  Custom elements demo</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

## Conclusion

It was a pretty nifty and simple way to finally dip my team's toes into web components!

And we successfully migrated a site that lived when everyone was playing PS2, XBox and Gamecube with a few lines of modern Javascript.

**Upon writing this post:**
I realized that I didn't do anything special, and I didn't even NEED to use web components. There was nothing stopping me from just using Javascript to [shove html into this site](https://stackoverflow.com/a/5536711/4096078) like our forefathers have done for the past decade, with something like [insertAdjacentHTML](https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML).

*Also, this post took me an hour to write, which is 5x longer than how long it took us to handle the original request.* 🤣🤣🤣

