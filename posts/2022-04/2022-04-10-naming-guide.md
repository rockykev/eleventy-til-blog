---
title: TIL of a guide for naming things
date: 2022-04-10
published: true
tags: ['dev', 'cleancode']
series: false
canonical_url: false
description: "Naming is hard. Naming is important because '.imgFloatLeft' no longer is accurate when we switch to 'ltr' format."
layout: layouts/post.njk
---


This is a very very excellent guide to naming things.

But honestly, it's been my bible.

There's so many things I agree with.

So something like this:

```css
// avoid this
.imgFloatLeft {
    float: left;
}

.imgFloatRight {
    float: right;
}

// Try this instead:

.rtfMedia--left {
 float: left;
}

.rtfMedia--right {
 float: right;
}

```
Why is this important? Because `.imgFloatLeft` no longer is accurate when we switch to `ltr` format.

There's so many great tips here, and I can't just copy/paste it all.

Read it.

https://blog.frankmtaylor.com/2021/10/21/a-small-guide-for-naming-stuff-in-front-end-code/
