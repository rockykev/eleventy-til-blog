---
title: TIL of the selectopt group
date: 2022-04-24
published: true
tags: ['html', 'forms', 'selectors', 'mdn', 'codepen']
series: false
canonical_url: false
description: "You can create some cool dividers for your field options using the 'optgroup' element."
layout: layouts/post.njk
---

You can create some cool dividers for your field options using the `optgroup` element. [MDN](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/optgroup)

```html
<select>
  <option>--Your Favourite Animal--</option>
  <optgroup label="Birds">
    <option>Blue Jay</option>
    <option>Cardinal</option>
    <option>Hummingbird</option>
  </optgroup>
  <optgroup label="Sea Creatures">
    <option>Shark</option>
    <option>Clownfish</option>
    <option>Whale</option>
  </optgroup>
  <optgroup label="Mammals">
    <option>Lion</option>
    <option>Squirrel</option>
    <option>Quokka</option>
  </optgroup>
</select>
```

You can even disable the whole group using the `disabled` attribute!

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="xxpGQjB" data-user="smashingmag" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/smashingmag/pen/xxpGQjB">
  Using the label Attribute with optgroup Elements</a> by Smashing Magazine (<a href="https://codepen.io/smashingmag">@smashingmag</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>
