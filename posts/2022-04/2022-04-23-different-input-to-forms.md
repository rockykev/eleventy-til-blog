---
title: TIL you can wire different inputs to different forms
date: 2022-04-23
published: true
tags: ['input', 'form', 'html']
series: false
canonical_url: false
description: "You can add the form element inside a input, to point specifically to the form you want to target."
layout: layouts/post.njk
---

Typically, you create a input element INSIDE your form element.

But what if you have a input field that lives OUTSIDE of that form wrapper?


```html
<form id="myForm" action="/form.php">
  <input id="name">
  <button type="submit">
</form>

<input type="email" form="myForm">
```

I can see this being super useful for search boxes.


Via [Those HTML Attributes You Never Use via Louis Lazaris](https://www.smashingmagazine.com/2022/03/html-attributes-you-never-use/)
