---
title: TIL of things to avoid at big companies
date: 2022-04-29
published: true
tags: ['job', 'twitter']
series: false
canonical_url: false
description: "Being someone's lieutenant, Tribal knowledge, Falling for perks, Getting raises, not readjustments, Lack of recognition."
layout: layouts/post.njk
---


This tweet rollup is from [Brandon Arvanaghi](https://threadreaderapp.com/thread/1504523654801022987.html)

## 5 traps to avoid when working at a big company:

**1. Being someone's lieutenant**

If you're good, someone at the company will try to collect you.

They’ll imply that loyalty to them will fast-track your career.

They have a few followers already in their trap.

Avoid them. They will never let you excel past them.

**2. Tribal knowledge**

Big companies are full of legacy tech. And custom-built internal tooling.

These are skills you can't bring anywhere else.

If you're not careful, you'll only be an expert at THEIR systems.

Focus on building both internal AND transferable expertise.

**3. Falling for perks**

Snacks in the office cost the company nothing. Dry cleaning costs the company nothing.

Flying you to Denver and getting you a hotel room costs the company nothing.

In your head, you're likely pricing these perks as worth $40k. Don't fall for that.

**4. Getting raises, not readjustments**

If you're a top performer at your company, best case, you get a 7% raise each year.

Even if they should, companies don't adjust people's pay by 50%.

If you were to switch jobs, you might see that 50% boost.

**5. Lack of recognition**

You should be recognized for 70% of all the work you do.

If you expect to be recognized for 100% of your work, you're likely not a team player.

If you're recognized for _less_ than 70% of your work, you're being taken for granted. And being underpaid.

I avoided these traps at big companies. Smaller ones face them too.

https://threadreaderapp.com/thread/1504523654801022987.html
