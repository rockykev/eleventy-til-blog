---
title: TIL of using the "is" property
date: 2022-04-18
published: true
tags: ['css', 'imgur', 'mdn']
series: false
canonical_url: false
description: "The ':is()' CSS pseudo-class function takes a selector list as its argument, and selects any element that can be selected by one of the selectors in that list."
layout: layouts/post.njk
---

![](2022-04-18-css-is-property_0.png)


> The :is() CSS pseudo-class function takes a selector list as its argument, and selects any element that can be selected by one of the selectors in that list. This is useful for writing large selectors in a more compact form.

The [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/:is)

```css
header p:hover,
main p:hover,
footer p:hover {
  color: red;
  cursor: pointer;
}

/* The above is equivalent to the following */
:is(header, main, footer) p:hover {
  color: red;
  cursor: pointer;
}
```

via [r/steve8708's subreddit post](https://www.reddit.com/r/webdev/comments/u6sqcp/css_tip_the_is_pseudoclass_can_help_keep_your/)
