---
title: TIL of this repo who made box shadow images
date: 2022-04-16
published: false
tags: ['codepen', 'joke', 'css']
series: false
canonical_url: false
description: "That's right! You can make a single-pixel box shadow. You can also create multiple versions on the same line."
layout: layouts/post.njk
---

The box-shadow property in CSS is for putting shadows on elements (sometimes referred to as “drop shadows”, ala Photoshop/Figma).

which uses this syntax:
```
box-shadow: 0 3px 10px rgb(0 0 0 / 0.2);
box-shadow: [horizontal offset] [vertical offset] [blur radius] [optional spread radius] [color];
```

Notice something? That's right! You can make a single-pixel box shadow. You can also create multiple versions on the same line.

```css
// single pixel
box-shadow: 100px 100px 0px rgb(0, 0, 0);

// multiple box shadows
box-shadow: 100px 100px 0px rgb(0 0 0),
            100px 100px 1px rgb(0 0 0),
            100px 100px 3px rgb(0 0 0);
```

via [CSS TRicks](https://css-tricks.com/almanac/properties/b/box-shadow/)

Well, anything you can make 1 pixel of, you can make it into images.

Behold: This repo turns images into their box-shadow equivelent!
https://github.com/AriPerkkio/img-to-box-shadow

Additionally, this is incredibly incredibly resource-intensive. 7meg CSS image? Heck yeah!
