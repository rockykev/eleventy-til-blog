---
title: TIL of a reference to quote the price of a website
date: 2022-04-26
published: true
tags: ['freelancing', 'webdev']
series: false
canonical_url: false
description: "Look, you can live in a generic rental with popcorn ceilings, odor-catching rugs, paper-thin walls and the cheapest appliances. Or you can live in a nice apartment with 10ft ceilings, hardwood floors, soundproof walls and modern appliances."
layout: layouts/post.njk
---

One of the most challenging questions I have to deal with is "How much does a website cost?"

That's like saying, "How much does a house cost?"

I like this post [How much does a website cost? Anywhere from $50 to $5,000,000+](https://www.viget.com/articles/how-much-does-a-website-cost-anywhere-from-50-to-5-000-000/) because I like the terms they used.

1. $50 Websites - “DIY”
2. $5,000 Websites - “Mom & Pop”
3. $50,000 Websites - “Brochureware”
4. $500,000 Websites - “Revenue Generator”
5. $5,000,000 Websites - “Mission Critical”
6. + - “the 1%”

I've done them all.

A "Mom & Pop" site is usually scaffolded up using WordPress, with a quick-and-dirty template that I can find that fits their needs. Forget version control... just push to master! Heck yeah I'm going to name files `business_template_2a.php` because it's about speed.

Where a "Revenue Generator" has me working weeks writing custom code, properly setting up a build system and version control, and ensuring the code is workable for the next handoff/team. There will be testing, ensuring that libraries are secure, and overall a lot more care in the process.

In house analogy:

Look, you can live in a generic rental with popcorn ceilings, odor-catching rugs, paperthin walls and the cheapest appliances.

Or you can live in a nice apartment with 10ft ceilings, hardwood floors, soundproof walls and modern appliances.

They both 'work'.
