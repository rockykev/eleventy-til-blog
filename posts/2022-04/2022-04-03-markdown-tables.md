---
title: TIL how to create tables in markdown
date: 2022-04-03
published: true
tags: ['markdown']
series: false
canonical_url: false
description: "To create a table in markdown, you literally draw a table out using pipes, and dashes."
layout: layouts/post.njk
---


To make a table, you follow this pattern:


```
| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |
```

Key points:
1. You don't actually need to space everything out perfectly.
2. It just has to be `---` dashes. More is fine.


But what if you want alignment?

`:` is the trick!

`:---` left align
`:---:` center
`---:` right align

```
| Syntax      | Description | Test Text     |
| :---        |    :----:   |          ---: |
| Header      | Title       | Here's this   |
| Paragraph   | Text        | And more      |
```

https://www.markdownguide.org/extended-syntax/
