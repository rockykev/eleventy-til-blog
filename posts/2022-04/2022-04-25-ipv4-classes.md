---
title: TIL about IPv4 Private IP addresses
date: 2022-04-25
published: true
tags: ['ip', 'certifications']
series: false
canonical_url: false
description: "The IP '10.0.0.1' is a Class A address. The IP '172.16.0.1' is a Class B address. The IP '192.168.0.1' is a Class C address."
layout: layouts/post.njk
---

This is on the Network+ tests by the way.

When the original engineers designed the IPv4 specification, they didn't realize we'd run out of IP addresses so quickly.

We slowed down the bleeding by using private IPs.

tl;dr - every device (every computer, every printer, every toaster that connects to the internet) needs its OWN IP address. A company can have a million devices in their private network (using private IPs), all sharing one single Public IP.

In other words:
* 192.168.0.100 - my home printer
* 192.168.0.101 - my computer
* 192.168.0.102 - my phone

That all go through the router and accesses the internet using `123.123.123.123`.

(the router's job is to properly return internet data back to the original source.)

Private IPs are broken into these classes (there's more, but this is only what Network+ Certification wants you to know)

The IP `10.0.0.1` is a Class A address.
The IP `172.16.0.1` is a Class B address.
The IP `192.168.0.1` is a Class C address.

How many IP addresses does that give?

10.0.0.0 – 10.255.255.255 (16,777,216 IP addresses)
172.16.0.0 – 172.31.255.255 (1,048,576 IP addresses)
192.168.0.0 – 192.168.255.255 (65,536 IP addresses)

ref: https://whatismyipaddress.com/private-ip


Typically, the more IP addresses you have, the more your network has to work to sort the data. So you want to give only what you need.

That's where you use subnet masking notation!
