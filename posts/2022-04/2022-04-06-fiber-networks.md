---
title: TIL different Fiber Networks
date: 2022-04-06
published: true
tags: ['network', 'certifications']
series: false
canonical_url: false
description: "My various notes on the Network+ exam for Fiber Networks."
layout: layouts/post.njk
---

These are all details on the Network+.


Fiber optic wire is sending light waves over the wire.

It's not like Ethernet cables (which are copper), and so it doesn't suffer from electromagnetic issues.

## Transciever Types:

Small form-factor Pluggable (SFP)
Quad small-form Pluggable (QSFP)

Quad is literally 4x the speed.

They also have PLUS versions (SFP+ & QSFP+), which are 10x the speed.

Let's use a `1GB SFP` as an example:
```

1GB SFP
1GB SFP+ === 10GB SFPs

1GB QSFP === Four 1GB SFP
1GB QSFP+ === 40GB SFPs

```


## Fiber Modes

There's Single Mode (which means it can only send OR recieve data),
and Multimode (which can send & recieve data at the same time)

So things like:
```
100BASE-FX
100BASE-SX
```

To help remember, that `S` means `S does not mean Single`.
So a 100BASE-SX is a multimode wire.

## Things to know

1. There's a bend-radius. So like if you bend the wire too hard, it'll lose packets.


## Tools used for Fiber Wires

**Reflectometer (OTDR)**
This tests the wire integrity, to see if it's flowing correctly.

**Fiber Light Meter**
This lets you see how much light is coming out from each end.


**Fusion Splicers**
This lets you slice fiber-optic wires into two pieces.
