---
title: TIL how WordPress does serverside
date: 2022-04-12
published: true
tags: ['podcast', 'wordpress', 'serverside']
series: false
canonical_url: false
description: "This isn't fully accurate, but for the means of describing server-side rendering, it's a good start!"
layout: layouts/post.njk
---

How WordPress works on a Server-side rendering Site

1 - Somebody hits the url
2 - That URL passes through apache server
3 - Goes through Php process. That it looks up the codebase... I need this file, the nav file, etc. It queries all those things that it has and needs.
4 - Then it goes into template mode, now that it has all the data.
5 - It starts to loop through the post (get the nav, get the sidebar, slap the footer)
6 - Runs it through plugins
7 - Finally it kicks it to the dev.

This isn't fully accurate, but for the means of describing server-side rendering, it's a good start!

[via Syntax Podcast: Hasty Treat WTF × SSR vs JamStack vs Serverless? - at 9:18](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkLnN5bnRheC5mbS9yc3M/episode/NTA5MDNkNDMtN2Y3Yy00NWUwLWEyZGItNGE1M2YxNGNiN2Vl?ep=14)
