---
title: TIL about the Network+ troubleshooting methodology
date: 2022-04-07
published: true
tags: ['network']
series: false
canonical_url: false
description: "Honestly, it's one of those things where if you break down a real problem, you probably already do all of this. "
layout: layouts/post.njk
---


The Comptia Network+ Troubleshooting Methodology is a specific step-by-step plan that they want all network admins to follow in situations.

It goes as follows:

1. Identify the problem
2. Establish a theory
3. Test the theory
4. Plan of Action
5. Implement the plan
6. Verify everything works
7. Document findings

Honestly, it's one of those things where if you break down a real problem, you probably already do all of this. Knowing their exact 7-step plan is necessary for passing the test.

Via https://www.comptia.org/content/guides/a-guide-to-network-troubleshooting
