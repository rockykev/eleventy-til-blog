---
title: TIL How to memorize certain IEEE 802 standards
date: 2022-04-05
published: true
tags: ['network', 'certifications']
series: false
canonical_url: false
description: "The IEEE 802 is a collection of networking standards that cover the physical and data-link layer specifications for technologies such as Ethernet and wireless."
layout: layouts/post.njk
---

The Institute of Electrical and Electronics Engineers Standards Association (IEEE SA) is an operating unit within IEEE that develops global standards in a broad range of industries.

The IEEE 802 is a collection of networking standards that cover the physical and data-link layer specifications for technologies such as Ethernet and wireless.

## 802.3af
It's PoE (Power of Ethernet)

To memorize, PoE is Path of Exile.
Well, `3af`, like Diablo 3 As Fuck. Which is Path of Exile.

## 802.1d

This is about the Spanning Tree Protocol, which is about avoiding loops.

`1d` like 1 dimensional. It's flat.

## 802.1x

This is about authentication and protection.

X like Megaman. (Megaman X)
And he's protecting the city.

## 802.3ad

This is LACP. (Think LA Cops)

This is adding multiple cords to work in unison.

Think of `ad` like, After Death, or the future.
Think of the film, the Matrix, with those robot squid things. They have a lot of robot squid legs.
