---
title: TIL of a visual way to see CSS Cascade
date: 2022-04-09
published: true
tags: ['css', 'codepen', 'mdn']
series: false
canonical_url: false
description: "But then questions start arising: is !important higher priority than say, the Shadow DOM generated object?"
layout: layouts/post.njk
---


Ever wonder what gets priority?

Ever wish it was in a funnel?

Why is this useful?
Because when you're writing CSS, there's a specificity. [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity)

As your CSS starts to grow, things can get real hairy real fast, and you start scoping your CSS. Some of you may even tack on hacks like `!important`.

And you can do weird stuff like do it's specificity weight.

But then questions start arising: will `!important` be higher priority than say, the Shadow DOM generated object?

This funnel clears that up!

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="gOXRzBa" data-user="miriamsuzanne" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/miriamsuzanne/pen/gOXRzBa">
  The Entire Cascade (as a funnel)</a> by Miriam Suzanne (<a href="https://codepen.io/miriamsuzanne">@miriamsuzanne</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

