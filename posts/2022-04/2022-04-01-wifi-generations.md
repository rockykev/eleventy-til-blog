---
title: TIL Wi-FI Generations
date: 2022-04-01
published: true
tags: ['network', 'certifications']
series: false
canonical_url: false
description: "Wifi versions and generations."
layout: layouts/post.njk
---

On Network+ certification:

There's a question to explain the wifi versions and generations.


## Wi-fi Generation

| IEEE Standard | Frequency | Max Linkrate |  How to remember |
| ----------- | ----------- | ----------- | ----------- |
| 802.11 (wi-fi 0) | 2.4Ghz | 2 Mbit | First version. OG version. |
| 802.11b (wi-fi 1) | 2.4Ghz | 11 Mbit | (b) - Base Version. Old. |
| 802.11a (wi-fi 2) | 5Ghz | 54 Mbit | (a) - Alpha, like Street Fighter Alpha. Faster. Stronger. |
| 802.11g (wi-fi 3) | 2.4Ghz | 54 Mbit | (g) - Goku. As strong as Alpha. |
| 802.11n (wi-fi 4) | 2.4, 5Ghz | 600 Mbit | (n) - Neutral zone. Does both freqs. Also 10x stronger. |
| 802.11ac (wi-fi 5) | 5Ghz | 3000 Mbit | (ac) - ACE. It's 5x stronger. |
| 802.11ax (wi-fi 6) | 2.4, 5Ghz | 6000+ Mbit | (ax) - AXE. It's twice as strong, and neutral zone. |

On Wiki, the max rate is different than what Network+ says? I dunno man.

## Wi-fi Security

WEP - Old version. Easy to hack. Single shared password.

WPA - uses tkip. Still uses some WEP under the hood. Uses EAP-TSL.

WPA2 - uses aes and ccmp. Also uses PKI for authentication (and EAP-TTSL)
Still has WPS (that button to allow anyone access for a few seconds).

REF:
https://en.wikipedia.org/wiki/Wi-Fi#Versions_and_generations
https://www.howtogeek.com/167783/htg-explains-the-difference-between-wep-wpa-and-wpa2-wireless-encryption-and-why-it-matters/
