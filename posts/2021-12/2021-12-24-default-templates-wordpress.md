---
title: TIL default template choices in WordPress
date: 2021-12-24
published: true
tags: ['templates', 'wordpress']
series: false
canonical_url: false
description: "This is a list of template pages that you can add in your custom-theme. It's a really nice feature that WordPress has built-in."
layout: layouts/post.njk
---

This is a list of template pages that you can add in your `custom-theme`.

For example:

If you make a file name `front-page.php`, WordPress will use that when you're loading the front page of your theme.

NOTE: It'll also have other checks. Like if `front-page.php` doesn't exist, it'll load `home.php` or `page.php`. The reference link points to all the types of pages.

The list:
```
index,
404,
archive,
author,
category,
tag,
taxonomy,
date,
embed,
home,
front-page,
privacy-policy,
page,
search,
single,
singular,
and attachment.
```

via https://developer.wordpress.org/themes/basics/template-hierarchy/

