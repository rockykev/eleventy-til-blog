---
title: TIL of a console.log way to avoid [object object]
date: 2021-12-29
published: true
tags: ['javascript', 'object', 'node']
series: false
canonical_url: false
description: "you can use JSON.stringify to get a object."
layout: layouts/post.njk
---


how to avoid `[object object]`

```js
const o = {
  u: {
    l: { course: 'node.js', a: { b: {}}}
  }
}


console.log(JSON.stringify(o, null, 2))
```

From Scott Moss' Introduction to Node.js on Frontend Masters.
https://frontendmasters.com/courses/node-js-v2/


