---
title: TIL adding properties conditionally using the spread syntax
date: 2021-12-02
published: true
tags: ['objects', 'javascript', 'conditionals']
series: false
canonical_url: false
description: "You can conditionally add properties to objects by ... in there."
layout: layouts/post.njk
---


You can conditionally add properties to objects!

```js

const givePerks = true;

const wizard = {
    health: 20
    spells: 'fireball',
    ... (givePerks && { perks: 'immune to fire' }),
}

```

if `givePerks === false` then the other object never gets added.







