---
title: TIL that it's not a pop-up, be more precise
date: 2021-12-08
published: true
tags: ['pop-up', 'ux', 'webdev']
series: false
canonical_url: false
description: "Teach others to stop using pop-up. It means like 10+ different things."
layout: layouts/post.njk
---

A visual guide to explaining all the various things a 'pop-up' could be.

I made this post because I was getting annoyed at how a non-web developer would request features, and get upset because they used the wrong word.

And in development, precision is key.


We'll be using the Wikipedia terms, because Wikipedia is the SOURCE OF ALL HUMAN TRUTH. (that's a joke.)

https://en.wikipedia.org/wiki/Graphical_widget

But I'd prefer to start somewhere.


## When they say 'pop-up', they might mean....

### New Window
These are new windows that pop-up.

![](2021-12-08-pop-up-is-wrong_0.png)

(image src: https://computer.howstuffworks.com/internet/basics/pop-up-blocker.htm)

### Pop-up Notification

This one is going to suck.

> The terms **pop-up notification, toast, passive pop-up, snackbar, desktop notification, notification bubble**, or **simply notification** all refer to a graphical control element that communicates certain events to the user without forcing them to react to this notification immediately, unlike conventional pop-up windows. Desktop notifications usually disappear automatically after a short amount of time. Often their content is then stored in some widget that allows the users to access past notifications at a more convenient time.

> On mobile devices, a push notification system is typically used.

There's a lot of different terms for this.

For web dev, it's when we make a un-intrusive notification to the website.
We also use that term (vomit 🤮🤮🤮) to mean words that show up in your Notification bar on your phone.

![](2021-12-08-pop-up-is-wrong_1.png)
![](2021-12-08-pop-up-is-wrong_2.png)
![](2021-12-08-pop-up-is-wrong_3.png)

(Image Src from the wikipedia page)

![](2021-12-08-pop-up-is-wrong_4.png)

(Image Src via https://www.drupal.org/project/toastr)


I hate this term.

[Wikipedia](https://en.wikipedia.org/wiki/Pop-up_notification)
### Modal

They often have a Title, a body, a 'x' in the top-right corner and a bunch of buttons.

![](2021-12-08-pop-up-is-wrong_5.png)

(Image Src: https://colorlib.com/wp/bootstrap-modals/)

[Wikipedia](https://en.wikipedia.org/wiki/Modal_window)
### Tool-tip

When you hover over something. On mobile, it's when you physically touch the element.
![](2021-12-08-pop-up-is-wrong_6.png)

[Wikipedia](https://en.wikipedia.org/wiki/Tooltip)

On OS systems, they're called [Balloon Help](https://en.wikipedia.org/wiki/Balloon_help).

### Alert Dialog Box
![](2021-12-08-pop-up-is-wrong_7.png)

(img src: https://www.queness.com/post/17436/create-consistent-browser-alert-dialogues-for-your-website)

[Wikipedia](https://en.wikipedia.org/wiki/Alert_dialog_box)

### File Dialog

When you click on a event and it launches a file directory.

![](2021-12-08-pop-up-is-wrong_8.png)

[Wikipedia](https://en.wikipedia.org/wiki/File_dialog)





Inspired by this post: [Stop Using ‘Pop-up’](https://adrianroselli.com/2021/07/stop-using-pop-up.html)
