---
title: TIL Accessibility changes and dynamic content
date: 2021-12-31
published: true
tags: ['accessibility']
series: false
canonical_url: false
description: "Using JavaScript, it is possible to dynamically change parts of a page without requiring the entire page to reload. But the result is that pages with accessibility issues do not return the value."
layout: layouts/post.njk
---

Using JavaScript, it is possible to dynamically change parts of a page without requiring the entire page to reload — for instance, to update a list of search results on the fly, or to display a discreet alert or notification which does not require user interaction.

If you want the screen reader to re-read things if it changes, use the `aria-live` attribute.

`<div aria-live="assertive">Waiting for a ride</div>`

You can pass in:

assertive - will interrupt whatever it's doing to announce.
polite - will announce the live region update when it next idles.
off - will not read the update.


> NOTE: Assistive technologies will announce dynamic changes in the content of a live region. Including an aria-live attribute or a specialized live region role (such as role="alert") on the element you want to announce changes to works as long as you add the attribute before the changes occur — either in the original markup, or dynamically using JavaScript.

Via:
[MDN Aria Live](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Live_Regions)
