---
title: TIL I'm done with FontAwesome
date: 2021-12-23
published: true
tags: ['html', 'css', 'library', 'blog']
series: false
canonical_url: false
description: "When I first discovered FontAwesome in 2013, I like everyone thought the same thing -- this is a MAJOR game changer. It is now 2022. As I look back, FontAwesome has become unsustainable."
layout: layouts/post.njk
---

Read the blog post at: https://dev.to/rockykev/im-officially-done-with-fontawesome-2h2f

----
When I first discovered FontAwesome in 2013, I like everyone thought the same thing -- this is a MAJOR game changer.

It is now 2022. As I look back, FontAwesome has become unsustainable.

I'm officially 'done' with FontAwesome.

## Credit where credit is due

I know this is a 'complaint'-style post.

(There's a lot of toxicity in software development) [https://jamie.build/dear-javascript.html]. This post is full of emotion (and contributes to that toxicity).

But credit where credit is due.

FontAwesome solved major problems with icons. The team worked really hard over the years.

We stand on the shoulders of giants.

But as we continue moving forward in web development, I'm sending FontAwesome into the 'thank you for your service' graveyard, along with jQuery, Bootstrap, Foundation, Gulp, and Sublime.

Additionally, the bulk of my complaints is the `css` portion of it. 90% of fontawesome projects I've worked in over the decade of development imported it this way.

Using FontAwesome as single file `svgs`, which is my current [preferred way of importing icons](https://fontawesome.com/docs/web/add-icons/svg-sprites), mitigates all of these problems. (Which then begs the question, "Why even use Font Awesome?")

## Problem #1 - version nightmare

If you were building a website in 2018, you may have used FontAwesome 5. Set it and forget it, right?

Let's say you need a gameboy icon. Here is one!

![gameboy icon](2021-12-23-blogpost-done-with-fontawesome_0.png)

[Direct link](https://fontawesome.com/v5/icons/game-console-handheld?s=solid)

```html
<i class="fal fa-game-console-handheld"></i>
```

But it doesn't work.

Here's all the reasons why.

**REASON 1**: You're on FontAwesome 5.0. And this is on FontAwesome 5.11.0.

**REASON 2**: This is a 'pro' font. You're using a free version.

**REASON 3**: You've been importing `solid` versions of fonts. You should You need the `light` versions.

So not only does FontAwesome have minor versions, it also has variants! There's regular versions, light versions, solid versions, duotone versions, brands... all in separate files, with separate markup.

This juggling of version and variant, if it's Free or Pro, and which of the many different files continues in FontAwesome 6.

## Problem #2 -  Massive font set

FontAwesome's strength is the range of icons.

* FontAwesome 3 - 321 icons (https://fontawesome.com/v3/icons/)
* FontAwesome 4 - 675 icons (https://fontawesome.com/v4/icons/)
FontAwesome 5 - 7,864 icons (https://fontawesome.com/v5/icons/)
* FontAwesome 6 - 16,000+ icons (https://fontawesome.com/search)

How many icons are you really using on your site? Maybe 6 or 7?

Most sites (and I'm speaking from my own background) only use a handful of icons, like `magnifying-glass` or `circle-question` (https://fontawesome.com/icons/circle-question?s=solid) or social media icons.

If you're importing a fontAwesome's `regular.css`, you're bringing in hundreds of fonticons you don't need. It's even more nightmare-ish if you're getting a `circle-question` icon (which lives in `regular.css`) AND a Facebook icon (which lives in `brands.css`). Now there's thousands of fonts imported... all eating bandwidth. Yum.

You want to a few icons on your site? Gotta import the [massive set of files](https://github.com/FortAwesome/Font-Awesome/tree/6.x/webfonts).

Defenders may say, "[Well you can tree-shake it](https://fontawesome.com/docs/apis/javascript/tree-shaking)" or "[optimize it](https://blog.webjeda.com/optimize-fontawesome/)".

And you're right! So... how many devs even know to do that or have the gumption to set that up?

TailwindCSS includes the entire kitchen sink as well. But they also include helpers to prune it all so you ship the absolute, smallest most performant css you can.

## Problem #3 - Fonts first

A major problem with font icons is that you're treating them like fonts first, not icons.

You want a [horse icon](https://fontawesome.com/icons/horse?s=solid) to be 100px wide.

This won't work:
```css
.fa-horse {
  width: 100px; /* does nothing */
  font-size: 100px;
}
```

Instead, you have to fiddle with this using `font-size`.

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="MWQBVYP" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/MWQBVYP">
  Untitled</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

[codepen](https://codepen.io/rockykev/pen/MWQBVYP?editors=1100)

That also means these icons are also affected by other css text modifiers, like `line-height`.

Are you screaming yet? 😱😱😱

## Problem #4 - Changing APIs

Let's use a real example.

You have a feature for visitors to leave a review - a simple leave a 1 to 5 star review.

Here's how to do that, depending on the FontAwesome version.
```html
// FontAwesome 3
<i class="icon-star"></i>

// FontAwesome 4
<i class="fa fa-star" aria-hidden="true"></i>

// FontAwesome 5
<i class="fas fa-star"></i>

// FontAwesome 6
<i class="fa-solid fa-star"></i>
```

Starting to see the problem?

If I wanted to upgrade FontAwesome 3 to FontAwesome 6, I'll have to go find all the markup and fix that.

Worst, try juggling multiple projects with multiple different versions of FontAwesome. It's a nightmare.

## Problem #5 - Locking you into the model

The last problem is that Pro is yearly.

If I have FontAwesome 5 Pro, which I purchased in 2019, and I wanted a Tiktok logo, I'll have to either:

Option 1: Keep FontAwesome 5 Pro and also include FontAwesome 6 brands
Option 2: Buy FontAwesome 6 Pro.

Just to get a TikTok logo, I now can either fracture my versions or pay $99.

## Summary

All of these problems do have workarounds. The thing is -- all these problems are strictly from using FontAwesome!

Instead, switch to SVGs. Boom! All the 5 problems disappeared. (And devs have been [banging their drum (2015 article)]...(https://cloudfour.com/thinks/seriously-dont-use-icon-fonts/) [to leaving icon fonts (2021 article)](https://www.irigoyen.dev/blog/2021/02/17/stop-using-icon-fonts/) for years...)

There's plenty of sites to score free SVGs or to buy SVGs. And SVGs are performant and AMAZING. ([Animated SVGs, wow!](https://lordicon.com/))

After years of FontAwesome, I'm done.
