---
title: TIL Blocks Vs Themes Style responsibility in WordPress
date: 2021-12-18
published: true
tags: ['wordpress']
series: false
canonical_url: false
description: "In WordPress Full-site editing mode - Who manages CSS styles? Blocks or Themes? Well, a bit of both!"
layout: layouts/post.njk
---

With WordPress moving to Block-style development, we are now reaching the unknown of responsibility.

Who manages CSS Styles? Blocks or Themes?

> Musically speaking, the intention is that the theme is the conductor of the blocks orchestra. The general aim is that we can absorb most if not all the structural responsibilities into blocks, and a large part of the style directives into blocks and elements. We can use the 80% rule there, but in a fairly loose way, mostly as set of expectations. Container blocks adopt positioning and layout responsibilities allowing the theme to focus on composition and styling. It also ensures multiple blocks — including third-parties — compose natively well together.

[reference from github discussion](https://github.com/WordPress/gutenberg/discussions/35469#discussioncomment-1450027)

** The theme's responsibility: **
Composition - How to lay out the blocks. Being able to read it.

For example:
![](2021-12-18-style-responsibility-blocks-vs-themes_0.png)

(via https://webdesign.tutsplus.com/articles/an-introduction-to-composition--webdesign-14508)

Styling - how it looks at the end. Maybe it's spacing between blocks, or layout with responsive design.

** The container blocks responsibility: **

Positioning and layout - How the elements look. Like spacing between header, subheader.

![](2021-12-18-style-responsibility-blocks-vs-themes_1.png)

(via https://morioh.com/p/848328c1a1ff)

