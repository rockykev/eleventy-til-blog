---
title: TIL Adding Tooltips without a library
date: 2021-12-11
published: true
tags: ['html', 'ui']
series: false
canonical_url: false
description: "You can add tooltips without a library by adding a title attribute in any html element"
layout: layouts/post.njk
---


That can be added with the `title` attribute, and used for a lot of different types of items.
```html

<p>
<abbr title="Dragon Ball Z">DBZ</abbr> has Goku go super Saiyan.
</p>

```

For Multiline:

```html
<p>Newlines in <code>title</code> should be taken into account,
like <abbr title="This is a
multiline title">example</abbr>.</p>
```

![](2021-12-11-title-attribute-for-easy-tooltips_0.png)


There's also inheritance:

```html
<div title="CoolTip">
  <p>Hovering here will show “CoolTip”.</p>
  <p title="">Hovering here will show nothing.</p>
</div>
```

The major issue is accessibility!
[The MDN recommends a tip for that](https://inclusive-components.design/tooltips-toggletips/)


REFERENCE:
[mdn title](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/title)
