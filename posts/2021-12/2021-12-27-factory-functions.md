---
title: TIL a fancy word for auto-generating objects, A Factory Function
date: 2021-12-27
published: true
tags: ['programming', 'patterns']
series: false
canonical_url: false
description: "So a factory function creates objects. That's all."
layout: layouts/post.njk
---


So a factory function creates objects.

something like

```js
function makeNewMage(theName) {

  const newMage = {
    name: theName,
    magic: 100,
    spell: 'fireball';
  }

  return newMage;

  };

const JakeTheMage = makeNewMage('Jake')
const SarahtheMage = makeNewMage('Sarah')

```

There's no real magic to it.
