---
title: TIL Find unused JavaScript
date: 2021-12-19
published: true
tags: ['javascript', 'devtools', 'optimization']
series: false
canonical_url: false
description: "Chrome's Coverage panel allows you to quickly locate JavaScript (and CSS) code that has — and has not — been used. To start, open Coverage from the More tools sub-menu in the DevTools menu"
layout: layouts/post.njk
---

Chrome’s Coverage panel allows you to quickly locate JavaScript (and CSS) code that has — and has not — been used. To start, open Coverage from the More tools sub-menu in the DevTools menu. Reload the page and the panel will show the percentage of unused code with a bar chart:

![](2021-12-19-find-unused-javascript_0.png)

Click any JavaScript file and unused code is highlighted with a red bar in the line number gutter.

VIA:
[15 DevTool Secrets for JavaScript Developers](https://blog.openreplay.com/15-devtool-secrets-for-javascript-developers)
