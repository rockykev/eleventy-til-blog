---
title: TIL You can change the blinking line
date: 2021-12-22
published: true
tags: ['css', 'inputs', 'mdn']
series: false
canonical_url: false
description: "The caret-color CSS property sets the color of the insertion caret, the visible marker where the next character typed will be inserted. This is sometimes referred to as the text input cursor."
layout: layouts/post.njk
---

The caret-color CSS property sets the color of the insertion caret, the visible marker where the next character typed will be inserted. This is sometimes referred to as the text input cursor.

![](2021-12-22-change-blinking-line-cursor_0.png)


VIA:
[mdn caret-color](https://developer.mozilla.org/en-US/docs/Web/CSS/caret-color)
