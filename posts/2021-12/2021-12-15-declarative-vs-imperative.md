---
title: TIL Declarative Vs Imperative Programming Paradigms
date: 2021-12-15
published: true
tags: ['programming', 'paradigms']
series: false
canonical_url: false
description: "Declarative Programming is like asking your friend to fix your car. You don’t care how to fix it, that’s up to her. Imperative Programming is like your friend calling your father that tells her how to fix your car step by step.
"
layout: layouts/post.njk
---



* `Declarative programming` is a programming paradigm that expresses the logic of a computation without describing its control flow.
* `Imperative programming` is a programming paradigm that uses statements that change a program’s state.

Declarative Programming is like asking your friend to fix your car. You don’t care how to fix it, that’s up to her.

Imperative Programming is like your friend calling your father that tells her how to fix your car step by step.


There's no right or wrong way. They're just patterns and models to help us write better code.

And from personal experience - it's always 80/20. It's never possible to go ALL IN on a pattern.


Source:
https://docs.microsoft.com/en-us/answers/questions/211050/declarative-vs-imperative-language.html
