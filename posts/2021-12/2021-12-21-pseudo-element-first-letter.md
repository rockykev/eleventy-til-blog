---
title: TIL CSS pseudo-element first-letter, to make a Drop Cap
date: 2021-12-21
published: true
tags: ['css', 'html']
series: false
canonical_url: false
description: "You can add a drop cap to a paragraph by using the ::first-letter pseudo-element."
layout: layouts/post.njk
---


A drop-cap is this:

![](2021-12-21-pseudo-element-first-letter_0.png)


You can add a drop cap to a paragraph by using the ::first-letter pseudo-element.

```css
p::first-letter {
  font-size: 250%;
}
```

It's accessible! It's also not hacking the page with nasty `<span>` tags.

REFERENCE:
[Accessible Drop Caps](https://adrianroselli.com/2019/10/accessible-drop-caps.html)
