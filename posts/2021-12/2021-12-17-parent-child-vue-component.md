---
title: TIL Getting parent data from child component
date: 2021-12-17
published: true
tags: ['vue', 'components']
series: false
canonical_url: false
description: "Sometimes we want to access data from a parent, but don’t want to go through the hassle of passing props."
layout: layouts/post.njk
---

## Access Parent Data( ) from Child Components

Sometimes we want to access data from a parent, but don’t want to go through the hassle of passing props.
If you just need to grab a quick value from a $parent’s data object, you can do it simply by referencing $parent:


```js

// In parent
data() {
  return {
    message: 'This is my message'
  }
}

// In child template
<div>{{ $parent.message }}</div> // <-- results in 'This is my message'

```

[11 Advanced Vue Coding Tricks](https://betterprogramming.pub/advanced-vue-tricks-6e315347c378)
