---
title: TIL Singleton pattern
date: 2021-07-18
published: true
tags: ['patterns']
series: false
canonical_url: false
description: "Singleton is a creational design pattern that restricts class to have only one instance, while also creating a global access point to this instance. It is used when control access to a shared resource is needed."
layout: layouts/post.njk
---


The Singleton Pattern:

Singleton is a creational design pattern that restricts class to have only one instance, while also creating a global access point to this instance. It is used when control access to a shared resource is needed.

Use this pattern when you need to ensure control access to a resource that might return inconsistencies if it is changed by two different objects at the same time. (Ex. Databases, state of an object)

```
class Singleton {
    private static instance: Singleton;

    private constructor() { }

    public static getInstance(): Singleton {
        if (!Singleton.instance) {
            Singleton.instance = new Singleton();
        }

        return Singleton.instance;
    }
}
```

Via [Design Patterns for Web Development](https://dev.to/mayruiz27/design-patterns-for-frontend-developers-2ii3?utm_source=digest_mailer&utm_medium=email&utm_campaign=digest_email)
