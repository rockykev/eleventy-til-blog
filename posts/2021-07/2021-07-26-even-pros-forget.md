---
title: TIL even professionals forget things
date: 2021-07-26
published: true
tags: ['dev']
series: false
canonical_url: false
description: "It's nice to be reminded that even the professionals forget basic things."
layout: layouts/post.njk
---

It's nice to be reminded that even the professionals forget basic things.

![](2021-07-26-even-pros-forget_0.png)
