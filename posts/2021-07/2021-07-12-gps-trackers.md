---
title: TIL the time Silicon Valley thieves stole GPS trackers
date: 2021-07-12
published: true
tags: ['humor']
series: false
canonical_url: false
description: "Silicon Valley thieves ade off with $18,000 worth of high-tech equipment – unfortunately for them, about 100 GPS tracking devices were among the items taken."
layout: layouts/post.njk
---


> Apair of Silicon Valley thieves burglarized a Santa Clara, Calif., startup and made off with $18,000 worth of high-tech equipment – unfortunately for them, about 100 GPS tracking devices were among the items taken.

> The duo broke into the offices of the Internet of Things (IoT) startup, Roambee, late last month and took several laptops along with devices that resemble cell phone chargers, KRON-4 reports. But those devices were actually Roambee’s ‘Bees,’ which are used by pharmaceutical, agricultural and consumer electronic companies to track goods within an accuracy of 16 feet using built-in Wi-Fi, GMS, Bluetooth and GPS functions.

> “We were able to pinpoint the location of these trackers to a warehouse in Union City and two of the devices had gone mobile, and the thieves were driving around with them in the East Bay,” Subramanian told KRON. The two men were arrested in Alameda and their storage locker containing drugs and other stolen property is being investigated in connection with a series of other robberies in the Bay Area.



Via http://nowiknow.com/the-problem-with-stealing-high-end-electronics-and-beer/
and https://www.ibtimes.com/silicon-valley-heist-thieves-easily-caught-after-stealing-18k-worth-gps-equipment-2552785

