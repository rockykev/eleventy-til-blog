---
title: TIL how many vendor prefixes are left as of 2021
date: 2021-07-10
published: true
tags: ['css']
series: false
canonical_url: false
description: "Per CSS Tricks, we have about 28 left!"
layout: layouts/post.njk
---


The major browsers use the following prefixes:

`-webkit-` (Chrome, Safari, newer versions of Opera, almost all iOS browsers including Firefox for iOS; basically, any WebKit based browser)
`-moz-` (Firefox)
`-o-` (old pre-WebKit versions of Opera)
`-ms-` (Internet Explorer and Microsoft Edge)

Via https://developer.mozilla.org/en-US/docs/Glossary/Vendor_Prefix


We don't actually need to memorize that stuff anymore, as the build process should handle that for us now, with tools like [Autoprefixer](https://github.com/postcss/autoprefixer).

The number of prefixes is slowly dying.

![](2021-07-10-vendor-prefixes_0.png)
[Image src from CSS Tricks](https://css-tricks.com/is-vendor-prefixing-dead/)

Per CSS Tricks, we have about 28 left!

