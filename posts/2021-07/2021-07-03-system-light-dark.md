---
title: TIL system light mode and dark mode
date: 2021-07-03
published: true
tags: ['css']
series: false
canonical_url: false
description: "Ever notice on mobile devices or on macs, tabs go from light to dark depending on the time?"
layout: layouts/post.njk
---

Ever notice on mobile devices or on macs, tabs go from light to dark depending on the time?

That's your system's color scheme changing.

You can also have your site's colors switch that way too!

> The `prefers-color-scheme` media feature is used to detect if the user has requested the page to use a light or dark color theme. It works with the following values:

> light: Indicates that the user has notified the system that they prefer a page that has a light theme (dark text on light background).
> dark: Indicates that the user has notified the system that they prefer a page that has a dark theme (light text on dark background).

Via Web.dev's article.


To simulate: Chrome devtools allows you switch the system color scheme to test it in action.

![](2021-07-03-system-light-dark_0.png)

Via
[Web.dev prefers-color-scheme](https://web.dev/prefers-color-scheme/)
