---
title: TIL array fill references the same object in memory
date: 2021-07-23
published: true
tags: ['javascript', 'gotchas']
series: false
canonical_url: false
description: "When you use `array.fill` to create inner arrays, it's actually just referencing the same space in memory."
layout: layouts/post.njk
---

When you use `array.fill` to create inner arrays, it's actually just referencing the same space in memory.

```js
let test = new Array(5).fill(new Array(5).fill("1"))

// RESULT:
// [
//   [ '1', '1', '1', '1', '1' ],
//   [ '1', '1', '1', '1', '1' ],
//   [ '1', '1', '1', '1', '1' ],
//   [ '1', '1', '1', '1', '1' ],
//   [ '1', '1', '1', '1', '1' ]
// ]

test[0][1] = "5";
// NEW RESULT:
// [
//   [ '1', '5', '1', '1', '1' ],
//   [ '1', '5', '1', '1', '1' ],
//   [ '1', '5', '1', '1', '1' ],
//   [ '1', '5', '1', '1', '1' ],
//   [ '1', '5', '1', '1', '1' ]
// ]
```


VIA
https://dev.to/crimsonmed/how-array-fill-can-break-arrays-3cdc
