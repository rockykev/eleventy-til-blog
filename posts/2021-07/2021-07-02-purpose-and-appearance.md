---
title: TIL Separating purpose and appearance in CSS
date: 2021-07-02
published: true
tags: ['css']
series: false
canonical_url: false
description: "Appearance is how the variable looks. Purpose is the intent of the variable."
layout: layouts/post.njk
---

Separate your CSS variables by purpose and appearance.

What is this solving?

Look how hacky this to create a dark mode:
```css
:root {
  --white-bg: white;
}

@media (prefers-color-scheme: dark) {
  :root {
    --white-bg: black;
  }
}
```

`--white-bg` is black? (that jackie chan meme)

Let's fix that.
```css
:root {
  --background: white;
}

@media (prefers-color-scheme: dark) {
  :root {
    --background: black;
  }
}
```

This is a very simple example. Let's get more complex.

A better way to do that is to describe purpose.

![](2021-07-02-purpose-and-appearance_0.jpg)

Appearance is how the variable looks.
Purpose is the intent of the variable.

> CSS variables named after purpose instead of appearance tolerate visual change. The variable name --accent is decoupled from the colour, which means changing the accent colour has no impact on its identity or consumers. In comparison the variables named after the colour are renamed each time the colour changes.

Via the refence at the bottom.


```css
:root {
  /* defined appearances */
  --light: FloralWhite;
  --dark: Navy;
  --blue: RoyalBlue;
  --red: Crimson;

  /* defined purpose */
  --textColor: black;
  --foreground:  --red;
  --middleground: --blue;
  --background: --light;
}

@media (prefers-color-scheme: dark) {
  :root {
  /* changing purpose */
    --textColor: white;
    --foreground: --blue;
    --middleground: --red;
    --background: --dark;
  }
}
```

So the background is currently `--light`. But on dark mode, it's `--dark`.

Also in our example above, `--light` is `FloralWhite`. That makes it flexible, and more importantly, scalable. Maybe down the line, someone wants it `Beige`. Boom - a single line change.


[MDN prefers-color-scheme](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme)

And https://www.callumhart.com/blog/css-variables-that-mimic-chameleons/
