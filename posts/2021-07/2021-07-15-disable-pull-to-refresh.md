---
title: TIL Disable pull-to-refresh on mobile
date: 2021-07-15
published: true
tags: ['css', 'mobile']
series: false
canonical_url: false
description: "By default, mobile browsers tend to provide a 'bounce' effect or even a page refresh when the top or bottom of a page (or other scroll area) is reached."
layout: layouts/post.njk
---

On Mobile devices:
If you pull your window down enough, it'll trigger a refresh.

Maybe you don't want that to happen, such as in a modal.

Look into the [overscroll-behavior - MDN link](https://developer.mozilla.org/en-US/docs/Web/CSS/overscroll-behavior).

```css
body {
 overscroll-behavior-y: contain;
}

```

> By default, mobile browsers tend to provide a "bounce" effect or even a page refresh when the top or bottom of a page (or other scroll area) is reached. You may also have noticed that when you have a dialog box with scrolling content on top of a page of scrolling content, once the dialog box's scroll boundary is reached, the underlying page will then start to scroll — this is called scroll chaining.

As with most things on mobile, check the [caniuse - css-overscroll-behavior](https://caniuse.com/css-overscroll-behavior)

As of 2021: Safari for iOS doesn't have it implemented.
