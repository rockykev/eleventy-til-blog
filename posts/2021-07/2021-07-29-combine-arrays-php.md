---
title: TIL combining PHP arrays
date: 2021-07-29
published: true
tags: ['php', 'arrays']
series: false
canonical_url: false
description: "array_merge, array_replace, and the (+) union operator"
layout: layouts/post.njk
---

First, there was `array_merge`.
Then `array_replace`.
Finally, the `+` operator (union).


Since all 3 do similar thing, and the docs don’t quite describe the difference between them, here’s a nice image of it.


This visual:
![](2021-07-29-combine-arrays-php_0.png)

(Via [array_merge vs array_replace vs + (plus aka union) in PHP](https://p.softonsofa.com/php-array_merge-vs-array_replace-vs-plus-aka-union/))


* `array_merge` and `array_replace` work just the same for Associative Arrays (key=>value arrays, or objects in JS), so they can be used interchangeably.


and
https://stackoverflow.com/questions/5394157/whats-the-difference-between-array-merge-and-array-array

