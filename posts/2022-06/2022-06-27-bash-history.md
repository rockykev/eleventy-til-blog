---
title: TIL about going up and down in your bash history
date: 2022-06-27
published: true
tags: ['commandline', 'terminal', 'security']
series: false
canonical_url: false
description: "Your bash history is saved with up/down arrow keys. Which means you shouldn't save your passwords on it."
layout: layouts/post.njk
---

Your bash history is saved with up/down arrow keys. Which means you shouldn't save your passwords on it.

ADD TO TIL:
Ctrl+R - reverse-search history
(left and right drops you back into your terminal)
