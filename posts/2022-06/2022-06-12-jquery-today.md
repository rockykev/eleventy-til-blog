---
title: TIL what jQuery's role is today
date: 2022-06-12
published: true
tags: ['jquery', 'javascript']
series: false
canonical_url: false
description: "Today’s JavaScript frameworks allow us to break the UI into components, making it easier to scale up an application. It creates abstractions for easier synchronization between the data and the view. We don't need to hack the DOM element directly, and instead let the framework handle it."
layout: layouts/post.njk
---

I always wondered, what is jQuery's role today?

jQuery pushed web development around the 2010s. It revolutionized how we interact with web pages!

One of the developers who continues to maintain jQuery, Michał Gołębiowski-Owczarek, even acknowledged that jQuery may have performance issues for large-scale applications.

> So their users no longer need to modify/access the DOM directly so often, and are often discouraged from doing that. In such an environment, using jQuery doesn’t make much sense. But that’s just a small part of the whole web ecosystem.”

Today’s JavaScript frameworks allow us to break the UI into components, making it easier to scale up an application. It creates abstractions for easier synchronization between the data and the view.

We don't need to hack the DOM element directly, and instead let the framework handle it.

There's also the performance issue.

> “We’ve improved jQuery performance a lot over the years,” he said, “but there are some obstacles that are hard to deal with. One of them is that to not conflict with the browser, jQuery doesn’t modify native prototypes — as some of the other libraries did (often later causing issues with standardization of conflicting APIs) — but it instead wraps DOM nodes with jQuery wrapper objects. Each operation on such a wrapper creates a new one. In most cases, it doesn’t matter, but for very complex applications with lots of DOM manipulation this can sometimes become an issue.”

It's also better for security

In 2019, it was [discovered in jQuery too](https://snyk.io/blog/after-three-years-of-silence-a-new-jquery-prototype-pollution-vulnerability-emerges-once-again/), leaving many web applications vulnerable to such assaults.

And modern frameworks escape everything and have a 'security-first' approach. A lot of older vulnerabilities have been eliminated thanks to these frameworks.

jQuery code (and vanilla JS) needs to be written with security in mind.

## So what about now?

jQuery shouldn't be used in any new projects.
[You might not need jQuery](https://youmightnotneedjquery.com/) has vanilla JS alternatives!

Thanks jQuery for all of your work. But its time to be sunsetted.
