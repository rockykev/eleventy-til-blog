---
title: TIL how images get displayed to the browser
date: 2022-06-23
published: true
tags: ['html', 'spec', 'image', 'performance']
series: false
canonical_url: false
description: "Image data is usually encoded in order to reduce file size. You can change when to decode the image."
layout: layouts/post.njk
---


Images elements in html have a bunch of new features of the past few years.

We all know about `loading`.

```html
<img src="/happy-face.png" width="300" height="250" alt="Example"  loading="lazy">
```

There's another one called decoding, which I've never used!

```html
<img src="/happy-face.png" width="300" height="250" alt="Example"   decoding="async">
```

This is straight from the [HTML Spec docs](https://html.spec.whatwg.org/multipage/images.html#decoding-images)

Image data is usually encoded in order to reduce file size. This means that in order for the user agent to present the image to the screen, the data needs to be decoded. Decoding is the process which converts an image's media data into a bitmap form, suitable for presentation to the screen. Note that this process can be slow relative to other processes involved in presenting content. Thus, the user agent can choose when to perform decoding, in order to create the best user experience.

In other words, there are 3 modes:

** auto (default) **

In both synchronous and asynchronous decoding modes, the final content is presented to screen after the same amount of time has elapsed. The main difference is whether the user agent presents non-image content ahead of presenting the final content.

* sync **

Image decoding is said to be synchronous if it prevents presentation of other content until it is finished. Typically, this has an effect of atomically presenting the image and any other content at the same time. However, this presentation is delayed by the amount of time it takes to perform the decode.

** async **

Image decoding is said to be asynchronous if it does not prevent presentation of other content. This has an effect of presenting non-image content faster. However, the image content is missing on screen until the decode finishes. Once the decode is finished, the screen is updated with the image.



Via [Those HTML Attributes You Never Use via Louis Lazaris](https://www.smashingmagazine.com/2022/03/html-attributes-you-never-use/)
