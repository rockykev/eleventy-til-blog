---
title: TIL default spellcheck in html
date: 2022-06-16
published: true
tags: ['html', 'mdn']
series: false
canonical_url: false
description: "The spellcheck is a global attribute that you can use to check spelling and grammar on HTML elements such as input fields and other editable elements."
layout: layouts/post.njk
---


The spellcheck is a global attribute that you can use to check spelling and grammar on HTML elements such as input fields and other editable elements.

![](2022-06-16-spellcheck-input_0.png)

In the first element, see the 'red swiggles'?


```html
<p contenteditable="true" spellcheck="true">
Thanks furr checkinng my speling :)</p>
```

This attribute is merely a hint for the browser: browsers are not required to check for spelling errors.

Typically non-editable elements are not checked for spelling errors, even if the spellcheck attribute is set to true and the browser supports spellchecking.

[MDN spellcheck](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/spellcheck)
