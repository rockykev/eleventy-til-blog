---
title: TIL a short explainer of the Execution Context
date: 2022-06-26
published: true
tags: ['javascript', 'javascript-advanced', 'mdn', 'eli5']
series: false
canonical_url: false
description: "It is an abstract concept that represents the environment in which JavaScript runs. What happens inside the execution context is two things basically. The first is parsing the code line by line and the second is storing the variables and functions into the memory."
layout: layouts/post.njk
---

What is the Execution Context?


We're going into deep Javascript territory here:

So Javascript has 2 steps when it goes from code into the engine (in the browser).

1. The creation Step:
This step picks out all the variables and functions, and makes a note that they exist.

This DOESN'T assign anything!

If you said

`const name = "Anakin"`,

then it makes a note that a `name` variable exists.


2. The execution Step:

This is where is assigns values.
That `name` variable now has a `Anakin` attached.

`const name = "Anakin"`,


But what about functions?

Well, this is were the Execution Context comes in.

> It is an abstract concept that represents the environment in which JavaScript runs. What happens inside the execution context is two things basically. The first is parsing the code line by line and the second is storing the variables and functions into the memory.


As Javascript runs top to bottom, it starts building code together.

```js
function timesTen(a){
    return a * 10;
}

// ... many lines later

const oneHundred = timesTen(10);

console.log(`There are ${oneHundred} apples`);
```

So here, it's creating a new Execution Context for `timesTen`. It pops it on the Call Stack as a task to do, does the task, then gets rid of the Execution Context.

So:

1. JS sees `oneHundred` needs a value.
2. JS then builds a new Execution Context for `timesTen()`, where it grabs all the parameters, prototypes, and inner code to get you data back.
3. If everything adds up, it'll then return that data. (`a * 10 = 100`)
4. It passes that data to `oneHundred`. `oneHundred = 100`
5. It now outputs that number in console.log.


Here's a visual on it - from the blog post #3.
![](2022-06-26-short-explainer-execution-context_0.png)



REFERENCES:
#1 - [The ECMA Script Definition](https://262.ecma-international.org/5.1/#sec-10.3)

#2 - [JS tutorial](https://www.javascripttutorial.net/javascript-execution-context/)

#3 - [Understanding JavaScript Execution Context and How It Relates to Scope and the `this` Context](https://levelup.gitconnected.com/learn-javascript-fundamentals-scope-context-execution-context-9fe8673b3164)
