---
title: TIL onError
date: 2022-06-21
published: true
tags: ['html', 'error', 'mdn']
series: false
canonical_url: false
description: "When a resource (such as an img or script) fails to load, an error event using interface Event is fired at the element that initiated the load, and the onerror() handler on the element is invoked."
layout: layouts/post.njk
---


While working with white-hat hackers who pen-tested my software, I noticed the test code they sent back looked something like:

```html

<img src="image.jpg" onerror="alert(1);">
```

If the image fails to show, it'll hit us with that sweet alert prompt!

What's a good use-case?

```html
<img src="imagefound.gif" onerror="this.onerror=null;this.src='imagenotfound.gif';" />
```

If the image fails to load, it'll:

1. set the `onerror=null` to avoid a infinite loop
2. replace the image with `imagenotfound.gif`

> When a resource (such as an `<img>` or `<script>`) fails to load, an error event using interface Event is fired at the element that initiated the load, and the onerror() handler on the element is invoked. These error events do not bubble up to window, but can be handled with a EventTarget.addEventListener configured with useCapture set to true.

via:

[MDN GlobalEventHandlers.onerror](https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onerror)
