---
title: TIL the global attribute translate
date: 2022-06-17
published: true
tags: ['html', 'mdn']
series: false
canonical_url: false
description: "A good use-case is if your brand has words that can be translated."
layout: layouts/post.njk
---

`translate` tells the browser whether the content  (its Text node children) should be translated when the page is localized, or whether to leave them unchanged.

A good use-case is if your brand has words that can be translated.

```html
<!-- In English -->
<p>The Dog Company LLC</p>

<!-- In Spanish - default -->
<p>La Compañía de Perros LLC</p>

<!-- In Spanish - with fix -->
<p translate="no">The Dog Company LLC</p>

```

