---
title: TIL the history of jQuery
date: 2022-06-11
published: true
tags: ['jquery', 'javascript']
series: false
canonical_url: false
description: "With the launch of jQuery in 2006, web developers were able to access and manipulate DOM and CSS faster and easier than ever before."
layout: layouts/post.njk
---

Check out this post via: [Why Outdated jQuery Is Still the Dominant JavaScript Library](https://thenewstack.io/why-outdated-jquery-is-still-the-dominant-javascript-library/)

According to BuiltWith, of the top 1 million websites:
* 78% of the use jQuery in some way,
* And for the most talked-about JavaScript library today, React, it’s used by a relatively paltry 14%.

## History of jQuery

With the launch of jQuery in 2006, web developers were able to access and manipulate DOM and CSS faster and easier than ever before. Thousands of open source jQuery widgets and plugins were created to handle previously tricky problems, like showing and hiding elements, rotating through image carousels, or picking dates on a calendar. The jQuery ecosystem became a playground full of tools for making new and interesting interactions possible on the web.

By using jQuery, developers could leave it up to the jQuery team to deal with these pitfalls.

In September 2007, jQuery UI officially launched as a set of user interface interactions, effects, widgets, and themes built on top of jQuery. Soon after, the team shifted their focus to provide a full set of APIs and methods to allow developers to create flexible, full-featured widgets that met high standards of quality. CSS effects such as easing and animation were added in and helped developers create more modern, enhanced experiences.

By the end of 2008, jQuery UI had an exploding community of users, developers, and interaction designers regularly providing updates and improvements to the project as best practices and style preferences evolved. Between 2009 and 2016, the community provided a variety of new official and unofficial themes and plugins, interoperability and other bug fixes, robust testing processes, and support for multiple versions of jQuery.

jQuery Mobile was conceived and announced in 2010, three years after the launch of jQuery. With jQuery Mobile, the project’s goals were to bring the ease-of-use of jQuery to HTML-capable mobile device browsers and to make it easier for developers to build progressively enhanced web applications. Led by Todd Parker of Filament Group, a development studio known for their work on cross platform and accessibility-first applications, jQuery Mobile launched its alpha release in October 2010.

In mid 2013, Jasper de Groot became the project lead and announced tighter development collaboration between jQuery UI and jQuery Mobile teams, sharing goals, roadmaps and code in order to ease the workload for both groups.

In 2015, the jQuery Foundation merged with the Dojo Foundation to form the "JS Foundation".

In September 2016, The UI team and jQuery Mobile teams merged, and the group focused more on maintenance and compatibility with jQuery Core.

jQuery UI and jQuery Mobile became an "JS Foundation" Emeritus project in 2018, signifying that the goals of the project had been achieved.

In 2019, the "JS Foundation" merged with the Node.js Foundation to form the "OpenJS Foundation".

Via [jQuery maintainers update and transition jQuery UI as part of overall modernization efforts](https://openjsf.org/blog/2021/10/07/jquery-maintainers-update-and-transition-jquery-ui/) and [jQuery maintainers continue modernization initiative with deprecation of jQuery Mobile](https://openjsf.org/blog/2021/10/07/deprecation-of-jquery-mobile/)

## Why things changed

Reason 1: Browser differences and limitations have become less important for several reasons. The first is that standardization has improved. The major browser vendors (Apple, Google, Microsoft, and Mozilla) collaborate on web standards through the Web Hypertext Application Technology Working Group.

Reason 2: browsers are updated more quickly than in the past.

Reason 3: Frameworks.

jQuery is typically only used for updating a page, relying on the server to provide the initial page.

Today’s JavaScript frameworks allow us to break the UI into components, making it easier to scale up an application. Components are tightly coupled between HTML, code, and even CSS. It creates abstractions for easier synchronization between the data and the view. We don't need to hack the DOM element directly, and instead let the framework handle it.

Newer frameworks encourage the declarative paradigm. Developer describes the UI and the framework manages the behavior. jQuery is imperative, describing everything step-by-step.

Via [The history and legacy of jQuery](https://blog.logrocket.com/the-history-and-legacy-of-jquery/)

## Why is it still alive

It’s embedded in a number of large projects — most notably, the WordPress platform. Many WordPress themes and plugins rely on jQuery.

The jQuery library is also a foundational layer of some of today’s most popular JavaScript frameworks and toolkits, like AngularJS and Bootstrap (version 4.0 and below).


## Does JS have a future in modern development?

That'll be tomorrow's TIL. :-)
(Spoiler, no.)
