---
title: TIL the origins of the term Algorithms
date: 2022-06-24
published: true
tags: ['history','math', 'trivia']
series: false
canonical_url: false
description: "Muhammad ibn Musa al-Khwarizmi's name is a reference to where he was from — 'al-Khwarizm' means that he was from Khwarazm, a region roughly on the border of modern-day Uzbekistan and Turkmenista. Al-Khwarizmi was, somehow, recorded as something close to 'Algorizmi' which, in turn, became our all-purpose math word: algorithm."
layout: layouts/post.njk
---

In the year 800, Muhammad ibn Musa al-Khwarizmi figured out Pi (π) at five digits, or 3.1416.

That's pretty impressive, with π being useful for understanding the universe.

He wrote a book titled ["The Compendious Book on Calculation by Completion and Balancing"](https://en.wikipedia.org/wiki/The_Compendious_Book_on_Calculation_by_Completion_and_Balancing) which is among the first algebra books in history.

Sidenote: He also wrote a book called “On the Calculation with Hindu Numerals,” which is widely credited with spreading the decimal (base-10) numbering system used around the world today.

Muhammad ibn Musa al-Khwarizmi’s name is a reference to where he was from — “al-Khwarizm” means that he was from Khwarazm, a region roughly on the border of modern-day Uzbekistan and Turkmenista.

Al-Khwarizmi” was, somehow, recorded as something close to “Algorizmi,” which, in turn, became our all-purpose math word: algorithm.

Boom!

via [Wondrium Daily, Pi: The Most Important Number in the Universe?](https://www.wondriumdaily.com/the-origins-of-pi/)

via [Now I know, WHICH CAME FIRST, THE ALGORITHM OR THE PI?](https://nowiknow.com/which-came-first-the-algorithm-or-the-pi/)
