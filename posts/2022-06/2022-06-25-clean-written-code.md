---
title: TIL about this work of beautiful code
date: 2022-06-25
published: true
tags: ['cleancode', 'javascript', 'readability']
series: false
canonical_url: false
description: "I'm a strong believer in writing code for tomorrow's developer. I'm incredibly anti-code-golf. (It's a beautiful art form, but keep it out of production code!)"
layout: layouts/post.njk
---

I'm a strong believer in writing code for tomorrow's developer. I'm incredibly anti-code-golf. (It's a beautiful art form, but keep it out of production code!)

So this post [Mohammad Faisal's The Greatest React Developer I Ever Met](https://javascript.plainenglish.io/the-greatest-react-developer-i-ever-met-1e58df22bb71), is absolutely validating.

> It’s a shame that I couldn’t understand my own code (if I looked at it after 3–4 months) but his code was so beautiful that a junior developer like myself had absolutely no problem understanding what was going on there.

```js
export const SomeComponent = () => {

  const userInfo = getUserInfo();

  const profileDetails = getProfileDetailsOfUser(userInfo.id);

  const aboutData = extractAboutData(profileDetails);

  const personalData = extractPersonalData(profileDetails)

  return (
    <UserDetails>
      <About data={aboutData} />
      <PersonalInfo data={personalData} />
    </UserDetails>
  )
}
```

What I find beautiful is that you don't need to even understand React! But you absolutely can see what data is being pulled in, what's happening to the data, and how the data moves around.

Gorgeous.
