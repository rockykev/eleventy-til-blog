---
title: TIL add link decoration to help with accessibility
date: 2022-06-18
published: true
tags: ['links', 'css', 'accessibility', 'mdn']
series: false
canonical_url: false
description: "One of the WCAG requirements is not to rely on color only when you want to distinguish a button or link from the rest of the text. Painting links in blue or another color doesn’t suffice since it still might not be visible for people with color blindness. The most typical method is underlining links; they can also appear in bold font."
layout: layouts/post.njk
---

I like this post a lot: [Designing Better Links For Websites And Emails: A Guideline](https://www.smashingmagazine.com/2021/12/designing-better-links-websites-emails-guideline)

> One of the WCAG requirements is not to rely on color only when you want to distinguish a button or link from the rest of the text. Painting links in blue or another color doesn’t suffice since it still might not be visible for people with color blindness. The most typical method is underlining links; they can also appear in bold font.

In other words, don't just slap a color on a link. Add more decoration like bold, or underline.

Example:
![](2022-06-18-accessibility-links_0.png)
