---
title: TIL Never deserializing untrusted data
date: 2022-06-05
published: true
tags: ['php','security', 'vulnerability', 'json']
series: false
canonical_url: false
description: "This happened the most in PHP, because for some reason, PHP developers love to serialize/deserialize objects instead of using JSON, but I’d say almost every case we saw where a server was deserializing a client object and parsing it led to a horrible exploit."
layout: layouts/post.njk
---

I was reading this post: [Learnings from 5 years of tech startup code audits](https://kenkantzer.com/learnings-from-5-years-of-tech-startup-code-audits/)

One major takeaway:

"Never deserialize untrusted data."

> This happened the most in PHP, because for some reason, PHP developers love to serialize/deserialize objects instead of using JSON, but I’d say almost every case we saw where a server was deserializing a client object and parsing it led to a horrible exploit.

I didn't really understand what the problem is. As primarily as JavaScript developer, I'd assume you'd WANT to turn external data to the format that the language prefers, right?

## What is Unserialize()

When you serialize an object, you create a string representation of it.

By using unserialize, we achieve exactly the opposite. Instead of turning an object into a string, we do it the other way around, and turn it into an object.

## Avoid passing user-controlled input to dangerous functions

> In PHP, as in every other programming language you use for web development, developers should avoid writing code that passes user-controlled input to dangerous functions. This is one of the basics of secure programming. Whenever a function has the capability to execute a dangerous action, it should either not receive user input, or the user-controlled data should be sanitized in order to prevent a malicious user from breaking the intended functionality.

via [Why You Should Never Pass Untrusted Data to Unserialize When Writing PHP Code](https://securityboulevard.com/2018/03/why-you-should-never-pass-untrusted-data-to-unserialize-when-writing-php-code/)

And when you visit the PHP docs on [unserialize](https://www.php.net/manual/en/function.unserialize.php), it even warns you!

![](2022-06-05-deserialize-untrusted-data_0.png)

> Do not pass **untrusted user input** to unserialize() regardless of the options value of allowed_classes.

Overall, this function can be abused by attackers to gain remote code execution, local file inclusion and a wide range of other vulnerabilities, depending on the code within available magic methods. Attackers can abuse this by deserializing their own malicious PHP objects.

## How to fix

> The fix? It’s far better to allow a user to send a JSON object (it has so few possible data types), and to manually construct the object based on the fields in that object. It’s slightly more work, but well worth it!
