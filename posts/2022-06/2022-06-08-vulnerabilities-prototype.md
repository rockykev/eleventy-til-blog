---
title: TIL obvious security vulnerabilities and prototype pollution
date: 2022-06-08
published: true
tags: ['vulnerabilities', 'security', 'javascript']
series: false
canonical_url: false
description: "Object-based inheritance gives JavaScript the flexibility and efficiency that web programmers have come to love – but it also makes it vulnerable to tampering. Malicious actors can make application-wide changes to all objects by modifying object, hence the name prototype pollution."
layout: layouts/post.njk
---

I was reading this post: [Learnings from 5 years of tech startup code audits](https://kenkantzer.com/learnings-from-5-years-of-tech-startup-code-audits/)

I shared another TIL from it.

But this one is super interesting:

## Obvious Security Vulnerabilities

> All the really bad security vulnerabilities were obvious.

> Hackers are lazy and they look for the lowest-hanging fruit. They won’t care about finageling even a very severe heap-spray vulnerability if they can reset a user’s password because the reset token was in the response (as [Uber found out circa 2016](https://hackerone.com/reports/173551)).

> The counterargument to this is that heavily weighting discoverability perpetuates ”Security by Obscurity,” since it relies so heavily on guessing what an attacker can or should know.

> But again, personal experience strongly suggests that in practice, discoverability is a great predictor of actual exploitation.

This has been my experience too.

We are using a service where white-hat hackers attack our sites, and point out the flaws. From my experience, it looks like they're using a handful of tools to automate all the normal tests and call it a day.

If the bounty was high enough, or there was enough of a incentive, I might see better results.



## Hell yeah frameworks

The other interesting thing was this one.

> Secure-by-default features in frameworks and infrastructure massively improved security... things like React default escaping all HTML to avoid cross-site scripting, and serverless stacks taking configuration of operating system and web server out of the hands of developers, dramatically improved the security of the companies that used them.


## Untrusted data & Prototype Pollution

> The common thread in all deserialization vulnerabilities is that giving a user the ability to manipulate an object that is subsequently used by the server is an extremely powerful capability with a wide surface area.

>  It’s conceptually similar to both prototype pollution, and user-generated HTML templates.

You might be familiar with [__proto__](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/proto), or dunderproto (double underscore proto)

> When new objects are created, they carry over the properties and methods of the prototype “object”, which contains basic functionalities such as toString, constructor and hasOwnProperty.

Object-based inheritance gives JavaScript the flexibility and efficiency that web programmers have come to love – but it also makes it vulnerable to tampering.

Malicious actors can make application-wide changes to all objects by modifying object, hence the name prototype pollution.

For example, this is possible:

```js
let customer = {name: "person", address: "here"}
console.log(customer.toString())
//output: "[object Object]"

customer.__proto__.toString = ()=>{alert("polluted")}

console.log(customer.toString())
// alert box pops up: "polluted"
```

We override a default function `toString()`, causing major disruption.

## History of Attacks

Apparently, there was a security vulnerability in Lodash about this in 2018 -- [Prototype Pollution](https://security.snyk.io/vuln/SNYK-JS-LODASHMERGE-173732).

In 2019, it was [discovered in jQuery too](https://snyk.io/blog/after-three-years-of-silence-a-new-jquery-prototype-pollution-vulnerability-emerges-once-again/), leaving many web applications vulnerable to such assaults.

```js
let a = $.extend(true, {}, JSON.parse('{"__proto__": {"devMode": true}}'))
console.log({}.devMode); // true
```

In fact, that whole post shows you how to re-create that bug in so many different ways! It's pretty crazy!

Security is fun.

REFERENCES
[Prototype pollution: The dangerous and underrated vulnerability impacting JavaScript applications](https://portswigger.net/daily-swig/prototype-pollution-the-dangerous-and-underrated-vulnerability-impacting-javascript-applications)
