---
title: TIL the different Javascript Engines
date: 2022-06-01
published: true
tags: ['javascript','node']
series: false
canonical_url: false
description: "There's dozens of JS engines. V8 ,Chakra, Spidermonkey, JavascriptCore Webkit, JerryScript..."
layout: layouts/post.njk
---

I was thinking about [Interop 2022](https://css-tricks.com/newsletter/293-interop-on-interop/) and it works overall.

So ECMAScript is managed by [tc39](https://github.com/tc39/proposals), who decides the future of Javascript.

But how companies implement Javascript is different. For example: Just because `fetch` exists as a spec, doesn't mean every engine does it exactly the same.

That made me think, what are those engines?

| JS runtime      | Engine |
| ----------- | ----------- |
| Google Chrome      | [V8](https://v8.dev/) |
| Edge (After 2018)  | [V8](https://v8.dev/)  |
| NodeJS | [V8](https://v8.dev/) |
| Edge (Pre-Dec 2018)  | [Chakra](https://github.com/chakra-core/ChakraCore)  |
| OG Internet Explorer | Chakra(Jscript) |
| Mozilla Firefox | [Spider Monkey](https://spidermonkey.dev/) |
| Mozilla | [Rhino](https://github.com/mozilla/rhino) |
| Safari | [JavascriptCore](https://trac.webkit.org/wiki/JavaScriptCore) Webkit |
| Opera | [Carakan](https://dev.opera.com/blog/carakan/) |
| Internet of Things | [JerryScript](https://jerryscript.net/) |

This whole thought was kicked off via this episode of [Syntax - Why do we need Web Interop? Another Standards Body?](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkLnN5bnRheC5mbS9yc3M/episode/Y2QzYWMwNmItOGM1ZC00Y2ZkLThlZGItN2EwYWJiMWI4M2Fm?sa=X&ved=0CAUQkfYCahcKEwigot69_Mv4AhUAAAAAHQAAAAAQLA)
