---
title: TIL the original Pong was pure hardware
date: 2022-06-28
published: true
tags: ['logicgates','reddit', 'hardware']
series: false
canonical_url: false
description: "A simple gate example would be a OR gate. It gets two connections on the input side and 1 on the output side..."
layout: layouts/post.njk
---

![](2022-06-28-pong-pure-hardware_0.png)

## How it works (ELI5)

Series of logic gates.

Given an input of high or low power, the gates form an output.

A simple gate example would be a OR gate. It gets two connections on the input side and 1 on the output side.

Each of those connections will be either in a high power state or low power state. Let’s call high power 1 and low power 0.

The gate will output a 1 when either input is 1.

NAND, NOR, AND, XOR and many other gate combinations exist.

AND gates will only output a 1 when both inputs are 1.

Used in combination, you create logical patterns that form more complex machines. This is the foundation of any computational logic.

Go play with some gates on logic puzzles. They are not difficult to grasp with a little practice.

via [CMFETCU](https://www.reddit.com/r/EngineeringPorn/comments/ul49zt/comment/i7t4xok/?utm_source=reddit&utm_medium=web2x&context=3)

How the [schematics work](https://web.archive.org/web/20220705185552/https://www.pong-story.com/LAWN_TENNIS.pdf).

---

> I worked on the PS3 platform, and I remember learning that the original release was backwards compatible with PS2 games by including the entire PS2 system on a single chip, a little thing off to the side.

Via [CalmCalmBelong](https://www.reddit.com/r/EngineeringPorn/comments/ul49zt/comment/i7thd83/?utm_source=reddit&utm_medium=web2x&context=3)

----
> Many, many first-generation console games were written in assembly. That used to be the best way to squeeze out every bit of performance and optimization from the limited hardware (because high level language compilers were much less sophisticated). Hell, some games today still use hand-coded ASM inline with their high level code for routines that require extreme optimization.

Via [PM-ME-YOUR-HANDBRA](https://www.reddit.com/r/EngineeringPorn/comments/ul49zt/comment/i7tkrp0/?utm_source=reddit&utm_medium=web2x&context=3)

[TIL of Rollercoaster Tycoon written in Assembly](https://www.reddit.com/r/todayilearned/comments/131q6b/til_roller_coaster_tycoon_was_programmed_by_one/) and a [random blogpost](https://www.pcgamesn.com/rollercoaster-tycoon/code-chris-sawyer) about it.

----

> Huh, so that scene in That 70's Show where Red and Kelso are trying to change the circuitry to adjust the length of the paddles wasn't too far off.

Via [SHOCK_VALUE_USERNAME](https://www.reddit.com/r/EngineeringPorn/comments/ul49zt/comment/i7tgp81/?utm_source=reddit&utm_medium=web2x&context=3)

and the [reddit post about it](https://www.reddit.com/r/electronics/comments/1185gp/in_that_70s_show_kelso_and_red_try_to_configure/)!

For more schematics of hardware: https://www.righto.com/


Reference:
[Via Reddit post](https://www.reddit.com/r/EngineeringPorn/comments/ul49zt/the_original_pong_video_game_had_no_code_and_was/)
