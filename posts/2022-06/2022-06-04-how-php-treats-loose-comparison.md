---
title: TIL how PHP treats the loose comparison (==) sign
date: 2022-06-04
published: true
tags: ['php','security']
series: false
canonical_url: false
description:
layout: layouts/post.njk
---

`5 == "5 of something"` is in practice treated as `5 == 5`.

PHP will effectively convert the entire string to an integer value based on the initial number. The rest of the string is ignored completely!

But if there's no number, it treats it as true!

`0 == "Example string" // true`

When does it become a problem?


```php
$login = unserialize($_COOKIE);

if ($login['password'] == $password) {
// log in successfully
}
```

Let's say an attacker modified the password attribute so that it contained the integer `0` instead of the expected string.

As long as the stored password does not start with a number, the condition would always return `true`, enabling an authentication bypass.

Note that this is only possible because deserialization preserves the data type. If the code fetched the password from the request directly, the 0 would be converted to a string and the condition would evaluate to false.

Via [Exploiting insecure deserialization vulnerabilities](https://portswigger.net/web-security/deserialization/exploiting)

