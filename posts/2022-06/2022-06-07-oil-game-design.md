---
title: TIL about Oil in Game dev
date: 2022-06-07
published: true
tags: ['gamedev', 'video']
series: false
canonical_url: false
description: "It 'feels' wrong when Link gets stuck on a block. Nintendo is fantastic at 'oiling', smoothing out this process so Link automatically moves downwards when he hits a block."
layout: layouts/post.njk
---

It's all about the details.

[Juice it or lose it - a talk by Martin Jonasson & Petri Purho](https://www.youtube.com/watch?v=Fy0aCDmgnxg&ab_channel=grapefrukt) is one of the most influential game dev videos ever for game devs, about creating these little details that create these amazing 'feels'.

Another one that I learned about recently (TIL!) is:

**Oil It or Spoil It**

> Whereas Juice is highly visible, Oil is really something you only notice when it's missing. A well-oiled hinge is smooth and silent, but a rusty one squeaks, groans, and annoys the crap out of you. If juice is all about making your game come alive and enriching interactions by maximizing the output you get for a single input, Oil is about minimizing the friction and effort that goes into making an input in the first place.

tl;dr -

Juice adds pleasure,
Oil removes pain.

![](2022-06-07-oil-game-design_0.png)

Here's an example. It 'feels' wrong when Link gets stuck on a block. Nintendo is fantastic at 'oiling', smoothing out this process so Link automatically moves downwards.

The whole post here goes into other details, like 'oil' in classic RPGs that ended up being smoothed out. 0

[Oil It or Spoil It](https://www.fortressofdoors.com/oil-it-or-spoil-it/)

For the full post:
[The 7 game dev resources that are SO GOOD they will change how you SPEAK](https://www.valadria.com/the-7-game-dev-resources-that-are-so-good-they-will-change-how-you-speak/)
-----------
