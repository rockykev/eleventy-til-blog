---
title: TIL an easy way to make rainbow text
date: 2022-06-10
published: true
tags: ['css', 'colors']
series: false
canonical_url: false
description: "You can create gradient text by tweaking just three simple properties: background-image, color, and background-clip!"
layout: layouts/post.njk
---

This neat trick!

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">CSS TIP 💡✨🌈<br><br>You can create gradient text by tweaking just three simple properties: background-image, color, and background-clip! <br><br>CodePen: <a href="https://t.co/5VpMsyIE9C">https://t.co/5VpMsyIE9C</a> <a href="https://t.co/qHLIRPDHOn">pic.twitter.com/qHLIRPDHOn</a></p>&mdash; Ale ☻ (@pokecoder) <a href="https://twitter.com/pokecoder/status/1466582413476200449?ref_src=twsrc%5Etfw">December 3, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


```css
.gradient-text {
    background-image: linear-gradient(45deg, blue, red);
    color: transparent;
    -webkit-background-clip: text;
}
```

<p class="codepen" data-height="300" data-default-tab="result" data-slug-hash="gOGpKqM" data-user="pokecoder" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/pokecoder/pen/gOGpKqM">
  Gradient Text</a> by Ale (<a href="https://codepen.io/pokecoder">@pokecoder</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

