---
title: TIL the multiple value in html
date: 2022-06-15
published: true
tags: ['html', 'input', 'mdn']
series: false
canonical_url: false
description: "The multiple attribute allows the user to enter multiple values on an '<input>'. It is a boolean attribute valid for file or email input types and the '<select>'
 element."
layout: layouts/post.njk
---

The multiple attribute allows the user to enter multiple values on an `<input>`. It is a boolean attribute valid for file or email input types and the `<select>` element.

## On a `<select>`

Most browsers displaying a scrolling list box for a `<select>` control with the multiple attribute set versus a single line dropdown when the attribute is omitted.

![](2022-06-15-multiple-attribute-html_0.png)

```html
  <select multiple name="cars" id="cars">
    <option value="volvo">Volvo</option>
    <option value="saab">Saab</option>
    <option value="opel">Opel</option>
    <option value="audi">Audi</option>
  </select>
  <br><br>
  <input type="submit" value="Submit">
```

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="WNzeOZX" data-user="rockykev" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/WNzeOZX">
  Untitled</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>

## on a input `type="email"`

For an email input, if and only if the multiple attribute is specified, the value can be a list of comma-separated email addresses. Any whitespace is removed from each address in the list.

```html
<input type="email" multiple name="emails" id="emails">
```

The `email` input displays the same, but will match the :invalid pseudo-class if more than one comma-separated email address is included if the attribute is not present.

## on a input `type="file"`

When multiple is set on the file input type, the user can select one or more files. The user can choose multiple files from the file picker in any way that their chosen platform allows (e.g. by holding down Shift or Control, and then clicking).

```html
<input type="file" multiple>
```

[MDN Multiple](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/multiple)
