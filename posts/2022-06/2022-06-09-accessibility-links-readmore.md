---
title: TIL a Accessible way to solve Read More
date: 2022-06-09
published: true
tags: ['accessibility', 'links', 'webdev', 'html']
series: false
canonical_url: false
description: "Screenreaders help sight-impaired visitors have a better experience. The most annoying thing on a news website is to hear this: Read more — link, read more — link, read more — link. There's two ways to solve it."
layout: layouts/post.njk
---

I do a lot of work with web accessibility at work. *(Not on this blog. I don't even think anybody reads this! If you're a visitor who needs accessibility, send me a email and details of your use-cases!)*

Screenreaders help sight-impaired visitors have a better experience. The most annoying thing on a news website is to hear this:

“Read more — link, read more — link, read more — link”

There's two ways to solve it.

## Solution 1

![](2022-06-09-accessibility-links-readmore_0.png)

No more read mores. Just link the whole thing.

## Solution 2

Aria-labels!

```html
<h4>News</h4>
<p>Eleks Design Team will participate in the Space Hackathon.
<a href="aerospace-hackathon.html" aria-label="Read more about Eleks participation in the Space Hackathon">Read more...</a>
</p>
<p>Projector Tech and Creative Institute launches five courses on web accessibility this year.
<a href="new-courses.html" aria-label="Read more about new courses on accessibility by Projector Institute">Read more...</a>
</p>
```

via [Designing Better Links For Websites And Emails: A Guideline](https://www.smashingmagazine.com/2021/12/designing-better-links-websites-emails-guideline)
