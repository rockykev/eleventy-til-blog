---
title: TIL a funky bug that crashed Google Docs
date: 2022-06-02
published: true
tags: ['bug', 'googledocs']
series: false
canonical_url: false
description: 'On May 5, 2022... a user crashed Google Docs by typing: And. And. And. And. And.'
layout: layouts/post.njk
---

This bug doesn't exist anymore.
But -- On May 5, 2022...

a user crashed Google Docs by typing:

`"And. And. And. And. And."`

![](2022-06-02-googledocs-bugs_0.png)

> It's case-sensitive. So trying with "and. and. And. and. And." doesn't cause it to crash.

> I've only tried in Google Chrome, with documents from three separate Google accounts (personal, G Suite Basic, and work one which might be enterprise). All three experience this same issue.

> When filtering the Chrome Dev tools network tab with the text "error", one can see a POST request being made to url starting with https://docs.google.com/document/u/1/jserror, and the payload says it's "Caused by: TypeError: Cannot read properties of null (reading 'C')":

via [Bleeping Support](https://www.bleepingcomputer.com/news/technology/google-docs-crashes-on-seeing-and-and-and-and-and/) and [Google Support](https://support.google.com/docs/thread/162510194/including-and-and-and-and-and-in-a-google-doc-causes-it-to-crash?hl=en)
