---
title: TIL the wbr element
date: 2022-06-29
published: true
tags: ['mdn','html', 'css']
series: false
canonical_url: false
description: "'<wbr>' is a HTML element that represents a word break opportunity—a position within text where the browser may optionally break a line, though its line-breaking rules would not otherwise create a break at that location."
layout: layouts/post.njk
---

`<wbr>` is a HTML element that represents a word break opportunity—a position within text where the browser may optionally break a line, though its line-breaking rules would not otherwise create a break at that location.

It looks like this:
```html
<!-- normal -->
<p>https://subdomain.somewhere.co.uk</p>

<!-- <wbr> -->
<p>https://subdomain<wbr>.somewhere<wbr>.co<wbr>.uk</p>
```

![](2022-06-29-wbr-element_0.png)

Is it useful? Meh.

I highly recommend reading [the original post](https://codersblock.com/blog/deep-dive-into-text-wrapping-and-word-breaking/?ref=sidebar) and instead, do line-breaks with CSS! (Or not. I"m not a cop.)

Via [Deep Dive into Text Wrapping and Word Breaking](https://codersblock.com/blog/deep-dive-into-text-wrapping-and-word-breaking/?ref=sidebar)

[MDN page](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/wbr)
