---
title: TIL how to open up your webcam with html
date: 2022-06-06
published: true
tags: ['webdev', 'input', 'webcam']
series: false
canonical_url: false
description: "This is done with the 'capture' attribute which can take two values."
layout: layouts/post.njk
---

This is done with the `capture` attribute which can take two values.

```html
// capture 'user' for front camera
<input type="file" capture="user" accept="image/*">

// capture 'environment' for front camera
<input type="file" capture="environment" accept="image/*">
```

> If this attribute is missing, the user agent is free to decide on its own what to do. If the requested facing mode isn't available, the user agent may fall back to its preferred default mode.

[MDN on input type file capture](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#capture)
