---
title: TIL to minimize internally-made acronyms
date: 2022-06-19
published: true
tags: ['jobstory', 'lifetip']
series: false
canonical_url: false
description: "Individually, a few acronyms here and there may not seem so bad, but if a thousand people are making these up, over time the result will be a huge glossary that we have to issue to new employees."
layout: layouts/post.njk
---

I'm not a fan of Elon Musk (or Steve Jobs, or [most famous people here]).

But we stand on the shoulders of giants, even if the giant believes in abusive workplaces. We can still pick up some things.

In May 2010, Musk sent a company-wide email to all employees at SpaceX. Something was bothering him and he wanted everyone’s attention. The subject line of his email? **Acronyms Seriously Suck**. Here’s an excerpt.

> "There is a creeping tendency to use made up acronyms at SpaceX. Excessive use of made up acronyms is a significant impediment to communication and keeping communication good as we grow is incredibly important.

> Individually, a few acronyms here and there may not seem so bad, but if a thousand people are making these up, over time the result will be a huge glossary that we have to issue to new employees. No one can actually remember all these acronyms and people don’t want to seem dumb in a meeting, so they just sit there in ignorance. This is particularly tough on new employees."

The actions:

1. Make a list of the acronyms you frequently use at work.

2. For each one, ask yourself whether using the acronym helps or hurts communication.

3. If it helps communication, make sure all employees you interact with know what the acronyms stand for.

4. If using the acronym hurts communication, stop using it and encourage your colleagues to follow suit.

[Acronyms Seriously Suck](https://www.linkedin.com/pulse/acronyms-seriously-suck-lesson-from-elon-musk-nathan-tanner/)
