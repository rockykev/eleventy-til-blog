---
title: TIL how to explain Typescript like i'm 5
date: 2022-06-31
published: true
tags: ['javascript','eli5', 'typescript']
series: false
canonical_url: false
description: "Typescript is quickly becoming a powerful tool for Javascript developers. Typescript provides optional static typing."
layout: layouts/post.njk
---

Typescript is quickly becoming a powerful tool for Javascript developers. Typescript provides optional static typing.

It lets you:
* avoid weird errors like `'undefined' is not a function.`
* It warns you of type errors within the editor, and not in the compiler stage.
* It allows you to define the 'type' of what's expected. No longer can you accidentally pass a string when your function was looking for a object.


I really like this reddit comment that ELI5:

The thing you have to remember is: TS is not a strictly typed language. It attempts to emulate it, but because JS is valid TS, you can break types whenever you want. It's only as strict (and therefore as useful) as you make it. It's why good projects turn strict on in the tsconfig and do not allow any. Even one any will break the chain of custody, because the TS is being asked not to worry about it and blindly assume all's well. It's no longer useful as a check.

Say I'm a giftshop, and I want to sell gift-wrapped bananas. We have full end to end checks to be sure we're dealing only with the best bananas modern science can grow. They never accidentally get swapped out with other fruit. We usually do the same with the giftwrapping but sometimes we ask you just trust that this giftwrapping is legit. Joe's been giftwrapping bananas for decades. He knows what he's doing. On paper, all looks good! Banana checks all the way, giftwrapping checks usually except some areas where we ask you trust us. CI says green, so ship it!

Suddenly I start receiving bananas in lockboxes, bananas wrapped in seven layers of packing tape, or even live gorillas holding bananas. Where do you think the problem occurred?

Does this restrict the libraries you can use? Cause some don't have TS? Absolutely, and in 2022 this feels easy to do, honestly. I do not want a library that can't be bothered to publish their own types. I cannot trust them.

Many companies even say TS isn't enough. We need GraphQL or swagger to type our services as well, so consumers understand the shape of data they're getting. This is a good thing.

The perils of not having types is expressed in JS in many ways actually. Its very loosey goosey with numbers and strings, because it's trying never to fail. It assumes this might happen because there's no types and JS was originally made in just a few days. But when your data is really important, an attitude of "JesuS take the wheel" doesn't cut it.

via [zephyrtr](https://www.reddit.com/r/webdev/comments/umxrf1/comment/i84znnv/?utm_source=reddit&utm_medium=web2x&context=3), with the original post [Typescipt is making me want to quit my job and become a farmer](https://www.reddit.com/r/webdev/comments/umxrf1/comment/i84znnv/?context=3)
