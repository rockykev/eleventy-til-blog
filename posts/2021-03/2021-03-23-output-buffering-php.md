---
title: TIL output buffering in PHP
date: 2021-03-23
published: true
tags: ['php']
series: false
canonical_url: false
description: "If you are outputting a lot of content in php, use ob_start()"
layout: layouts/post.njk
---

If you are outputting a lot of content, then you should use:

```php
add_shortcode('my-shortcode', 'shortcode_function');

function shortcode_function( $args ) {
  ob_start();
  ?>
  <!-- your contents/html/(maybe in separate file to include) code etc -->
  <?php

  return ob_get_clean();
}
```

> Without output buffering (the default), your HTML is sent to the browser in pieces as PHP processes through your script.
> With output buffering, your HTML is stored in a variable and sent to the browser as one piece at the end of your script.

![](2021-03-23-output-buffering-php_0.png)

![](2021-03-23-output-buffering-php_1.png)

REFERENCE:
https://stackoverflow.com/a/2832179/4096078
https://stackoverflow.com/a/40500555/4096078
