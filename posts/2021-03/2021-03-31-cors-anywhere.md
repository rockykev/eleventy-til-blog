---
title: TIL about the CORS-ANYWHERE project
date: 2021-03-31
published: true
tags: ['webscraping', 'heroku']
series: false
canonical_url: false
description: "I was building a web scrapper for a game using Vue, Axios, and Cheerio."
layout: layouts/post.njk
---

I was building a web scrapper for a game using Vue, Axios, and Cheerio.

I kept running into the CORS policy error.
> ... has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.

Since my tool was a pure Frontend application (Not Node), I had to find a workaround.

The solution is [cors-anywhere](https://github.com/Rob--W/cors-anywhere).

Request Examples:
`http://localhost:8080/http://google.com/` - Google.com with CORS headers

It's best to create your own microservice for it.

I did it by:

1. Signing up for a free [Heroku](https://dashboard.heroku.com/) account

2. Forking the [cors-anywhere](https://github.com/Rob--W/cors-anywhere) repo.

3. Creating a new app in Heroku.

4. Finally, connecting the forked repo to Heroku.

With that new URL, I can now web scrape away!


![](2021-03-31-cors-anywhere_0.png)




