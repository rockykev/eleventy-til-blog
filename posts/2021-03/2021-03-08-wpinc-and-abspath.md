---
title: TIL about WPINC and ABSPATH
date: 2021-03-08
published: true
tags: ['wordpress']
series: false
canonical_url: false
description: "WPINC & ABSPATH -- If this file is called directly, abort."
layout: layouts/post.njk
---

I've been seeing this a lot in WordPress plugins.
Sometimes it's `WPINC`, and sometimes it's `ABSPATH`.

Only after 5 years have I decided to figure out what's happening under the hood.

```php
<?php
// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

```

> if ( ! defined( 'WPINC' ) ) die; and if ( ! defined( 'ABSPATH' ) ) exit; add an extra layer of security by preventing any direct access to your plugin file. ABSPATH is a PHP constant defined by WordPress in its core.

> If your plugin file is accessed from outside of WordPress, the constant ABSPATH or WPINC will not be defined, so it exits the plugin code, preventing any unauthorized access to your code.

> ABSPATH and WPINC are defined in WordPress core as:

> define( 'ABSPATH', dirname(dirname(__FILE__)) . '/' );
> define( 'WPINC', 'wp-includes' );
> Both are used for same purpose.

Via https://wordpress.stackexchange.com/a/196544/132722

And from another comment:
> Given these are used for security, I'd go for ABSPATH. Rationale being that ABSPATH is not only defined first in the WP bootstrapper, WPINC is defined under a conditional in the same file and, as a result, is more likely to experience a future regression.

Via https://wordpress.stackexchange.com/a/269716/132722

REF:
https://wordpress.stackexchange.com/questions/108418/what-are-the-differences-between-wpinc-and-abspath
