---
title: TIL Scroll Margin for anchor links
date: 2021-03-03
published: true
tags: ['css', 'html']
series: false
canonical_url: false
description: "When you use anchor links, it jumps to the content with the assigned ID. But it sucks."
layout: layouts/post.njk
---

When you use anchor links, it jumps to the content with the assigned ID.

Example:
`website.com/#title2`

will take you to the element
`<h2 id="title2">IM TITLE 2</h2>`

Unfortunately, when the site jumps to it, it's pretty awful. It jumps PAST the content.

We can now use this little snippet to control that jump.

```css
[id] {
  scroll-margin-top: 2ex;
}
```



For one site, I actually modified it to
```css
[id] {
  scroll-margin-top: 25ex;
}
```

Your milage will vary.

REFERENCE:
https://www.smashingmagazine.com/2021/02/things-you-can-do-with-css-today/

