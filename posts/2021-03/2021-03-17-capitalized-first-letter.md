---
title: TIL this one-line script to capitalize the first letter of a word
date: 2021-03-17
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "Other langauges have this. JS has to do it manually"
layout: layouts/post.njk
---

A short one-line script:

Vanilla Javascript:

`const capitalize = (str) => str.charAt(0).toUpperCase() + str.slice(1)`


Lodash
```
const _ = require('lodash');

_.capitalize('dog'); // 'Dog'
```

If it's frontend -- It's always preferrable and more performant to use CSS.
```
.capitalize {
  text-transform: capitalize;
}
```



REFERENCE:
https://javascript.plainenglish.io/15-helpful-javascript-one-liners-946e1d1a1653
and
https://masteringjs.io/tutorials/fundamentals/capitalize
