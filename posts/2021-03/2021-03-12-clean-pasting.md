---
title: TIL Clean Pasting
date: 2021-03-12
published: true
tags: ['html']
series: false
canonical_url: false
description: "Clean Paste"
layout: layouts/post.njk
---


This is more in the 'How many times do we have to teach you this lesson, Old Man?' area.

> When you copy text from the internet, a bunch of formatting and HTML gets copied, too. Copy a link’s text and paste it into Mac Notes, and there’s some of the text formatting of the website, plus the link.

> I almost never want this extra stuff, so I use an alternate paste dubbed the ‘Clean Paste.’
> On MacOS, instead of `CMD + V`, try `CMD + OPTION + SHIFT + V`. You’ll get just the text, minus all the extras.

ADDED THIS: On Windows, it's `CTRL + SHIFT + V`.

Another option is to:

1. Paste without formatting, if that exists.
2. Paste in a notepad file (Windows only?). (Mac's Notepad does save styles, so I dunno. I guess you can paste it in a .md file or in Nano/VIM but if you know what that means, you probably already know about this pitfall.)


Also via that post:

> Note: I learned this via the newsletter [Recomendo](https://www.recomendo.com/), which I (pause for effect) recommend. It’s so helpful me and I’ve looked it up so many times, that I’m re-sharing it here.


REFERNCE:
https://til.hashrocket.com/posts/fazma1l5nu-clean-paste-
