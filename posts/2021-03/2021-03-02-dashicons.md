---
title: TIL Dashicons - WordPress' Official Icon Font
date: 2021-03-02
published: true
tags: ['wordpress', 'icons']
series: false
canonical_url: false
description: "WordPress has their own Icon Font."
layout: layouts/post.njk
---

WordPress has their own Icon Font.

Here it is:
https://developer.wordpress.org/resource/dashicons/#smiley

## Usage:

```php
function wpdocs_add_my_custom_menu() {
    // Add an item to the menu.
    add_menu_page(
        __( 'My Page', 'textdomain' ),
        __( 'My Title', 'textdomain' ),
        'manage_options',
        'my-page',
        'my_admin_page_function',
        'dashicons-admin-media'
    );
}
```

```html

<h2 class="dashicons-before dashicons-smiley">A Cheerful Headline</h2>

<h2><span class="dashicons dashicons-smiley"></span> A Cheerful Headline</h2>
```

## The future

Dashicon might not be fully replaced, but they will be using SVGs in the future.

> In an effort to move things forward, and per discussion in the core design chat (link requires registration), it was suggested that an ultimate release of Dashicons be made, to wrap up existing requests (adding 36 new icons), close down to new requests, and to focus future efforts on the new Icon component.

Via https://make.wordpress.org/design/2020/04/20/next-steps-for-dashicons/

