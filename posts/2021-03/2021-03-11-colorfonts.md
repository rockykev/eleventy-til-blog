---
title: TIL Colorfonts
date: 2021-03-11
published: true
tags: ['css', 'design']
series: false
canonical_url: false
description: "But what if you could define more than one color per glyph? That's ColorFonts!"
layout: layouts/post.njk
---

## What are Color Fonts?

> But what if you could define more than one color per glyph? What if you could make your letters blue and purple, or have gradients running between blue and purple, or even have half a dozen colors or more applied to a single font family?

> Well, with the emergence of OpenType color fonts, you can do just that.

![](2021-03-11-colorfonts_0.png)
via https://design.tutsplus.com/tutorials/what-are-color-fonts--cms-31467


## More history
> The OpenType-SVG font format was initially designed by Mozilla & Adobe and became an industry standard in early 2016, when other big players including Microsoft & Google agreed on a single format to support color fonts.

> All of them (including Apple) have previously developed and implemented their own proprietary color formats to display emojis on their operating systems, while many other companies built other custom color font technologies for the gaming, video or print industries.

> There are now four major color font formats that fit into regular font files: `SBIX`, `COLR`, `CBDT` and `SVG`, each having it own specificities. Read the full story here or check this simplified recap:

Via https://www.colorfonts.wtf/

## Support
There's `SVG`, and there's also `COLR`

I'm a bit confused at this. Either caniuse.com isn't accurate, or something else.
But SVG fonts are not usable on any modern browser except Safari.
[https://caniuse.com/svg-fonts](https://caniuse.com/svg-fonts)

(Note - a [issue](https://github.com/Fyrd/caniuse/issues/2458) is listed in the repo)

According to https://www.colorfonts.wtf/
![](2021-03-11-colorfonts_1.png)

Additionally - this piece of text about `COLR`
> COLR is supported by all major web browsers, and you can use such OpenType color fonts in more and more Windows software like Word and paint.net. The file size of OpenType COLR is usually significantly smaller than the OpenType SVG variant. Although FontCreator doesn’t support it yet, it is also good to know OpenType COLR is the only color extension that works with variable fonts.
> via https://www.high-logic.com/font-editor/fontcreator/tutorials/create-opentype-color-fonts#:~:text=COLR%20is%20supported%20by%20all,than%20the%20OpenType%20SVG%20variant.

While ChromaCheck checks which OpenType color formats are supported in this browser, and
it states that `SBIX` and `COLR` are supported, but not `SVG`. (The SVG part is accurate)
https://pixelambacht.nl/chromacheck/

