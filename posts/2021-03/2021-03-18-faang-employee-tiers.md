---
title: TIL about FAANG's employee tiers
date: 2021-03-18
published: true
tags: ['workplace']
series: false
canonical_url: false
description: "Facebook/Google Tier system"
layout: layouts/post.njk
---

I never understood the tier system.
I did know that the salary & benefits expectations are massively different from one and the other.

Check out [levels.fyi](https://www.levels.fyi/) to see the packages at major companies.

But still, what does it mean?

## Expectations
Responsibilities and expectations.
(This was yoinked completely from https://www.forbes.com/sites/quora/2013/12/09/what-is-the-expectation-out-of-each-software-engineering-level-at-facebook/?sh=5b0014163bce, which is a blog post from a Quora answer.)

Also note - this article is from 2013. Roles may be slightly more defined, but the core set of responsibilities are still provide a roadmap.
### E3

* Write code and tests, push to production under basic supervision.

Know how to use source control, code review tools, how run the code locally."I have added this button, and now, with this dependency I forgot and just added, it is on the live site!"(Interns effectively fit here, with less or no production access.)

### E4

* Understand design and applications of what is being developed.

* Understand production change mechanisms.

* Make minor design decisions w/o supervision.

Write documentation (not applicable to Facebook, but applies to Google, that has the same leveling)."I noticed that the way our feature stores data about the friends liking our gadget can be improved and have come up with a new design. Here is a design doc that was approved last week, here is the code to shape our current data in the DB, here is the new production code, that is backwards compatible. It is being pushed tonight, I am going to be on-call in the office. Fingers crossed."

### E5/M0

* Senior level for an engineer, entry level for managers.

* Understand business domain. Knows basic usage numbers and their trends.

* Can formulate new problems and solve them.

Act as an architect.

Lead the team as necessary.In manager role: should enable the team to function by ensuring that the resources are available. Run 1:1-s and performance reviews.Engineer: "We can aggregate this data and introduce another tab on the page seen by group admins. For groups with 50+ people this would not violate privacy. Looking at the usage pattern of our current features, our users would love the proposed feature. I ran several regression tests and confirm we can handle it with extra XXX of quota and permissions."

Manager: "Sounds awesome! Move fast and break things, I will clear it with our privacy team and get you a green light. Will take care of resources too. Also, do you want an intern, perhaps for the visualization part?"

### E6/M1

Staff/Principal engineer level.

* More autonomy and larger scope compared to E5/M0.

* Should be able to completely run his team as if it's a small startup in a bigger ecosystem.

Engineer: "We often need to compute certain statistics among friends-of-friends. My team has a pipeline for it. Teams A, B, and C seem to be doing the same things. I have talked with senior people from them and they see the benefits of unified infrastructure. I have gathered specifications from teams A, B and C and seems that there are features X, Y, and Z that we can do within a quarter to our mutual benefit. Three people who are working on it from my team's side will invest more time in the next quarter to have a unified solution coded and tested."

Manager: "This is a strong call for a new project. Several other teams have senior/principal people looking for new opportunities within the company, and I will make sure to explain them our prospects as well. We should start a motion to pull new hires as well, since we will need them in half a year. Also, how does your work align with a launch of a new datacenter in XXX? You may well be the guinea pig for their new SSD-based machines you seem to be able to make good use of."

### E7/M2

Lead focus areas.

Broad cross-team focus.

For an idea on scale, think "newsfeed ranking" or "mobile advertisement.

"Engineer: "Mobile advertisements is going to grow and generate more revenue. I am wondering if devices with stronger CPUs or devices with larger screens would generate more revenue and what could we do to leverage it. Tomorrow, I am having lunch with a E7 from mobile UI team and we will talk about some A/B tests that may show us whether there is room there.

"Engineer: "We seem to be generating more paging activity than usual for the past month. I did some research and realized that mobile ads shown on devices manufactured by XXX sometimes don't go through. This also seems to be connected with YYY installed on the phone. I am meeting my engineering team tomorrow to investigate this further -- could be a false alarm, but we'd rather be sure."

Manager: "Given how I understand our core business now, mobile ads are transitioning from mobile-browser-level to mobile-app-level. This will call for a reorg in the teams in about 2 quarters and I need to get aligned with them on our prospective plans."

### E8/D1

Director level.

Grow business at the level where the world is constantly evolving.

Keep and improve the culture.

Engineering director: "We will be retiring our efforts in X and moving them into Y. Here is the rollout plan for the next one and a half years. It is ambitious, but I have spoken with the leads of all the products and we believe we can make it.", where X and Y are related technologies in data storage or infrastructure or programming languages area.(Think Jeff Dean and Sanjay Ghemawat introducing MapReduce or BigTable to the whole Google engineering organization.)

Engineering director: "We will be opening another engineering office in Europe. It will focus on reliability thanks to time zone difference."

Director: "Mobile is our future, and we should aim at launching our own phone within two years."

Director: "Wall is obsolete and Timeline is how I see the things by the end of this year."

Director: "X seems to be just about the perfect guy to send to Asia and open a new engineering office there. X, what do you think?"

###  E9/D2

The meta-skill of envisioning, creating, and supporting all the above from scratch.


## The path of promotion:
> The TLDR version of years in Facebook is:
Year 1 was learn to become a good engineer,
Year 2 was learn become a good Tech Lead
Year 3–4 were to start something new from scratch. Even though we were not successful but learnt how to do it
Year 5–9 have been following the model from year 3–4 i.e. start something new, launch and improve the product and finally scale it and make it successful.
Early years were a mix of my ability, luck and being in the right place. If the three senior engineers hadn’t left the team, I would not have become Tech Lead so soon and would have been at least one level lower and my lifetime earnings at FB 25% lower
Later years were mainly my ability in finding the next opportunity before others and then executing well.
E6->E7 and E7->E8 promotions were mainly about making something successful. My execution could have been perfect but if the product didn’t work, I would not have been promoted.

Via https://medium.com/@anyengineer/how-much-i-made-as-a-really-good-engineer-at-facebook-9366151b52db



