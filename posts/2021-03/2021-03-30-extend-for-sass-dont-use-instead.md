---
title: TIL @extend and it's alternatives
date: 2021-03-30
published: false
tags: ['sass', 'css']
series: false
canonical_url: false
description: "I learned about `@extend` as a way to grab the styles from a element."
layout: layouts/post.njk
---

I learned about `@extend` as a way to grab the styles from a element.

I wanted to make a `fake-h3` element, within a span.

OPTIONS:

1) To just completely take the css properties would mean that if h3 was updated, then my component would be out of sync. ❌

2) I couldn't just go into the core files and turn the `h3` into a mixin. ❌

3) I used `@extend` make it work.

And just as quickly, my team lead pointed out why you shouldn't use `@extend`.

Essentially:
> Using @extend duplicates every instance of that selector. Every time you add a new ruleset for h4, Sass will include label as part of the selector chain. It's very unlikely that you want to do this in your CSS. And it's why you should avoid @extend. It introduces specificity and inheritance problems, and increases your file size.

Via https://webinista.com/updates/dont-use-extend-sass/

Instead, I switched to placeholder selectors! And it achieved the same goal that I was looking for!

```scss
h4, %h4 {
  color: #c09;
  font-size: 1.2rem;
  font-weight: 100;
}

label {
  @extend %h4;       // Extend the placeholder instead
  cursor: pointer;
}
```


REFERENCE:
https://webinista.com/updates/dont-use-extend-sass/

Just as quickly
