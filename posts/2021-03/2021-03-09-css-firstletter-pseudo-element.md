---
title: TIL About the first-letter pseudo-element
date: 2021-03-09
published: true
tags: ['css']
series: false
canonical_url: false
description: "There's a `first-letter` pseudo-element."
layout: layouts/post.njk
---

There's a `first-letter` pseudo-element.

![](2021-03-09-css-firstletter-pseudo-element_0.png)

REFERENCE:
https://twitter.com/karaluton/status/1369415514188087296/photo/1
