---
title: TIL Triggering Reflow
date: 2021-03-19
published: true
tags: ['seo', 'html']
series: false
canonical_url: false
description: "Have you ever been reading an article online when something suddenly changes on the pag"
layout: layouts/post.njk
---

> Have you ever been reading an article online when something suddenly changes on the page? Without warning, the text moves, and you've lost your place. Or even worse: you're about to tap a link or a button, but in the instant before your finger lands—BOOM—the link moves, and you end up clicking something else! Via [Google's CLS description](https://web.dev/cls/)

Google ranks that metric and calls it the [Cumulative Layout Shift](https://web.dev/cls/), within it's Core Web Vitals.

It happens when you trigger a reflow on the page.

For example:

* Causes reflow: Changing absolute positioning

* Does not cause reflow: Changing transform or opacity

Why? Because changing `absolute positioning` properties uses the CPU, while `transform` causes the browser to create a GPU layer for the element.

Translate(), the element still occupies its original space (sort of like position: relative), unlike in changing the absolute positioning. And overall, Translate() is more efficient and will result in shorter paint times for smoother animations.

@Paulirish created a list of all the JS elements that trigger reflow:
https://gist.github.com/paulirish/5d52fb081b3570c81e3a

![](https://developers.google.com/speed/images/reflow-chart.png)

And you can find more information about it here as well:
https://developers.google.com/speed/docs/insights/browser-reflow

