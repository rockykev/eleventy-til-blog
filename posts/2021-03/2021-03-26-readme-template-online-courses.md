---
title: TIL the Readme Template
date: 2021-03-26
published: true
tags: ['git', 'documentation']
series: false
canonical_url: false
description: "I discovered this readme template from Frontend Masters. "
layout: layouts/post.njk
---

I do a lot of projects from online courses.
Heck, my personal Github has 100+ repos.

The biggest value isn't just following along and learning the process, but also what you took away from it.

I discovered this readme template from Frontend Masters. It's awesome because it provides a checklist of what you were working on, and also have you think through the entire project after completion.

```
## Table of contents
* Overview
  * The challenge
  * Screenshot
  * Links
* My process
  * Built with
  * What I learned
  * Continued development
* Useful resources
  * Author
  * Acknowledgments

Note: Delete this note and update the table of contents based on what sections you keep.
```

VIA:
https://github.com/mattstuddert/readme-template
