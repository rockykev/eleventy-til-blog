---
title: TIL The Media Engagement Index
date: 2021-03-04
published: true
tags: ['seo']
series: false
canonical_url: false
description: "In 2018, Google Chrome changed the way autoplay works by including the Media Engagement Index (MEI)"
layout: layouts/post.njk
---

Google Chrome changed the way autoplay works.
(Where Safari outrights stops it.)

When you first visit a site, video doesn't autoplay.
But sometimes, it does.

Why?

There's a few reasons.

1. If the user interacts with the domain. (click, tap, etc)
2. On desktop, if the Media Engagement Index (MEI) threshold has been crossed.
3. If the user adds the site to their homescreen on mobile, or installed it as a PWA on desktop.

**What is Media Engagement Index (MEI)**
The MEI measures an individual's propensity to consume media on a site. Chrome's current approach is a ratio of visits to significant media playback events per origin:

* Consumption of the media (audio/video) must be greater than 7 seconds.
* Audio must be present and unmuted.
* Tab with video is active.
* Size of the video (in px) must be greater than 200x140.

REF:
https://developers.google.com/web/updates/2017/09/autoplay-policy-changes
