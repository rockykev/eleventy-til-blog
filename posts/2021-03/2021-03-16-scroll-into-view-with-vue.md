---
title: TIL how to jump to a element within a Vue Component
date: 2021-03-16
published: true
tags: ['vue']
series: false
canonical_url: false
description: "You use the $el selector and a scrollIntoView"
layout: layouts/post.njk
---

I couldn't figure out how to jump to an element within Vue.
After a lot of trial and error, this worked!

Use the `this.$el.querySelector('.message')` to select elements within a component.

I also included the new `scroll-margin-top` so the jump is so much more nicer.

```js
<template>
  <div>
  <h1 class="message">Hi there</h1>
  <component @click="scrollToMyElement" />
  </div>
</template>

<script>
export default {
  props: {
  },
  data: function () {
    return {}
  },
  methods: {
    scrollToError() {
      this.$el.querySelector('.message').scrollIntoView({ behavior: 'smooth' });
    },
  }
}
</script>

<style>
  .message {
    scroll-margin-top: 20ex;
}
</style>
 ```

