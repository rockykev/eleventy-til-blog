---
title: TIL CSS variable tricks
date: 2021-10-17
published: true
tags: ['css', 'javascript', 'variables']
series: false
canonical_url: false
description: "If you want to change the background with Javascript, you can use a CSS variable."
layout: layouts/post.njk
---

This is a neat trick.

If you want to change the background with Javascript, you can use a CSS variable.

```html
<!-- BEFORE  -->
<section
    class="newsletter"
    style="background-image: url('/assets/ui/decorative/newsletter-lg-aj1891101.svg')">
</section>
```

Notice that you're using a 'hardcoded' style tag and URL.

IF you were controlling the backgound with JS, you can target the `--thumb` variable directly.
Like this:

```html
<!-- AFTER -->
<section
    class="newsletter"
    style="--thumb:url('/assets/ui/decorative/newsletter-lg-aj1891101.svg')">
</section>

<style>
.newsletter {
    background-image: var(--thumb);
    background-size: cover;
    background-position: 100% 50%;
}
</style>
```

VIA:
[Practical Use Cases For CSS Variables](https://ishadeed.com/article/practical-css-variables/?utm_source=pocket_mylist)
