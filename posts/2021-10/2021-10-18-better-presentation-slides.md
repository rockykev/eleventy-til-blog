---
title: TIL about making better presentation slides
date: 2021-10-18
published: true
tags: ['slides', 'presentation']
series: false
canonical_url: false
description: "If you're going to have code on your slides, make sure it's the least amount of code necessary to convey the same meaning.The attendees came to listen to you, not to read."
layout: layouts/post.njk
---

Tip 1:
If you're going to have code on your slides, make sure it's the least amount of code necessary to convey the same meaning.

Tip 2:
Slides are not there to contain the content of your talk either. You might not even need words to begin with. After all, the attendees came to listen to you, not to read. Use contrasting colors, make sure that people in the back can see your content.

Tip 3:
Do not assume that you will have Internet connection on stage. Localhost is like your childhood dog, it'll always be there for you.

Tip 4:
Have backups. If your demo breaks on stage, revert to showing the video of the same thing.

VIA:
[What I wish someone told me about speaking at tech conferences](https://dev.to/tlakomy/what-i-wished-someone-told-me-about-speaking-at-tech-conferences-3opp?utm_source=pocket_mylist)
