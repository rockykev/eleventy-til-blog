---
title: TIL using Flexbox column-gaps
date: 2021-10-26
published: true
tags: ['css', 'flexbox']
series: false
canonical_url: false
description: "Using Column-gaps lets you space things out correctly, AND properly lays out your container elements."
layout: layouts/post.njk
---

![](2021-10-26-flexbox-column-gaps_0.png)

VIA:
[My 3 tips about Flexbox that will make your CSS better](https://dev.to/melnik909/my-3-tips-about-flexbox-that-will-make-your-css-better-50g6)

