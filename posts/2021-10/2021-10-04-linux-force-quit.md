---
title: TIL how to force-quit in Linux Terminal
date: 2021-10-04
published: true
tags: ['linux', 'raspberrypi', 'terminal']
series: false
canonical_url: false
description: "I was goofing off with my raspberry pi and broke something. Then I used ps, and then kill -9 12345 to kill that app."
layout: layouts/post.njk
---

You know how in Windows, when you have that problem like a hanging software, you can [Force Quit it](https://www.hellotech.com/guide/for/how-to-force-quit-on-windows-10-pc)?

Mac has it too.

But how exactly do you that in a Linux environment?

I was goofing off with my raspberry pi and broke something. There were errors, and then other things wouldn't work.

The key word is Process Status (`ps`)
`$ ps`


>  Let’s say you know the Chrome process is what you need to kill... For that, you can make use of the ps command and filter the output through grep. The ps command reports a snapshot of a current process and grep prints lines matching a pattern. The reason why we filter ps through grep is simple: If you issue the ps command by itself, you will get a snapshot listing of all current processes. We only want the listing associated with Chrome. So this command would look like:

`$ ps aux | grep chrome`

a = show processes for all users
u = display the process’s user/owner
x = also show processes not attached to a terminal

Then to kill it, you get the `PID`.
So if Chrome was PID 12345, you would do:

`kill -9 12345`

9 - represents the 	Kill signal

[More info on killing processes](https://www.linux.com/training-tutorials/how-kill-process-command-line/)
