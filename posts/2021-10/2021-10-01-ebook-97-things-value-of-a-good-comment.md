---
title: TIL the value of a good comment
date: 2021-10-01
published: true
tags: ['booksummary', '97things', 'comments']
series: false
canonical_url: false
description: "Inside your code should be explanations about what the code is supposed to be doing. On the other hand, you can go too far in your commenting. Make sure that your comments clarify your code but do not obscure it."
layout: layouts/post.njk
---

> Comments are not evil. They are as necessary to programming as basic branching or looping constructs. Most modern languages have a tool akin to javadoc that will parse properly formatted comments to automatically build an API document. This is a very good start, but not nearly enough. Inside your code should be explanations about what the code is supposed to be doing. Coding by the old adage, "If it was hard to write, it should be hard to read," does a disservice to your client, your employer, your colleagues, and your future self.

> On the other hand, you can go too far in your commenting. Make sure that your comments clarify your code but do not obscure it. Sprinkle your code with relevant comments explaining what the code is supposed to accomplish. Your header comments should give any programmer enough information to use your code without having to read it, while your in-line comments should assist the next developer in fixing or extending it.

I follow this adage: Make the next developer's life easier. Sometimes it's comments, sometimes it's just really clear code. Who that developer is also important. If it's a junior, maybe add a bit more detail. If it's to a lead, maybe it's less.


[Thing 16 of 97 things Ebook](https://97-things-every-x-should-know.gitbooks.io/97-things-every-programmer-should-know/content/en/thing_16/)
