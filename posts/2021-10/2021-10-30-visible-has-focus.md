---
title: TIL difference between document.hidden vs document.hasFocus()
date: 2021-10-30
published: true
tags: ['javascript', 'visibility', 'stackoverflow']
series: false
canonical_url: false
description: "Do you need to check if the tab is currently active/Visible?"
layout: layouts/post.njk
---

Do you need to check if the tab is currently active/visible?

`document.hidden`: returns true if website running it is not VISIBLE

`document.hasFocus()`: returns true if tab is currently running AND browser is in focus by computer

REFERENCE:
[Stack Overflow](https://stackoverflow.com/a/65104552/4096078)
