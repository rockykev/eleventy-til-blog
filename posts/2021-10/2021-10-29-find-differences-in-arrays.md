---
title: TIL Finds the differences between arrays
date: 2021-10-29
published: true
tags: ['arrays', 'javascript']
series: false
canonical_url: false
description: "Finds the differences between arrays"
layout: layouts/post.njk
---

Finds the differences between arrays

```javascript
const differenceInArrays = (array1, array2) =>  {
    const set = new Set(array2);
    return array1.filter(x => !set.has(x));
};
differenceInArrays(["apple", "orange", "banana"], ["apple", "orange", "mango"]); // ["banana"]
differenceInArrays([10, 12, 5], [66, 10, 6]); // [12, 5]
```

Via
[21 Useful JS snippets](https://javascript.plainenglish.io/21-useful-javascript-snippets-for-everyday-development-9e66e33bfb86)
