---
title: TIL How to set up Archi Steam Farm on Raspberry Pi
date: 2021-10-03
published: true
tags: ['raspberrypi', 'steam']
series: false
canonical_url: false
description: "Archi Steam Farm and a tampermonkey script can auto-farm steam cards then sell them."
layout: layouts/post.njk
---

[Archi Steam Farm](https://github.com/JustArchiNET/ArchiSteamFarm) lets you farm steam cards while you live your life.
Get this [Tampermonkey script](https://github.com/Nuklon/Steam-Economy-Enhancer), and you can autosell everything.



NOTE: I tried this on my Raspberry Pi B Rev 2, and it did not work.
I then tried it on my Raspberry Pi 4, and it's up and running!

## Part 0: Getting a Raspberry Pi Installed

For the Raspberry Pi Documentation for [Getting Started](https://www.raspberrypi.org/documentation/computers/getting-started.html#setting-up-your-raspberry-pi)


1. Get Raspberry Pi Imager

HEADS UP - this is so stupid.

2. Use the secret `Ctrl+Shift+X` to open up Advanced Options, so you can config things like ssh, wifi, etc.

Then have it copy into a SD card.

## Part 1: Assuming you have installed Raspberry Pi OS

In your terminal (Mac, Linux, or use the Bash Terminal for Windows):
`ssh pi@192.168.x.x`
Default password is `raspberry`

Then apt-update
`sudo apt-get update`

## Part 2: Archi Steam Farm

1. Follow their instructions. https://github.com/JustArchiNET/ArchiSteamFarm/wiki/Setting-up

**a. Get Debian `.NET Core` prereqs.**

Raspberry Pi 4 already had everything. You can always try it again.
```
sudo apt install libc6 libgcc1
sudo apt install libgssapi-krb5-2 libssl1.1 libstdc++6 zlib1g libicu63
```

**b. Download [latest ASF release](https://github.com/JustArchiNET/ArchiSteamFarm/releases)**

For Raspberry Pi 4, I got `ASF-linux-arm.zip`.
Technically, `ASF-linux-arm64.zip` would work, but Raspberry PI 4 OS 64-bit (as of Oct 3, 2021), isn't stable yet.

**c. Extract the archive.**

From the `home` folder:
`$ unzip ASF-linux-arm.zip -d ArchiSteamFarm`

**d. Configure ASF**

We now need to make a [json config](https://github.com/JustArchiNET/ArchiSteamFarm/wiki/Configuration) file with our username, password.

Enter the config folder:
`$ cd ArchiSteamFarm/config`

Open with nano (the file can be named anything. I named it main.json. You can name it MyAccount.json or whatever.)
`$ nano main.json`

In the json file - replace this with your stuff.
Then Ctrl+X to save and quit.
```
{
  "Enabled": true,
  "SteamLogin": "MySteamAccountName",
  "SteamPassword": "MySteamAccountPassword"
}
```


**e. Launch ASF.**

Via [this reddit post](https://www.reddit.com/r/Steam/comments/71gcux/guidehow_to_make_a_raspberry_pi_a_cheap_low_power/)

You have to change permissions on the folder for some reason.

So return to your home folder and chmod it.
`$ sudo chmod +x ArchiSteamFarm`

Then switch from the pi user to the admin.
`$ sudo su`

Finally, launch the code.
`$ ./ArchiSteamFarm`

It'll kick up this screen.
![](2021-10-03-archi-steam-farm_0.png)


