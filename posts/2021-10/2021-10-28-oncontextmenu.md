---
title: TIL oncontextmenu feature for right-clicking
date: 2021-10-28
published: true
tags: ['javascript', 'events']
series: false
canonical_url: false
description: "The oncontextmenu property lets us assign an event handler function when the right mouse button is clicked on the browser tab."
layout: layouts/post.njk
---

## oncontextmenu

The oncontextmenu property lets us assign an event handler function to it to handle the contextmenu event. The event is triggered when the right mouse button is clicked on the window. The browser context menu will activate unless the default behavior is prevented.

For example, we can disable right click by writing:

```javascript
document.oncontextmenu = (event) => {
  event.preventDefault();
}
```

REFERENCE:
[MDN](https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/oncontextmenu)
[JavaScript Events Handlers — oncontextmenu and oncuechange](https://javascript.plainenglish.io/javascript-events-handlers-oncontextmenu-and-oncuechange-fd6474fdf96e)
