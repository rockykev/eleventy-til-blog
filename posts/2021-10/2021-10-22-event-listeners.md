---
title: TIL Event Listeners in devtools
date: 2021-10-22
published: true
tags: ['javascript', 'events', 'devtools']
series: false
canonical_url: false
description: "Today I learned that you can identify the Event Listener directly in the Devtools!"
layout: layouts/post.njk
---

Today I learned that you can identify the Event Listener directly in the Devtools!

This is Chrome btw.
![](2021-10-22-event-listeners_0.png)

REFERENCE:
[How to debug event listeners with your browser's developer tools](https://gomakethings.com/how-to-debug-event-listeners-with-your-browsers-developer-tools/?utm_source=pocket_mylist)
