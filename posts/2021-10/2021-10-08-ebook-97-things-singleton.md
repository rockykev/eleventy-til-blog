---
title: TIL the Singleton Pattern is hotly debated
date: 2021-10-08
published: true
tags: ['booksummary', '97things', 'patterns']
series: false
canonical_url: false
description: "Singleton Pattern is useful when exactly one object is needed to be used as a global instance to coordinate across the system. But it got problems."
layout: layouts/post.njk
---


Singleton Pattern is useful when exactly one object is needed to be used as a global instance to coordinate across the system.

It was introduced in the 1994 book [Design Patterns](https://en.wikipedia.org/wiki/Design_Patterns#See_also), and regarded as a important source for OOP theory and design.

> In an interview with InformIT in 2009, Erich Gamma stated that the book authors had a discussion in 2005 on how they would have refactored the book and concluded that they would have recategorized some patterns and added a few additional ones. Gamma wanted to remove the Singleton pattern, but there was no consensus among the authors to do so.[9]

This [SO post says it pretty well when to use it/don't use it](https://stackoverflow.com/a/142450/4096078):

> Singletons solve one (and only one) problem: Resource Contention.

> If you have some resource that
> (1) can only have a single instance, and
> (2) you need to manage that single instance,

> you need a singleton.

> There aren't many examples. A log file is the big one. You don't want to just abandon a single log file. You want to flush, sync and close it properly. This is an example of a single shared resource that has to be managed.

> It's rare that you need a singleton. The reason they're bad is that they feel like a global and they're a fully paid up member of the GoF Design Patterns book. When you think you need a global, you're probably making a terrible design mistake.

Some more good examples to use singletons:

* Reading configuration files that should only be read at startup time and encapsulating them in a Singleton.

* Read only singletons storing some global state (user language, help filepath, application path) are reasonable.

* A database connection pool. The pool manages the creation, destruction, and lifetime of all database connections for the entire application ensuring that no connections are 'lost'.

[More JS examples](https://www.dofactory.com/javascript/design-patterns/singleton)

But there's also a lot of reasons to avoid them.

> The single-instance requirement is often imagined. In many cases it's pure speculation that no additional instances will be needed in the future. Broadcasting such speculative properties across an application's design is bound to cause pain at some point. Requirements will change. Good design embraces this. Singletons don't.

> Singletons cause implicit dependencies between conceptually independent units of code. This is problematic both because they are hidden and because they introduce unnecessary coupling between units. This code smell becomes pungent when you try to write unit tests, which depend on loose coupling and the ability to selectively substitute a mock implementation for a real one. Singletons prevent such straightforward mocking.

> Singletons also carry implicit persistent state, which again hinders unit testing. Unit testing depends on tests being independent of one another, so the tests can be run in any order and the program can be set to a known state before the execution of every unit test. Once you have introduced singletons with mutable state, this may be hard to achieve. In addition, such globally accessible persistent state makes it harder to reason about the code, especially in a multi-threaded environment.

> Multi-threading introduces further pitfalls to the singleton pattern. As straightforward locking on access is not very efficient, the so-called double-checked locking pattern (DCLP) has gained in popularity. Unfortunately, this may be a further form of fatal attraction. It turns out that in many languages DCLP is not thread-safe and, even where it is, there are still opportunities to get it subtly wrong.

[thing 73](https://97-things-every-x-should-know.gitbooks.io/97-things-every-programmer-should-know/content/en/thing_73/)

