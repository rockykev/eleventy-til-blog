---
title: TIL KISS (Keep it simple stupid)
date: 2021-10-02
published: false
tags: ['booksummary', '97things', 'development']
series: false
canonical_url: false
description: "I'm a fan of Keep it Stupidly Simple (KISS) (There's other ways to interpret the the last part)."
layout: layouts/post.njk
---

I'm a fan of Keep it Stupidly Simple (KISS) (There's other ways to interpret the the last part).
My obsession is to KISS is because I really care about writing code that makes it easy to read, hand off, and extend.

This post is great:
> I am not the slightest bit interested in your program.

> I am surrounded by problems and have a to-do list as long as my arm. The only reason I am at your website right now is because I have heard an unlikely rumor that every one my problems will be eliminated by your software. You'll forgive me if I'm skeptical.

Everyone is super busy, all the time. Telling someone to 'go read the docs' is a douche move.

KISS.

> If your software is a library, then I carry on reading your web page looking for a quick start guide. I want the equivalent of "Hello world" in a five-line no-brainer with exactly the output described by your website. No big XML files or templates to fill out, just a single script. Remember, I have also downloaded your rival's framework. You know, the one who always claims to be so much better than yours in the forums? If all is working, onto the tutorial.

> There is a tutorial isn't there? One that talks to me in language I can understand?

> And if the tutorial mentions my problem, I'll cheer up. Now I'm reading about the things I can do it starts to get interesting, fun even. I'll lean back and sip my tea — did I mention I was from the UK? — and I'll play with your examples and learn to use your creation. If it solves my problem, I'll send you a thank-you email. I'll send you bug reports when it crashes, and suggestions for features too. I'll even tell all my friends how your software is the best, even though I never did try your rival's. And all because you took such care over my first tentative steps. How could I ever have doubted you?

And I like this post a lot because the answer isn't always 'elegant code'.
Sometimes it's comments.
Sometimes it's documentation.
Heck, sometimes it's videos!

It's whatever I have to do to make sure that you, the user, can read, consume, and extend.
And if you can't, then I failed you.

[Thing 40 of 97 things Ebook](https://97-things-every-x-should-know.gitbooks.io/97-things-every-programmer-should-know/content/en/thing_40/)
