---
title: TIL writing cleaner If statements
date: 2021-10-27
published: true
tags: ['javascript', 'cleancode', 'ifstatements']
series: false
canonical_url: false
description: "Graphs and visual flow of various if-else statements"
layout: layouts/post.njk
---

I probably write about this a lot.

This is from [Clean Up Your Code by Removing ‘if-else’ Statements](https://bytefish.medium.com/clean-up-your-code-by-removing-if-else-statements-31102fe3b083?source=email-f85b306cec29-1624615204182-digest.weekly--31102fe3b083----0-59------------------c7469fcf_1fb4_47c0_9f6f_a88fa806b9a3-1-300418ef_7289_4082_8b07_feb53ae6152e), and this is the best version I ever saw of this.

## Using If statements
```javascript
function action(ranking){
  if(ranking == 'A'){
    travel()
  }
  else if (ranking == 'B'){
    shopping()
  }
  else if (ranking == 'C'){
    watchTV()
  }
  else if (ranking == 'D') {
    review()
  }
}
```
![](2021-10-27-visual-if-statements_0.png)


## Using Switch
```javascript
function action(ranking){
  switch(ranking){
    case 'A':
      travel()
      break

    case 'B':
      shopping()
      break
    case 'C':
      watchTV()
      break

    case 'D':
      review()
      break
  }
}

```

![](2021-10-27-visual-if-statements_1.png)

## Using Key values
```javascript
let strategies = {
  'A': travel,
  'B': shopping,
  'C': watchTV,
  'D': review
}
function action(ranking){
  let strategy = strategies[ranking]
  strategy()
}
```

![](2021-10-27-visual-if-statements_2.png)
