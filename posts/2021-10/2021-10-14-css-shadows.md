---
title: TIL How to actually do CSS Shadows
date: 2021-10-14
published: true
tags: ['css', 'design', 'ux']
series: false
canonical_url: false
description: "A real world shadow has two main distinctive parts. Direct light casts the first shadow and ambient light casts the second."
layout: layouts/post.njk
---

Not going to lie - I'm pretty bad with shadows.

![AH MY FREAKIN' EYES](2021-10-14-css-shadows_0.png)

This is all from this post ->
[Design detail: crafting better shadows for interaction](https://uxdesign.cc/design-detail-crafting-better-shadows-for-interaction-b94796a29664?utm_source=pocket_mylist)

## Properly doing shadows
A real world shadow has two main distinctive parts.

Direct light casts the first shadow and ambient light casts the second.

**Directional Light:**
> For our first shadow, we want to emphasise the elevation in our UI element, and a strong directional light is the source we need to create this effect. I use a directional light shadow that has more offset than the ambient light shadow, as it is comes in from a single angle.

> This shadow will also be more defined and darker than an ambient light shadow.

![](2021-10-14-css-shadows_1.png)

**Shadow cast by ambient light**

> A shadow cast by ambient light is more diffuse, softer and has less vertical offset than that cast by directional light as it comes from all angles.

> One thing to keep in mind is that the higher the elevation of an element, the fainter the ambient shadow. It starts of dark, (at small elevations, it can appear darker than that cast by directional light) and than becomes faint until it disappears.

![](2021-10-14-css-shadows_2.png)


**Combined**

When you put them together, it gives is a more realistic look.

![](2021-10-14-css-shadows_3.png)
## Shadow codes:
```css

/* Shadow for a low elevation */
div {
  box-shadow:
      0 2px 3px hsla(0, 0%, 0%, 0.12) /* Ambient light shadow */,
      0 3px 6px hsla(0, 0%, 0%, 0.14) /* Direct light shadow */;
}

/* Shadow for a higher elevation */
div {
  box-shadow:
      0 4px 12px hsla(0, 0%, 0%, 0.12) /* Ambient light shadow */,
      0 14px 24px hsla(0, 0%, 0%, 0.16) /* Direct light shadow */;
}

/* Shadow for a highest elevation (faint ambient light shadow) */
div {
  box-shadow:
      0 5px 12px hsla(0, 0%, 0%, 0.4) /* Ambient light shadow */,
      0 18px 32px hsla(0, 0%, 0%, 0.16) /* Direct light shadow */;
}

```

