---
title: TIL Library sites are running their own code
date: 2022-01-23
published: true
tags: ['console', 'libraries', 'javascript']
series: false
canonical_url: false
description: ""
layout: layouts/post.njk
---


Visit a library's website (for their documentation, CDN, or just to learn more about it).

Very often, the site is running the code they are promoting.

So you can open up console.log and test it!

For example - jQuery's site is running jquery in the background.

So you can do something like:

`$('h2')`

