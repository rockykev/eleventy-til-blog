---
title: TIL getting all files with an extension in Node using Glob
date: 2022-01-31
published: false
tags: ['node', 'file-directory']
series: false
canonical_url: false
description: "This helper function to get all the files that end in a specific extension in node"
layout: layouts/post.njk
---

I wanted to find all json files within a folder structure.

`npm i glob`

```js
const glob = require('glob');

function getFilesFromPath(path, extension) {
  return glob.sync(`${path}/**/*.${extension}`);
}

// get me all .gz files inside the aws directory
const allZippedFiles = getFilesFromPath('./cheerio_data/aws/', "gz");

// get me all the scss files inside the source directory
const allScssFiles = getFilesFromPath('./src/', "scss");
```



Resources:
* https://stackoverflow.com/a/52024318/4096078
* https://stackoverflow.com/a/60604686/4096078
