---
title: TIL a quick way to log out [object object]
date: 2022-01-18
published: true
tags: ['javascript', 'console', 'object']
series: false
canonical_url: false
description: "JSON.stringify"
layout: layouts/post.njk
---

```js
const o = {
  u: {
    l: { course: 'node.js', a: { b: {}}}
  }
}

// how to avoid [object object]
// JSON.stringify(value, replacer, space)
console.log(JSON.stringify(o, null, 2))
```



[MDN docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify)
