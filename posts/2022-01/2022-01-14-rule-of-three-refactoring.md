---
title: TIL the Rule of Three for Refactoring
date: 2022-01-14
published: true
tags: ['refactoring', 'junior', 'rule', 'dev']
series: false
canonical_url: false
description: "The first time you run into a problem, solve it. The second time, just copy it. The third time, then you make that consideration."
layout: layouts/post.njk
---

The first time you run into a problem, solve it.
The second time, just copy it.
The third time, then you make that consideration.
