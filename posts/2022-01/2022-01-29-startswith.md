---
title: TIL startWith
date: 2022-01-29
published: false
tags: ['mdn', 'javascript', 'string']
series: false
canonical_url: false
description: "Getting a boolean result of true or false, depending on if the string startsWith a specific substring"
layout: layouts/post.njk
---


Getting a boolean result of true or false, depending on if the string startsWith a specific substring.

## How it works:
```js
//startswith
let str = 'To be, or not to be, that is the question.'

console.log(str.startsWith('To be'))          // true
console.log(str.startsWith('not to be'))      // false
console.log(str.startsWith('not to be', 10))  // true
```

I was looking at ripping string apart by spaces (using the [String.prototype.split](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split) method), then found this other neat method to do string comparison.

[mdn - startsWith](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith)
