---
title: TIL that keyCode is depreciated
date: 2022-01-20
published: true
tags: ['depreciated', 'mdn', 'keyboard']
series: false
canonical_url: false
description: "KeyboardEvent.keyCode is depreciated. Because it was inconsistent. Use a the code version instead, it's more readable."
layout: layouts/post.njk
---


I noticed that Vue 3 is [migrating out of keycodes](https://v3.vuejs.org/guide/migration/keycode-modifiers.html#_3-x-syntax)

> KeyCode was deprecated because in practice it was “inconsistent across platforms and even the same implementation on different operating systems or using different localizations.”

KeyboardEvent.keyCode is depreciated. https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/keyCode

What are keyCodes?
Check [WesBo's repo on keyCodes](https://github.com/wesbos/keycodes/blob/gh-pages/scripts.js#L3)


Instead, switch to [code](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code).


```js
window.addEventListener("keydown", function(event) {
  if (event.defaultPrevented) {
    return; // Do nothing if event already handled
  }

  switch(event.code) {
    case "KeyS":
    case "ArrowDown":
      // Handle "back"
      updatePosition(-moveRate);
      break;
    case "KeyW":
    case "ArrowUp":
      // Handle "forward"
      updatePosition(moveRate);
      break;
    case "KeyA":
    case "ArrowLeft":
      // Handle "turn left"
      angle -= turnRate;
      break;
    case "KeyD":
    case "ArrowRight":
      // Handle "turn right"
      angle += turnRate;
      break;
  }

  refresh();

  // Consume the event so it doesn't get handled twice
  event.preventDefault();
}, true);

```
