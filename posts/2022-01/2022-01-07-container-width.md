---
title: TIL A nice way to use calc to get margin space
date: 2022-01-07
published: true
tags: ['css', 'layout']
series: false
canonical_url: false
description: "I love it when bootstrap containers automatically create padding based on size."
layout: layouts/post.njk
---

I love it when bootstrap containers automatically create padding based on size.

```css
// margin method
.container {
  margin: 2rem 5rem;
}


// using calc method
.container {
  max-width: calc(100% - 10rem);
  margin: 0 auto;
}
```

The margin method always keeps the content centered, which is one benefit.

But I like the calc method, because regardless of the screen size, there's a lot more control with math.
