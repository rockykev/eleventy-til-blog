---
title: TIL doing things that makes your team's lives easier
date: 2022-01-27
published: true
tags: ['work', 'devlife']
series: false
canonical_url: false
description: "What can I do to make my team (team lead, teammates, managers, other teams) lives easier?"
layout: layouts/post.njk
---


PROTIP: Once a week, ask yourself: What can I do to make my team (team lead, teammates, managers, other teams) lives easier?

Maybe it's reducing your team lead's workload. Or tackling a ticket nobody wants. Or writing documentation about how to solve a hard task. Or heck, even writing meeting notes or finding bugs!

You keep doing this enough and CONGRATS -- You now have a laundry list of your accomplishments. 1-on-1s are easy-peasy. Updating your resume is no-brainer. You're super valuable, and people can't stop gushing over you.
