---
title: TIL the hover media feature
date: 2022-01-09
published: false
tags: ['css', 'mdn', 'media-queries']
series: false
canonical_url: false
description: "If you're on a phone or tablet, chances are -- you cannot hover. But if you're on desktop using a mouse -- you can!"
layout: layouts/post.njk
---

Today I learned!

There's a media feature to test if the system you're operating in can hover.

If you're on a phone or tablet, chances are -- you cannot hover.
But if you're on desktop using a mouse -- you can!

```css
@media (hover: hover) {
  a:hover {
    background: yellow;
  }
}
```

[mdn - hover](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/hover?utm_source=pocket_mylist)
