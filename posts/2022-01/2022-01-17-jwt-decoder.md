---
title: TIL a JWT decoder and how it works
date: 2022-01-17
published: true
tags: ['security', 'json', 'jwt']
series: false
canonical_url: false
description: "JWT (JSON Web Tokens) are essentially just JSON objects that are hashed. This site decodes them and explains how it works"
layout: layouts/post.njk
---

JWT (JSON Web Tokens) are essentially just JSON objects that are hashed.

[This site can decode it, while also teaching you about it](https://jwt.io/).

![](2022-01-17-jwt-decoder_0.png)

But to explain it some more:

1. You're given a hash.

2. Inside the hash is a header (to explain the algorithm, and the type)

3. Also included is the payload.

4. There is also a secret handshake code.

5. Put all that together, and you got a pretty neat way to send secure tokens back and forth.



