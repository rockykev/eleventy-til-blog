---
title: TIL writing modes
date: 2022-01-10
published: true
tags: ['accessibility', 'rtl', 'css', 'mdn']
series: false
canonical_url: false
description: "The 'writing-mode' CSS property determines if lines are laid our horizontally, or vertically. The direction is determined by the context of the direction of the content."
layout: layouts/post.njk
---

The `writing-mode` CSS property determines if lines are laid our horizontally, or vertically.

```css
writing-mode: horizontal-tb; /* horizontal top-bottom */
writing-mode: vertical-lr; /* vertical left-right */
writing-mode: vertical-rl; /* vertical right-left */

writing-mode: sideways-lr; /* sideways left-right - EXPERIMENTAL */
writing-mode: sideways-rl; /* sideways right-left - EXPERIMENTAL */

```

The direction is determined by the context of the direction of the content.

Left-to-right languages - lts (Like English)
Right-to-left languages - rtl (like Hebrew or Arabic)


If all browsers did it correctly, it should look like this:

![](2022-01-10-writing-mode-css_0.png)



[MDN - writing-mode](https://developer.mozilla.org/en-US/docs/Web/CSS/writing-mode)

