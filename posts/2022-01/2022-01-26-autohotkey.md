---
title: TIL about AutoHotKey
date: 2022-01-26
published: true
tags: ['automation', 'gaming']
series: false
canonical_url: false
description: "When you're getting annoyed at running the same commands over and over again."
layout: layouts/post.njk
---

In a game I was playing, it was getting super annoying to:
1. press up
2. Buy an item (you could only buy one)
3. wait 5 seconds for the animation
4. repeat it 1000 more times.

I discovered this: [AutoHotKey](https://www.autohotkey.com/)

> Powerful. Easy to learn.
> The ultimate automation scripting language for Windows.

So what you do is you write a script that fires keystrokes.
You then open your script in AutoHotkey, and then fire the 'activator' key.

Here's the script I found:

Filename: dangaronpa-autobuy.ahk
```
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

~CapsLock::
{
 loop
 {
  if GetKeyState("CapsLock", "T")
  {
   Send {Down down}
   Send {Return down}
   Sleep, 100
   Send {Return up}
   Send {Down up}
   Sleep, 7000
  }
  else
  {
   Break
  }
 }
}
return

```

How to use:
1. I run the script in AutoHotkey
2. When I press CapsLock...
3. It loops through that event.
4. It'll send down, hit the action, sleep, repeat.


[Reference - Danganronpa guide by n4f](https://steamcommunity.com/sharedfiles/filedetails/?id=1497324144)
