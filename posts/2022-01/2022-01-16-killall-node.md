---
title: TIL end all node processing scripts
date: 2022-01-16
published: true
tags: ['javascript', 'commandline', 'terminal', 'bash', 'node', 'stackoverflow']
series: false
canonical_url: false
description: "killall node - a sure fire way to kill a node.js server that won't stop running."
layout: layouts/post.njk
---



`killall node` - a sure fire way to kill a node.js server that won't stop running.


Windows: `taskkill /f /im node.exe`

[StackOverflow](https://stackoverflow.com/a/14790921/4096078)
