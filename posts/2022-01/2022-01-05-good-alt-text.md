---
title: TIL Writing really good alt text
date: 2022-01-05
published: true
tags: ['accessibility', 'images', 'copywriting']
series: false
canonical_url: false
description: "One way to think of alternative text is to think about how you would read the page containing the image to someone over the phone, without mentioning that there is an image present. Whatever you say instead of the image is typically a good start for writing the alternative text."
layout: layouts/post.njk
---

Good alt text is an art. So let's repeat that.

## ALT TEXT INFO

WHAT ALT TEXT IS:
* Shows up when you hover over an image.
* Read by the screen reader
* Placeholder when there's no image.
* Replacement text when the image isn't available.

WHAT ALT TEXT IS NOT:
* A replacement for a image's caption, title, or legend.
* Content to supplement the image.

> In fact, if you tried to detail everything in the image, then the actual focus, the bit that mattered in terms of the article, might get lost among the irrelevant detail. Like when you look up a recipe, and the bulk of the content is about how the smell of rosemary reminds the author of their childhood – sorry, don't care, I just want to know how to roast potatoes.

[Writing great alt text: Emotion matters](https://jakearchibald.com/2021/great-alt-text/?utm_source=pocket_mylist)


## THE RULE OF THUMB:
> the intent is that replacing every image with the text of its alt attribute does not change the meaning of the page.

> One way to think of alternative text is to think about how you would read the page containing the image to someone over the phone, without mentioning that there is an image present. Whatever you say instead of the image is typically a good start for writing the alternative text.

This isn't my opinion. This is directly from the HTML specs: https://html.spec.whatwg.org/multipage/images.html#alt

There's actually more to it, which I've covered in some other TILs, under `accessibility`.

For example:
* Add alt text all non-decorative images.
* Keep it short and descriptive, like a tweet.
* Don’t include “image of” or “photo of”.
* Leave alt text blank if the image is purely decorative. (`alt=''` is perfectly fine for decoration.)
* It's context dependent. Don't describe everything, just why it's important to the article.
*

## EXAMPLES:


![](https://static.hwpi.harvard.edu/files/styles/os_files_xlarge/public/online-accessibility-huit/files/032012_stadiumsteps_131_223861_01.jpg?m=1585245978&itok=FnpY5HbU)

Via [Write good Alt Text to describe images](https://accessibility.huit.harvard.edu/describe-content-images)

**Bad Alt text (don't use):**
A picture of a empty stadium.

(No need to describe that it's a picture. It does not help with context.)

**Alt-text with no context:**
A mostly empty stadium.

**Alt-text on a page about recent turnout for track tryouts:**
Harvard Stadium with two lone runners bounding up the steps.

**Alt-text on page about renovation projects:**
Harvard Stadium with cracked concrete pillars.


![](2022-01-05-good-alt-text_0.png)

**Bad Alt text (don't use):**
Tasty nachos! Yum!

(This does not describe the image. You also shouldn't add context to supplement the image)


**Alt-text with no context:**
Bag of Doritos, party size.

**Alt-text on a store page:**
Doritos Tortilla Chips, Nacho Cheese, 14.5 oz bag, party-size

**Alt-text on page about food portions:**
Party-size Doritos weighing 14.5 oz.

Via [Alt Text](https://moz.com/learn/seo/alt-text)


![](2022-01-05-good-alt-text_1.jpg)

**Bad Alt text (don't use):**
Stack of 8 pancakes with 27 blueberries. A mint is on top. The pancakes are positioned centered on the plate, but the image itself is angled slightly to show half of the plate, and half of the panckes.

(Too much un-neccessary details. It's not actually explaining 'why' the image exists within the article.)

**Alt-text with no context:**
Stack of pancakes with whole blueberries as toppings.


**Alt-text on a restaurant page:**
8 crisp pancakes made with whole-grain flour, with organic blueberry toppings.

**Alt-text on a cooking/recipe site:**
Stack of pancakes with whole blueberries as toppings.

Via [Alt Text](https://moz.com/learn/seo/alt-text)


