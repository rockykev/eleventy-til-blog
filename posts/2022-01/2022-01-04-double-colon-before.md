---
title: TIL that it's ::before, not :before
date: 2022-01-04
published: true
tags: ['css']
series: false
canonical_url: false
description: "So what should you be using? Unless you time travel, double-colon."
layout: layouts/post.njk
---

The actual specs:

> The syntax of a pseudo-element is "::" (two U+003A COLON characters) followed by the name of the pseudo-element as an identifier. Pseudo-element names are ASCII case-insensitive. No white space is allowed between the two colons, or between the colons and the name.

The W3C proposals and standards
https://drafts.csswg.org/selectors-4/#pseudo-element-syntax


Via the blog post:

> When I asked colleague more experienced than myself, I remember them saying, “they’re both valid”. So I just assumed it was a kind of syntactic sugar, both being acceptable.

> Fast forward a few years, I remember reading an article stating that single-colon notation was some kind of backwards-compatibility artifact of the standardization process. Both were valid, but double-colon was “more right”.

So what should you be using? Unless you time travel, double-colon.

```css
/* CORRECT: CSS3 syntax */
::before

/* INCORRECT: CSS2 syntax */
:before
```

> [Chris Coyier]: We covered this a few years back as well. The only reason you’d use the single-colon syntax is if you’re needing to support IE 8. The deepest people tend to go these days is IE 11, but it’s more common not to support IE at all. So yeah, double colon for sure.


REFERENCE:
[Hairsplitting the Pseudo-Element Syntax](https://blog.jim-nielsen.com/2021/pseudo-element-syntax/)
[MDN - ::before](https://developer.mozilla.org/en-US/docs/Web/CSS/::before)
[CSS-Tricks](https://css-tricks.com/to-double-colon-or-not-do-double-colon/)
