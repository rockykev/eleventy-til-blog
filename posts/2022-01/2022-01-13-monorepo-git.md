---
title: TIL that monorepos are so big and can't be used in git
date: 2022-01-13
published: true
tags: ['git', 'monorepo', 'codebase']
series: false
canonical_url: false
description: "Git literally cannot handle all that code for those companies. So they rolled out their own. (With blackjack and hookers)"
layout: layouts/post.njk
---

Monorepos as big as Facebook, Microsoft and Google don't use git, because of it's limitations.

tl;dr: Git literally cannot handle all that code for those companies.

> Our oldest Rust codebase dates to 2016, when the rate of source code changes in Facebook’s monorepo started to encroach on the maximum commit rate that the Mercurial source control management tool could keep up with. In

Facebook actually had to roll out their own source code manager. From Mercurial to a project called (Mononoke.)[https://engineering.fb.com/2021/04/29/developer-tools/rust/]

Google did the same, called (Piper)[https://m-cacm.acm.org/magazines/2016/7/204032-why-google-stores-billions-of-lines-of-code-in-a-single-repository/fulltext]


> The Windows Git repository includes about 3.5 million files that weigh in at about 300GB when you check them into Git. Git, however, wasn’t built for a project of this size, so Microsoft developed the Git Virtual File System to be able to get the benefits of using Git without having to wait hours for even the simplest of Git commands to run.

Where Microsoft uses the GVFS (Git Virtual File System) to handle their [source control](https://devblogs.microsoft.com/devops/announcing-gvfs-git-virtual-file-system/).

> The Git client was never designed to work with repos with that many files or that much content. You can see that in action when you run “git checkout” and it takes up to 3 hours, or even a simple “git status” takes almost 10 minutes to run. That’s assuming you can get past the “git clone”, which takes 12+ hours.
