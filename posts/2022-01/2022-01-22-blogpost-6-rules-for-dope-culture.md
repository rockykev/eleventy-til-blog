---
title: TIL Blogpost - These 6 rules created a really dope culture at my job
date: 2022-01-22
published: true
tags: ['jobstory', 'culture', 'blog']
series: false
canonical_url: false
description: "In almost any televised sport, watch the players. Teammates high five. Win or lose, opponents say 'good game'. Players tend to be good sports. It's rare to seeing players belittle a fellow teammate. Because at the end of the day, it's just another day. It's just another game. There's a thousand more. And another opportunity to shine."
layout: layouts/post.njk
---

Read the post here: https://dev.to/rockykev/these-6-rules-created-a-really-dope-culture-at-my-job-5fj5

----

We as a industry do a lot of lip service about being inclusive, creating welcoming environments, and making things a 'fun place to be'.

Yet, the industry can be tribal/hostile.

* [Open source developers](https://jamie.build/dear-javascript.html) get attacked.
* Languages get mocked and practioners laughed at... like PHP, which runs [77.5% of the internet](https://w3techs.com/technologies/details/pl-php), or Javascript, which is [98% of the internet](https://w3techs.com/technologies/details/cp-javascript).
* Framework tribes bubble up and thousands of bloggers write posts comparing the Reacts, Vues, Sveltes, vanilla (no links because not creating backlinks to THAT noise) and why only one shall reign supreme
* Imposter Syndrome is real. With so many routes to becoming a developer (self-taught, to CS degrees, bootcamps), everyone is on extremely different wavelengths.

That toxicity spreads to the person, who brings that into their workplace.

The workplace already has it's own set of problems.

Changing deadlines, bad code/bugs, office politics, time vampires and meeting overload... overwork, stress, and only recently did developers realize [mental health](https://insights.stackoverflow.com/survey/2021#section-demographics-mental-health) is a thing.

And let's not forget remote work, for those fortunate to get it, also has it's own challenges.

It's freakin' tough to be a developer.

## The current reality

I'm an introvert.

As a introvert, we're told that **"closed mouths don't get fed"**. So you [speak up or you die](https://observer.com/2017/01/introverts-underrepresented-managerial-positions/). What they didn't teach you is, **"The loudest person in the room gets what they want."**

Have you ever been in a room where you're in a group of incredibly smart people and you're the percieved 'dumb quiet one'? It usually goes like this:

* You bring up a concern.
* Then the loudest person starts speaking to address your concern.
* Everyone starts shaking their head and nobody has any counterpoints.
* You don't agree, but the social pressure stops you from talking.

The loudest person wasn't mean or belittling. They just brought more passion to the table. Everyone else, who may share the same concern as you do, shrug from mental exhaustion or not feeling like they want to.

People don't feel **psychological safety**.

> “Psychological safety at work doesn’t mean that everybody is nice all the time. It means that you embrace the conflict and you speak up, knowing that your team has your back, and you have their backs.”

> “People need to feel comfortable speaking up, asking naïve questions, and disagreeing with the way things are in order to create ideas that make a real difference,” says Altman.

via https://www.ccl.org/articles/leading-effectively-articles/what-is-psychological-safety-at-work/

For those who prefer code --

```
Psychological Safety === Better everything
```

## Creating Psychological Safety

### Rule 1: Start from a place of positivity

In almost any televised sport, watch the players. Teammates high five. Win or lose, opponents say "good game". Players tend to be good sports. It's rare to seeing players belittle a fellow teammate.

Because at the end of the day, it's just another day. It's just another game. There's a thousand more. And another opportunity to shine.

That's how we see it!

Assume positive intent.

In code reviews, feedback is viewed from the lens of the positive. We identify what they've done right, and give them praise for it.

We also give clear feedback and we explain why.

This is what you see in my team:
> Great work on this feature! You really took the time to implement this correctly, and it's going to help us so much in the long run - thank you! I noticed that you missed this. Take a look at this PR and can you try to align it to yours? [PR link]

### Rule 2 - Reminders that we're always learning

Web development changes and evolves. Libraries from three years ago might be outdated/insecure today. In web development, the moment you master something, it's already outdated. Welcome to the real world!

Let's accept the big picture: Coding is hard!

And the result? Imposter syndrome is very real.

To create psychological safety, my team is honest with ourselves. Nobody is a expert. We all feel like imposters. And more importantly, we're always learning.

We remind each other by:

1. Sharing our favorite learning resources, like courses or books. Shoutout to [Frontend Masters](https://frontendmasters.com/)
2. Pushing everyone to contribute to the internal knowledge base. Best way to learn is to teach it [Learn by Teaching](https://en.wikipedia.org/wiki/Learning_by_teaching) is a method of teaching for eons.
3 - Providing a safe space to asks questions. In my company, we have a channel called 'Riddle Me This'. Everyone asks questions, even the experienced devs.

But more importantly, we're not just yelling into the void.

Someone shares a resource, and suddenly... ❤️❤️❤️ everywhere. Everyone on the team appreciates it. We're all naturally curious and hungry for knowledge. (And if they aren't... they're a bad hire.)

### Rule 3: Celebrate as a team

We win as a team. It takes a lot of people to bring a new feature to production. The feature may have been brainstormed by a developer. Another developer took on the task. A bunch of other developers code reviewed it. Another developer may deploy it.

It's never ever just one person.

We go out of our way to remind people of their contribution.

We do that with:

* Shoutouts during standup. Someone did something we like, we let the whole team know.
* Shoutouts in chat. What better way to make it visible?
* Shoutouts in code review description. It's a great feeling to know your code inspired another.
* Monthly shoutouts. Emails praising your work.

It takes a village.

### Rule 4: Problems aren't personal, it's just another todo.

We win as a team. Which also means, we lose as a team.

Finger pointing is a waste of energy.

Quote that.

When a manager demanded to know who caused a bug, I said it was me. DONE. NEXT QUESTION PLEASE. Doesn't matter that I was no where near the code.

Many industries follow the [swiss cheese model](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1298298/), including medical professionals. When there's a medical mishap, there's never a single fault. Typically, a issue went through multiple layers of defense, and we all failed.

![swiss cheese model](2022-01-22-blogpost-6-rules-for-dope-culture_0.png)

It's the same in dev work.

When there's a problem, there's only TWO STEPS.

STEP 1: How we can fix the problem?
STEP 2: How do we mitigate future problems (and we throw more layers of defense)

Everything else is a waste of everyone's time and energy.

In my team, we separate the problem from the person. Then we get to work!

### Rule 5: Leading by example

![bad leader vs good leader](2022-01-22-blogpost-6-rules-for-dope-culture_1.png)

Let's face it. Good leaders lead from the front.

Do you really trust the manager who demands everyone get in the office by 8am, for that manager to waddle in at 9am?

All leaders should be able to do the following:

* Give shoutout and appreciation.
* Apologize for mistakes.
* Say "I don't know".

A leader who is unable to recognize someone's talents is egotistic, and cannot be trusted to support you without a reward.

A leader who is unable to apologize is WEAK and lacks self-realization. In their viewpoint, there's right and wrong, and they're always right.

A leader who is unable to say "I don't know" is insecure and inexperienced. They believe only they can do everything, and may be controlling/difficult.

Everyone on my team inspires to grow and be the best. I push everyone to be leaders, in hopes of forming their own teams one day.

It's important for all of us to show high-quality leadership through example.

### Rule 6: Frequent check-ins

"Stand-ups help with blocks", people say.

Maybe. Stand-ups just tell if someone is alive.

Someone needs to FEEL psychologically safe to share what blockers they have. Otherwise, they'll quickly share what they're doing and pass the mic. I know from experience.

Not everyone communicates the same. I communicate using visual aids and writing. My teammate prefers to jump on calls. Another teammate ONLY communicates through PRs and feedback.

We facilitate check-ins by creating many points of touch, until we figure out their mode of communication.

There's no one-size fits all and requires a lot of trial-and-error. But it's worth it.

## Good Culture is creating Psychological Safety

The industry can get pretty toxic, which can create toxic cultures and many many horror stories.

With that unknown, new hires have a lot of fears coming in.

Once psychological safety is established, the results is real growth. They'll tackle projects. They'll own code. They'll even start pushing the envelope and make us all level up.

These small details cause major ripple effects for everyone.
