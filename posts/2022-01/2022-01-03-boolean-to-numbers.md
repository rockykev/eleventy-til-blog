---
title: TIL converting Booleans to Numbers
date: 2022-01-03
published: true
tags: ['helpers', 'javascript']
series: false
canonical_url: false
description: "This helper is great for APIs. Not every language parses true/false the same. But every language knows that 0 is false, and 1 is true."
layout: layouts/post.njk
---

This helper is great for APIs. Not every language parses true/false the same. But every language knows that 0 is false, and 1 is true.

## Boolean To Number

```js
// helper
const booleanToNumber = (object) => {
    for (const key in object) {
        if (typeof object[key] === "boolean") {
            object[key] = object[key] === false ? 0 : 1;
        }
    }

    return object;
};

// example
const user = {
  firstName: 'Jar Jar',
  lastName: 'Binks',
  isJedi: false,
  isAlliance: true
}

console.log(booleanToNumber(user));
// {firstName: 'Jar Jar', lastName: 'Binks', isJedi: 0, isAlliance: 1}
```

Via
[7 JavaScript Functions to Master Objects Manipulation and Make Your Code Cleaner](https://medium.com/@giuliamalaroda/7-javascript-functions-to-master-objects-manipulation-and-make-your-code-cleaner-cc142a269b13)
