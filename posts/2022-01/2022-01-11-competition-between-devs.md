---
title: TIL competition between developers
date: 2022-01-11
published: true
tags: ['react', 'devstories', 'twitter']
series: false
canonical_url: false
description: "This tribal-fanboy-stanning is silly."
layout: layouts/post.njk
---

> Dan Abramov: some for the nicest, kindest and most amazing people you’ll meet and hang out with will be the people working on the competing projects

I really like this tidbit.

Sometimes, the tribalism gets real. Suddenly, a fan of X Library gets into a yelling match with a fan of Y Library.

Which is silly. Because their creators love to chat.

Evan You openly admits admiration for the React team devs, and borrowing the best parts from React to put into Vue.

This tribal-fanboy-stanning is silly.

https://twitter.com/dan_abramov/status/1470613770569404419
