---
title: TIL web dev debugging parameter trick
date: 2022-01-15
published: true
tags: ['url', 'params', 'debugging']
series: false
canonical_url: false
description: "You probably shouldn't do this on prod, but you can add debugging mode to allow for specific features. Something like: site.com?debug"
layout: layouts/post.njk
---

You probably shouldn't do this on prod, but you can add debugging mode to allow for specific features.

Something like: `site.com?debug`

Features include:

* Automatically running specific JS codes
* Turning on console logs/errors
* adding custom classes (like a red box around everything)

Endless ideas!

