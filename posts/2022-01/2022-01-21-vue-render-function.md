---
title: TIL Vue's Render Function
date: 2022-01-21
published: true
tags: ['vue', 'render', 'advanced']
series: false
canonical_url: false
description: "A good example is that you want to generate a lot of list items, and writing them in the '<template>' section makes it look like spaghetti."
layout: layouts/post.njk
---

In Vue, you can render HTML content in the `<template>` section.

But say you need to dynamically render HTML.

A good example is that you want to generate a lot of list items, and writing them in the `<template>` section makes it look like spaghetti.


```js
export default {
	render (h) {
		return h('div', {}, [...]);
	}
}
```

Dynamic rendering of a component
`h` stands for hypertext (As in HTML)

```js
Vue.component('example', {
	props; ['ok'],
	render(h) {
		return this.ok ? h(Foo) : h(Bar)
	}
})
```

For the react version:

```js
const App = () => {
  return React.createElement(
    "div",
    {},
    React.createElement("h1", {}, "Adopt Me!")
  );
};
```


A better example is located in the Vue Docs, about switching between various header tags.


[Render function](https://v3.vuejs.org/guide/render-function.html)
