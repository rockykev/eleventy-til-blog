---
title: TIL Vue3 has modifiers built in for input forms
date: 2022-01-19
published: true
tags: ['vue', 'forms']
series: false
canonical_url: false
description: 'v-model.number="num2" type="text"'
layout: layouts/post.njk
---


Within a Input, you can force the type to be a number. Take a look -> v-model.number

```html
    <input v-model.number="num2" type="text" />
```

This is often useful when the input type is `text`. If the input type is `number`, Vue can automatically convert the raw string value to a number, and you don't need to add the `.number` modifier to v-model. If the value cannot be parsed with `parseFloat()`, then the original value is returned.


[Form Modifiers](https://v3.vuejs.org/guide/forms.html#modifiers)
