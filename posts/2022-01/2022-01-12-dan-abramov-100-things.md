---
title: TIL Dan Abramov's 100 things
date: 2022-01-12
published: true
tags: ['twitter', 'devtips']
series: false
canonical_url: false
description: "100 things from Dan Abramov. This post has a few of my favorites."
layout: layouts/post.njk
---

Dan Abramov's 100 things learned on the react team: https://threadreaderapp.com/thread/1470613731071696896.html

Some of my favorites:

## Product Related

10. if you fix something, add a test that fails. if you don’t add one, you’re tacitly agreeing that someone else (including you) will break it again.


> 30. if you can’t prove out a migration strategy with a small team on a big codebase, you don’t have a migration strategy. start again

> 33. if something doesn’t make sense, no amount of documentation or explanations will make it make sense.

51. you keep hearing complaints about an API. the fix is simple, like flipping a switch. fine!! you change it. suddenly, the other half, previously quiet, comes frustrated. oops. turns out both options were equally disliked. congrats, you’ve just churned your users for no reason

57. thought experiments are your main tool as an API designer. “imagine a tabbed view…” “imagine a combobox…” “suppose we have a feed and a profile…” is how many breakthroughs begin. you start with a situation, think through ideal behavior — from user and system point of view

## Dev Teams


> 41. taking ownership over something unowned (but that everybody cares about) is one of the most valuable things you can do on a team. try not to drop it though (i’m guilty of that)

49. all people are irreplaceable. every iteration of a team is a new team in some sense. in some ways this is poignant, in others this is exciting

58. that clever optimization? probably unnecessary and will cost you a few weeks of bug hunting. oh, and good luck ripping it out

61. if you’re not sure between recommending two approaches, wait it out. it might be because you don’t know all the constraints yet. it’s like solving nonogram puzzles. as you solve problems, they fill out the constraints for other problems. at some point only few options work

82. “i don’t know” and “i don’t understand this part” and “can you say this again” are phrases you should expect to say (and hear) in productive meetings

83. you need to create space for design discussions that isn’t crammed into a regular sync meetings. most enlightening discussions i’ve seen are free-flowing, didn’t have a predetermined agenda, went into rabbit holes, and needed a few hours to get to a conclusion. write it down!

89. the way each person thinks makes a lasting contribution to the team. “what would X do” is not only a useful question that X can answer but also a way to nudge our own thinking for years going forward

99. newer people on the team won’t know the norms if you don’t tell them. if reviews are slow in general they might think they specifically are being ghosted. be explicit about everything and always follow-up on how they feel and what’s concerning them


## Being on the internet

> 5. a couple of trolls, even well-intentioned, can poison an entire discussion space.

> 19. don’t DM with a troll. it’s a waste of time. don’t get dragged into twitter feuds. not enough space for nuance. redirect discussion to a technical long-form space. keep DMs for friends and people you trust

> 34. people will copy and paste every code snippet you write. even the ones marked with “don’t copy paste, this is the bad example, the good one is below”

> 35. your personal social media account shouldn’t be a primary source of truth for news in a mature project. people will get FOMO plus it’s legitimately hard to track. you can repost but keep news in official sources (like GH)

72. people have opinions on everything. even if you’re unable to handle the volume or respond individually, it helps to provide a void to shout into so that people feel heard, the discussion is consolidate, and doesn’t spill out into random platforms. then you can revisit it

73. much of Programming Discourse is shaped by people who aren’t shipping production code and have lost touch with the reality. get used to it and carry on. oh by the way… you are one of those people now
