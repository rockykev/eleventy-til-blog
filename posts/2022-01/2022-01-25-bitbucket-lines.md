---
title: TIL How to highlight specific lines in bitbucket
date: 2022-01-25
published: true
tags: ['git', 'url', 'bitbucket']
series: false
canonical_url: false
description: "You may want to highlight specific lines in bitbucket by using #lines-1,2,3"
layout: layouts/post.njk
---


You may want to highlight specific lines in bitbucket to identify the lines you want someone to be aware of.

You can do that by adding the line paramters, and including what lines you want.

EXAMPLE:
```
.../stripey.css#lines-10,11,15
```


PATTERN
```
URL#lines-[LINE-NUMBER-HERE-COMMA-SEPERATED]
```


![](2022-01-25-bitbucket-lines_0.png)
