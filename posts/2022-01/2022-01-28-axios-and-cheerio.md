---
title: TIL a quick recipe involving Axios and Cheerio
date: 2022-01-28
published: true
tags: ['web-scraping', 'javascript', 'fetch', 'recipe', mdn]
series: false
canonical_url: false
description: "A quick recipe to http request an api and use cheerio to web-scrape it"
layout: layouts/post.njk
---

[Axios](https://axios-http.com/docs/intro) is a more feature-rich [fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API).

It's also great because Fetch has limited support (back in the day. It has better support now).

Cheerio is a library to web scrape.

It allows you to visit a site, download it in pure html fashion, then traverse the content and only pull the data you need.
## Using Axios

```js
const axios = require("axios")
const cheerio = require("cheerio");

const getBreeds = async () => {
  try {
    return await axios.get("https://dog.ceo/api/breeds/list/all")
  } catch (error) {
    console.error(error)
  }
}

const countBreeds = async () => {
  const breeds = getBreeds()
    .then(response => {
      if (response.data.message) {
        console.log(`Got ${Object.entries(response.data.message).length} breeds`)
      }
    })
    .catch(error => {
      console.error(error)
    })
}

countBreeds()
```
