---
title: TIL Rainbow Books for CD formats
date: 2022-12-18
published: true
tags: ['history', 'cd', 'data', 'videogames']
series: false
canonical_url: false
description: "Several volumes of specifications covering the topic,  together called the 'Rainbow Books', were released over a period of 20 years from 1980 to 2000."
layout: layouts/post.njk
---


Today I learned about Rainbow Books!

![](2022-12-18-rainbow-books_0.png)

These books define various aspects of the CD format, including physical dimensions, data encoding, error correction, and other technical specifications.

> Several volumes of specifications covering the topic,  together called the "Rainbow Books", were released over a period of 20 years from 1980 to 2000.

* The first in the series, the "Red Book", specs the original audio Compact Disk (CD).
* The "Yellow Book" is an extension of the red book, introducing CD-ROM data and ISO-9600.
* The "Orange Book" adds CD-DA specs, CD-R (Recordable), and CR-WR (Rewritable).
* The "White Book" is dedicated to Video-CD (VCD).
* The "Green Book" is about CD-Interactive (CD-I).
* The "Blue Book" is about Enhanced-CD (ECD) for multimedia support.
* The "Scarlet Book" covers Super Audio (SACD) introducing high-resolution audio.
* The "Purple Book" is about Double Density CD (DDCD) pushing capacity from 650MiB to 1.3GiB.
* The "Cyan Book" covers the 9660 filesystem specs extensively.


For reference: https://obsoletemedia.org/audio/compact-disc-rainbow-books/

For PSX Games, they're considered [CD-ROM XA](https://en.wikipedia.org/wiki/CD-ROM#CD-ROM_XA_extension), part of the Yellow Book.

The PlayStation File System (PSFS) is a proprietary file system used by the PS1 that extends the CD-ROM XA format.

Via [THE POLYGONS OF DOOM: PSX](https://fabiensanglard.net/doom_psx/index.html)
