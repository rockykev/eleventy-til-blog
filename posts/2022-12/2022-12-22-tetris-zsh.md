---
title: TIL playing Tetris inside Zsh
date: 2022-12-22
published: true
tags: ['terminal', 'easteregg']
series: false
canonical_url: false
description: "If you're using a Mac, you're probably using iTerm. 1. Load iTerm 2. launch emacs. 3. Launch Tetris."
layout: layouts/post.njk
---


Today I learned you can [play Tetris inside zsh](https://www.reddit.com/r/archlinux/comments/f9o87f/zsh_has_tetris_built_in/)


In zsh:

```bash
autoload -Uz tetriscurses

tetriscurses
```

If you're using a Mac, you're probably using iTerm.


1. Load iTerm
2. launch emacs `$ emacs`
3. Launch Tetris


Hit Escape, X to get to the command line.

![](2022-12-22-tetris-zsh_0.png)

Then type `tetris`.

![](2022-12-22-tetris-zsh_1.png)

> Emacs Games can be found in /usr/share/emacs/22.1/lisp/play
To get you started, here are some game names that I know work:

* tetris
* pong
* snake
* solitaire (Not what you think)
* gomoku (sort of like Connect 4)
* 5x5
* dunnet (text based adventure game)
* landmark
* doctor


Via https://computers.tutsplus.com/tutorials/how-to-play-tetris-pong-and-other-hidden-games-on-your-mac--mac-44485

