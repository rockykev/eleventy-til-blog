---
title: TIL the simulating a Pipe function
date: 2022-12-03
published: true
tags: ['js', 'functionalprogramming']
series: false
canonical_url: false
description: "It’s a pipe function that allows you to chain multiple operations together by taking a series of functions as arguments and applying them in a specific order to the input."
layout: layouts/post.njk
---


What does this function do?

```js
const surprise = (...fns) => input => fns.reduce(
  (acc, fn) => fn(acc), input
)
```

It’s a pipe function that allows you to chain multiple operations together by taking a series of functions as arguments and applying them in a specific order to the input.


Instead of doing something like this.

```js
const toUpperCase = str => str.toUpperCase()
const removeSpaces = str => str.replace(/\s/g, "")
const addExclamation = str => str + "!"

toUpperCase(removeSpaces(addExclamation("Subscribe to Bytes")))
```

You can do something like this.

```js
const pipe = (...fns) => input => fns.reduce(
  (acc, fn) => fn(acc), input
)

const formatString = pipe(toUpperCase, removeSpaces, addExclamation)

formatString("Subscribe to Bytes") // SUBSCRIBETOBYTES!
```

There’s currently a perpetual TC39 proposal for adding a true Pipe operator (|>) to JavaScript here: https://github.com/tc39/proposal-pipeline-operator

