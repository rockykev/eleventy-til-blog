---
title: TIL how to get what browser with just css
date: 2022-12-05
published: true
tags: ['browsers', 'safari', 'css']
series: false
canonical_url: false
description: "Today I learned about how to tell what browser you are using with just CSS."
layout: layouts/post.njk
---


Today I learned about how to tell what browser you are using with just CSS.

```css
/** Internet Explorer */
@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
  div {
    display: block;
  }
}

/** Microsoft Edge */
@supports (-ms-ime-align: auto) {
  div {
    display: block;
  }
}

/** Mozilla Firefox */
@-moz-document url-prefix() {
  div {
    display: block;
  }
}

/** Safari */
@media not all and (min-resolution: 0.001dpcm) {
  div {
    display: block;
  }
}

/** Chrominum */
@media screen and (-webkit-min-device-pixel-ratio: 0) and (min-resolution: 0.001dpcm) {
  div:not(*:root) {
    display: block;
  }
}

```


via [How to Create Browser Specific CSS Code](https://www.browserstack.com/guide/create-browser-specific-css)
