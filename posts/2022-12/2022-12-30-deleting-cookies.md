---
title: TIL how to delete all cookies with Javascript
date: 2022-12-30
published: true
tags: ['javascript', 'cookies']
series: false
canonical_url: false
description: "If someone selects 'Reject', you want to remove all the tracking cookies. You can easily clear all cookies stored in a web page by accessing the cookie using document.cookie and clearing it."
layout: layouts/post.njk
---

I was building a plugin to add a Consent Bar because of the California Consumer Privacy Act (CCPA).

If someone selects 'Reject', you want to remove all the tracking cookies.

You can easily clear all cookies stored in a web page by accessing the cookie using document.cookie and clearing it.

```js
const clearCookies = document.cookie.split(';').forEach(cookie => document.cookie = cookie.replace(/^ +/, '').replace(/=.*/, `=;expires=${new Date(0).toUTCString()};path=/`));
```

via [20 Killer JavaScript One Liners](https://dev.to/saviomartin/20-killer-javascript-one-liners-94f)
