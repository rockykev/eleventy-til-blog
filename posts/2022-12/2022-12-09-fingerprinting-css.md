---
title: TIL fingerprinting with CSS
date: 2022-12-09
published: true
tags: ['security', 'css']
series: false
canonical_url: false
description: "Fingerprinting refers to building up enough metadata about a user that you can essentially figure out who they are. JavaScript has access to all sorts of fingerprinting possibilities, which then combined with the IP address that the server has access to, means fingerprinting is all too common."
layout: layouts/post.njk
---

Today I learned that you can get fingerprinting through CSS!

## What is fingerprinting

> It’s a term that refers to building up enough metadata about a user that you can essentially figure out who they are. JavaScript has access to all sorts of fingerprinting possibilities, which then combined with the IP address that the server has access to, means fingerprinting is all too common.

## How to do it


Here's a proof of concept:

```css
.pointer {
  background-image: url('/unique-id/pointer=none')
}

@media (any-pointer: coarse) {
  .pointer {
    background-image: url('/unique-id/pointer=coarse')
  }
}

@media (any-pointer: fine) {
  .pointer {
    background-image: url('/unique-id/pointer=fine')
  }
}
```

> By sending a variety of media queries that apply to specific browser characteristics, the browser will select a set of styles that apply to itself. We then trick the browser into sending this information back to the server by setting the background-image of these styles to a specific URL. The server will then respond with HTTP Status 410 (Gone) to avoid any requests of these characteristics on subsequent reloads.

via [https://csstracking.dev/](https://csstracking.dev/)


Via the site, let's get even more data.

```css

// building the identity of the user
// check if they have a pointer, and a theme preference
.body {
  --unique-identifier: 'foo' // unique generated ID
  --pointer: 'none'
  --theme-preference: 'none'

  // Only make one request
  background-image: url("/some/url/?" + var(--unique-identifier) + "&" + var(--pointer) + "&" + var(--theme-preference))
}

// Detect pointer type and theme
@media (any-pointer: coarse){
  body {
    --pointer: 'coarse'
  }
}

@media (prefers-color-scheme: dark) {
  body {
    --theme-preference: 'dark'
  }
}

```
And here's the code tracking your fingerprints and updating Javascript:

[I consent -- Capture my information](https://csstracking.dev/fingerprint)

via [CSS-Based Fingerprinting](https://css-tricks.com/css-based-fingerprinting/)



