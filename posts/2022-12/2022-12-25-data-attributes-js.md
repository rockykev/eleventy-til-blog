---
title: TIL accessing different data attributes
date: 2022-12-25
published: true
tags: ['javascript', 'data', 'html']
series: false
canonical_url: false
description: "You can access it in javascript using element.getAttribute('data-sandwich'). A better way is to go with the Element.dataset property."
layout: layouts/post.njk
---


Say you had data you wanted to store on a element.


```html
<div id="lunch" data-sandwich="tuna" data-drink="soda" data-side="chips" data-snack="cookie" data-payment-method="credit card">
	Lunch!
</div>

```

To use it, you can use:

```js
const lunchElement = document.getElementById('lunch');

const lunchSandwich = lunchElement.getAttribute('data-sandwich');

console.log(lunchSandwich); // Output: "tuna"
```

But that's a lot of data you have to pass for `data-drink`, `data-side`, `data-snack`, and `data-payment-method`.


A better way is to go with the `Element.dataset` property.

```js
let lunch = document.querySelector('#lunch');
let data = lunch.dataset;

let data = {
	drink: 'soda',
	paymentMethod: 'credit card',
	sandwich: 'tuna',
	side: 'chips',
	snack: 'cookie'
};
```

via [Managing data attributes with the dataset property in vanilla JavaScript](https://gomakethings.com/managing-data-attributes-with-the-dataset-property-in-vanilla-javascript/)
