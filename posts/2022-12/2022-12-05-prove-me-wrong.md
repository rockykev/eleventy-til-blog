---
title: TIL Proving people wrong
date: 2022-12-05
published: true
tags: ['inspiration', 'seals']
series: false
canonical_url: false
description: "In chapter 3 where during training, a instructor said to another student, 'You're a tiny little man. Those waves will break you in half.' The student said, 'I won't quit.'"
layout: layouts/post.njk
---

I'm currently reading the book "Make Your Bed" By William McRaven.

In chapter 3 where during training, a instructor said to another student, "You're a tiny little man. Those waves will break you in half."

The student said, "I won't quit."

The instructor leaned in and said:

PROVE ME WRONG.

> “SEAL training was always about proving something. Proving that size didn’t matter. Proving that the color of your skin wasn’t important. Proving that money didn’t make you better. Proving that determination and grit were always more important than talent.”

 via https://theprocesshacker.com/blog/make-your-bed-book-summary/#h-chapter-3-only-the-size-of-your-heart-matters

