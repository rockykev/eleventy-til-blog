---
title: TIL that Japanese sites are dark and dense
date: 2022-12-11
published: true
tags: ['webdesign', 'development', 'asia']
series: false
canonical_url: false
description: "In Japan, websites are often look incredibly outdated."
layout: layouts/post.njk
---

In Japan, websites are often look incredibly outdated.

Here's a comparison of Yahoo from Japan Vs America
![Yahoo Japan's Homepage - Dec 11 2022](2022-12-11-japan-sites-dense_0.png)

![Yahoo America's Homepage - Dec 11 2022](2022-12-11-japan-sites-dense_1.png)

## Why?

This blog post [Why Japanese Web Design Is So… Different](https://web.archive.org/web/20221201110913/https://randomwire.com/why-japanese-web-design-is-so-different/) points out the following:

> Go on a safari around some of Japan’s most popular sites and here’s what you can expect to find (see Goo, Rakuten, Yomiuri, NicoNico, OKWave, @cosme, and more):

* Dense tightly packed text
* Tiny low-quality images
* More columns than you can count
* Bright clashing colours and flashing banners
* Overuse of outdated technologies like Flash

----
##  My main takeaways from that post:

* Japanese doesn’t have italics or capital letters which limits the opportunities for adding visual punch that you get with Latin alphabets.

*  In general Japanese culture does not encourage risk-taking or standing out from the crowd. Once a precedent has been set for things looking or behaving a certain way then everybody follows it, regardless of whether there is a better solution.

* Walk around one of Tokyo’s main hubs like Shibuya and you’re constantly bombarded with bright neon advertisements, noisy pachinko parlours (game arcades), and crowds of rambunctious salarymen or school kids.

* Mobile Legacy – Japan was using their version of the mobile web on advanced flip phones long before the iPhone came along and in even larger numbers than personal computers. Back then the screens were tiny and the way sites had to be designed to cram content into this small space has continued to influence the way things are now.


## With data

That's why I really like this post: [the peculiar case of japanese web design (Via the Wayback Machine)](https://web.archive.org/web/20221203194551/https://sabrinas.space/)

The data points from the site:
![](2022-12-11-japan-sites-dense_2.png)

![](2022-12-11-japan-sites-dense_3.png)

Notice how websites in Japan tend to lean heavily towards the Dark/Dense category of images.

I highly recommend reading that post about how that data was gathered.
