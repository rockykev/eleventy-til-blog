---
title: TIL adding a helper function for CSS classes
date: 2022-12-14
published: true
tags: ['css', 'javascript', 'utility']
series: false
canonical_url: false
description: "I like this helper. This lets you quickly add/remove CSS classes to your elements in a easy-to-read way."
layout: layouts/post.njk
---

I like this helper.
This lets you quickly add/remove CSS classes to your elements in a easy-to-read way.

```js
function addClass(elems, ...classes) {
	// Add the .color-blue class
	for (let elem of elems) {
		elem.classList.add(...classes);
	}
}

function removeClass(elems, ...classes) {
	for (let elem of elems) {
		elem.classList.remove(...classes);
	}
}

let p = document.querySelectorAll('p');
addClass(p, 'color-blue', 'text-large'); // add two classes to the p element
```

Via https://gomakethings.com/how-to-create-vanilla-javascript-helper-functions-to-add-and-remove-classes-from-multiple-elements/
