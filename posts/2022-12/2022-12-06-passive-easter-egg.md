---
title: TIL passive aggressive easter egg
date: 2022-12-06
published: true
tags: ['easteregg', 'search', 'css']
series: false
canonical_url: false
description: "Go to this corporate website and open the search (top right) and then submit with an empty search 7-8 times for a passive aggressive response."
layout: layouts/post.njk
---

> Go to this corporate website and open the search (top right) and then submit with an empty search 7-8 times for a passive aggressive response.

This the site: https://www.apa.com.au/

![](2022-12-06-passive-easter-egg_0.png)

Spoilers:

After the 7th click, it displays each word one at a time.

What. Are. You. Looking. for. ?

via [justrhysism on Reddit](https://www.reddit.com/r/javascript/comments/m2rdx1/comment/gqq8qyx/?utm_source=reddit&utm_medium=web2x&context=3)
