---
title: TIL how JSON bet on simplicity
date: 2022-12-13
published: true
tags: ['json', 'api', 'history']
series: false
canonical_url: false
description: "I had a goal of being able to put JSON standard on the back of a business card"
layout: layouts/post.njk
---

Amazon bet big on APIs. In their [API mandate](https://chrislaing.net/blog/the-memo/) from 2002:

> All service interfaces, without exception, must be designed from the ground up to be externalizable. That is to say, the team must plan and design to be able to expose the interface to developers in the outside world. No exceptions.

It's incredibly forward thinking for twenty years ago.

Today, building something like headless WordPress is something we take for granted.

Overall, it works like this. A server outputs JSON data. Then, you consume that JSON data to do something with it. That something could mean build a website, a app, or even have it push to your refrigerator!

It actually doesn't have to be JSON. It could be XML, or YAML.

## What is JSON and why did it become the most popular?

> While working at State Software, the duo (Douglas Crockford & Chip Morningstar) began tinkering with the web and, more specifically JavaScript. They wanted to be able to pass data back and forth between a server and a browser... without needing to fully refresh the page. There wasn’t a great way to do it.

> Douglas Crockford launches JSON.org, which describes the data interchange format he and Chip Morningstar had created a year prior.

> Years later, when describing the history of JSON, Crockford stressed the simplicity of the format. “One of the key design goals behind JSON was minimalism. My idea was that the less we have to agree on in order to inter-operate, the more likely we’re going to be able to inter-operate well,” he began, “I had a goal of being able to put JSON standard on the back of a business card.”

![](2022-12-13-json-simplicity_0.png)

> For a while, JSON stayed relatively small, passed around and implemented as compatible parsers in various programming languages. Each time a new parser was released, Crockford would add it to his website. And slowly, a community began to form around the standard.

> In 2007, Ruby on Rails added support for JSON serialization to their data retrieval API, automatically enabling JSON for thousands of applications. It was a turning point for the format. Years later, in 2014, JSON was adopted by the ECMA standards body and officially specified. By then, it was already the more popular choice among web developers.

Via [AJAX without the X: The History of JSON by Jay Hoffmann](https://thehistoryoftheweb.com/ajax-without-the-x-the-history-of-json/)
