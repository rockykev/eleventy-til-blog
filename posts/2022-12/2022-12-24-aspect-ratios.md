---
title: TIL aspect ratios
date: 2022-12-24
published: true
tags: ['css', 'iframe']
series: false
canonical_url: false
description: "We can target iframe elements directly with a few lines of CSS to preserve their dimensions in responsive layouts, without the need for a wrapper element."
layout: layouts/post.njk
---

I didn't even consider about resizing a iframe to fit.

> We can target iframe elements directly with a few lines of CSS to preserve their dimensions in responsive layouts, without the need for a wrapper element.

> Set the height and width of the iframe to 100%. Then assign an aspect-ratio property with a value of width / height. For ourHD videos, we’d use 16 / 9.


```html

<style>
iframe {
	aspect-ratio: 16 / 9;
	height: 100%;
	width: 100%;
}
</style>

<iframe src="https://player.vimeo.com/video/28523422?h=7755bc3e5f" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
```


via [Responsive iframes with the CSS aspect-ratio property](https://gomakethings.com/responsive-iframes-with-the-css-aspect-ratio-property/)
