---
title: TIL how Street Fighter 2's Title animation worked
date: 2022-12-10
published: true
tags: ['videogames', 'sprites']
series: false
canonical_url: false
description: "Looking at the spritesheet, how the heck does the small logo become the big logo?"
layout: layouts/post.njk
---

You might remember this kick-ass arcade graphic.

![](2022_12_10_sf.gif)

Via the sheet explorer, we can look at the GFXROM. The tiles used for the logo can be found in sheet 0x8200.

![](2022-12-10-streetfighter2-logo_0.png)

So how the heck does the small logo become the big logo?

* The small logo moves down to give enough clearance for the big one.
* The logo spins twice.
* After the first spin, the small logo is replaced with the big one.
* The tiles are not flipped horizontally or vertically. Their positions are merely rotated around the logo center.
* Translucency is achieved by drawing the text every other frame.
* If you move fast enough the effect is visually "acceptable".


And the SNES version

![](2022-12-10-sf-snes_spin.gif)


> One year after the arcade release, Capcom published a Super Nintendo port on Jul 15, 1992. They did a superb job, managing to have all the characters, moves and beautiful GFX despite using a game console with less capacity and lower resolution.

> Amazingly they managed to render a better spinning effect thanks to the SNES Mode 7 + HDMA.

> For the subtitle translucency, a simple dithering with a sprinkle of wobbling did the trick.


Read the full article to see each element move piece by piece! It's really fascinating!

via [STREET FIGHTER II, SPIN WHEN YOU CAN'T](https://fabiensanglard.net/sf2_spin/index.html)

