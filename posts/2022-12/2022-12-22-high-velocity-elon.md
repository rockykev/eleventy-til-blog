---
title: TIL about Ian Brown
date: 2022-12-22
published: true
tags: ['twitter', 'workplace']
series: false
canonical_url: false
description: "During the call, Musk said that Twitter would need to do a 'total rewrite of the whole thing' if it wanted to have a 'really high velocity.'"
layout: layouts/post.njk
---


For me, this is current events. But for you, this might be ancient history.

You might be familiar with [forced acquisition of Twitter](https://en.wikipedia.org/wiki/Acquisition_of_Twitter_by_Elon_Musk), who was toying with the market until he was sued to buy it.

Well, after massive layoffs and a [whole bunch of boneheaded moves](https://www.reddit.com/r/OutOfTheLoop/comments/ynqq98/whats_going_on_with_twitter_and_elon_musk/), Musk continued to beat the "we need a re-write" drum.

During the call, Musk said that Twitter would need to do a “total rewrite of the whole thing” if it wanted to have a “really high velocity.”

“Wait, seriously, a total rewrite? That’s your prediction for velocity?” A developer asked in the shadows.

“Yeah,” Musk quickly replied.

The dev asked: “break it down.”

And that's where Musk, in his infinite technical wisdom, shows what kind of human being he was. Unable to give a technical answer, Musk asked who this guy was, then called him a jackass.

You can watch the [whole video here](https://www.reddit.com/r/facepalm/comments/zsghfu/elon_musk_getting_owned_by_a_former_twitter/).

Who was that developer who challenged Musk? That was Netflix engineer [Ian Brown](https://www.linkedin.com/in/ianbrown/), who worked at Twitter for more than 8 years as the Sr. Engineering Manager.

In the words of Richard Feynman: “For a successful technology, reality must take precedence over public relations, for nature cannot be fooled.”

You have real people who want to do what's best for Twitter. But then you have a egotistic manchild so sensitive to being emotionally hurt, that he will continue you burn it to the ground. Well, it's your money, Elon!


P.S. If only someone wrote a post about why complete re-writes are bad ideas. [Oh wait here's one from 2000](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/).
