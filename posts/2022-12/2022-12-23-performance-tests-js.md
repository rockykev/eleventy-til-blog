---
title: TIL Performance tests
date: 2022-12-23
published: true
tags: ['performance', 'javascript', 'optimization']
series: false
canonical_url: false
description: "While doing performance tests, I was curious how long a loop was running. I used the performance.now() trick."
layout: layouts/post.njk
---


While doing performance tests, I was curious how long a loop was running.

I used this trick to calculate the performance of a function.


```js

const start = performance.now();
const end = performance.now();

const total = start - end;

console.log(`function took ${total} milliseconds to run`);
```


via [12 Important JavaScript Functions Every Web Developer Should Know](https://javascript.plainenglish.io/12-important-javascript-functions-every-web-developer-should-know-e488c4bbf521)
