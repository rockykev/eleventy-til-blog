---
title: TIL the Description List element
date: 2022-12-28
published: true
tags: ['html', 'semantic', 'tables']
series: false
canonical_url: false
description: "Lists of name–value pairs (or, in some cases, name–value groups) are a common pattern across the web, in part due to their versatility. HTML lets us mark up these lists with a combination of three elements"
layout: layouts/post.njk
---

via [Ben Myer's On the <dl>](https://benmyers.dev/blog/on-the-dl/)


How would you make this?

![](2022-12-28-description-list-element_0.png)


A bunch of `div`, `p`, and `span` tags?

What if you went with the [Description List](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl) element?


Check it out!


```html
<div>
	<h1>Kobold</h1>
	<small>Small humanoid (kobold), lawful evil</small>
	<dl>
		<div>
			<dt>Armor Class</dt>
			<dd>12</dd>
		</div>
		<div>
			<dt>Hit Points</dt>
			<dd>5 (2d6 - 2)</dd>
		</div>
		<div>
			<dt>Speed</dt>
			<dd>30 ft.</dd>
		</div>
	</dl>
	<dl aria-label="Ability Scores">
		<div>
			<dt>STR</dt>
			<dd>7 (-2)</dd>
		</div>
		<div>
			<dt>DEX</dt>
			<dd>15 (+2)</dd>
		</div>
		<div>
			<dt>CON</dt>
			<dd>9 (-1)</dd>
		</div>
		<div>
			<dt>INT</dt>
			<dd>8 (-1)</dd>
		</div>
		<div>
			<dt>WIS</dt>
			<dd>7 (-2)</dd>
		</div>
		<div>
			<dt>CHA</dt>
			<dd>8 (–1)</dd>
		</div>
	</dl>
	<dl aria-label="Proficiencies">
		<div>
			<dt>Senses</dt>
			<dd>Darkvision 60 ft.</dd>
			<dd>Passive Perception 8</dd>
		</div>
		<div>
			<dt>Languages</dt>
			<dd>Common</dd>
			<dd>Draconic</dd>
		</div>
		<div>
			<dt>Challenge</dt>
			<dd>1/8 (25 XP)</dd>
		</div>
	</dl>
	<dl aria-label="Traits">
		<div>
			<dt>Sunlight Sensitivity</dt>
			<dd>
				While in sunlight, the kobold has disadvantage
				on attack rolls, as well as on Wisdom (Perception)
				checks that rely on sight.
			</dd>
		</div>
		<div>
			<dt>Pack Tactics</dt>
			<dd>
				The kobold has advantage on an attack roll against
				a creature if at least one of the kobold's allies
				is within 5 ft. of the creature and the ally
				isn't incapacitated.
			</dd>
		</div>
	</dl>
	<h2 id="actions">Actions</h2>
	<dl aria-labelledby="actions">
		<div>
			<dt>Dagger</dt>
			<dd>
				<i>Melee Weapon Attack:</i> +4 to hit, reach 5 ft.,
				one target. <i>Hit:</i> (1d4 + 2) piercing damage.
			</dd>
		</div>
		<div>
			<dt>Sling</dt>
			<dd>
				<i>Ranged Weapon Attack:</i> +4 to hit, reach 30/120 ft.,
				one target. <i>Hit</i>: (1d4 + 2) bludgeoning damage.
			</dd>
		</div>
	</dl>
</div>
```


> Lists of name–value pairs (or, in some cases, name–value groups) are a common pattern across the web, in part due to their versatility. HTML lets us mark up these lists with a combination of three elements:

* The `<dl>`, or description list, element, which wraps the entire list of name–value pairs

* The `<dt>`, or description term, element, which represents a name in our name–value pairs

* The `<dd>`, or description detail, element, which represents a value in our name–value pairs

Comparing it to Javascript Objects, it's

`label = { key : value }`

Or `dl = { dt : dd }`

## What are the benefits?

You totally could have used a bunch of divs.

So benefits:

1. The screenreader could tell the user how many name–value groups are in the list.

2. The screenreader could tell the user how far into the list they are.

3. The screenreader could treat the list as one block that the user could skip over if they're uninterested in it.

## Other examples

![](2022-12-28-description-list-element_1.png)

![](2022-12-28-description-list-element_2.png)

![](2022-12-28-description-list-element_3.png)

