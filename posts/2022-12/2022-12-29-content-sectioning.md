---
title: TIL Content sectioning
date: 2022-12-29
published: true
tags: ['html', 'webdev', 'browser']
series: false
canonical_url: false
description: "Content sectioning or landmarks, like sidebar, content, footer. You know, like better names."
layout: layouts/post.njk
---

I really like this visual for websites.

Content sectioning or landmarks

![](2022-12-29-content-sectioning_0.png)


via [A short guide to help you pick the correct HTML tag](https://dev.to/polgarj/a-short-guide-to-help-you-pick-the-correct-html-tag-56l9)
