---
title: TIL functions in sass
date: 2022-12-08
published: false
tags: ['css', 'sass', 'function']
series: false
canonical_url: false
description: "You can declare functions in sass using @function. You can even write for loops, return values, and even create @warn and @error."
layout: layouts/post.njk
---


You can declare functions in sass using `@function`.

You can even write for loops, return values, and even create @warn and @error.


Why? I'd say, why not?

```scss
$base-font-size: 16px;
$min-width: 320px;
$max-width: 1200px;

@function font-size($viewport-width) {
  $font-size: $base-font-size + (($viewport-width - $min-width) / ($max-width - $min-width)) * 4rem;
  @return $font-size;
}

h1 {
  font-size: font-size(600px);
}

```


REF:
https://sass-lang.com/documentation/at-rules/function
