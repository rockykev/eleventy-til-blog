---
title: TIL the history of fonts
date: 2022-12-21
published: true
tags: ['fonts', 'history', 'browser']
series: false
canonical_url: false
description: "We came up with all sorts of clever techniques to do what was called 'image replacement' for text. Some of the techniques involved CSS and background images. One of the techniques involved Flash"
layout: layouts/post.njk
---

Some notes I took from [The State Of The Web](https://adactio.com/articles/18580)

## All about Fonts


### How Fonts worked before webfonts

During the early stages of the internet, you only had a few fonts to use. You’d have to guess which fonts were likely to be available on Windows or Mac.

> But what if you wanted to use a typeface that didn’t come installed with an operating system? Well, you went into Photoshop and made an image of the text. Now the user had to download additional images. The text wasn’t selectable and it was a fixed width.


> We came up with all sorts of clever techniques to do what was called “image replacement” for text. Some of the techniques involved CSS and background images. One of the techniques involved Flash. It was called sIFR: Scalable Inman Flash Replacement. A later technique called Cufón converted the letter shapes into paths in Canvas.


### Font Embedding

> There was only one browser that supported font embedding: Microsoft’s Internet Explorer. The catch was that you had to use a proprietary font format called Embedded Open Type.


### Web Open Font Format

Or WOFF!

> The impasse was broken with a two-pronged approach. First of all, we got a new font format called Web Open Font Format or WOFF. It could be used to take a regular font file and wrap it in a light veneer of metadata about licensing. There’s a sequel that’s even better than the original, WOFF2.


### Variable Fonts

> You can typeset text on the web to be light, or regular, or bold, or anything in between.

> When you use CSS to declare the font-weight property, you can use keywords like “normal” or “bold” but you can also use corresponding numbers like 400 or 700. There’s a scale with nine options from 100 to 900. But why isn’t the scale simply one to nine?

> On today’s web you could have 999 font-weight options.


## Everything Else

### The Overview Effect

> Astronauts have been known to experience something called the overview effect. It’s a profound change in perspective that comes from seeing the totality of our home planet in all its beauty and fragility.


### Verschlimmbessern

> There’s a great German word, “Verschlimmbessern”: the act of making something worse in the attempt to make it better. Perhaps we verschlimmbessert the web.


### NASA

> There’s a story told about the first time President Kennedy visited NASA. While he was a getting a tour of the place, he introduced himself to a janitor. And the president asked the janitor what he did. The janitor answered:

> "I’m helping put a man on the moon."

> It’s the kind of story that’s trotted out by company bosses to make you feel good about having your labour exploited for the team. But that janitor’s loyalty wasn’t to NASA, an organisation. He was working for something bigger.



