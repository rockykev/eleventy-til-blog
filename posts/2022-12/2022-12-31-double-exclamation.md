---
title: TIL Double Exclamation shorthand for booleans
date: 2022-12-31
published: true
tags: ['javascript', 'booleans']
series: false
canonical_url: false
description: "JavaScript (and TypeScript) lets you convert a non-Boolean value to Boolean using the double exclamation shorthand. '!!' "
layout: layouts/post.njk
---

Javascript follows Truthy/Falsy values.

If you're not familiar with it:

![](2022-12-31-double-exclamation_0.png)

(Via Wes Bos)

But if you want to make it explicit:

JavaScript (and TypeScript) lets you convert a non-Boolean value to Boolean using the double exclamation shorthand.


```js
const emptyStr = ''
const nonEmptyStr = 'test'

const emptyStrBool = !!emptyStr //false
const nonEmptyStrBool = !!nonEmptyStr //true
```


via [Understanding the exclamation mark in TypeScript](https://blog.logrocket.com/understanding-exclamation-mark-typescript/)
