---
title: TIL the difference between a VPN and a reverse proxy
date: 2022-12-06
published: true
tags: ['networking', 'homenetwork']
series: false
canonical_url: false
description: "A reverse proxy makes a private resource publicly available. A VPN lets you connect to a private network and access the resources on it securely when away from it."
layout: layouts/post.njk
---


Via OP on Reddit:

Can someone explain why do people seem to prefer Reverse Proxy instead of VPN?

## What is a Reverse Proxy

> Imagine you have 3 services. Nextcloud, Octoprint and Home Assistant. All of those want to open a port for web traffic. Of course you could do "example.com" for Nextcloud, "example.com:5000" for Octoprint and "example.com:8123" for Home Assistant. But that sucks.

> You have to remember port numbers. So you get out a reverse proxy and then you can route the traffic from "nc.example.com" to "localhost:80" and "octoprint.example.com" to "localhost:5000" and "ha.example.com" to "localhost:8123" and it will work as you imagine. There are some settings to set so that the application isn't super confused why nothing matches but it's pretty simple.

Via [asyx](https://www.reddit.com/r/selfhosted/comments/zpuh3j/comment/j0w7h0y/?utm_source=reddit&utm_medium=web2x&context=3)


A reverse proxy makes a private resource publicly available.

You can also consolidate authorization.

> Consolidating auth is one of the benefits of using a reverse proxy. Using each service's own auth method means you'll probably never change your passwords, because it's too much of a pain in the ass to touch every single service you're running every time. Also, each of those services may have different auth capabilities, so you don't wind up with a consistent experience unless you're just using the most basic auth features.


> A reverse proxy sits in front of a web server and receives all the requests before they reach the origin server. It works similarly to a forward proxy, except in this case it’s the web server using the proxy rather than the user or client. Reverse proxies are typically used to enhance performance, security, and reliability of the web server.

> For example, you can have a non-WordPress site hosted at example.com domain on Server A and have its blog running on WordPress at example.com/blog URL hosted on Server B. You can achieve this by adding a reverse proxy for the server hosting your primary site. You can configure the reverse proxy to redirect requests to the blog to a different server (e.g. a managed WordPress host like Kinsta).

via https://kinsta.com/blog/reverse-proxy/

## What is a VPN

> A VPN is a tunnel that is secure into your private network. You DON'T want to have services easily accessible. A VPN is what you do if you want to be absolutely sure that whoever hits your network from the internet is a verified user. This is what you do with your torrent seed box or your media streaming. Or your document storage. You absolutely don't want your pirated stuff or your tax returns or any other official business just being accessible on the internet. But what if your tax consultant needs that document when you are in their office? That's when you bust out the VPN, access your document storage service and send them the document.

Via [asyx](https://www.reddit.com/r/selfhosted/comments/zpuh3j/comment/j0w7h0y/?utm_source=reddit&utm_medium=web2x&context=3)


A VPN lets you connect to a private network and access the resources on it securely when away from it.

Via https://www.reddit.com/r/selfhosted/comments/zpuh3j/can_someone_explain_why_do_people_seem_to_prefer/
