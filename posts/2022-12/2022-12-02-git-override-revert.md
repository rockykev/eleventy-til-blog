---
title: TIL reverting git
date: 2022-12-02
published: true
tags: ['git', 'undo']
series: false
canonical_url: false
description: "Two solutions. One is re-write history and hide your mistakes. The other is showing the history, to remind you of your mistakes."
layout: layouts/post.njk
---


Issue: you pushed to master instead of your branch... again.

I do it all the time because I suck.



## Solution 1: Remove the history

This method re-writes history and rolls back your changes.

It's great if you push, realize your mess up, and quickly want to roll back. Highly recommended for those not in teams or if you own the project.

It's bad for those who do not allow force pushes.


How to:
Step 1: create a new branch.

Step 2: check out master.
`git checkout master`

Step 3: reset master by X commits.
`git reset --hard HEAD~3`

Step 4: check out your new branch again.


What happening?

Your branch is now ahead of master by X commits.
All those commit changes will move to that new branch.


Step 5: Force push.

Remember that

master <-- 3 commits behind
branch <-- currently has the new commits

`git push origin master --force`

Via https://stackoverflow.com/a/1628584


## Solution 2: Revert

This method undo's the prior commit, and makes a change in history that you reverted.

If other people have already pulled the changes you want to revert, they will need to pull the new changes you pushed after the revert.



```
git revert commit12345
git push origin master
```
