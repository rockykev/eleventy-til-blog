---
title: TIL about some sites to inspire you
date: 2022-07-24
published: true
tags: ['inspiration','website']
series: false
canonical_url: false
description: "After years of web development, I've come to the conclusion that these sites are simply the most beautiful sites ever. If you ever need a inspiration, here you go."
layout: layouts/post.njk
---

After years of web development, I've come to the conclusion that these sites are simply the most beautiful sites ever.

If you ever need a inspiration, here you go.


## https://www.lingscars.com/

![](2022-07-24-inspiring-sites_0.jpg)

## http://wall.org/

![](2022-07-24-inspiring-sites_1.png)

## https://motherfuckingwebsite.com/

![](2022-07-24-inspiring-sites_2.png)

## http://www.homerswebpage.com/

![](2022-07-24-inspiring-sites_3.png)

## https://cyber.dabamos.de/88x31/

![](2022-07-24-inspiring-sites_4.jpg)
