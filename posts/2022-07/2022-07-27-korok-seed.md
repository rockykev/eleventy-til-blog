---
title: TIL Korok seed puzzle pattern
date: 2022-07-27
published: true
tags: ['gaming', 'gamedev']
series: false
canonical_url: false
description: "Why I love this is that the designers thought up a few puzzle categories, and sprinkled the open world with these. It creates a very effective piece of optional game play."
layout: layouts/post.njk
---

One of my hobbies is reading about how to create incredibly effective things modularly. For example, in Skyrim has close to 200 dungeons. But they're all just kits. Roughly a couple of different art styles and mechanics, remixed/smashed together in many different ways to create all of these beautiful 'handcrafted' dungeons. (And their dungeon content team consists of TEN people! 2 full-time kit art folks, and 8 level designers. [source](https://www.gamedeveloper.com/design/skyrim-s-modular-approach-to-level-design))

`u/_pe5e_` on reddit did a deep dive on Korok puzzles for the game, Breath of the Wild. If you're not familiar with Breath of the Wild, it's an open world game. And Koroks are hidden creatures (900 of them) that you can collect by doing a small puzzle. In other open world games, where you hide collectables in a bush or behind a tree, BoTW does require you to do a bit more.

His full post is here: [An extensive look at Korok Puzzles](https://www.reddit.com/r/Breath_of_the_Wild/comments/v95lu7/an_extensive_look_at_korok_puzzles/)

![](2022-07-27-korok-seed_0.png)

Why I love this is that the designers thought up a few 'puzzle categories', and sprinkled the open world with these. It creates a very effective piece of optional game play.


## The different types of Korok puzzles are as followed:

**Lonely Puzzles**

Under this category fall the following types of Korok Puzzles:

* Lonely Rock
* Lonely Rock (buried)
* Lonely Acorn
* Lonely Balloon

These are the most basic Korok Puzzle and ultimately contain the main function of Koroks. Some objects you find around the map that dont quite fit in with their surroundings. Be it a lonely rock with nothing else around or even a hidden acorn pot or ballon. The most basic one is the lonely rock type where you simply have to lift a rock to find the Korok in question. Some lonely rocks are buried with something above them to hide them better. That can be anything from leaves, slates, doors or stone rubble piles. Remove what lays above the rock, lift it and you get yourself another Korok. Lonely Acorns are often hidden near trees but can also be found elsewhere. Break them with an arrow, weapon, bomb or something else to unlock the Korok. The Lonely Balloon is the airborne version of this puzzle type. Shoot the hidden balloon and earn yourself another Korok.

**Pattern Puzzles**

Under this category fall the following types of Korok Puzzles:

* Block Pattern
* Rock Pattern

Here you need to complete a pattern of some sort. Block Patterns are made out of metal blocks and you need to place the missing cube at the correct spot to match the completed block structure. Rock Patterns are made out of rocks where one or more rocks are missing to complete the pattern (usually one or more circles). Both the missing cube and rock are always hidden closeby to the puzzle. Search the metallic block with the Magnesis rune and walk into the direction where the rock is missing in the pattern to find them.

**Sparkle Puzzles**

Under this category fall the following types of Korok Puzzles:

* Chasing Sparkles
* Climbing Sparkles
* Frozen Sparkles

These Koroks require you to inspect some sparkles that are hidden in different places. Chasing Sparkles are exactly want it sounds like. They will rush around in an area and you have to catch up to them to inspect them and get the Korok. Climbing Sparkles are different as they dont move and are instead located high up like on top of trees that stand out or poles. These also only become Visible once you get close enough. They cant be seen if you are too far away. Lastlly Frozen Sparkles are sparkles that are frozen in ice. Melt them with a fire weapon or campfire to break the ice and then inspect the sparkle to get your Korok.

**
Ring Puzzles**
Under this category fall the following types of Korok Puzzles:

* Dash Ring
* Dive Ring

Both of these puzzles contain a ring you must travel through. Dash Rings start by standing on a tree stump with a leaf on it pointing into the direction in which the ring will spawn. Your task is to get to the ring before it runs out as indicated by its dots. Dive Rings only occur in water. Here the ring is made out of plants that float on the water and make the shape of a ring. You have to jump into it to spawn the Korok. You dont have to dive, a simple jump or fall from your paraglider is sufficient. Swimming into the ring from its side doesnt work however, you need to come from above.

**Hole Puzzles**

Under this category fall the following types of Korok Puzzles:

* Ball-in-a-cup
* Basketball
* Minigolf

These puzzles all require you to place an object into a hole. Ball-in-a-cup puzzles look like the game that they are named after. You have a small metal boulder attached with a chain to some sort of container with a hole to place it in. That can be a hollow tree stump or a well. Use the Magnesis rune to put the boulder into its hole to get the Korok. Basketball puzzles expect you to throw or otherwise shoot a rock into a circle of spikey rocks that stand out of a body of water. There usually lots of rocks above the ring, so you can attempt it multiple times. Minigolf puzzles have the simplest concept out of these three. You need to place a big rock boulder into a hole. This is usually done by using Stasis but simply rolling a boulder down a hill works too as long as you dont overshoot it. Sometimes the hole is also a small pond of lava instead.

**Flower Puzzles**

Under this category fall the following types of Korok Puzzles:

* Flower Chase
* Flower Count

Both of these puzzles revolve around flowers. Flower Chase puzzles use a single yellow flower. Once you find it and walk over it, it will teleport to a new nearby location. Do this up to 10 times and after you collected the last one the Korok will appear. Most of these follow a route across the ground but some also require you to climb sidways or up a wall. In Flower Count puzzles there exist five groups of flowers. Each group has a different amounf of flowers from one to five. You need to walk through them in the correct order counting upwards. If you mess up the puzzle resets.

**Pinwheel Puzzles**

Under this category fall the following types of Korok Puzzles:

* Pinwheel (Acorn)
* Pinwheel (Balloon)

Pinwheel puzzles are puzzles where either some acorns or balloons for you to shoot spawn once you get near the pinwheel. In the acorn version a few acorn pots are thrown into the air around you and you need to shoot them midair. You can use the Stasis rune as a help to freeze them. In the balloon version some balloons will spawn in for you to shoot. They are either hidden, move around or teleport around to make it harder to hit them. You can also use bomb arrows and the Remote Bomb runes to hit multiple balloons at the same time.

**Fruit Puzzles**

Under this category fall the following types of Korok Puzzles:

* Fruit Match

* Fruit Prayer

Both of these puzzles involve fruits. In Fruit Match puzzles you need to match the look of three different plants by removing the fruit of it from the right location. Either apples or Durians from trees or Voltfruits from cacti. If you mess up and remove a fruit on the wrong lcoation of the plant, then you will have to wait until the fruits respawns on the plant to retry the puzzle again. These dont respawn according to the blood moon but rather with a 1% chance each minute when you are far enough away and as such it can be a pain if you get unlucky.Fruit Prayer puzzles require you to place one or more fruits into a prayer bowl until all have at least one. The most common one is with apples but it can also include other fruits like durians or peppers.




