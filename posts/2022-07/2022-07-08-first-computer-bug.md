---
title: TIL the first computer bug
date: 2022-07-08
published: true
tags: ['history','bug']
series: false
canonical_url: false
description: "On September 9, 1947, a team of computer scientists and engineers reported the world’s first computer bug. The team at Harvard University in Cambridge, Massachusetts, found that their computer, the Mark II, was delivering consistent errors. "
layout: layouts/post.njk
---

On September 9, 1947, a team of computer scientists and engineers reported the world’s first computer bug. The team at Harvard University in Cambridge, Massachusetts, found that their computer, the Mark II, was delivering consistent errors.

When they opened the computer’s hardware, they found ... a moth. The trapped insect had disrupted the electronics of the computer.

![](2022-07-08-first-computer-bug_0.png)

What I love about this is their troubleshooting notes!

The engineers 'debugged' this from 8am to 345pm.

They not only solved it by removing the bug, they even tapped that bad boy right on the paper.

Hot damn!

Via [Sep 9, 1947 CE: World’s First Computer Bug](https://education.nationalgeographic.org/resource/worlds-first-computer-bug)
