---
title: TIL ES6 Proxy's apply method
date: 2022-07-06
published: true
tags: ['javascript','mdn', 'advanced']
series: false
canonical_url: false
description: "Using JavaScript proxies you can wrap an existing object and intercept access to its attributes or methods, even if they don’t exist. You can also redefine fundamental operations for that object."
layout: layouts/post.njk
---

Using JavaScript proxies you can wrap an existing object and intercept access to its attributes or methods, even if they don’t exist. You can also redefine fundamental operations for that object.

So if you wanted to create a proxy of `getFullName`...

```js
const user = {
    firstName: 'John',
    lastName: 'Doe'
}

const getFullName = function (user) {
    return `${user.firstName} ${user.lastName}`;
}


const getFullNameProxy = new Proxy(getFullName, {
    apply(target, thisArg, args) {
        return target(...args).toUpperCase();
    }
});

console.log(getFullNameProxy(user)); // JOHN DOE
```

This mutates the result!

What's a good use case?

```js
function sum(a, b) {
  return a + b;
}

const handler = {
  apply: function(target, thisArg, argumentsList) {
    console.log(`Calculate sum: ${argumentsList}`);
    // expected output: "Calculate sum: 1,2"

    return target(argumentsList[0], argumentsList[1]) * 10;
  }
};

const sumTriple = new Proxy(sum, handler);

console.log(sum(1, 2));
// expected output: 3
console.log(sumTriple(1, 2));
// expected output: 30

```

Now you can keep the `sum` function AND a unique version, like sumTriple!

[MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy)

[MDN for handler.apply()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy/Proxy/apply)

