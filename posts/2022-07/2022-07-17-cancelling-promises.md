---
title: TIL cancelling promises
date: 2022-07-17
published: true
tags: ['javascript','mdn']
series: false
canonical_url: false
description: "There is a cross-platform (Node, Browsers, etc) cancellation primitive called AbortController. You can use it to cancel functions that return promises rather than promises themselves."
layout: layouts/post.njk
---

I was debugging a strange promise statement, and I needed to force cancel the promise.

There is a cross-platform (Node, Browsers, etc) cancellation primitive called AbortController. You can use it to cancel functions that return promises rather than promises themselves.


```js
const controller = new AbortController();

const task = new Promise((resolve, reject) => {
  //...
  controller.signal.addEventListener("abort", () => {
    reject();
  });
});

controller.abort();

```
via [StackOverflow](https://stackoverflow.com/a/65805464/4096078)

The `AbortController` interface represents a controller object that allows you to abort one or more Web requests as and when desired.

[MDN - AbortController](https://developer.mozilla.org/en-US/docs/Web/API/AbortController)
