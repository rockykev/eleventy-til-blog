---
title: TIL asciinema
date: 2022-07-09
published: true
tags: ['terminal', 'teaching']
series: false
canonical_url: false
description: "A frequent scenario that I use to teach my team is recording videos of doing terminal commands."
layout: layouts/post.njk
---


A frequent scenario that I use to teach my team is recording videos of doing terminal commands.

There's a sweet terminal program called [asciinema](https://asciinema.org/) built just for this.

`brew install asciinema`

Here's an example:
[![asciicast](https://asciinema.org/a/455413.svg)](https://asciinema.org/a/455413)

<script id="asciicast-455413" src="https://asciinema.org/a/455413.js" async></script>
