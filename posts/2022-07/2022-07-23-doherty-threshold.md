---
title: TIL the Doherty Threshold
date: 2022-07-23
published: true
tags: ['click','ux']
series: false
canonical_url: false
description: "If a computer responds to a users input in less than 400ms, the user will take less time to make their next decision."
layout: layouts/post.njk
---

Doherty Threshold

If a computer responds to a users input in less than `400ms`, the user will take less time to make their next decision.

Takeaway:
* Provide system feedback within 400 ms in order to keep users’ attention and increase productivity.
* Use perceived performance to improve response time and reduce the perception of waiting.
* Animation is one way to visually engage people while loading or processing is happening in the background.
* Progress bars help make wait times tolerable, regardless of their accuracy.
* Purposefully adding a delay to a process can actually increase its perceived value and instill a sense of trust, even when the process itself actually takes much less time.


via [https://lawsofux.com/en/doherty-threshold/](https://lawsofux.com/en/doherty-threshold/)

There is some caveats:

> I'm not disputing the findings... But this is a paper published by IBM encouraging people to upgrade their computers (in 1982 when they were the dominant manufacturer in personal computing), and should be read with that in mind.

---

> In my experience, latency/snappiness is directly proportional to how satisfied I am when using a device or computer, and above a certain threshold it becomes enraging.

One example that comes to mind is my first Kindle: it was lightweight, battery lasted a month, beautiful screen... a great device. But if you had to go back a dozen pages to reread a passage, you'll immediately miss your paper books, just because the screen redraw takes half a second.

---

> This study is hardly the only one equating response time with profits and user satisfication... look into studies by Amazon on the same thing... they correlate things like an extra 10ms on page loads with 15% drops in profits.

Response time is critically important... and if your app is below that - find a way to do some A/B testing, fix it, and see where it gets you. IF you can show better customer retention/profits or better throughput from staff, you'll win the argument.

---

> If my company is aware of this study, I suspect they're taking the wrong lesson from it.

The app has plenty of regretfully-slow operations (slow "searches" and paging, individual item-pages that load a ton of shit "just in case any user might want to see it", workflow buttons that take ages)... But instead they ask for more "One Click And Fields Get Magically Filled Out Somehow" javascript buttons.

While this may save some time for users, it does so only by speeding up things which were already fluid, like raw data entry into fields. What it doesn't do is solve the issue of users losing their train of thought.

---

> I think the "400ms" thing is inferred from Figure 7 where, if you squint you can see that the Expert line takes a sharp upturn right around 400ms, and the Average line does the same at about 300ms. The Expert line's steep slope may be an indication that 400ms is where human processing speed becomes the bottleneck in the particular type of interaction used for the test.

> Obviously, 400ms is not acceptable for all type of interaction though. Microsoft did a really cool study on touch interface latency where they prototyped a device that provided an experience indistinguishable from pen-and-paper. IIRC, that occurred at around the 10ms mark (I didn't re-watch the video to get the specifics though, sorry).

> Edit: The pen-and-paper-like experience starts at 1ms. At 10ms there's still noticeable lag.

-----

> The 400 ms threshold cited for the Doherty threshold is from a 1982 research paper. I imagine that in 1982, 400 ms response times felt pretty darn good.
Also, the nature of the tasks that users performed in 1982 may have meant that there was no benefit to <400ms response times. (If it takes twenty seconds to read and understand the output, and another ten to key in your next command, then a 400 ms delay is not a meaningful productivity drag.)

Today, I would guess that the threshold is closer to 50 ms.

(Also, "time spent by the computer waiting for the user" makes sense in the context of a 1982 research paper. That has changed in the last 28 years, to say the least.)

All of this is a long way off saying that if you are making UX decisions based on 28 year old research, you are on shaky ground!

---

references:

More conversations about it:
https://news.ycombinator.com/item?id=24031521

https://www.reddit.com/r/programming/comments/2bar4m/til_about_the_doherty_threshold_400ms_response/
