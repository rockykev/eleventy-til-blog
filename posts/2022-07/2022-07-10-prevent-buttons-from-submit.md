---
title: TIL another way to prevent buttons from submitting HTML
date: 2022-07-10
published: true
tags: ['button','form', 'javascript']
series: false
canonical_url: false
description: "When you have two buttons, you can use 'type=button' to stop them from submitting a form."
layout: layouts/post.njk
---

Here's a standard form.

Hit that button, the console displays that message.

```html

<form id="say-hi">
	<button>Activate Me</button>
</form>

<script>
let form = document.querySelector('#say-hi');
form.addEventListener('submit', function () {
	console.log('Someone said hi!');
});
</script>

```

What happens when you have two buttons? Maybe you'll use a conditional and `event.preventDefault()` to stop it.


```html

<form id="say-hi">
	<button>Activate Me</button>
	<button id="toggle-something">Toggle Something</button>
</form>

<script>
form.addEventListener('submit', function (event) {

	// Ignore the #toggle-something button
	if (event.submitter.matches('#toggle-something')) {
		event.preventDefault();
	}

	console.log('Someone said hi!');

});

</script>

```

Another way is just to add `type="button"`

```html
<form id="say-hi">
	<button>Activate Me</button>
	<button id="toggle-something" type="button">Toggle Something</button>
</form>
```

Now "Activate Me" will trigger the submit, where the Toggle Something will be ignored.

Woot! Less Javascript!


via [How to prevent buttons from causing a form to submit with HTML](https://gomakethings.com/how-to-prevent-buttons-from-causing-a-form-to-submit-with-html/)
