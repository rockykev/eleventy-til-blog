---
title: TIL software interoperability
date: 2022-07-16
published: true
tags: ['api','stackoverflow']
series: false
canonical_url: false
description: "The term 'software interoperability' refers to the capability of different solutions to communicate with one another freely and easily. Systems that are interoperable exchange information in real-time, without the need for specialized IT support or behind-the-scenes coding."
layout: layouts/post.njk
---

I'm a big proponent of creating APIs.

In 2002, Jeff Bezos issued a mandate.

> All teams will henceforth expose their data and functionality through service interfaces.”

That forward thinking created the API/microservices space, as it formed the basis for much of the modern API design paradigm, and one of the main reasons for AWS being such a powerhouse.

But at the core - it's exactly what Bezos points out.

Provide a easy way for two pieces of software to talk to each other.

That's where I learned about "Software Interoperability".

The term “software interoperability” refers to the capability of different solutions to communicate with one another freely and easily. Systems that are interoperable exchange information in real-time, without the need for specialized IT support or behind-the-scenes coding.

[via formstack post](https://www.formstack.com/resources/blog-software-interoperability)

When done well, it's invisible.

For example:

Netflix's homepage is [powered by 700 microservices](https://medium.com/refraction-tech-everything/how-netflix-works-the-hugely-simplified-complex-stuff-that-happens-every-time-you-hit-play-3a40c9be254b)(as of 2017)

Creating smaller microservices provides a lot of benefits.
* easier testing
* smaller deploys
* looser decoupling
* smaller failure surfaces

It comes with a lot of it's own problems too! (Which [StackOverflow's blog](https://stackoverflow.blog/2020/05/13/ensuring-backwards-compatibility-in-distributed-systems/) outlines!)
