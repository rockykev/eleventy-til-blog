---
title: TIL how a MRI disabled iOS devices in a facility
date: 2022-07-01
published: true
tags: ['IT','weird', 'ios']
series: false
canonical_url: false
description: "It wasn't a EMP. It was helium gas."
layout: layouts/post.njk
---

tl;dr: It wasn't a EMP. It was helium gas.

> Exposing iPhone to environments having high concentrations of industrial chemicals, including near evaporating liquified gasses such as helium*, may damage or impair iPhone functionality.

## The post:

This is probably the most bizarre issue I've had in my career in IT. One of our multi-practice facilities is having a new MRI installed and apparently something went wrong when testing the new machine. We received a call near the end of the day from the campus stating that none of their cell phones worked after testing the new MRI. My immediate thought was that the MRI must have emitted some sort of EMP, in which case we could be in a lot of trouble. We're still waiting to hear back from GE as to what happened. This facility is our DR site so my boss and the CTO were freaking out and sent one of us out there to make sure the data center was fully operational. After going out there we discovered that this issue only impacted iOS devices. iPads, iPhones, and Apple Watches were all completely disabled (or destroyed?). Every one of our assets was completely fine. It doesn't surprise me that a massive, powerful, super-conducting electromagnet is capable of doing this. What surprises me is that it is only effecting Apple products. Right now we have about 40 users impacted by this, all of which will be getting shiny new devices tonight. GE claims that the helium is what impacts the iOS devices which makes absolutely no sense to me. I know liquid helium is used as a coolant for the super-conducting magnets, but why would it only effect Apple devices? I'm going to xpost to r/askscience~~, but I thought it might spark some interest on here as well.~~ Mods of r/askscience and r/science approved my post. Here's a link to that post: https://www.reddit.com/r/askscience/comments/9mk5dj/why_would_an_mri_disable_only_ios_devices/

## UPDATE 1:

I will create another post once I have more concrete information as I'm sure not everybody will see this.

Today was primarily damage control. We spent some time sitting down with users and getting information from their devices as almost all of them need to be replaced. I did find out a few things while I was there.

I can confirm that this ONLY disabled iphones and apple watches. There were several android users in the building while this occurred and none of them experienced any long term (maybe even short term) issues. Initially I thought this only impacted users on one side of the building, but from what I've heard today it seems to be multiple floors across the facility.

The behavior of the devices was pretty odd. Most of them were completely dead. I plugged them in to the wall and had no indication that the device was charging. I'd like to plug a meter in and see if it's drawing any power, but I'm not going to do this. The other devices that were powering on seemed to have issues with the cellular radio. The wifi connection was consistent and fast, but cellular was very hit or miss. One of the devices would just completely disconnect from cellular like the radio was turned off, then it would have full bars for a moment before losing connectivity again. The wifi radio did not appear to have any issues. Unfortunately I don't have access to any of the phones since they are all personal devices. I really can only sit down with it for a few minutes and then give it back to the end user.

We're being told that the issue was caused by the helium and how it interacts with the microelectronics.

u/captaincool and u/luckyluke193 brought up some great points about helium's interaction with MEMS devices, but it seems unlikely that there would have been enough helium in the atmosphere to create any significant effects on these devices. We won't discount this as a possibility though. The tech's noted that they keep their phones in plastic ziplock bags while working on the machines. I don't know how effective they would be if it takes a minuscule amount of He to destroy the device, and helium being as small as it is could probably seep a little bit in to a plastic bag.

We're going to continue to gather information on this. If I find out anything useful I will update it here. Once this case is closed I'll create a follow-up as a new post on this sub. I don't know how long it will take. I'll post updates here in the meantime unless I'm instructed to do otherwise.

## UPDATE 2 :

I discovered that the helium leakage occurred while the new magnet was being ramped. Approximately 120 liters of liquid He were vented over the course of 5 hours. There was a vent in place that was functioning, but there must have been a leak. The MRI room is not on an isolated HVAC loop, so it shares air with most or all of the facility. We do not know how much of the 120 liters ended up going outdoors and how much ended up inside. Helium expands about 750 times when it expands from a liquid to a gas, so that's a lot of helium (90,000 m3 of gaseous He).


## UPDATE- A few days later

It's been a few weeks since our little incident discussed in my original post.

If you didn't see the original one or don't feel like reading through the massive wall of text, I'll summarize: A new MRI was being installed in one of our multi-practice facilities, during the installation everybody's iphones and apple watches stopped working. The issue only impacted iOS devices. We have plenty of other sensitive equipment out there including desktops, laptops, general healthcare equipment, and a datacenter. None of these devices were effected in any way (as of the writing of this post). There were also a lot of Android phones in the facility at the time, none of which were impacted. Models of iPhones and Apple watches afflicted were iPhone 6 and higher, and Apple Watch series 0 and higher. There was only one iPhone 5 in the building that we know of and it was not impacted in any way. The question at the time was: What occurred that would only cause Apple devices to stop working? There were well over 100 patients in and out of the building during this time, and luckily none of them have reported any issues with their devices.

In this post I'd like to outline a bit of what we learned since we now know the root cause of the problem.

I'll start off by saying that it was not some sort of EMP emitted by the MRI. There was a lot of speculation focused around an EMP burst, but nothing of the sort occurred. Based on testing that I did, documentation in Apple's user guide, and a word from the vendor we know that the cause was indeed the Helium. There were a few bright minds in my OP that had mentioned it was most likely the helium and it's interaction with different microelectronics inside of the device. These were not unsubstantiated claims as they had plenty of data to back the claims. I don't know what specific component in the device caused a lock-up, but we know for sure it was the helium. I reached out to Apple and one of the employees in executive relations sent this to me, which is quoted directly from the iPhone and Apple Watch user guide:

Explosive and other atmospheric conditions: Charging or using iPhone in any area with a potentially explosive atmosphere, such as areas where the air contains high levels of flammable chemicals, vapors, or particles (such as grain, dust, or metal powders), may be hazardous. Exposing iPhone to environments having high concentrations of industrial chemicals, including near evaporating liquified gasses such as helium*, may damage or impair iPhone functionality. Obey all signs and instructions.*

Source: [Official iPhone User Guide](https://help.apple.com/iphone/12/#/iph301fc905) (Ctrl + F, look for "helium")

NOTE: I couldn't find a archive that had this info. -RK

They also go on to mention this:

If your device has been affected and shows signs of not powering on, the device can typically be recovered.  Leave the unit unconnected from a charging cable and let it air out for approximately one week.  The helium must fully dissipate from the device, and the device battery should fully discharge in the process.  After a week, plug your device directly into a power adapter and let it charge for up to one hour.  Then the device can be turned on again.

I'm not incredibly familiar with MRI technology, but I can summarize what transpired leading up to the event. This all happened during the ramping process for the magnet, in which tens of liters of liquid helium are boiled off during the cooling of the super-conducting magnet. It seems that during this process some of the boiled off helium leaked through the venting system and in to the MRI room, which was then circulated throughout the building by the HVAC system. The ramping process took around 5 hours, and near the end of that time was when reports started coming in of dead iphones.

If this wasn't enough, I also decided to conduct a little test. I placed an iPhone 8+ in a sealed bag and filled it with helium. This wasn't incredibly realistic as the original iphones would have been exposed to a much lower concentration, but it still supports the idea that helium can temporarily (or permanently?) disable the device. In the video I leave the display on and running a stopwatch for the duration of the test. Around 8 minutes and 20 seconds in the phone locks up. Nothing crazy really happens. The clock just stops, and nothing else. The display did stay on though. I did learn one thing during this test: The phones that were disabled were probably "on" the entire time, just completely frozen up. The phone I tested remained "on" with the timestamp stuck on the screen. I was off work for the next few days so I wasn't able to periodically check in on it after a few hours, but when I left work the screen was still on and the phone was still locked up. It would not respond to a charge or a hard reset. When I came back to work on Monday the phone battery had died, and I was able to plug it back in and turn it on. The phone nearly had a full charge and recovered much quicker than the other devices. This is because the display was stuck on, so the battery drained much quicker than it would have for the other device. I'm guessing that the users must have had their phones in their pockets or purses when they were disabled, so they appeared to be dead to everybody. You can watch the video Here

We did have a few abnormal devices. One iphone had severe service issues after the incident, and some of the apple watches remained on, but the touch screens weren't working (even after several days).

I found the whole situation to be pretty interesting, and I'm glad I was able to find some closure in the end. The helium thing seemed pretty far fetched to me, but it's clear now that it was indeed the culprit. If you have any questions I'd be happy to answer them to the best of my ability. Thank you to everybody to took part in the discussion. I learned a lot throughout this whole ordeal.

Update: I tested the same iPhone again using much less helium. I inflated the bag mostly with air, and then put a tiny spurt of helium in it. It locked up after about 12 minutes (compared to 8.5 minutes before). I was able to power it off this time, but I could not get it to turn back on.


[MRI disables every iOS device in facility](https://web.archive.org/web/20220703130101/https://www.reddit.com/r/BestofRedditorUpdates/comments/un8eda/mri_disables_every_ios_device_in_facility/)
