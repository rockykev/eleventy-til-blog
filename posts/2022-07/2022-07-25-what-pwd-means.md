---
title: TIL what pwd means (and a bunch of others)
date: 2022-07-25
published: true
tags: ['terminal','history']
series: false
canonical_url: false
description: "It's short for print working directory"
layout: layouts/post.njk
---

Today I learned that `pwd` is short for "print working directory"


Some more random things:

* `su` stands for "switch Users"
* but `sudo` stands for "Superuser do"
* `awk` is named after Alfred Aho, Peter Weinberger and Brian Kernighan.

Awk is a utility that enables a programmer to write tiny but effective programs in the form of statements that define text patterns that are to be searched for in each line of a document and the action that is to be taken when a match is found within a line. Awk is mostly used for pattern scanning and processing. It searches one or more files to see if they contain lines that matches with the specified patterns and then perform the associated actions.

[Alfred Aho](https://en.wikipedia.org/wiki/Alfred_Aho) is  best known for his work on programming languages, compilers, and related algorithms, and his textbooks on the art and science of computer programming

[Peter Weinberger](https://en.wikipedia.org/wiki/Peter_J._Weinberger) was involved in Bell Labs, and now works at Google.

[Brian Kernighan](https://en.wikipedia.org/wiki/Brian_Kernighan) wrote the first book on C.


* `pwd` stands for "print working directory"

* `.` is a shortcut for CWD.

It's why you need to include `./` to execute a command in the working path. It's because the current working directory isn't part of $PATH.

* `grep` stands for "Global regular expression print".

* `dd` was originally cc for Convert and Copy, but cc was already being used for the C Compiler, so they just moved it one letter up in the alphabet.

via this [post by r/deja_geek](https://www.reddit.com/r/sysadmin/comments/v65uv6/comment/ibdpa46/?utm_source=reddit&utm_medium=web2x&context=3)
