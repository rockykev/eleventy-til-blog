---
title: TIL Considering Device Obsolescence with web development
date: 2022-07-14
published: true
tags: ['webdev','browsers']
series: false
canonical_url: false
description: "I'm incredibly guilty of pushing to build for the latest tech. For good reason too - you can write less code, use more native functions, and things are just more likely to work."
layout: layouts/post.njk
---

I'm incredibly guilty of pushing to build for the latest tech. For good reason too - you can write less code, use more native functions, and things are just more likely to work.

Except when it doesn't!

This [mom with her Chromebook and iPad](https://blog.jim-nielsen.com/2022/a-web-for-all/) can't open a site because of modern JavaScript.

What's crazy about this story is that this isn't very far-fetched! I code on a chromebook that's EoD was a year ago!

Here's the story (in abridged form):

> I asked my Mom what was wrong.

> “I can’t open this website I always use to make a reservation to volunteer. It doesn’t work on our computer and it doesn’t work on my iPad. But it does work on Mike’s (my brother) laptop, so I’ve just been using his. Can you fix it?”

> She has relatively modern tech: an iPad (Safari) and a Chromebox (Google Chrome).”

> I looked at the version of Chrome on my parent’s computer. Version 76! I knew we were at ninety-something in 2022, so I figured that was the culprit. “I’ll just update Chrome,” I thought.

> Turns out, you can’t. From what I could gather, the version of Chrome was tied to ChromeOS which couldn’t be updated because of the hardware. No new ChromeOS meant no new Chrome which meant stuck at version 76.

> But what about the iPad? I discovered that my Mom’s iPad was a 1st generation iPad Air. Apple stopped supporting that device in iOS 12, which means it was stuck with whatever version of Safari last shipped with iOS 12.

> So I had two older browsers that couldn’t be updated. It was device obsolescence because you couldn’t install the latest browser.

> I discovered the website authors were shipping JavaScript that used the optional chaining operator (?.), an unsupported syntax in older browsers that caused the entire website to fail.

via [Jim Nielsen's The Optional Chaining Operator, “Modern” Browsers, and My Mom](https://blog.jim-nielsen.com/2022/a-web-for-all/)
