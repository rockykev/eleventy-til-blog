---
title: TIL Microsoft Teams can analyze your feelings
date: 2022-07-28
published: true
tags: ['ai', 'security']
series: false
canonical_url: false
description: "Microsoft Teams set up a new feature set. They're known as Purview classifiers that can help prevent abuse, harassment, other illegal activities on Teams."
layout: layouts/post.njk
---

Microsoft Teams set up a new feature set. They're known as Purview classifiers that can help prevent abuse, harassment, other illegal activities on Teams.

> Microsoft Purview Communication Compliance is an insider risk solution that helps minimize communication risks by helping you detect, capture, and act on inappropriate messages in your organization.

src:[Learn about communication compliance](https://docs.microsoft.com/en-us/microsoft-365/compliance/communication-compliance?view=o365-worldwide)

> When configured if Purview's A.I. scans or spots anything that relates to any of these themes, the messages or content are sent for moderation. Do note, that the feature is Built with privacy by design. This means that usernames are pseudonymized by default, role-based access controls are built-in, investigators are explicitly opted in by an admin, and audit logs are in place to help ensure user-level privacy.

src:[Microsoft details Purview classifiers that can help prevent abuse, harassment, other illegal activities on Teams](https://www.onmsft.com/news/microsoft-teams-classifiers-prevent-abuse)

Interesting.

A AI that monitors chats and flags statements based on some specific actions. Ignoring the horrifying level of tracking, let's keep going.

Some of the new classifications: src: [MC387035: Microsoft Purview: Additional classifiers for Communication Compliance (preview)](https://pupuweb.com/mc387035-microsoft-purview-additional-classifiers-communication-compliance-preview/)

> The following new classifiers will soon be available in public preview for use with your Communication Compliance policies.

* Leavers: The leavers classifier detects messages that explicitly express intent to leave the organization, which is an early signal that may put the organization at risk of malicious or inadvertent data exfiltration upon departure.

* Corporate sabotage: The sabotage classifier detects messages that explicitly mention acts to deliberately destroy, damage, or destruct corporate assets or property.

* Gifts & entertainment: The gifts and entertainment classifier detect messages that contain language around exchanging of gifts or entertainment in return for service, which may violate corporate policy.

* Money laundering: The money laundering classifier detects signs of money laundering or engagement in acts design to conceal or disguise the origin or destination of proceeds. This classifier expands Communication Compliance's scope of intelligently detected patterns to regulated customers such as banking or financial services who have specific regulatory compliance obligations to detect for money laundering in their organization.

* Stock manipulation: The stock manipulation classifier detects signs of stock manipulation, such as recommendations to buy, sell, or hold stocks in order to manipulate the stock price. This classifier expands Communication Compliance's scope of intelligently detected patterns to regulated customers such as banking or financial services who have specific regulatory compliance obligations to detect for stock manipulation in their organization.

* Unauthorized disclosure: The unauthorized disclosure classifier detects sharing of information containing content that is explicitly designated as confidential or internal to certain roles or individuals in an organization.

* Workplace collusion: The workplace collusion classifier detects messages referencing secretive actions such as concealing information or covering instances of a private conversation, interaction, or information. This classifier expands Communication Compliance's scope of intelligently detected patterns to regulated customers such as banking, healthcare, or energy who have specific regulatory compliance obligations to detect for collusion in their organization.


Gotta admit a laugh for this one:

![](2022-07-28-microsoft-team-purview_0.png)

via [reddit poster mr_tyler_durden](https://www.reddit.com/r/sysadmin/comments/v3b2mn/comment/iaxrg4u/?utm_source=reddit&utm_medium=web2x&context=3)

> Hey Joe, I just got an offer from one of our top competitors and I think I’m going to accept. It would be a shame if I left a copy of our clients on my personal laptop haha. While I’ve got you, I want to see if you can help me delete some company data that doesn’t reflect well on me. I can make it worth your while, if you know what I mean. How about a few gift certificates to that restaurant your wife loves? It can be our little secret. Speaking of secrets, I’ve also could use some help shuffling around some money in the budget so the suits don’t get suspicious, I think your friend in accounting might be able to help me out if you can connect us. And you didn’t hear this from me but you are going to want to unload your stocks before the next earnings report, it is not going to be good, get out while you can. Lastly I need to tell you about a new project that’s very hush-hush, I’m not even supposed to know about it but it’s going to be a game changer and you need to get out ahead of this. As always let’s keep all this just between the two of us, no need for anyone else to know what’s going on. Let’s get lunch soon!


Anyways, as of July 28, there is an update!

Updated July 28, 2022: Thank you for your feedback. At this time, we will not be moving forward with rolling out 93251 Communication Compliance – Leavers classifier as outlined. We are evaluating changes based on feedback and will announce our new plan via Message center when we are ready proceed. Your feedback is greatly appreciated.


In other words -- there's still detection to support actions like union busting.

<iframe src="https://giphy.com/embed/XAdbHJywVjF5K" width="480" height="274" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
