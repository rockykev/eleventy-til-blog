---
title: TIL tips for making a API Wrapper
date: 2022-07-20
published: true
tags: ['javascript','reddit']
series: false
canonical_url: false
description: "The API wrapper should do zero 'interpreting' (or as close to zero as you can get)."
layout: layouts/post.njk
---


On Reddit, a poster [u/Revelnova asks about advice with publishing their first NPM package](https://www.reddit.com/r/webdev/comments/vam4yt/publishing_my_first_npm_package_any_advice/).

Via [u/Revelnova](https://www.reddit.com/r/webdev/comments/vam4yt/comment/ic34xz3/?utm_source=reddit&utm_medium=web2x&context=3):

> The objective for the client is to make transforming and fetching content data super easy for developers using the CMS platform.

> Their stance was that by making the client as simple as possible, I’m not locking users into the use cases I imagine but instead leaves room for developers to build features on top of the client.


The feedback via [r/ecafyelims](https://www.reddit.com/r/webdev/comments/vam4yt/comment/ic38h90/?utm_source=reddit&utm_medium=web2x&context=3):

> In my experience, the best API wrappers are the ones that are so thin, they're transparent.

> You might already do this, but just some suggestions:

> * The API wrapper should do zero "interpreting" (or as close to zero as you can get).

> * Aim so that the wrapper doesn't even need to document the endpoints because the endpoints are documented by the API, and there's no difference in use.

> * Don't code the endpoints or parameters into the wrapper, but instead the endpoints are variables which get passed by the user.


This is all great.

`r/ecafyelims` has one big one:

> Aim so that if the API is updated to allow a new endpoint or parameter, the wrapper will already work because the endpoints are not hard-coded.

For example:

```js
// This method:
// hardcodes params and endpoints
const products = await apiWrapper.getProducts(categoryId, colorName);


// This method:
// opens up more flexibility
const products = await apiWrapper('products').get({categoryId, colorName});

// that way, you can also do this
const stores = await apiWrapper('stores').get({nearZipCode});
```

That way, if the API is updated later to add a new endpoint (e.g. "stores"), you don't have to touch the wrapper. It'll just work.

