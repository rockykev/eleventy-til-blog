---
title: TIL Number formatting with Javascript
date: 2022-07-22
published: true
tags: ['numbers','javascript', 'mdn']
series: false
canonical_url: false
description: "Say you wanted to convert this into a string, with all the commas and everything. You can use the browser Intl package to use this."
layout: layouts/post.njk
---

Say you wanted to convert this into a string, with all the commas and everything.

You can use the browser Intl package to use this.

```js
let num = 1234567890987654321;

let str = Intl.NumberFormat('en-US').format(num);
// results: '1,234,567,890,987,654,400'
```

But some countries format numbers differently. Spain uses periods instead of commas.

```
let num = 1234567890987654321;

let str = Intl.NumberFormat('en-ES').format(num);
// returns '1.234.567.890.987.654.400'

```

If you leave `NumberFormat(undefined)`, it'll default to the user!

[MDN - intl](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl)

via [How to create a formatted string from a number with the Intl.NumberFormat API](https://gomakethings.com/how-to-create-a-formatted-string-from-a-number-with-the-intl.numberformat-api/)
