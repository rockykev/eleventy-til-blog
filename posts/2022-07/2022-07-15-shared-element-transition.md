---
title: TIL the Shared Element Transitions
date: 2022-07-15
published: true
tags: ['animation','css']
series: false
canonical_url: false
description: "These cool effects exist on mobile. And Google and the Chrome team has been pushing for the web to have those complex transitions too."
layout: layouts/post.njk
---

This is still super-experimental. So much that I can see this being 'ready' in five years.

<blockquote class="imgur-embed-pub" lang="en" data-id="a/gsC8EXV" data-context="false" ><a href="//imgur.com/a/gsC8EXV"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>


These cool effects exist on mobile.

And Google and the Chrome team has been pushing for the web to have those complex transitions too.


> Creating a transition from one page to another after clicking a link is, well, impossible. But, it isn't exactly easy within a single-page app either.

> Even in a simple case, where content fades from one screen to another, it means rendering your page with both states at once, performing a cross-fade, then removing the old state.

via [Chrome Developer's blog](https://developer.chrome.com/blog/shared-element-transitions-for-spas/)


They've been hard at work at a `Shared Elements Transitions API`, which will allow us to do that.

Test it yourself: https://sleekwp.dev/fun/page-transition/

To check on the status: https://chromestatus.com/feature/5193009714954240

Via
[Smooth and simple page transitions with the shared element transition API](https://developer.chrome.com/blog/shared-element-transitions-for-spas/)

and this Reddit post [I played around with Chrome's new "Shared Element Transitions"](https://www.reddit.com/r/webdev/comments/uyoit1/i_played_around_with_chromes_new_shared_element/)
