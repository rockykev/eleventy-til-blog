---
title: TIL constant failures
date: 2022-07-13
published: true
tags: ['inspiration','quote']
series: false
canonical_url: false
description: "I've missed more than 9000 shots in my career. I've lost almost 300 games. 26 times, I've been trusted to take the game winning shot and missed. I've failed over and over and over again in my life. And that is why I succeed"
layout: layouts/post.njk
---

I typically don't like sharing quotes because they mean different things for different people.

I've been using this for a decade.

I've missed more than 9000 shots in my career. I've lost almost 300 games. 26 times, I've been trusted to take the game winning shot and missed. I've failed over and over and over again in my life. And that is why I succeed.

~Michael Jordan
