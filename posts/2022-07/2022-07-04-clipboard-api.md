---
title: TIL about the Clipboard API
date: 2022-07-04
published: true
tags: ['javascript','mdn', 'webapi']
series: false
canonical_url: false
description: "The Clipboard API can be used to implement cut, copy, and paste features within a web application."
layout: layouts/post.njk
---

The Clipboard API can be used to implement cut, copy, and paste features within a web application.

Most other modern browsers have access to the `navigator.clipboard` global object, which is how you’d want to interact with the clipboard in modern web applications.

* text content, using `navigator.clipboard.readText()` and `navigator.clipboard.writeText()`.

* images, rich text, HTML, and other rich content, using `navigator.clipboard.read()` and `navigator.clipboard.write()`.

You do have to wrap it in a promise.


```html
<html>
    <head>
        <script >
            async function copyText() {
                let textArea = document.getElementById("myText")

                const permission = await navigator.permissions.query({ name: 'clipboard-read' });
                if (permission.state === 'denied') {
                    return console.error("Damn, we don't have permissions to do this")
                }
                try {
                    await navigator.clipboard.writeText(textArea.value)
                    console.log("done!")
                } catch (e) {
                    console.error("Error while copying text!")
                    console.error(e.message)
                }

            }
        </script>
    </head>
    <body>
        <textarea rows="5" cols="89" id="myText"></textarea>
        <button onclick="copyText()">Share!</button>
    </body>
</html>
```


[MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Interact_with_the_clipboard)

[More info via StackOverflow Answer](https://stackoverflow.com/a/49886460/4096078)

via [JavaScript 101: Accessing the Clipboard](https://betterprogramming.pub/javascript-101-accessing-the-clipboard-ad63f38e7d0)
