---
title: TIL Disable users from highlighting text
date: 2022-07-30
published: true
tags: ['html', 'css', 'accessibility']
series: false
canonical_url: false
description: "You can use the 'user-select' property to prevent an element from being highlighted by the user."
layout: layouts/post.njk
---

I get it. You got a request from your boss to be a terrible human being.

They want to disable right-clicks.
They want to disable zoom to really hurt the accessibility.
They want to remove highlighting.

Heck, if they had it their way, you'll have to do a blood sacrifice to see their 'precious' content.

Well, to at least remove highlighting --

You can use the `user-select` property to prevent an element from being highlighted by the user.

```css
.no-highlight {
  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
}
```

[mdn user-select](https://developer.mozilla.org/en-US/docs/Web/CSS/user-select)
