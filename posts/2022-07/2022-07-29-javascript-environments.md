---
title: TIL more Javascript Environments
date: 2022-07-29
published: true
tags: ['javascript']
series: false
canonical_url: false
description: "This is back to why Web Interop exists! Because we have a specific Javascript standard (via ECMAScript Language Specs, by the TC39), and many many different companies/businesses who implement Javascript."
layout: layouts/post.njk
---


Today I learned about more Javascript environments.

The ones we know very well are the popular ones.

V8 is Google's open-source Javascript engine. (Node is built on that!)

SpiderMonkey is Mozilla's Open-source Javascript Engine.

JavascriptCore is used in Apple's WebKit engine. (Bun uses JavascriptCore!)

I was thinking about some others:

* [AWS CloudFront Functions](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/functions-javascript-runtime-features.html) support a lot of Javascript, but are missing later features.

* Bloomberg Terminal has it's own JS environment. (If you're not familiar with the Bloomberg Terminal, read about it [here](https://www.fastcompany.com/3051883/the-bloomberg-terminal) and [here](https://www.vox.com/2020-presidential-election/2019/12/11/21005008/michael-bloomberg-terminal-net-worth-2020)

* Java-based Javascript environments, like GraalVM, or Nashhorn.

* [Hermes](https://hermesengine.dev/), which is a JS engine optimized for React Native.

* [Libjs](https://libjs.dev/), which is SerenityOS's Javascript Engine.

* [AppScripts](https://developers.google.com/apps-script), which is integrated with Google Docs. They swapped it out for node.

* [Espruino](https://www.espruino.com/), which is Javacript for micro-controllers.

This is back to why Web Interop exists!
Because we have a specific Javascript standard (via ECMAScript Language Specs, by the TC39), and many many different companies/businesses who implement Javascript.
