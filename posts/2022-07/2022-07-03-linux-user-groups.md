---
title: TIL linux user groups
date: 2022-07-03
published: true
tags: ['permissions','linux', 'user']
series: false
canonical_url: false
description: "UID 0 is reserved for root user. UID 1-99 is reserved for other predefined accounts. UID 100-999 is reserved for system accounts. Also notice that user accounts tend to start at 1000. UID above 999 are for normal user accounts."
layout: layouts/post.njk
---

What is it?

`/etc/passwd` is a configuration file which stores user account information, like username, user ID and group ID.

```bash
rocky@mac:~$ cat /etc/passwd

root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
...
rocky:x:1000:1000:Rocky K,,,:/home/rocky:/bin/bash
```

It's broken into 7 fields.

![](2022-07-03-linux-user-groups_0.png)


Notice that there's a lot of special custom user accounts?

`UID 0` is reserved for root user.
`UID 1-99` is reserved for other predefined accounts.
`UID 100-999` is reserved for system accounts.

Also notice that user accounts tend to start at 1000.

`UID above 999` are for normal user accounts.

To organize permissions, you can assign permissions to the group, and assign people into that group!

via [Understanding /etc/passwd file in Linux](https://dev.to/kcdchennai/understanding-etcpasswd-file-in-linux-1k2d)
