---
title: TIL How to auto-grow textarea with pure css
date: 2022-07-31
published: true
tags: ['css', 'no-js', 'forms']
series: false
canonical_url: false
description: " To do this, you replicate the content of the '<textarea>' in an element that can auto expand height, and match its sizing.Same font, same padding, same margin, same border… everything."
layout: layouts/post.njk
---


**To autogrow the text area with Javascript:**

it's

```js
const growers = document.querySelectorAll(".grow-wrap");

growers.forEach((grower) => {
  const textarea = grower.querySelector("textarea");
  textarea.addEventListener("input", () => {
    grower.dataset.replicatedValue = textarea.value;
  });
});
```


**To do it a CSS way:**

To do this, you replicate the content of the `<textarea>` in an element that can auto expand height, and match its sizing.

Same font, same padding, same margin, same border… everything.

<p class="codepen" data-height="300" data-default-tab="html,result" data-slug-hash="XWKEVLy" data-user="chriscoyier" style="height: 300px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;">
  <span>See the Pen <a href="https://codepen.io/chriscoyier/pen/XWKEVLy">
  Easiest Autogrowing Textarea</a> by Chris Coyier  (<a href="https://codepen.io/chriscoyier">@chriscoyier</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://cpwebassets.codepen.io/assets/embed/ei.js"></script>


```css
.grow-wrap {
  /* easy way to plop the elements on top of each other and have them both sized based on the tallest one's height */
  display: grid;
}
.grow-wrap::after {
  /* Note the weird space! Needed to preventy jumpy behavior */
  content: attr(data-replicated-value) " ";

  /* This is how textarea text behaves */
  white-space: pre-wrap;

  /* Hidden from view, clicks, and screen readers */
  visibility: hidden;
}
.grow-wrap > textarea {
  /* You could leave this, but after a user resizes, then it ruins the auto sizing */
  resize: none;

  /* Firefox shows scrollbar on growth, you can hide like this. */
  overflow: hidden;
}
.grow-wrap > textarea,
.grow-wrap::after {
  /* Identical styling required!! */
  border: 1px solid black;
  padding: 0.5rem;
  font: inherit;

  /* Place on top of each other */
  grid-area: 1 / 1 / 2 / 2;
}

body {
  margin: 2rem;
  font: 1rem/1.4 system-ui, sans-serif;
}

label {
  display: block;
}

```
It’s an identical copy, just visually hidden with `visibility: hidden;`.

If it’s not exactly the same, everything won’t grow together exactly right.

We also need `white-space: pre-wrap;`` on the replicated text because that is how textareas behave.

via [The Cleanest Trick for Autogrowing Textareas](https://css-tricks.com/the-cleanest-trick-for-autogrowing-textareas/)

