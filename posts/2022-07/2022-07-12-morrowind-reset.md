---
title: TIL Morrowind would reboot the XBox if it ran out of memory and hide it behind a loading screen
date: 2022-07-12
published: true
tags: ['gaming','hacks']
series: false
canonical_url: false
description: "One of my favorite tricks for Morrowind is that if you are running out of memory [on Xbox], you can reboot the Xbox without the user knowing. So if you had a really long loading time, that was us rebooting the Xbox."
layout: layouts/post.njk
---

Todd Howard: "One of my favorite tricks for Morrowind is that if you are running out of memory [on Xbox], you can reboot the Xbox without the user knowing. So if you had a really long loading time, that was us rebooting the Xbox."

via [this tweet](https://twitter.com/JeffGrubb/status/1308831239361757185) & [youtube video](https://www.youtube.com/watch?v=WI2IPeocbAA)

How does it work?
Edit: And Microsoft considered it 3 OS'. The Hypervisor runs on the metal, then the game OS and the "shared" OS which runs the dash and apps. But personally I wouldn't really consider the hypervisor an OS, it's only job is to manage the other 2 and allocate resources to those VMs


via [TH3_ST0CK's comment](https://www.reddit.com/r/XboxSeriesX/comments/iyfzjc/comment/g6d9mh2/?utm_source=reddit&utm_medium=web2x&context=3)

## Some bonus game hacks:

Game Dev Trivia: To create the effect of an ammunition pack exploding and sending bullets everywhere when a backpack weak spot is destroyed in Tom Clancy's The Division, a tiny invisible AI enemy is attached to the backpack, shoots wildly all around and then suicides.

via [this tweet](https://mobile.twitter.com/Thylander/status/1305442619758125057)

More war stories about video games: https://arstechnica.com/video/series/war-stories
