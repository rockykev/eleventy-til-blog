---
title: TIL Decorative Images
date: 2022-09-27
published: true
tags: ['accessibility', 'html', 'images']
series: false
canonical_url: false
description: "make a blank alt='' for presentation images - aka image might already be given using adjacent text, or the image might be included to make the website more visually attractive."
layout: layouts/post.njk
---

Decorative images don’t add information to the content of a page.

For example, the information provided by the image might already be given using adjacent text, or the image might be included to make the website more visually attractive.

For example:
* Visual styling such as borders, spacers, and corners;
* Supplementary to link text to improve its appearance or increase the clickable area;
* Illustrative of adjacent text but not contributing information (“eye-candy”);
* Identified and described by surrounding text.


You should add an empty alt attribute (alt="").

![](_2022-09-27-decorative-images_0.png)

Screen readers also allow the use of WAI-ARIA to hide elements by using role="presentation". However, currently, this feature is not as widely supported as using a null alt attribute. This allows you to search for images on the site with empty or missing alt attributes (so you can fill them in), but if they have role="presentation" you know you can skip over them as they are purely decorative.


via [W3C - Decorative Images](https://www.w3.org/WAI/tutorials/images/decorative/)

via [101 Digital Accessibility (a11y) tips and tricks](https://dev.to/grahamthedev/101-digital-accessibility-tips-and-tricks-4728)
