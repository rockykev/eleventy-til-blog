---
title: TIL a code snippet to check if the value is of type
date: 2022-09-18
published: true
tags: ['javascript', 'gist']
series: false
canonical_url: false
description: This short snippet helps us check!"
layout: layouts/post.njk
---

Checks if the provided value is of the specified type.

This short snippet lets us do a quick test!

```js
const is = (type, val) => ![,null].includes(val) && val.constructor === type;

// examples
is(Array, [1]); // true
is(ArrayBuffer, new ArrayBuffer()); // true
is(Map, new Map()); // true
is(RegExp, /./g); // true
is(Set, new Set()); // true
is(WeakMap, new WeakMap()); // true
is(WeakSet, new WeakSet()); // true
is(String, ''); // true
is(String, new String('')); // true
is(Number, 1); // true
is(Number, new Number(1)); // true
is(Boolean, true); // true
is(Boolean, new Boolean(true)); // true
```

via [Check if value is of type](https://www.30secondsofcode.org/js/s/is)
