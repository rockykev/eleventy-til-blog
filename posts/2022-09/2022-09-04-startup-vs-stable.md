---
title: TIL the difference between startups and stable
date: 2022-09-04
published: true
tags: ['teams', 'work']
series: false
canonical_url: false
description: "On a stable team, you're working on handling features, patching up insecurities, and maintaining the ship. The ship is working. But someone still needs to do maintenance to keep it at sea. At a startup, you're fighting for your right to survive. This metaphoric ship is both at sea, while also being built. You're learning instantly to deliver immediately. (and for most startups, job security is often uncertain)"
layout: layouts/post.njk
---

I was listening to a podcast [Android's Unlikely Success: How a ragtag team of developers created the world's most popular mobile OS](https://corecursive.com/android-with-chet-haase/)

The quote that got out of me:

> They were working hard because they were fighting for their right to exist. Android is huge now. But in 2010, that wasn’t the case.

It reminded me the two team dynamics --

## The difference between a Startup Team and a Stable Team:

On a stable team, you're working on handling features, patching up insecurities, and maintaining the ship. The ship is working. But someone still needs to do maintenance to keep it at sea.

At a startup, you're fighting for your right to survive. This metaphoric ship is both at sea, while also being built. You're learning instantly to deliver immediately. (and for most startups, job security is often uncertain)

I cut my teeth at startups. Hard to say if it was a great experience or not; 80 hour weeks are never easy.

Now I manage a stable team. Tickets are scoped. Everything has a clear start/end. There's very little uncertainty.

I do recommend both to fully flesh yourself out as a developer.
