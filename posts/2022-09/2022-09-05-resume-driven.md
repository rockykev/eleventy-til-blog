---
title: TIL Resume-Driven Development
date: 2022-09-05
published: true
tags: ['resume', 'work']
series: false
canonical_url: false
description: "The selection of technologies is given highest priority in the current field of activity. Moreover, future employers and jobs may specifically be chosen according to these criteria. "
layout: layouts/post.njk
---

Of course I'm a full-stack javascript engineer using sveltekit-tailwind-node.

I am a part of RDD (Resume-Driven Development).

> A strategy to build an impressive personal profile seems clear: The selection of technologies is given highest priority in the current field of activity. Moreover, future employers and jobs may specifically be chosen according to these criteria. Frequent changes are inherent to not get stuck in one topic. Unfortunately, the actual requirements in the current development project take a back seat then. **A satirical term for this phenomenon has recently emerged in developer circles: Résumé-Driven Development (RDD). Its essence lies in a focus on current technology trends that fill gaps in the applicant's profile, thereby completing it and making it appear more impressive.** Focusing on the profile's attractiveness inevitably supersedes or replaces project-specific requirements which should in the first place determine the technologies to be used.

via [Résumé-Driven Development: How IT trends affect the job market for software developers](https://www.wearedevelopers.com/magazine/resume-driven-development#:~:text=A%20satirical%20term%20for%20this,making%20it%20appear%20more%20impressive.)

