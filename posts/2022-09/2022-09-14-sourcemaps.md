---
title: TIL avoiding including Sourcemaps
date: 2022-09-14
published: false
tags: ['compression', 'performance', 'buildtools']
series: false
canonical_url: false
description: "Sourcemaps are a base64 representation of the original source into a production-ready JavaScript asset."
layout: layouts/post.njk
---

Sourcemaps are great.

1. It allows us to minify/obfuscate files so we send smaller files to users.
2. It maps where each spot was 'minified', so developers can quickly debug issues in our non-minified code.

One mistake I see developers do is include the Sourcemaps in their production files by bundling the two together.

On the technical side - Sourcemaps are a base64 representation of the original source into a production-ready JavaScript asset.

> Inlining source maps means that you’re not just sending your JavaScript resources to users, but also their source maps, which can lead to oversized JavaScript assets that take longer to download and process.

via [Part I Chapter 2 JavaScript](https://almanac.httparchive.org/en/2022/javascript#metrics)

Keep them separate. And in the build tools, separate it so production builds don't even create sourcemaps.
