---
title: TIL npx
date: 2022-09-07
published: true
tags: ['npm', 'node']
series: false
canonical_url: false
description: "This command allows you to run an arbitrary command from an npm package (either one installed locally, or fetched remotely), in a similar context as running it via npm run."
layout: layouts/post.njk
---

Around NPM 5.2, they released a cool feature called `npx`

> This command allows you to run an arbitrary command from an npm package (either one installed locally, or fetched remotely), in a similar context as running it via npm run.

For example:

The [rimraf](https://www.npmjs.com/package/rimraf) is a library for deleting files.

`npx rimraf ./**/node_modules`

That will do the following:

1. Fetch the NPM servers remotely for `rimraf` binary.
2. Get the `rimraf` library and put it in NPM cache.
3. Run the binary like it was part of your project.

[](https://docs.npmjs.com/cli/v7/commands/npx)
