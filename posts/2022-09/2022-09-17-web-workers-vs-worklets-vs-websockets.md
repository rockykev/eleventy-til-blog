---
title: TIL the difference between Web workers, Service workers, Worklets, Websockets
date: 2022-09-17
published: true
tags: ['webworkers', 'browser', 'chromeextension']
series: false
canonical_url: false
description: "Web Workers are scripts that offload work from the main thread. Worklets connect to the rendering pipeline. Websockets opens up a 2-way connection."
layout: layouts/post.njk
---

In my effort to understand Service Workers, I hit a few other terms I wasn't familiar with.

## What are these 'workers' solving for?

**Web workers** are general-purpose scripts that enable us to offload processor-intensive work from the main thread. They fall into 3 types: Dedicated Workers, Shared Workers, and Service Workers.

If you think about your typical Javascript file that is included in your HTML document via a `<script>` tag, that runs on the main thread. If there is too much activity on the main thread, it can slow down the site, and can lead to blocking the DOM and user functionality.

**Web workers** (and by extension **Service Workers**), and **Worklets** are all scripts that run on a separate thread. Information needed will have to be passed between the worker and the main script. Putting the processes on different threads allows us to get rid of the DOM blocking problem.

## The Tl;dr:

* **Service workers** are a TYPE of Web Worker. They are a proxy between the browser and the network. By intercepting requests made by the document, service workers can redirect requests to a cache, enabling offline access.

* **Worklets** are hooks into the browser’s rendering pipeline, enabling us to have low-level access to the browser’s rendering processes such as styling and layout. They only have support in Chrome, and not Safari/Firefox. (as of 2022)

* **Web Sockets** establishes an open and persistent two-way connection between the browser and server to send and receive messages over a single connection triggered by events. Unlike the 'workers', **Web Sockets** are part of the main thread which gives them the ability to manipulate the DOM.


## More Specific details about each

### Service Workers
A service worker sits between the app, the browser, and the server, providing a secure connection that runs in the background on a separate thread.

Service workers are used in a “offline-first” development. We can store assets in the local cache instead of the network, provide critical information if the user goes offline, prefetch things so they’re ready when the user needs them, and provide fallbacks in response to network errors. They’re fully asynchronous but, unlike Web Sockets, they have no access to the DOM since they run on their own threads.


Example:

> Consider the messaging app Slack, this is available both as a web app and a desktop app. If you go offline and try to open it in a browser(slack.com) you will unsurprisingly be shown a generic page indicating that you are offline. When the same application is opened as a desktop app, you will be able to see that the UI is rendered along with your past conversations even without the internet. New messages do not show up but we continue to see the data that already exists without the internet. We can say that desktop apps provide a better offline experience which is closer to the online experience in comparison to the web application. The main philosophy behind supporting an offline experience is to own up to the error and provide a more native-like experience instead of straight away refusing to serve the user because she is offline. Such applications are referred to as Progressive Web Applications (PWA).
via [Service Worker in Browser Extensions](https://medium.com/whatfix-techblog/service-worker-in-browser-extensions-a3727cd9117a)

### Worklets

**Worklets** are a very lightweight, highly specific, worker. They enable us as developers to hook into various parts of the browser’s rendering process. When a web page is being rendered, the browser goes through a number of steps - Style, Layout, Paint, & Composite.

![](_2022-09-17-web-workers-vs-worklets-vs-websockets_0.png)

Through “paint worklets” (JavaScript classes with a special paint() function), it allows us to dynamically create images using a syntax almost identical to HTML canvas. That uses the The Paint API, to give us low-level access to CSS rendering.

via [https://css-tricks.com/creating-generative-patterns-with-the-css-paint-api/#aa-what-is-the-css-paint-api](https://css-tricks.com/creating-generative-patterns-with-the-css-paint-api/#aa-what-is-the-css-paint-api)

Example:
Generating complex animations with JS and CSS, without blocking the thread.

### Web Sockets
A Web Socket is a two-way communication protocol. Think of this like an ongoing call between you and your friend that won’t end unless one of you decides to hang up. The only difference is that you are the browser and your friend is the server.

Example:
You’re on your way out and you decide to switch on Google Maps. You probably already know how Google Maps works, but if you don’t, it finds your location automatically after you connect to the app and keeps track of it wherever you go. It uses real-time data transmission to keep track of your location as long as this connection is alive. That’s a Web Socket establishing a persistent two-way conversation between the browser and server to keep that data up to date. A sports app with real-time scores might also make use of Web Sockets this way.
## References:
* [Web workers vs Service workers vs Worklets](https://bitsofco.de/web-workers-vs-service-workers-vs-**Worklets**/)
* [The Difference Between Web Sockets, Web Workers, and Service Workers](https://css-tricks.com/the-difference-between-web-sockets-web-workers-and-service-workers/)
