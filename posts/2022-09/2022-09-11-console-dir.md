---
title: TIL about console.dir
date: 2022-09-11
published: false
tags: ['debug', 'mdn', 'javascript']
series: false
canonical_url: false
description: "If you console.log a element, you will get it's HTML markup. If you want it as a JS object, use console.dir instead"
layout: layouts/post.njk
---


By default, if you `console.log` a element, you will get it's HTML markup.

It'll look like this:

```js

console.log(document.querySelector("#element"))

// In the browser console
<p id="element">
    <span>Stuff</span>
</p>
```

What if you want it as a Javascript Object? For example, you want to see what object's properties like it's innerHTML, or it's siblings?

You instead should use `console.dir`

[MDN](https://developer.mozilla.org/en-US/docs/Web/API/console/dir)

> The method console.dir() displays an interactive list of the properties of the specified JavaScript object. The output is presented as a hierarchical listing with disclosure triangles that let you see the contents of child objects.

> In other words, console.dir() is the way to see all the properties of a specified JavaScript object in console by which the developer can easily get the properties of the object.
