---
title: TIL @Supports
date: 2022-09-02
published: true
tags: ['css', 'defensive-coding']
series: false
canonical_url: false
description: "Unsupported CSS can break things. You typically wrap it in a @Supports rule."
layout: layouts/post.njk
---

Unsupported CSS can break things.

You typically wrap it in a `@Supports` rule.
```
@supports (display: flex) {
    .flex-container > * {
        text-shadow: 0px 0px 2px blue;
        float: none;
    }

    .flex-container {
        display: flex;
    }
}

```

But that's if you KNOW.

What if you don't?

```html

<button>
  Send
</button>


<style>
/* The background color won't change on :hover or :focus
 because the invalid :touch pseudo-class makes the whole
list invalid  */
button:focus,
button:hover,
button:touch {
  background: red;
}
</style>
```

This is where `:where()` comes in handy!

The cool thing about `:where()`


```css
button:where(:focus,
             :hover,
             :touch,
             :smell,
             :feel,
             :admire) {
  background: red;
}
```

Doesn't break! Love it!

(but also you should fix your rules.)

[Here’s what I didn’t know about :where()](https://www.matuzo.at/blog/2022/heres-what-i-didnt-know-about-where/)
