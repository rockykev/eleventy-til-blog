---
title: TIL a tool to generate contributors to a open-source github project
date: 2022-09-24
published: true
tags: ['git', 'generator']
series: false
canonical_url: false
description: "I found this sweet tool that helps identify who are all the contributors to a project at contrib.rocks"
layout: layouts/post.njk
---

I found this sweet tool that helps identify who are all the contributors to a project.

![](_2022-09-24-generate-contributors-github_0.png)

The Generator: [contrib.rocks](https://contrib.rocks)
The Repo: https://github.com/lacolaco/contributors-img/


The Code Snippet
```html
<a href="https://github.com/RockyKev/jitsi-enhancer/graphs/contributors">
  <img src="https://contrib.rocks/image?repo=RockyKev/jitsi-enhancer" />
</a>

Made with [contrib.rocks](https://contrib.rocks).
```

