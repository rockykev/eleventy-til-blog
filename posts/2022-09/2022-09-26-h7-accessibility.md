---
title: TIL that adding a psuedo-h7
date: 2022-09-26
published: true
tags: ['accessibiilty', 'html', 'mdn']
series: false
canonical_url: false
description: "Natively, there are 6 heading levels. What if accessibility-wise, you wanted a new heading level?"
layout: layouts/post.njk
---


Natively, there are 6 heading levels.
Via the MDN: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Heading_Elements


Within the accessibility realm:

https://www.w3.org/TR/wai-aria-1.1/#aria-level

If you want to define a new heading level, you should use:

```
<h6>A deeply nested section</h6>
  <p>Some Text in heading level 6</p>
  <div role="heading" aria-level="7">
    This is effectively a <h7> even though they don't exist!
  </div>
  <p>[...content...]</p>
```

via [W3C's Using role=heading to identify headings](https://www.w3.org/WAI/GL/wiki/Using_role%3Dheading_to_identify_headings#Example_2)
via [101 Digital Accessibility (a11y) tips and tricks](https://dev.to/grahamthedev/101-digital-accessibility-tips-and-tricks-4728)
