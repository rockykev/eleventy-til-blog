---
title: TIL about youtube-dl
date: 2022-09-10
published: true
tags: ['video', 'terminal']
series: false
canonical_url: false
description: "youtube-dl is a command-line program to download videos from YouTube.com and a few more sites."
layout: layouts/post.njk
---

I share this as research/knowledge, and not so you can pirate things. Knowledge isn't malicious. It's what you do with that knowledge. For example: How lockpicks work, or how to break into servers, how to snoop wifi networks.

The project on Github - [Youtube-dl](https://github.com/ytdl-org/youtube-dl)

> youtube-dl is a command-line program to download videos from YouTube.com and a few more sites.

What this app is doing is downloading the webpage, as well as downloading the resources involved for that page to work. When you visit a URL with a video, the video has to download/stream to your computer anyways.
## So why doesn't it work on Netflix and other sites?

Via [this comment](https://github.com/ytdl-org/youtube-dl/issues/1564#issuecomment-152821417):
> There are a couple of really interesting reads on Netflix's engineering blog regarding their use of HTML5 video and Encrypted Media Extensions.

I tried digging into the [Netflix Engineering blog](https://netflixtechblog.com/) to see what I could find. There wasn't much.

There are workarounds. Not interested in discussing them.

## Example

This [video](https://vimeo.com/250506079), from Dominik Lohmann, titled "My favorite free and open-source Software".

```
$  ./youtube-dl --referer "https://vimeo.com" "https://vimeo.com/250506079"


[vimeo] 250506079: Downloading webpage
[vimeo] 250506079: Downloading JSON metadata
[vimeo] 250506079: Downloading JSON metadata
WARNING: Unable to download JSON metadata: HTTP Error 403: Forbidden
[vimeo] 250506079: Downloading akfire_interconnect_quic m3u8 information
[vimeo] 250506079: Downloading akfire_interconnect_quic m3u8 information
[vimeo] 250506079: Downloading fastly_skyfire m3u8 information
[vimeo] 250506079: Downloading fastly_skyfire m3u8 information
[vimeo] 250506079: Downloading akfire_interconnect_quic MPD information
[vimeo] 250506079: Downloading akfire_interconnect_quic MPD information
[vimeo] 250506079: Downloading fastly_skyfire MPD information
[vimeo] 250506079: Downloading fastly_skyfire MPD information
[dashsegments] Total fragments: 17
[download] Destination: My favorite free and open-source Software-250506079.fdash-fastly_skyfire_sep-video-d43cd3af.mp4
[download] 100% of 44.92MiB in 00:18
[dashsegments] Total fragments: 17
[download] Destination: My favorite free and open-source Software-250506079.fdash-fastly_skyfire_sep-audio-bcf9550c.m4a
[download] 100% of 2.92MiB in 00:04
[ffmpeg] Merging formats into "My favorite free and open-source Software-250506079.mp4"
Deleting original file My favorite free and open-source Software-250506079.fdash-fastly_skyfire_sep-video-d43cd3af.mp4 (pass -k to keep)
Deleting original file My favorite free and open-source Software-250506079.fdash-fastly_skyfire_sep-audio-bcf9550c.m4a (pass -k to keep)


```

