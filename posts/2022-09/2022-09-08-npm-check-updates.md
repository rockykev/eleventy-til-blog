---
title: TIL Npx npm-check-updates
date: 2022-09-08
published: true
tags: ['node', 'npm']
series: false
canonical_url: false
description: "Updating packages is frequent chore to do so you are always running the latest security patches."
layout: layouts/post.njk
---

Updating packages is frequent chore to do so you are always running the latest security patches.

To do that:

Step 1: `npx npm-check-updates -u`

That updates all the major versions of your packages.

Step 2: Then run `npm install` to install everything.

Step 3: Finally, run and test.

For my projects, that often breaks code. Major versions of packages often change how configurations/setup works.

But spending some time doing this upkeep will ensure that vulnerabilities will be patched up!

Via [Upgrading npm dependencies](https://www.carlrippon.com/upgrading-npm-dependencies/)
