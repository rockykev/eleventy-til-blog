---
title: TIL what a Shader is
date: 2022-09-01
published: true
tags: ['gamedev', 'steam']
series: false
canonical_url: false
description: "Modern GPUs are incredibly flexible. Developers use shaders - to program the GPU to perform effects and complex rendering techniques. Devs write code in a shader language from an API (such as OpenGL) and a shader compiler in the video driver translates that code into binaries that your PC's GPU can run"
layout: layouts/post.njk
---


While playing with my Steam Deck, I noticed it would frequently update the game and download more 'Shaders'. It would be  Getting a "Shader pre-caching Update".

Curious, I decided to read about it.


**What are shaders**

Shaders are responsible for a great deal of the lighting and shading that goes into a given 3D scene.

> "Modern GPUs are incredibly flexible, but this flexibility comes at a cost - they are insanely complicated. To unlock this power, developers use shaders - programs that the GPU runs just like a CPU runs an application - to program the GPU to perform effects and complex rendering techniques. Devs write code in a shader language from an API (such as OpenGL) and a shader compiler in the video driver translates that code into binaries that your PC's GPU can run. This compiling takes processing power and time to complete, so modern PC games usually get around this by compiling shaders during periods in which framerate doesn't matter, such as loadtimes.

via [The Dolphin emulator developers spent two years fixing a nearly-impossible-to-solve stuttering problem](https://na.alienwarearena.com/ucf/show/1765366/boards/gaming-news/News/the-dolphin-emulator-developers-spent-two-years-fixing-a-nearly-impossible-to-solve-stuttering-problem)

In other words, they're a program that helps with rendering the scene.

> Most shaders do not have to be compiled in real-time when you’re playing games, because if they were, you would be faced with rather severe stutters in framerate- like, the entire game stopping until the shader is done rendering for the first time.

<blockquote class="imgur-embed-pub" lang="en" data-id="a/0SEOaHw" data-context="false" ><a href="//imgur.com/a/0SEOaHw"></a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>

> In the world of emulation, this shader compilation stutter is quite common. Because the original console hardware and its shader management is inaccessible, emulators are instead required to compile shaders in real-time if workarounds aren’t put in place.

> For example, users of the Cemu Wii U Emulator were often required to share and download pre-compiled shader caches if they wanted to play through games without experiencing these stutters. As another example, the Dolphin GameCube and Wii Emulator was prone to unplayable shader stutter, but only in certain games where new shaders were being introduced frequently.

So, what about the Steam Deck downloading Shaders?

> Basically, Steam Deck shader pre-caching is Steam’s way of pre-downloading a shader cache for a given game, allowing it to be played without shader compilation stutter interfering with gameplay or reducing performance.



[What is Steam Deck Shader Pre-Caching?](https://www.gamersdirector.com/steam/what-is-steam-deck-shader-pre-caching/)

