---
title: TIL Breath of the Wild's many parameters
date: 2022-09-09
published: true
tags: ['gamedev', 'zelda']
series: false
canonical_url: false
description: "One of my favorite things to do is dig into how games are made on the data side."
layout: layouts/post.njk
---

One of my favorite things to do is dig into how games are made on the data side.

This repo from leoetlino has Breath of the Wild parameters and data!

https://github.com/leoetlino/botw

Some real neat examples:

* Did you know that each hour of the day has different weather/temperature? Even details like there's fog, wind, moisture and the 'featureColor'. [here](https://github.com/leoetlino/botw/blob/master/WorldMgr/normal.winfo.yml)

* What about chemical systems in BoTW. Like conduction speed, or fire heat transfer rate? How about lightning distance? You can see that all [here](https://github.com/leoetlino/botw/blob/master/Chemical/system.chmres.yml)


* That music for each area has parameters controlling when they fade in/fade out depending on if it's you transitioning from indoor to outdoor, or battle? [here](https://github.com/leoetlino/botw/blob/master/Sound/Music/CastleBgm.mscinfo.yml)

* That NPCs have certain states/statuses based on the state of Link and the weather? [here](https://github.com/leoetlino/botw/blob/master/Quest/BalladOfHeroGerudo.quest.yml)


This stuff is fun to dig into!
