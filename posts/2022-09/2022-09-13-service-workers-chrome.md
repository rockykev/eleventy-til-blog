---
title: TIL switching over to Service Workers with Chrome extensions
date: 2022-09-13
published: false
tags: ['chrome', 'extensions', 'mdn']
series: true
canonical_url: false
description: "Background Pages are like 'hidden pages' that a developer would create behind the scenes. That hidden page would do various JS needs to simulate functionality. Vs a service worker is a script that your browser runs in the background, separate from a web page, opening the door to features that don't need a web page or user interaction"
layout: layouts/post.njk
---


In the context of Chrome Extensions:

the major difference between Manifest V2 and Manifest V3 is that in Manifest V3, the Chrome extension platform moves from background pages to service workers.

> "service worker is a script that your browser runs in the background, separate from a web page, opening the door to features that don't need a web page or user interaction." This is the technology that enables native-like experiences such as push notifications, rich offline support, background sync, and "Add to Home Screen" on the open web. Service workers were inspired in part by background pages in Chrome Extensions, but they iterate and improve on this model by tuning it for web-scale.

via https://developer.chrome.com/docs/extensions/mv3/migrating_to_service_workers/

## Background Pages
Background Pages are like 'hidden pages' that a developer would create behind the scenes. That hidden page would do various JS needs to simulate functionality. One major example is having music play. You can have music load inside a page behind the scenes, to bypass any permissions you may have set for the parent page.

They have a lot of use-cases.

For my use-cases, it does pop up a tiny little window. It doesn't feel 'native'.

## Service Worker
A service worker is a script that stands between your website and the network, giving you, among other things, the ability to intercept network requests and respond to them in different ways. [via SmashingMagazine](https://www.smashingmagazine.com/2016/02/making-a-service-worker/)

Service worker scripts run in a separate thread in the browser from the pages they control. There are ways to communicate between workers and pages, but they execute in a separate scope. That means you won’t have access to the DOM of those pages, for example. I visualize a service worker as sort of running in a separate tab from the page it affects; this is not at all accurate, but it is a helpful rough metaphor for keeping myself out of confusion.

JavaScript in a service worker must not block. You need to use asynchronous APIs. For example, you cannot use localStorage in a service worker (localStorage is a synchronous API).

A service worker does the bulk of its work by listening for relevant events and responding to them in useful ways. Different events are triggered at different points in a service worker’s lifecycle. [The MDN](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers) points to the lifestyle events.

## Storage

> One of the main things to get used to when adopting service workers is that they are short-lived execution environments. In more practical terms, an extension's service worker will start up, do some work, and get terminated repeatedly throughout a user's browser session. This poses a challenge to extension developers accustomed to long-lived background pages as application data is not immediately available in global variables.

In other words, you'll use the [Storage API](https://developer.chrome.com/docs/extensions/reference/storage/)

That storage area NEEDS to be validated by Google, to ensure you aren't doing anything malicious.

> Unlike the local and sync storage areas, the managed storage area requires its structure to be declared as JSON Schema and is strictly validated by Chrome. This schema must be stored in a file indicated by the "managed_schema" property of the "storage" manifest key and declares the enterprise policies supported by the extension. via [Manifest V3 Storage](https://developer.chrome.com/docs/extensions/mv3/manifest/storage/)

