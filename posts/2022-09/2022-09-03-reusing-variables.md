---
title: TIL reusing variables
date: 2022-09-03
published: false
tags: ['programming', 'ruleofthumb']
series: false
canonical_url: false
description: "Do not reuse variables. Do not double define variables that represent the same thing."
layout: layouts/post.njk
---

These two things resonated with me.

> Do not reuse variables. It is not a variable used in a certain sense until a particular time but is used in another meaning from a specific time.

The Functional Programmer in me is always about not creating mutations. The cost of declaring multiple variables that are slightly different is minimal. You rather have it exist and let it be garbage collected, rather than trying to over-optimize.

> Do not double define variables that represent the same thing. Do not define dependent variables. For example, suppose B changes depending on A’s value. In that case, it is functionalized and calculated based on A. If you define both and set B every time A changes, you will forget. Of course, if the calculation takes a long time, it needs to be cached, but even in that case, the variable should not be directly accessible.

This is similar to the first one.

You're already doing something wrong if you're doing weird things like:

```js

let a = 10;
let b = a; // WTF?

a = 20;
b = a; // Double WTF?
```

It's about trying to keep variables as "pure" as you can, making them consts.

If you need it to be cached, consider memoization instead.


Via this post: [16 Things I Always Say to My Juniors for Bug-Free Programming](https://javascript.plainenglish.io/16-things-i-always-say-to-my-juniors-for-bug-free-programming-b59606404dd6)


