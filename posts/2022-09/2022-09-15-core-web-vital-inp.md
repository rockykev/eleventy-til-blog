---
title: TIL good numbers for Web Metrics
date: 2022-09-15
published: false
tags: ['lighthouse', 'metrics']
series: false
canonical_url: false
description: " Given that the 'good' FID threshold is 100 ms, we can say that at least 90% of websites meet this bar. In fact, we know from the analysis done in the Performance chapter that 100% of websites actually have good FID experiences on desktop devices, and 92% on mobile devices. FID is an unusually permissive metric."
layout: layouts/post.njk
---

If you're not familiar with [Core Web Vitals](https://web.dev/vitals/), it's Google's way of measuring website speed. It's the 'next step' of Lighthouse Performance scores.

One of the Core Web Vital metrics is FID.

As of 2022, Google is currently in the process of changing Core Web Vitals. It has added two new metrics to the mix: INP (Interaction To Next Paint) and TTFB (Time to First Byte). [via SearchEngineJournal](https://www.searchenginejournal.com/interaction-to-next-paint-inp-everything-you-need-to-know/457153/#:~:text=The%20main%20difference%20between%20INP,long%20they%20take%20to%20process.) [and via SeoRoundtable](https://www.seroundtable.com/google-core-web-vitals-fid-with-inp-33429.html)

![](_2022-09-15-core-web-vital-inp_0.gif)

## For First Input Delay (FID):

> Given that the “good” FID threshold is 100 ms, we can say that at least 90% of websites meet this bar. In fact, we know from the analysis done in the Performance chapter that 100% of websites actually have “good” FID experiences on desktop devices, and 92% on mobile devices. FID is an unusually permissive metric.

Most sites should be good on that front. Nice job internet!

## For Interaction to Next Paint (INP):

It's when a user visits the page.

* INP, however, is concerned with the following types of "logical" interactions:
    * Any mouse click of an interactive element.
    * Any tap of an interactive element on any device that includes a touchscreen.
    * The press of a key on a physical or onscreen keyboard. (Keydown and keyup)
    * There is more than one event that could be considered an interaction.
    * Any tap interaction could also include pointerup and pointerdown events.
* This is a direct contrast to FID (First Input Delay).
* FID simply measures only the first response of interaction by a particular user.
* The measurement of INP is a representation of how long a user has to take in order to interact with the entire page.
* If a page has less than 50 total interactions, INP takes into consideration the interaction that has the absolute worst delay.


> Consider that a “good” INP score is 200 milliseconds or less. At the median, both mobile and desktop score below this threshold, but the 75th percentile is another story, with both mobile and desktop segments well within the “needs improvement” range. This data, quite unlike FID, suggests that there are many opportunities for websites to do everything they can to run fewer long tasks on pages, which are a key contributor to less-than-good INP scores.

via [Part I Chapter 2 JavaScript](https://almanac.httparchive.org/en/2022/javascript#metrics)

## The Main Differences

First Input Delay (FID) considers only the first interaction on the page.

Interaction to Next Paint (INP) takes into consideration all page interactions.

## Optimizing

How does one handle INP?

Check out Google's dev blogs for optimization tips[How do modern frameworks perform on the new INP metric](https://developer.chrome.com/blog/inp-in-frameworks/#how-does-javascript-affect-inp)
