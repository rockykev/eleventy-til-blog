---
title: TIL how outsiders see Javascript
date: 2022-09-25
published: true
tags: ['typescript', 'javascript', 'horror']
series: false
canonical_url: false
description: "The bane of my existence - 'SyntaxError: Cannot use import statement outside a module'"
layout: layouts/post.njk
---


A year ago, I had a argument with another developer about our preferred OS system for development. I pointed out Windows. He pointed out Macs. We went into a heated debate. (BTW we're both wrong. Linux reigns supreme for development.)

When I showed him how I set up my Windows, he pointed out all the edge-cases. He was absolutely right. Then I picked up a Mac, and kept hitting issue after issue. He showed me how to fix them.

We noticed that we both were "experts" at our OS. Both Macs and PCs have peculiar issues. So why did we like that specific OS? Because internally, we knew how to solve the issues we hit immediately. I can boot up Windows, hit 10 edge-cases and resolve it immediately, all without realizing it. Same with my Mac friend.

I really like [this post - Today’s Javascript, from an outsider’s perspective](https://lea.verou.me/2020/05/todays-javascript-from-an-outsiders-perspective/) because it kinda showcases the weird side of Javascript.

These are all problems that a regular Javascript developer just knows how to handle.

Here's the step by step issue:

1. John finds a JS file on Github wants to use. It's a algorithm form a library.
2. John tries the following:

```
$ npm install packageName --save
$ node index.js

Warning: To load an ES module, set "type": "module" in the package.json or use the .mjs extension.
SyntaxError: Cannot use import statement outside a module
```

3. John then tries to run `npm init` and adds the `type: module`.

```
SyntaxError: Cannot use import statement outside a module
```

4. John tries to create a simple `index.html` file with `<script type="module" src="index.js">`, and open the file up from the browser.

Error.

5. John tries to `npm install` to generate a `node_modules` folder and get it working.
He finds the library file, and points to it directly.

```
Incorrect MIME type: text/html
```

5. He realizes that he's in the `file://` and need to set up a localhost server. He opens it in firefox.

```
Incorrect MIME type: text/html
```

6. He realizes that the library is actually a typescript file, using a `.js` extension!

The final quote:

> John gives up. Concludes never to touch Node, npm, or ES6 modules with a barge pole.
> Note that John is a computer scientist that knows a fair bit about the Web: He had Node & npm installed, he knew what MIME types are, he could start a localhost when needed. What hope do actual novices have?
