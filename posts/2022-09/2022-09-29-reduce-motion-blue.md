---
title: TIL reduce-motion-blur
date: 2022-09-29
published: true
tags: ['accessibility', 'css']
series: false
canonical_url: false
description: "Dizziness and vertigo are symptoms of a vestibular balance disorder. If the user has set up a user preference that they prefer motion-based animation be disabled/reduced, we can tap into that. As web developers, we can also support them."
layout: layouts/post.njk
---

What is vestibular balance disorder? Dizziness and vertigo are symptoms of a vestibular balance disorder. Balance disorders can strike at any age, but are most common as you get older. Your ear is a complex system of bone and cartilage.

via [What is vestibular balance disorder?](https://www.hopkinsmedicine.org/health/conditions-and-diseases/vestibular-balance-disorder#:~:text=What%20is%20vestibular%20balance%20disorder,system%20of%20bone%20and%20cartilage.)

If the user has set up a user preference that they prefer motion-based animation be disabled/reduced, we can tap into that.

As web developers, we can also support them.

```css
.animation {
  animation: pulse 1s linear infinite both;
}

/* Tone down the animation to avoid vestibular motion triggers like scaling or panning large objects. */
@media (prefers-reduced-motion) {
  .animation {
    animation: dissolve 2s linear infinite both;
  }
}
```


via the [MDN - prefers-reduced-motion](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-reduced-motion)
