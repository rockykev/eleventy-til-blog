---
title: TIL Icon Fonts being the suckfest that they are
date: 2022-09-28
published: true
tags: ['accessibility', 'fonts', 'svg']
series: false
canonical_url: false
description: "In 99% of all use-cases, SVGs are better."
layout: layouts/post.njk
---


I hate font icons.

In 99% of all use-cases, SVGs are better.

Now to add that 0.9% more (to make it 99.9%), SVGs are also much more accessible.

For example:
```
<img class="lightbulb" alt="Lightbulb moment!" src="https://upload.wikimedia.org/wikipedia/commons/2/2b/BulbIcon.svg">
```

Boom! Alt text + SVG, it's a freakin' win.

Try using the lightbulb font form font-awesome.
https://fontawesome.com/icons/lightbulb?s=solid&f=classic

```
<i class="fa-solid fa-lightbulb"></i>
```
Way to go. You made this inaccessible.

By the way, if you're using Font Awesome, switch to the SVG Sprites.
https://fontawesome.com/v5/docs/web/advanced/svg-sprites

