---
title: TIL Replace Only Fixes the First Encounter
date: 2022-09-06
published: true
tags: ['javascript', 'mdn']
series: false
canonical_url: false
description: "Replace only finds the first occurrence. ReplaceAll fixes it all."
layout: layouts/post.njk
---



For [Replace](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replace), it will find and replace the first occurrence.

Instead, you want to use [ReplaceAll](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/replaceAll)

```js
const petName = 'Leo'

const placeholder = '{NAME}'

const reminderTemplate = '{NAME} is due for another visit. Please call us so we can set up a new appointment. We look forward to seeing you and {NAME} soon.'

const reminder = reminderTemplate.replaceAll(placeholder, petName)
```

