---
title: TIL ARIA role presentation
date: 2022-09-30
published: true
tags: ['accessibility', 'semantic', 'html']
series: false
canonical_url: false
description: "Think of this as a reset. You use role=presentation to remove semantic meaning from an element."
layout: layouts/post.njk
---

The presentation role and its synonym none remove an element's implicit ARIA semantics from being exposed to the accessibility tree.

Think of this as a reset. You use `role="presentation"` to remove semantic meaning from an element.

```html
<!-- how it looks -->
<h2 role="presentation">Democracy Dies in Darkness</h2>


<!-- how it acts -->
<div>Democracy Dies in Darkness</div>
```



via [MDN](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/presentation_role)

