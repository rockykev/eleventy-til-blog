# CSS trick

## HOW TO TARGET LANGUAGE  (in TIL)

p[lang|="en"] {}

targets
<p lang="en-us">
<p lang="en-uk">


## A COOL TRICK with CSS attribute selectors (in TIL)

E[attr^=val] == starts
E[attr$=val] == ends
E[attr*=val] == whose

Say you wanted to target all links with pdfs at the end. You can use this css class to target it and add a pdf icon.

a[href$=pdf] {background-image: url(pdficon.gif;}
a[href$=pdf] { content: ' (PDF)'; }


## Cool trick to counting numbers (in TIL)

body { counter-reset: invalidCount; }

:invalid {
background-color: pink
counter-increment: invalidCount
}

p:before {
content: "You have " counter(invalidCount) " invalid entries";
}


## Active - Focus  -- Added to TIL.
:active       Adds a style to an element that is activated
:focus        Adds a style to an element that has keyboard input focus
:hover        Adds a style to an element when you mouse over it
:lang         Adds a style to an element with a specific lang attribute
:link         Adds a style to an unvisited link
:visited      Adds a style to a visited link

PROTIP: 
Always style 
:active
:focus at the same time. Some people get to your link or selection by keyboard. 


## NOT Trick with CSS -- Experimental. 
Not fully supported and still debated. 
https://caniuse.com/css-matches-pseudo

https://i.imgur.com/DANU0KJ.png

Useful for matching specific attributes.

matches is safari
webkit chrome


> ------------- LAST AT 12/29/20 ------------------------< 



TWO WAYS TO HACK SPECIFITY

https://i.imgur.com/PDIPEXD.png

don't do
.disabled { cursor: default !important; }

do

.disabled.disabled.disabled { curos: default; }

https://i.imgur.com/dqimp6r.png


QUOTE LANGUAGE QUOTE: 
https://i.imgur.com/ZqHpGDi.png



@supports tag 
https://developer.mozilla.org/en-US/docs/Web/CSS/@supports


```
No need to overthink sass. 
It's really just this. 
// PURE CSS -- 

header {
  background: blue;
  font-size: 1rem;
} 

header h1 {
	font-size: 3rem;
	font-weight: 900;
}

header h2 {
	font-weight: 2rem;
}

header h2 .bg-green {
	background: green; 
}

// SASS -- Things are nestled
header {
  background: blue;
  font-size: 1rem;

	h1 {
		font-size: 3rem;
		font-weight: 900;
	}

	h2 {
		font-weight: 2rem;
		
		.bg-green {
		background: green;
	}

}
```


For viewport -- 
Don't ever do minimum-scale maxium scale 1.
https://i.imgur.com/CYXBPnD.png

Otherwise people can't zoom.
Only reason to do it is for a game.


There's OS Mobile-specific queries. 
For like tapping and stuff. 


#### CSS Hyphensations
p {
	hyphens: auto; 
}



### CSS Columns

p { columns: 18em 3; }
https://www.w3schools.com/css/css3_multiple_columns.asp
https://developer.mozilla.org/en-US/docs/Web/CSS/columns




### Create cards 

using the backface-visibility hidden. 

So when you transform a playing card with tranform -- it'll flip the card backwards.  But if you use backface-visibility, it'll hide the content so it looks more like a card.




