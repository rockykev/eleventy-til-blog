## Achievements


## Scripts to autogenerate things

1 - edit generaetyearOfFiles.js and replace 2023 with something
2 - `node generateYearOfFiles.js`
3 - it gets made in the root.
4 - copy it over


## BLOG POSTS

https://dev.to/rockykev/im-officially-done-with-fontawesome-2h2f
https://dev.to/rockykev/these-6-rules-created-a-really-dope-culture-at-my-job-5fj5
https://dev.to/rockykev/how-i-became-a-developer-in-about-15-years-552k
https://dev.to/rockykev/solving-a-2002-website-issue-with-web-components-2hmf
https://dev.to/rockykev/what-new-hires-think-about-3i99
https://dev.to/rockykev/5-principles-i-followed-to-onboard-new-devs-and-keep-my-sanity-a-retrospect-38i7


----

      // Detect when in viewpoint: https://usefulangle.com/post/113/javascript-detecting-element-visible-during-scroll

https://github.com/anaibol/awesome-serverless


------
---------
```
let isPrime = true;
const startWatching = () => {
    console.log('Started Watching!');
}
This is too much code to check for the boolean condition and invoke the function,

if (isPrime) {
    startWatching();
}
How about using the short-hand using the AND(&&) operator? Yes, avoid if statements altogether. Cool, right?

isPrime && startWatching();
```

----------


24. https://github.com/you-dont-need/You-Dont-Need-Loops
More advanced methods (and way outside of my knowledge level) are these methods.
### Corcusion

### Transducers

### Monoids

### F-Algebras

----------

35. Speed up loop
    https://thedailywtf.com/articles/The-Speedup-Loop

-------




## Idea box

How to create a JS file that runs in the command line.
https://github.com/sporadic-labs/tile-extruder
* Check the package.json - you install this globally and 'tile-extruder' in terminal
* That then goes to the cli.


1. Random game dev stuff
https://www.gamasutra.com/features/game-developer-magazine/

https://www.gamasutra.com/category/programming/


2. No config webpack.
   https://til.hashrocket.com/posts/ijrkkaioxi-production-mode-tree-shaking-in-webpack-by-default

3. ES2021 --
https://h3manth.com/ES2021/

4.
5.
6.
7.

12. // random utility functions
    https://youmightnotneed.com/lodash/

13. http://youmightnotneedjquery.com/

14. https://www.toptal.com/wordpress/bitbucket-wordpress-version-control

15. // data based snippets?
    https://youmightnotneed.com/date-fns/
    https://github.com/you-dont-need/You-Dont-Need-Momentjs


16. https://github.com/vuejs/awesome-vue#open-source

17. Storybook and Vue
    https://auth0.com/blog/using-storybook-with-vuejs/
    https://storybook.js.org/docs/guides/guide-vue/
    https://www.learnstorybook.com/intro-to-storybook/vue/en/get-started/


20. [](https://youmightnotneed.com/)




27. https://medium.com/hackernoon/im-harvesting-credit-card-numbers-and-passwords-from-your-site-here-s-how-9a8cb347c5b5

29 - phaser texture packer
http://free-tex-packer.com/

30. Making your own NPM package.
    https://codeburst.io/extracting-a-react-js-component-and-publishing-it-on-npm-2a49096757f5


32. How to create a website
    Adding more to the resources:

AWS WAY:
https://www.freecodecamp.org/news/how-to-host-a-static-site-in-the-cloud-in-4-steps/

web hosting that's free
https://github.com/ripienaar/free-for-dev#ide-and-code-editing

If you're using a JS framework - (React, Vue, Svelte) - you might have heard of netlify or Vercel (formerly nextjs).
tl;dr: Follow their guide. Your project lives in github (or bitbucket), and then you deploy it to their servers with every new push. It autobuilds and it's really nice.

Another method (the more traditional method) -

1. buy web hosting (google it, or use AWS. I use HostKoala)
2. Buy a url (via namecheap or godaddy or whatever. I use porkbun)
3. You'll get a method to ftp into the site.
4. From there, you can dump your html/css files in the `public` folder. (Depends on the server... look up their documentation.)
   The FTP method again is very traditional, and there's a lot of different directions. From adding more security (SSH), to using automated workflows (github workflows so you upload to github and it auto-uploads to ftp).

Digital Ocean has a \$5 tier but you can practice on a Linux vm on your own personal computer first. I would definitely try to deploy something before your first job, even if it was just GitHub pages for front end or Glitch for backend (probably not codepen). Linux admin and DevOps are different skills to work on, so don't get discouraged if things are rough at first. Just keep googling.
My company uses it, and most of our infrastructure is written in cloudformation yaml manifests. This way your architecture is stored in code and is (in my opinion) easier to comprehend how things are set up, once you get your head around the syntax. We deploy with an open source tool called iidy (is it done yet), which is really easy to work with compared to things like ansible.

I’ve never used them but Microsoft azure and google cloud are two other major ecosystems, but I’ve you’re vying for employability then AWS is the most widely used.

I host a lot of my projects on AWS amplify, it’s free for a project or two and only costs me like .50 cents a month once I got a bunch on it, also check out heroku too, you just need to connect your github repo

34. Puppetter
    https://github.com/checkly/puppeteer-recorder
    https://www.checklyhq.com/docs/puppeteer-recorder/


39. How to load dynamic images in Vue and Nuxt with ease
    https://blog.lichter.io/posts/dynamic-images-vue-nuxt/


43. https://developer.mozilla.org/en-US/docs/Web/CSS/paint-order
    https://stackoverflow.com/questions/26634201/add-stroke-around-text-on-the-outside-with-css

44. vue-js-modal tricks
    change background
    .v--modal-overlay[data-modal='modal-hidden-surprises'] {
    background: red;
    }



45. understanding big O
https://medium.com/weekly-webtips/a-quick-guide-on-big-o-notation-time-complexity-2ada6ce6c546


46. When you npm install a library, a library may have a requirement of a specific node.js version you have to use for their project to work.
    You can find this information in the package.json
    "engines": {
    "node": "12.13.0",
    "npm": "6.12.0"
    },

Juan Cortez 36 minutes ago
If you run --ignore-engines, you’re basically saying, I dont care what node.js version you want me to use, install anyways.

47. Webpack reload wordpress
    https://medium.com/@devs_group/wordpress-vue-js-with-webpack-and-hot-reload-7c4faea9d0d9

49. https://tobiasahlin.com/blog/common-flexbox-patterns/#3x3-grid-constrained-proportions-11


52. Tiem math
    secondsToTime: function(seconds, timeType) {
    let resultArray = []

         switch (timeType) {
           case 'day':
             resultArray[0] = Math.floor(seconds / 86400)
               .toString()
               .padStart(2, '0')

             resultArray[1] = Math.floor(seconds % 86400)
               .toString()
               .padStart(2, '0')
             break

           case 'hour':
             resultArray[0] = Math.floor(seconds / 3600)
               .toString()
               .padStart(2, '0')

             resultArray[1] = Math.floor(seconds % 3600)
               .toString()
               .padStart(2, '0')
             break

           case 'minute':
             resultArray[0] = Math.floor(seconds / 60)
               .toString()
               .padStart(2, '0')

             resultArray[1] = Math.floor(seconds % 60)
               .toString()
               .padStart(2, '0')
             break

           case 'second':
             resultArray[0] = Math.floor(seconds % 60)
               .toString()
               .padStart(2, '0')
             break
         }

         return resultArray

    },

    let remainingDays = this.secondsToTime(this.remainingTime, 'day')
    let remainingHours = this.secondsToTime(remainingDays[1], 'hour')
    let remainingMinutes = this.secondsToTime(remainingHours[1], 'minute')
    let remainingSeconds = this.secondsToTime(remainingMinutes[1], 'second')

         console.log('remainingDays', remainingDays)
         console.log('remainingHours', remainingHours)
         console.log('remainingMinutes', remainingMinutes)
         console.log('remainingSeconds', remainingSeconds)

         this.remainingDays = remainingDays[0]

53. https://stackify.com/13-ways-to-tail-a-log-file-on-windows-unix/
    Tailing

54. ftp deploy
    https://medium.com/swlh/setup-the-simplest-ci-with-bitbucket-pipelines-7989e934e700

55. How to
    https://learngitbranching.js.org/


59. Post id in wordpress
    Post content revision in wp_posts


61. https://twitter.com/dj_link/status/964195826745692161?lang=en

Game hacks:

https://t.co/pPQxka7v7B?amp=1

https://twitter.com/DJ_Link/status/964195826745692161
https://www.gamasutra.com/view/news/249475/More_dirty_coding_tricks_from_game_developers.php


65. Post LOL project
    Using vue-js-modal
    Using date-fns
    https://www.sitepoint.com/date-fns-javascript-date-library/

Using the print mechanism thing
Using tailwind to display/hide info

66. When to use the number input

Via Form Design Patterns (pg 100)

```
There’s a difference between the way these values are
announced. Understanding this helps us see that while
the way browsers implement the number input may seem
buggy at first — it isn’t.
For example, IE11 and Chrome will ignore non-numeric
input such as a letter or a slash. Some older versions of iOS
will automatically convert “1000” to “1,000.” Safari 6 strips
out leading zeros. Each example seems undesirable, but
none of them stop users from entering true numbers.
Some numbers contain a decimal point such as a price;
other numbers are negative and need a minus sign. Unfortunately, some browsers don’t provide buttons for these
symbols on the keypad. If that wasn’t enough, some desktop
versions of Firefox will round up huge numbers.
In these cases, it’s safer to use a regular text box to avoid
excluding users unnecessarily. Remember, users are still
able to type numbers this way — it’s just that the buttons
on the keypad are smaller. To further soften the blow, the
numeric keyboard can be triggered for iOS users by using
the pattern attribute.25
<input type="text" pattern="[0-9]*">
```

```
In short, only use a number input if:
• incrementing and decrementing makes sense
• the number doesn’t have a leading zero
• the value doesn’t contain letters, slashes, minus
signs, and decimal points.
• the number isn’t very large
```


68. 2 sentence summary of everything. (194 sentences)
    https://github.com/97-things/97-things-every-programmer-should-know/blob/master/en/SUMMARY.md



* Domain specific vs primative
https://97-things-every-x-should-know.gitbooks.io/97-things-every-programmer-should-know/content/en/thing_65/

* Learn foreign languages
https://97-things-every-x-should-know.gitbooks.io/97-things-every-programmer-should-know/content/en/thing_49/



* Encapsulate behaviors
https://97-things-every-x-should-know.gitbooks.io/97-things-every-programmer-should-know/content/en/thing_32/





69. Data structures & algorithms. (2 parter?) https://dev.to/iuliagroza/complete-introduction-to-the-30-most-essential-data-structures-algorithms-43kd?utm_source=digest_mailer&utm_medium=email&utm_campaign=digest_email

Every day - add 2 new things from that post.

70. Using AND
    https://blog.greenroots.info/my-favorite-javascript-tips-and-tricks-ckd60i4cq011em8s16uobcelc?utm_campaign=Frontend%2BWeekly&utm_medium=email&utm_source=Frontend_Weekly_214

More random game dev shit
https://www.gamasutra.com/view/feature/194772/dirty_game_development_tricks.php



71. Creating a mandatory param
    Required Function Params
    NUMBER 8
    https://blog.greenroots.info/my-favorite-javascript-tips-and-tricks-ckd60i4cq011em8s16uobcelc?utm_campaign=Frontend%2BWeekly&utm_medium=email&utm_source=Frontend_Weekly_214

72. The run command in Devtools
    https://miro.medium.com/max/700/1*hk4R78EHvQqu2HmpUSLKyQ.gif
    https://medium.com/javascript-in-plain-english/use-chrome-devtools-like-a-senior-frontend-developer-99a4740674

73. Capture a Film strip of how the site is processing your page
    https://miro.medium.com/max/700/1*PIuIAKbuu-QaoAvVk02PiQ.gif
    https://medium.com/javascript-in-plain-english/use-chrome-devtools-like-a-senior-frontend-developer-99a4740674

74. animating numbers
    https://css-tricks.com/animating-number-counters/

75. this sweet animation with pure code
    https://frankforce.com/dissecting-a-dweet-9-city-sunset/

76. https://blog.hubspot.com/service/joel-test

77. https://dev.to/sylwiavargas/5-ways-to-refactor-if-else-statements-in-js-functions-208e

I. Data Structures



https://discoverthreejs.com/book/

## Arrays

Stores data
`[1, 2, 3, 4, 5, 6]`
an array is a continuous block of memory;

## Linked Lists

## Stacks

## Queues

## Maps & Hash Tables

## Graphs

## Trees

## Binary Trees & Binary Search Trees

## Self-balancing Trees (AVL Trees, Red-Black Trees, Splay Trees)

## Heaps

## Tries

https://www.ritambhara.in/wp-content/uploads/2017/05/Screen-Shot-2017-05-01-at-4.01.38-PM.png

It's organized like a tree.
Good usecase is for searching. Like how with each key press, it goes down the list and finds more and more results.

## Segment Trees

## Fenwick Trees

## Disjoint Set Union

## Minimum Spanning Trees

---

II. Algorithms

## Divide and Conquer

A category of algorithms.
3 stages - Divide, Conquer, Merge.
For example - It's organizing an array from A to Z. It might break the array in a few pieces, organize those pieces. Then merge them back in. Then repeat until it's finished.

## Sorting Algorithms (Bubble Sort, Counting Sort, Quick Sort, Merge Sort, Radix Sort)

## Searching Algorithms (Linear Search, Binary Search)

## Sieve of Eratosthenes

## Knuth-Morris-Pratt Algorithm

## Greedy I (Maximum number of non-overlapping intervals on an axis)

## Greedy II (Fractional Knapsack Problem)

## Dynamic Programming I (0–1 Knapsack Problem)

https://en.wikipedia.org/wiki/Knapsack_problem#:~:text=The%20knapsack%20problem%20is%20a,is%20as%20large%20as%20possible.
The knapsack problem is a problem in combinatorial optimization: Given a set of items, each with a weight and a value, determine the number of each item to include in a collection so that the total weight is less than or equal to a given limit and the total value is as large as possible. It derives its name from the problem faced by someone who is constrained by a fixed-size knapsack and must fill it with the most valuable items.

https://www.youtube.com/watch?v=xCbYmUPvc2Q
Dynamic programming is about subproblems, not remembering patterns to fill cells in with. I watched EVERY ONE of Tuschar Roy's videos and found myself MEMORIZING how to fill out the cells INSTEAD of really knowing what was going on.

## Dynamic Programming II (Longest Common Subsequence)

## Dynamic Programming III (Longest Increasing Subsequence)

## Convex Hull

## Graph Traversals (Breadth-First Search, Depth-First Search)

## Floyd-Warshall / Roy-Floyd Algorithm

## Dijkstra's Algorithm & Bellman-Ford Algorithm

## Topological Sorting

---

26 awesome cheatsheet
https://dev.to/madza/26-awesome-cheatsheets-from-twitter-2jmh?utm_source=digest_mailer&utm_medium=email&utm_campaign=digest_email

MAC:
https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/EgBNT_xUwAArRlM.jpg

LINUX:
https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/EguzvtdVkAABamM.jpg

MARKDOWN:
https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/EezubrwUYAAo8UP.png

TERMINAL:
https://res.cloudinary.com/practicaldev/image/fetch/s--f4XK1_pY--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://pbs.twimg.com/media/Efr4cGTU4AUQdxM.jpg

Separate:
CSS:
https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/EfRco_iU0AEbb0C.png

REGEX:
https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/EfBhh73U4AE-nBN.jpg

Separate - JS:
https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/Ee_QA4BUMAADI34.jpg

https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/EeAWAIbUEAEF2qM.png

https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/Eek_TRNU0AA3R-U.png

https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/EfXP3wEU0AAqQPY.jpg

https://res.cloudinary.com/practicaldev/image/fetch/s--xJvitEh---/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://pbs.twimg.com/media/EeqQBQRUEAIbNLk.png

70 - IDE Sandbox code editing online
REGEX:
https://res.cloudinary.com/practicaldev/image/fetch/https://pbs.twimg.com/media/EfBhh73U4AE-nBN.jpg

71. 3D text with ztext library

https://bennettfeely.com/ztext/

<p class="codepen" data-height="281" data-theme-id="light" data-default-tab="result" data-user="rockykev" data-slug-hash="WNwVdep" data-preview="true" style="height: 281px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" data-pen-title="ztext.js test">
  <span>See the Pen <a href="https://codepen.io/rockykev/pen/WNwVdep">
  ztext.js test</a> by rockykev (<a href="https://codepen.io/rockykev">@rockykev</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

72. Vue Multiselect and lack of accessibility (Post on OCT 1st)

What is the problem with <select>
--> https://css-tricks.com/striking-a-balance-between-native-and-custom-select-elements/

Vue Multi-select issues: https://vue-multiselect.js.org/#sub-getting-started
I'm really disappointed at vue-multiselect:

It rips apart all the accessibility benefits of select, throws everything into a list, and doesn't provide overrides for aria settings.

Ignoring that - you can't even tie a <label> to it, since it's a bunch of divs.

The final thing is that the dev calls an element within the selector a label, which just confuses the hell out of everything.

Good news: It is a goal in V3.0 https://github.com/shentao/vue-multiselect/issues/1246#issuecomment-625104127

ACTION: Wait for V3.0

SUBSTORY --- Alternative use
https://moderncss.dev/custom-select-styles-with-pure-css/

SUBSTORY --- INSTEAD OF select, maybe this?
https://medium.com/@kollinz/dropdown-alternatives-for-better-mobile-forms-53e40d641b53

SUBSTORY --- Hybrid approach: https://css-tricks.com/striking-a-balance-between-native-and-custom-select-elements/

Watch this video: https://www.youtube.com/watch?time_continue=2&v=hcYAHix-riY&feature=emb_title

73. https://moderncss.dev/
    Just a lot of generic stuff.

---
## https://www.smashingmagazine.com/2019/02/buttons-interfaces/

78. CSS Reset vs normalize
    https://elijahmanor.com/blog/css-resets

```
To all the people wanting to copy this:

This is terrible for accessibility—it will mess up the focus order.

Don’t reverse order with CSS No entry sign.
```

80. Name your commits convention
    type>(<scope>): <short summary>
    https://www.telerik.com/amp/10-good-practices-building-maintaining-large-vuejs-projects/WEx1ZE1sRUVUWkE5S0dNbEhBNXJyUkU3T1Q4PQ2
    https://github.com/angular/angular/blob/master/CONTRIBUTING.md#commit

81. Caching function calculation results
    We want to cache the result of the function operation. When it is called later, if the parameters are the same, the function will no longer be executed, but the result in the cache will be returned directly. What can we do?
    https://medium.com/javascript-in-plain-english/you-must-understand-these-14-javasript-functions-1f4fa1c620e2

82. Various apis to play with
    https://medium.com/swlh/my-top-5-apis-for-new-developers-5191031da102

83. Giant project --> Maybe use the greensock piece to learn from?
    https://css-tricks.com/the-making-of-netlifys-million-devs-svg-animation-site/

84. Optional chaining might be bad
    A comparison of using optional chaining is like using ! important on css. Its brittle.
    https://blog.jim-nielsen.com/2020/web-technologies-and-syntax/

85. Functional Programmings VS OOP
    https://res.cloudinary.com/practicaldev/image/fetch/s--Cejb3jaT--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/i5alyim73c795y5fgza8.jpg

86. Screenshot code
    https://dev.to/niharrs/6-awesome-ways-to-present-your-code-3jj2

87. Easily Edit Any Website Text
    https://dev.to/myogeshchavan97/most-useful-features-provided-by-chrome-developer-tools-that-you-must-know-bak?utm_source=digest_mailer&utm_medium=email&utm_campaign=digest_email

88. Take Screenshot Of The Entire Page
    Find The Unused CSS From Your Website
    & Local Overrides
    https://dev.to/myogeshchavan97/most-useful-features-provided-by-chrome-developer-tools-that-you-must-know-bak?utm_source=digest_mailer&utm_medium=email&utm_campaign=digest_email

89. https://www.reddit.com/r/LifeProTips/comments/j39u54/lpt_when_giving_advice_use_the_phrase_perhaps_in/



91. Key words to use.
    When giving advice, use the phrase “perhaps” in replacement of “I think” so it comes off more as a suggestion and not an opinion.

prefer "have you considered....?"

"Something to think about..."

"You might consider...."

"Some people find...."

I had a great lecturer who said "there might be an argument for . . . " I always appreciated the gentility rather than telling me what I'd done wasn't the most effective way

https://www.reddit.com/r/LifeProTips/comments/j39u54/lpt_when_giving_advice_use_the_phrase_perhaps_in/


92. Puppeteer RECIPES:

Getting Text
const text = await page.\$eval('h1', el => el.innerText);
assert.equal(text, 'Example domain');

Typing Text
await page.goto('URLHERE');
await page.focus('trix-editor');
await page.keyboard.type('Typing some text');
await page.screenshot({ path: 'keyboard.png' });

Forms
await page.waitForSelector('#email', { visible: true });
await page.type('#email', 'EMAILHERE@EMAIL.com');
await page.click('#btnNext');
await page.waitForSelector('#password');
await page.type('#password', 'abcdef', { delay: 20 });
await page.click('#btnLogin');

Creating PDFs
await page.goto('https://example.com')
await page.pdf({ path: 'example.com.pdf', format: 'A4'});

Wiki Search game
async function handleLink() {
const h1 = await page.\$eval('h1', el => el.innerText);

if (h1 === 'Philosophy') {
return;
}

await page.click('a');
return handleLink();
}

await page.goto('https://en.wikipedia.org/wiki/Wikipedia:Random');
await handleLink();

EVASION FROM BOTS
npm i puppeteer-extra-plugin-stealth

emulate accessibility

```
const browser = await puppeteer.launch();
const page = await browser.newPage();
await page.goto('https://web.dev/live/');

await page.emulateVisionDeficiency('blurredVision');
await page.screenshot( { path: 'blurred-vision.png', fullPage: true });

await page.emulateVisionDeficiency('achromatopsia');
await page.screenshot( { path: 'achromatopsia.png', fullPage: true });

await page.emulateVisionDeficiency('deuteranopia');
await page.screenshot( { path: 'deuteranopia.png', fullPage: true });
```

LIGHTHOUSE
lighthouse --only-audits=is-on-https --output=json https://umaar.com

USE MOUSE POINTER
await installMouseHelper(page);

await page.mouse.move(100, 200);

await page.mouse.down();

await page.mouse.up();

INTERCEPT CONSOLE.Logging
page.on('console', msg => {
console.log('LOG: ', msg.text());
});

93. aspect ratio
    /_ Minimum aspect ratio _/
    @media (min-aspect-ratio: 8/5) {
    div {
    background: #9af; /_ blue _/
    }
    }

/_ Maximum aspect ratio _/
@media (max-aspect-ratio: 3/2) {
div {
background: #9ff; /_ cyan _/
}
}

https://developer.mozilla.org/en-US/docs/Web/CSS/@media/aspect-ratio

94. Vue Cheatsheets

https://www.vuemastery.com/pdf/Vue-3-Cheat-Sheet.pdf
https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf

95. https://github.com/Twipped/InterviewThis
    Questions to ask Companies.

References
https://www.paulirish.com/2012/why-moving-elements-with-translate-is-better-than-posabs-topleft/

via https://github.com/yangshun/front-end-interview-handbook/blob/master/contents/en/css-questions.md#is-there-any-reason-youd-want-to-use-translate-instead-of-absolute-positioning-or-vice-versa-and-why

97. https://medium.com/javascript-scene/10-interview-questions-every-javascript-developer-should-know-6fa6bdf5ad95

98.
THIS:
https://medium.com/javascript-in-plain-english/15-helpful-javascript-one-liners-946e1d1a1653
--> DID number 3
--> Did 4
--> Did 13

99. Rot.js
http://roguebasin.roguelikedevelopment.org/index.php?title=Cellular_Automata_Method_for_Generating_Random_Cave-Like_Levels

100. Markov Chain and development
https://www.reddit.com/r/explainlikeimfive/comments/1tnjck/eli5_markov_chains/


# Advanced WordPress Security by Grant Stokley --

## How to get the WP author:

PG 24

### METHOD 1:

WordPress to tell them what it is. If you type /?author=1 after the domain
name in the URL address bar in your browser, like this...
https://yourwpwebsite.com/?author=1

You will be redirected to a URL containing the user_login name of the 1
st

WordPress user. The 1st user in every WordPress installation is the administrator user. Let's say for example that you aren't using the default
admin name, instead you are using spaceghost, so as a result spaceghost
will appears in the URL after the redirect.
https://yourwpwebsite.com/author/spaceghost/

How to defend:
Redirect if Author is in the query string.

(Book shows you how)

### Method 2

JSON is an open standard data format that is human-readable. When you
request content from or send content to the API, the response will also be

returned in JSON. If you type https://yourwpwebsite.com/wp-
json/wp/v2/users into your browser, JSON will be returned to you revealing

the user_login name again.

How to defend:
Redirect if they visit that page.
Maybe a method to turn it off?

### Method 3

XML-RPC method -- You pack hundreds of usernames/password combos in a single payload and make a request.

The attacker is looking for a success response.

How to defend?
Requires modifying a wp-includes.

## Disable the Plugin Editor

pg 38

Disable the Plugin Editor
This is an easy one-liner configuration change. However, you must edit your
wp-config.php file.

1. Locate your wp-config.php in the root directory of your
   WordPress Installation.
2. Use your file editor of choice and insert
   define('DISALLOW_FILE_EDIT', true); in between the opening comments
   after <?php and the before the MySQL comments.
3. Save and exit the file editor.
4. Restart Apache, sudo apache2ctl restart
5. Verify in the Dashboard that Plugin Editor option is now disabled.
   The wp-config.php should look something like Figure 6-2:

## WP-config security

https://i.imgur.com/OcrEzdf.png

99. Find object key by value.
```
function getKeyByValue(object, value) {
  return Object.keys(object).find(key => object[key] === value);
}


const map = {"first" : "1", "second" : "2"};
console.log(getKeyByValue(map,"2"));
// https://stackoverflow.com/a/28191966/4096078
```

100. False negatives in checking objects
https://dmitripavlutin.com/check-if-object-has-property-javascript/#:%7E:text=The%20first%20way%20is%20to,own%20properties%20of%20the%20object.




# This section is from WordPress the Missing Manual

## Temporary wp-config.php siteurl

https://i.imgur.com/gN8XCix.png

## Why the index.php silence is golden file exists.

https://i.imgur.com/O0V3YNx.png

## WordPress dope ass features

https://i.imgur.com/mdDakFt.png

100. SVG ANIMATIONS

SVG --
M, m - move to (M is absolute)
L, l - live

accessibility
https://i.imgur.com/X1ZiqOq.png

SVGs uses fill and stroke

transform-origin tip - use percentages.

## The hardware acceleration hack

https://i.imgur.com/lSkLJ4k.png

## codepen with greensock

https://codepen.io/sdras/pen/Wramvo?editors=1010

## Add Animation delays

https://i.imgur.com/0il1iyu.png

## codepen with timeline

https://codepen.io/sdras/pen/ByEWON

## If animation pops and you want to fix that

Some times the visibility will look goofy on load, right BEFORE animation.

Use this hack to fix it.
https://i.imgur.com/SS8MZmg.png





---



This has been moved to /content-ideas-accessibility.md

## UX

---

1. BOOKS TO Read
   https://elementor.com/blog/best-web-design-books/

Common UX Book problems

```
* They’re outdated
* They’re difficult to read
* The topics are spot-on, but the way they’re written is off-putting (e.g. egotistic, sexist, condescending).
* They shallowly cover the subject matter
* The book is nothing more than a compilation of free blog posts or videos all merged into a pricey book format.
```

1. The Principles of Beautiful Web Design: Designing Great Web Sites is Not Rocket Science!
2. Building Progressive Web Apps: Bringing the Power of Native to the Browser
3. 6 Practical WordPress Projects

```
It will teach you how to build the following from scratch :

A WordPress theme
A plugin (using Vue)
Endpoints for the REST API (like if you want to design a child theme)
Admin page
A non-blog website
```

7. UX Design 2020: The Ultimate Beginner's Guide to User Experience
8. Color Studies

## Deployment and Serverless

---

https://www.freecodecamp.org/news/top-5-things-to-learn-first-when-getting-started-with-aws/

## Optimzation

---

OPTIMIZATION
https://pustelto.com/blog/optimizing-css-for-faster-page-loads/

FE Javascript performance
https://www.debugbear.com/blog/front-end-javascript-performance

Building a Lighthouse measurement score in the footer.

Elminating render blocking css
https://dev.to/prototyp/improving-website-performance-by-eliminating-render-blocking-css-and-javascript-28ei?utm_source=digest_mailer&utm_medium=email&utm_campaign=digest_email

## Advanced JS

---

https://www.rithmschool.com/courses/advanced-javascript

https://dev.to/ryanameri/mastering-hard-parts-of-javascript-callbacks-i-3aj0

https://github.com/nas5w/javascript-tips-and-tidbits

“JavaScript for impatient programmers” (free to read online)
https://exploringjs.com/impatient-js/

“Deep JavaScript” (free to read online)
https://exploringjs.com/deep-js/

82. Be able to implement these built-in methods
    Array.prototype.map; Array.prototype.filter ; Array.prototype.some ; Array.prototype.reduce ; Array.prototype.flat; Implement JSON.stringfy
    https://medium.com/javascript-in-plain-english/you-must-understand-these-14-javasript-functions-1f4fa1c620e2

Debouncing and Throttling
https://medium.com/javascript-in-plain-english/you-must-understand-these-14-javasript-functions-1f4fa1c620e2

## LIST ITEMS

A bunch of things
https://thisthat.dev/

---

## Phaser and gamedev stuff

James
https://browsergameshub.com/
https://www.youtube.com/watch?v=J3MgUfyE63M&list=PLlultXOnQ04Qj5vm4Cf8l2zlFg7_4A7i8&index=14

Notes of Phaser 3
https://rexrainbow.github.io/phaser3-rex-notes/docs/site/shader-grayscale/

Some guy's blog
https://www.stephengarside.co.uk/blog/?tag=phaser%203

Enable cursor keys
// enable cursor keys
this.cursors = this.input.keyboard.createCursorKeys();

    this.input.on('pointerdown', function (pointer) {
        console.log(pointer.x, pointer.y);
    });

---

## BASIC STUFF

1. Solid Principles
   https://thefullstack.xyz/solid-javascript

2. [ ] You can describe full Redux flow [:books:](https://www.youtube.com/watch?v=1w-oQ-i1XB8)

3. https://dev.to/junlow/my-favourite-javascript-learning-resources-on-github-4okb
   Break down a lot of this stuff.

---

## Contains lots of things.

1. https://github.com/nas5w/javascript-tips-and-tidbits

2. https://www.reddit.com/r/ExperiencedDevs/comments/gtxs1g/40_year_old_software_engineers_what_do_you/

3. https://www.reddit.com/r/SideProject/comments/gvsjw6/interviewsschool_complete_guide_to_prepare_for/

4. https://github.com/troxler/awesome-css-frameworks - A list of crazy amounts of cess

5. https://dev.to/jorge_rockr/everything-you-need-to-know-about-node-js-lnc

6. Tools dump
   https://dev.to/iainfreestone/50-developer-tools-to-make-your-life-a-little-easier-4oc5

7. Lots of things to break down.
   https://github.com/ryanmcdermott/clean-code-javascript

8. https://medium.com/@biratkirat/97-journey-every-programmer-should-accomplish-a0c53dbbfd47
   file:///C:/Users/tower8/Downloads/97-things-every-programmer-should-know-en.pdf
   https://legacy.gitbook.com/book/97-things-every-x-should-know/97-things-every-programmer-should-know/details

http://www.codeblocq.com/2016/01/Untrack-files-already-added-to-git-repository-based-on-gitignore/
you added stuff to cache but want to remove.

https://github.com/97-things/97-things-every-programmer-should-know/blob/master/en/SUMMARY.md
Act with Prudence ~ Edward Garson
Apply Functional Programming Principles ~ Edward Garson
Ask, “What Would the User Do?” (You Are Not the User) ~ Giles Colborne
Automate Your Coding Standard~ Filip van Laenen
Beauty Is in Simplicity ~ Jørn Ølmheim
Before You Refactor ~ Rajith Attapattu
Beware the Share ~Udi Dahan
The Boy Scout Rule ~Robert C. Martin (Uncle Bob)
Check Your Code First Before Looking to Blame Others~Allan Kelly
Choose Your Tools with Care ~ Giovanni Asproni
Code in the Language of the Domain ~Dan North
Code Is Design ~ Ryan Brush
Code Layout Matters ~ Steve Freeman
Code Reviews ~Mattias Karlsson
Coding with Reason ~ Yechiel Kimchi
A Comment on Comments ~ Cal Evans
Comment Only What the Code Cannot Say ~ Kevlin Henney
Continuous Learning ~ Clint Shank
Convenience Is Not an -ility ~ Gregor Hohpe
Deploy Early and Often ~ Steve Berczuk
Distinguish Business Exceptions from Technical ~Dan Bergh Johnsson
Do Lots of Deliberate Practice ~Jon Jagger
Domain-Specific Languages ~ Michael Hunger
Don’t Be Afraid to Break Things ~ Mike Lewis
Don’t Be Cute with Your Test Data ~ Rod Begbie
Don’t Ignore That Error! ~ Pete Goodliffe
Don’t Just Learn the Language, Understand Its Culture ~Anders Norås
Don’t Nail Your Program into the Upright Position ~Verity Stob
Don’t Rely on “Magic Happens Here” ~ Alan Griffiths
Don’t Repeat Yourself ~ Steve Smith
Don’t Touch That Code! ~ Cal Evans
Encapsulate Behavior, Not Just State ~ Einar Landre
Floating-Point Numbers Aren’t Real ~Chuck Allison
Fulfill Your Ambitions with Open Source ~ Richard Monson-Haefel
The Golden Rule of API Design ~Michael Feathers
The Guru Myth ~Ryan Brush
Hard Work Does Not Pay Off ~Olve Maudal
How to Use a Bug Tracker ~ Matt Doar
Improve Code by Removing It ~Pete Goodliffe
Install Me ~ Marcus Baker
Interprocess Communication Affects Application Response Time ~Randy Stafford
Keep the Build Clean ~Johannes Brodwall
Know How to Use Command-Line Tools ~Carroll Robinson
Know Well More Than Two Programming Languages ~Russel Winder
Know Your IDE ~Heinz Kabutz
Know Your Limits ~ Greg Colvin
Know Your Next Commit ~Dan Bergh Johnsson
Large, Interconnected Data Belongs to a Database ~Diomidis Spinellis
Learn Foreign Languages ~Klaus Marquardt
Learn to Estimate ~ Giovanni Asproni
Learn to Say, “Hello, World” ~ Thomas Guest
Let Your Project Speak for Itself ~ Daniel Lindner
The Linker Is Not a Magical Program ~Walter Bright
The Longevity of Interim Solutions ~Klaus Marquardt
Make Interfaces Easy to Use Correctly and Hard to Use Incorrectly ~ Scott Meyers
Make the Invisible More Visible ~Jon Jagger
Message Passing Leads to Better Scalability in Parallel Systems ~Russel Winder
A Message to the Future ~Linda Rising
Missing Opportunities for Polymorphism ~ Kirk Pepperdine
News of the Weird: Testers Are Your Friends ~Burk Hufnagel
One Binary ~ Steve Freeman
Only the Code Tells the Truth ~ Peter Sommerlad
Own (and Refactor) the Build ~Steve Berczuk
Pair Program and Feel the Flow ~Gudny Hauknes, Kari Røssland, and Ann Katrin Gagnat
Prefer Domain-Specific Types to Primitive Types ~Einar Landre
Prevent Errors ~Giles Colborne
The Professional Programmer ~ Robert C. Martin (Uncle Bob)
Put Everything Under Version Control ~Diomidis Spinellis
Put the Mouse Down and Step Away from the Keyboard ~Burk Hufnagel
Read Code ~Karianne Berg
Read the Humanities ~Keith Braithwaite
Reinvent the Wheel Often ~Jason P. Sage
Resist the Temptation of the Singleton ~Sam Saariste
The Road to Performance Is Littered with Dirty Code Bombs ~Kirk Pepperdine
Simplicity Comes from Reduction ~Paul W. Homer
The Single Responsibility Principle ~Robert C. Martin (Uncle Bob)
Start from Yes ~Alex Miller
Step Back and Automate, Automate, Automate ~Cay Horstmann
Take Advantage of Code Analysis Tools ~Sarah Mount
Test for Required Behavior, Not Incidental Behavior ~Kevlin Henney
Test Precisely and Concretely ~Kevlin Henney
Test While You Sleep (and over Weekends) ~Rajith Attapattu
Testing Is the Engineering Rigor of Software Development ~ Neal Ford
Thinking in States ~Niclas Nilsson
Two Heads Are Often Better Than One ~Adrian Wible
Two Wrongs Can Make a Right (and Are Difficult to Fix) ~Allan Kelly
Ubuntu Coding for Your Friends ~Aslam Khan
The Unix Tools Are Your Friends ~Diomidis Spinellis
Use the Right Algorithm and Data Structure ~Jan Christiaan “JC” van Winkel
Verbose Logging Will Disturb Your Sleep ~Johannes Brodwall
WET Dilutes Performance Bottlenecks ~ Kirk Pepperdine
When Programmers and Testers Collaborate ~Janet Gregory
Write Code As If You Had to Support It for the Rest of Your Life ~Yuriy Zubarev
Write Small Functions Using Examples ~Keith Braithwaite
Write Tests for People ~ Gerard Meszaros
You Gotta Care About the Code ~ Pete Goodliffe
Your Customers Do Not Mean What They Say ~Nate Jackson

## DETAILED STUFF

13. For loop performance.
    https://www.blog.duomly.com/for-loop-while-loop-do-while-loop-and-other-javascript-loops-comparison-and-performance/

actual testing: https://jsben.ch/index

```
If you are dealing with hundreds of thousands or millions of items, the differences will become a bit more noticeable. But the reality is, rarely does a client-side application deal with such large results of data anyway, because you should be using pagination and other techniques.

This falls under the category “premature optimization.” Unless you can feel a lag, and profiling shows this to be a hot spot, code readability should always trump a couple milliseconds’ improvement.
```

```
You’ll find that ymwv based on: platform, engine, hardware/precision, and most importantly, when you are. New releases of all your runtimes, firmware, CPUs etc dramatically impact operational throughput.
As it’s a moving target, what’s more important to understand is your rationale, use cases and how the tools you have are applied.
I recommend looking into the work many hundreds of people have contributed over the past decade on this subject at jsperf. Millions of samples against consistent testing rubricks have been computed and accumulated for your comparative evaluation over the past decade (see revision history for ~650 test revisions of this subject alone, each of which has a unique sample history aggregate distributed by browser platform):
https://jsperf.com/for-vs-foreach/654
```

```
If you're iterating 10k items at a time and squabbling over microseconds (a thousandth of a millisecond) you're spending your time unwisely. Code is for humans to read first, computers second. I'm bracing myself for the inevitable “sometimes every mi...
```

```
Who cares about microseconds?
If you are working on a high performance super critical server application, you either wouldn't use JavaScript in the first place, or you would be an experienced developer who knows what he is doing and who doesn't just take the first result on his “how to sum an array” Google search…
```

Remote work
Something I picked up from half a decade of remote work:
As we're working remotely since we tend to be working on different things - clarity can get muddled. A reminder to be helpful and provide links to repos, jira tickets, screenshots, videos, etc. And try to answer the 5 Ws. (who what where etc). Don't be afraid to jump in a call if you think it's too hard to explain, or screen-record with audio, pointing to what you mean.

1 - better speaking
This is the list I got most value from: - Radical Candor - Thanks for the feedback - How fucked up is your management - Work Rules - Dictator's Handbook (mostly for anti-patterns to look out for/gain inspiration from _sarcasm_)

    Diminishing returns:
    - How to destroy a tech start-up
    - The Phoenix Project (or it's parent The Goal)

    Heard great feedback, but didn't get round reading myself:
    - The Culture Map

2 - Clean code

Effective java

Designing data intensive applications

https://www.reddit.com/r/ExperiencedDevs/comments/gm20nw/i_finished_reading_accelerate_over_the_weekend/

finished reading "Accelerate" over the weekend, and I have some thoughts
https://itrevolution.com/book/accelerate/ -- https://medium.com/@laksdoddi/book-summary-accelerate-by-nicole-forsgren-gene-kim-jez-humble-848e421c4f27

Continuous Delivery is another great one, also written by Jez Humble, who co-wrote Accelerate. It gets more detailed than Accelerate. Both great books.

Supposedly, they had ~2000 data points from possibly hundreds of different companies. Recommend watching talks by Jez Humble, he talks a lot about it. Like here : https://youtu.be/CN6uhzNM4eA?t=438

Enterprise patterns
k How to Read a Book

3 - Join the ACM
https://www.acm.org/membership/membership-benefits

4 - if you're a 10 year developer or a 10x1 year developer

TERMS I DONT KNOW
Experience with Cloud Formation and or Terraform
Experience with high-scale transactional data processing
Experience designing and querying web-scale databases
Experience with Azure and/or AWS
Things here: https://github.com/gaearon/whatthefuck.is/issues/

```
Are you learning new technologies? (From your original post, it sounds like yes.)

Are the new technologies teaching you something new? (For example, if you learned Ruby on Rails and then you learned Django, what new concepts did you pick up? In contrast, what about Go channels vs. Erlang processes vs. Akka actors vs. POSIX threads?)

Is your knowledge of the stack deepening? (For example, if you work in back-end development, are you learning more about operations? Are you learning more about the front-end?)

Are you honing your intuition for system design? Can you design something for an MVP without boxing yourself in? What about hard requirements around performance, availability, etc.?

Can you discuss trade-offs and their impact to the user and business?

Can you break down large initiatives into smaller increments that can be shipped to production with little risk?

Can you mentor?

Are you aware of non-functional requirements (the so-called -ilities) and can consider them when the business is thinking solely in terms of new features?

Do you consider the security implications of your solution? (This is really another set of non-functional requirements, but it deserves to be called out separately.)

Can you motivate a team?

Resolve interpersonal conflict?

Consider the broader business landscape/market/economy?
```

https://www.reddit.com/r/ExperiencedDevs/comments/gvfuvq/how_do_you_know_if_youre_on_a_path_towards_having/fsoxv3d/

5 - Do not coast

```
I've seen it over and over. Devs, in their early or mid 50's, settle into a roll and try and coast out their last 10ish years.

And then layoffs come. Management has to decide if their high pay is worth their domain expertise vs 2 entry level devs who don't have kids and lawns and broken water heaters and 5 weeks of vacation. Eventually the answer is "no".

The older devs find themselves too old to learn a new industry (or even a new company in the same industry), but too young to retire comfortably.

I keep bringing this up, but it's because I see it happen about once a year. It's a mini tragedy. They go from being a "top dog" for years, making a comfortable living, to having to make some hard financial decisions that affect their retirement plans, and their quality of life.

Plan for retirement years before you have to, and "stay hungry" at work.
https://www.reddit.com/r/ExperiencedDevs/comments/gtxs1g/40_year_old_software_engineers_what_do_you/fsfjtus/
```

6 - leetcode grinding
https://www.reddit.com/r/ExperiencedDevs/comments/fa7wcc/lets_have_a_real_discussion_on_leetcode_for/fiwyyd9/

```
On preparing, you've got three sorts of interviews in front of you.

Code. Leetcode is half of it, but data structures is honestly the other half. Before anyone ever grinds on something like Leetcode, please just refresh basic data structures. Arrays, lists, trees, hashtables, graphs. How to add, traverse, and remove stuff. How fast each of those operations is, as big-O. And how much extra memory each approach takes up vs the others. Then know one language well enough to code whatever in it, but *only* one language; more won't help much if at all.

Design. How to build large systems, but also how to ask refining questions, lay out a direction, provide some options, call out pros/cons of each option, and make a call. Bonus points if you can call out where it might fall down in the future, or where you'd give more thought if X was true.

Behavioral. "What was the last disagreement you had with a coworker, and how'd it go?" If you're working in industry, these shouldn't be terrible, but I'd think back on a project that went well (and why) and a project that went poorly (and what you'd change with the experience you got there).
```

Blind curated 75 is a good mix, https://leetcode.com/list/xoqag3yj/

7 - Feedback

```
First in a one on one, or other private conversation, tell them that you are looking to improve yourself and would like feedback on areas to focus. Don’t ask for or expect the feedback right away. Even better would be to tell them up front that you are happy to give them time to think about it. Follow up that conversation with an email or DM to say that you are looking forward to receiving their feedback.

Next, getting negative feedback is difficult. You have to receive it in the right way or you will make more difficult for them to give it next time. Whether you agree with the feedback or not, just say “Thank you” and “I will work on that”. DO NOT try to explain, or justify why you did / do what they are giving feedback on.

Next, ask for their help in improving in that / those areas. “Do you have any advice on steps I can take to improve at X?”.

Lastly, follow up. “I’ve been trying Y to work on the X feedback you gave me. Have you noticed any improvement?” “Is there something else I could try?”

Here are some more specific questions that I find help me coach people.

“What is a goal you have for me?” “What more can I do to achieve that goal?”

“I would like to take on a personal challenge, do you have any suggestions?”

“What could I have done better on my last project?”

“What is something I can do / work on to prepare me for my next promotion?”
https://www.reddit.com/r/ExperiencedDevs/comments/e3odia/how_to_deal_with_managers_who_give_you_zero/f94fzum/
```

8 - List of books
https://www.reddit.com/r/ExperiencedDevs/comments/f8m07f/does_anyone_here_read_books/fim766h/

9 - https://www.reddit.com/r/ExperiencedDevs/comments/gtxs1g/40_year_old_software_engineers_what_do_you/fsf01c1/
Software is more about the team building it and people using it than it is about the code.

Databases:
I would guess an intermediate amount? I think it's probably more important to know how databases generally work.

indexes and how/why they work
optimize queries
sharding/partitioning
ACID properties and what they mean

## Projects

https://www.reddit.com/r/incremental_games/comments/ahf6nx/how_to_make_an_incremental_game/
https://mikemcl.github.io/decimal.js/

Minesweeper in vue
https://www.skillshare.com/classes/Minesweeper-A-playful-intro-to-Vue/2048913723?lessonsTab=on&discount=G6LWG&teacherRef=3932130&via=free-enrollment&utm_campaign=teacher-discount-2048913723&utm_source=ShortUrl&utm_medium=teacher-discount
https://www.reddit.com/r/vuejs/comments/innkr9/minesweeper_game_built_with_vue_vuex_vuetify_and/

Incremental game
https://www.reddit.com/r/incremental_games/comments/j3xv7i/does_there_exist_a_guide_or_tutorial_on_game_to/

Snake
https://dev.to/nitdgplug/learn-javascript-through-a-game-1beh

PHASER:
https://www.codecademy.com/learn/paths/create-video-games-with-phaser
https://www.udemy.com/course/making-html5-games-with-phaser-3/

Flappy bird Doodle Jump
https://www.freecodecamp.org/news/javascript-tutorial-flappy-bird-doodle-jump/

Roguelike - Rotjs
https://blog.logrocket.com/building-a-roguelike-game-with-rot-js/

## Random Libraries

https://github.com/munrocket/parallax-effect

## Random stuff

    Also - you might get inspired by these:

Sweet Text effects:
https://animate.style/
Dope-ass transitions/animations:
https://animejs.com/ (pronounced 'ah-na-mae').
https://greensock.com/showcase/ (and farting turtle: https://codepen.io/sdras/pen/BaaQqKL) (edited)
Pinned by tyler h
2:18
this just blew my brain: https://turbulent.ca/

Rocky Kev 2:21 PM
You can always suggest it in little ways too.
I was using this progressbar.js library for data charts.
And then I found this example:
http://kimmobrunfeldt.github.io/progressbar.js/examples/password-strength/
He puts the progress bar animation in a text field! :open_mouth:
