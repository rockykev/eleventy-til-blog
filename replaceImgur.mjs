import fs from "fs";
import path from "path";
import fetch from "node-fetch";

const imgurRegex = /https?:\/\/(?:i\.)?imgur\.com\/\w+\.(?:jpg|jpeg|gif|png)/g;

const thePath = "posts/"; // Replace with the path to your directory

const postFolders = [
  // '2019-01',
  // '2019-08',
  // '2019-09',
  // '2019-10',
  // '2019-11',
  // '2019-12',
  // '2020-01',
  // '2020-02',
  // '2020-03',
  // '2020-04',
  // '2020-05',
  // '2020-06',
  // '2020-07',
  // '2020-08',
  // '2020-09',
  // '2020-10',
  // '2020-11',
  // '2020-12',
  // '2021-01',
  // '2021-02',
  // '2021-03',
  // '2021-04',
  // '2021-05',
  // '2021-06',
  // '2021-07',
  // '2021-08',
  // '2021-09',
  // '2021-10',
  // '2021-11',
  // '2021-12',
  // '2022-01',
  // '2022-02',
  // '2022-03',
  // '2022-04',
  // '2022-05',
  // '2022-06',
  // '2022-07',
  // '2022-08',
  // '2022-09',
  '2022-10',
  '2022-11',
  '2022-12',
  '2023-01',
  '2023-02',
  '2023-03',
];


function replaceImgurWithLocal(directoryPath) {
  fs.readdir(directoryPath, (err, files) => {
    if (err) {
      console.error("Error reading directory:", err);
      return;
    }

    files.forEach((filename) => {
      if (path.extname(filename) === ".md") {
        const filePath = path.join(directoryPath, filename);
        const imageName = path.parse(filename).name;
        let imageCount = 0;

        fs.readFile(filePath, "utf8", (err, data) => {
          if (err) {
            console.error("Error reading file:", err);
            return;
          }

          let newData = data;
          let match;
          while ((match = imgurRegex.exec(data))) {
            const imgurUrl = match[0];
            const fileExtension = path.extname(imgurUrl);
            const fileName = `${imageName}_${imageCount}${fileExtension}`;

            fetch(imgurUrl)
              .then((response) => {
                if (response.ok) {
                  const dest = path.join(directoryPath, fileName);
                  const fileStream = fs.createWriteStream(dest);
                  response.body.pipe(fileStream);
                  console.log(`Downloaded image from ${imgurUrl}`);

                  // Replace the image URL in the Markdown file with the new local path
                  newData = newData.replace(imgurUrl, fileName);
                  fs.writeFile(filePath, newData, "utf8", (err) => {
                    if (err) {
                      console.error("Error writing file:", err);
                    } else {
                      console.log(`Updated ${filePath}`);
                    }
                  });
                } else {
                  console.warn(`Image at ${imgurUrl} could not be fetched`);
                }
              })
              .catch((err) => {
                console.error(`Error fetching image at ${imgurUrl}:`, err);
              });

            imageCount++;
          }
        });
      }
    });
  });

  console.log("all finished.");
}




postFolders.forEach(folder => {

  replaceImgurWithLocal(`posts/${folder}`);

})
